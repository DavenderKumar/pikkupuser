//
//  StoryVC.swift
//  RoyoRide
//
//  Created by Prashant on 07/07/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit
import AVFoundation

class StoryVC: UIViewController {
    
    @IBOutlet weak var imgViewBG: UIImageView!
    @IBOutlet weak var clcView: UICollectionView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var pageIndicator: CHIPageControlJaloro!
    
    var storyArray = [StoryModel]()
    var collectionViewDataSource : CollectionViewDataSourceCab?
    var player:AVPlayer?
    var durationInSeconds = 0
    var currentTimeInSeconds = 0
    var previousIndex = -1
    
    var timeObserver:Any?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  https://homepages.cae.wisc.edu/~ece533/images/watch.png
        // Do any additional setup after loading the view.
        
       
       
        getStory()
//        configureCollectionView()
    }
    
    
    func setupPageController(){
        
        pageIndicator.numberOfPages = storyArray.count
        pageIndicator.radius = 3.0
        pageIndicator.set(progress: 0, animated: true)
        pageIndicator.currentPageTintColor = UIColor.red
    }
    
    func configureStoryData(){
        
         setupPageController()
         configureCollectionView()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if let player = player{
            
            player.pause()
            if let timeObserver = timeObserver{
                
                player.removeTimeObserver(timeObserver)
            }
            self.player = nil
        }
        
    }
    
    
    func setupPlayer(url:String?){
        
        if let player = player{
            
            if let timeObserver = timeObserver{
                
                player.removeTimeObserver(timeObserver)
            }
            self.player = nil
        }
        
        if let url = URL(string:url ?? "") {
            let asset = AVAsset(url: url)
            let playerItem = AVPlayerItem(asset: asset)
            player = AVPlayer(playerItem: playerItem)
            timeObserver =  player?.addPeriodicTimeObserver(forInterval: CMTime(seconds: 1, preferredTimescale: 2), queue: DispatchQueue.main) {[weak self] (progressTime) in
                if let duration = self?.player?.currentItem?.duration {
                    
                    let durationSeconds = CMTimeGetSeconds(duration)
                    let seconds = CMTimeGetSeconds(progressTime)
                    let progress = Float(seconds/durationSeconds)
                  //  self?.durationInSeconds = Int(durationSeconds)
                   // self?.currentTimeInSeconds = Int(seconds)
                    
                    DispatchQueue.main.async {
                        //  self?.progressBar.progress = progress
                        if progress >= 1.0 {
                            //  self?.progressBar.progress = 0.0
                        }
                    }
                }
            }
        }
    }
    
    func playVideo(){
        
        player?.play()
    }
    
    func pauseVideo(){
        
        player?.pause()
    }
    
    func muteVideo(){
        
        player?.isMuted = !(/player?.isMuted)
    }
    
    @IBAction func btnCrossAction(_ sender: Any) {
        
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.popVC()
    }
    
    
    
    private func configureCollectionView() {
        
        let configureCellBlock : ListCellConfigureBlockCab = {
            [weak self] (cell, item, indexPath) in
            
            if let cell = cell as? StoryClcCell, let model = item as? StoryModel {
                let isVideo = /model.video_url != "" ? true : false
                cell.imgView.isHidden = isVideo
                cell.viewPlayerContainer.isHidden = !(isVideo)
                
                if self?.previousIndex == -1 && isVideo{
                    
                    self?.setupPlayer(url: self?.storyArray[indexPath.row].video_url)
                    cell.setupPlayer(player: self?.player)
                    self?.previousIndex = indexPath.row
                    
                }
               
              
              
            }
            
        }
        
        let didSelectBlock : DidSelectedRowCab = { [weak self] (indexPath, cell, item) in
            
            
            if self?.player?.timeControlStatus == AVPlayer.TimeControlStatus.playing{
                self?.pauseVideo()
            } else{
                
                self?.playVideo()
            }
        }
        
        
        
        
        let height = self.view.frame.height
        let width = self.view.frame.width
        
        
        collectionViewDataSource = CollectionViewDataSourceCab(items:  storyArray , collectionView: clcView, cellIdentifier:R.reuseIdentifier.storyClcCell.identifier, cellHeight:height, cellWidth: width , configureCellBlock: configureCellBlock, aRowSelectedListener: didSelectBlock)
        
        
        collectionViewDataSource?.centreCellListener = {[weak self](indexPath) in
            
          
            self?.pageIndicator.set(progress: indexPath.row, animated: true)
            guard let storyModel = self?.storyArray[indexPath.row] else{return}
             let isVideo = /storyModel.video_url != "" ? true : false
            if isVideo{
                
                if self?.previousIndex == indexPath.row {
                    
                    self?.playVideo()
                    return
                    
                }
                if let cell = self?.clcView.cellForItem(at: indexPath) as? StoryClcCell{
                    cell.imgView.image = nil
                    self?.setupPlayer(url: storyModel.video_url)
                    cell.setupPlayer(player: self?.player)
                    
                    cell.imgView.isHidden = isVideo
                    cell.viewPlayerContainer.isHidden = !(isVideo)
                    
                }
                
            }
            else{
                
                if let cell = self?.clcView.cellForItem(at: indexPath) as? StoryClcCell{
                    cell.imgView.isHidden = isVideo
                    cell.viewPlayerContainer.isHidden = !(isVideo)
                    guard let url = URL(string:/storyModel.image_url) else{return}
                    cell.imgView.kf.setImage(with:url)
                    
                }
                
            }
             self?.previousIndex = indexPath.row
            
            
        }
        
        collectionViewDataSource?.scrollViewListener = {[weak self](_) in
            
            
            self?.pauseVideo()
        }
        
        
        
        clcView.delegate = collectionViewDataSource
        clcView.dataSource = collectionViewDataSource
        clcView.reloadData()
    }
    
}



extension StoryVC{
    
    func getStory(){
        
    
             let token = /UDSingleton.shared.userData?.userDetails?.accessToken
            let story = BookServiceEndPoint.getStory
            story.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) { (response) in
                
                switch response {
                case .success(let data):
                    
                    if let storyArray = data as? [StoryModel]{
                        
                        self.storyArray = storyArray
                        
                    }
                    self.configureStoryData()
                    break
                    
                case .failure(let strError):
                    Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
                }
            }
        
        
    }
}
