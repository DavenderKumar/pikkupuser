//
//  StopTableViewCell.swift
//  RoyoRide
//
//  Created by Work on 19/03/21.
//  Copyright © 2021 CodeBrewLabs. All rights reserved.
//

import UIKit

class StopTableViewCell: UITableViewCell {

    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var addressLabel:UILabel!
    @IBOutlet weak var hintLabel:UILabel!
    @IBOutlet weak var iv:UIImageView!
    
    var stop:Stops?{
        didSet{
            if let address = stop?.address, !address.isEmpty{
                addressLabel.isHidden = false
                addressLabel.text = address
                hintLabel.isHidden = true
            }else{
                addressLabel.isHidden = true
                hintLabel.isHidden = false
            }
        }
    }
    
}
