//
//  chatRightTVC.swift
//  Buraq24
//
//  Created by Apple on 05/08/19.
//  Copyright © 2019 CodeBrewLabs. All rights reserved.
//

import UIKit

class chatRightTVC: UITableViewCell {

    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblChatMsg: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBg.setViewBackgroundColorHeader()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
