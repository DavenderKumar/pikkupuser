//
//  SelectBrandCell.swift
//  Buraq24
//
//  Created by MANINDER on 04/10/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import CoreLocation


class SelectBrandCell: UICollectionViewCell {
    
    
    //MARK:- Outlets
    
    @IBOutlet var imgViewBrand: UIImageView!
    @IBOutlet var viewImgBrand: UIView!
    @IBOutlet weak var labelEstimateTime: UILabel!
    
    @IBOutlet weak var labelEstimatePrice: UILabel!
    @IBOutlet var lblBrandName: UILabel!
    @IBOutlet weak var lblwait: UILabel!
    
    //MARK:- properties
    var indexPath: IndexPath?
    var estimatedDistanceInKm: Float?
    var estimatedTimeinMins: Int?
    
    
    
    //MARK:- Functions
    
    func markAsSelected(selected : Bool , model : Brand, drivers: [Driver]?, estimatedDistanceInKm: Float?, estimatedTimeinMins: Int?, request: ServiceRequest?) {
        
        self.estimatedDistanceInKm = estimatedDistanceInKm
        self.estimatedTimeinMins = estimatedTimeinMins
        
        viewImgBrand.cornerRadius(radius: viewImgBrand.bounds.width/2)

        let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
        switch template {
        case .DeliverSome:
            let colorValue = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.secondary_colour ?? DefaultColor.color.rawValue)
            selected == true ? viewImgBrand.addBorder(width: APIBasePath.isShipUsNow ? 3 : 2, color: colorValue) :  viewImgBrand.addBorder(width: 0, color: .gray)
            imgViewBrand.sd_setImage(with: model.imageURL, completed: nil)
            print(model.imageURL)
            lblBrandName.text = /model.brandName
            lblBrandName.textColor =  selected == true ? colorValue : .colorDarkGrayPopUp
            
        default:
            var colorValue  = UIColor()
            if APIBasePath.isPikkup {
                colorValue  = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.Btn_Text_Colour ?? DefaultColor.color.rawValue)
            } else{
                colorValue  = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.secondary_colour ?? DefaultColor.color.rawValue)
            }
            //let colorValue  = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.secondary_colour ?? DefaultColor.color.rawValue)
            selected == true ? viewImgBrand.addBorder(width: APIBasePath.isShipUsNow ? 3 : 2, color: colorValue) :  viewImgBrand.addBorder(width: 0, color: .gray)
            
            
            guard let urlString = model.brandImage?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
            guard let url = URL(string: urlString) else {return}
           // kf.setImage(with: url)
            
            imgViewBrand.sd_setImage(with: url, completed: nil)
            print(model.imageURL)
            if APIBasePath.isPikkup {
                if /model.productNames?.count > 0 {
                    lblBrandName.text = "\(/model.brandName)"
                    lblwait.text = "(\(/model.productNames?[0]))"
                } else{
                    lblBrandName.text = /model.brandName
                }
                
            } else{
                lblBrandName.text = /model.brandName
            }
           
            lblBrandName.textColor =  selected == true ? colorValue : .colorDarkGrayPopUp
        }
       
       // imgViewBrand.sd_setImage(with: model.imageURL, completed: nil)
        
       
        
        let filteredDriver = drivers?.filter({/$0.category_brand_id == /model.categoryBrandId}).first
        let product = model.products?.first
        var totalCost : Float = 0.0
       
        
        if filteredDriver == nil {
            
            let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
            switch template {
            case .Corsa:
                
            if let pricePPD = product?.pricePerDistance {
               totalCost = totalCost + (pricePPD * /estimatedDistanceInKm)
              }
              
              //if let pricePPM = product?.price_per_hr {
              //     totalCost = totalCost + (pricePPM * Float(/estimatedTimeinMins))
              // }
              
              totalCost = totalCost + Float(/product?.alphaPrice) // Base price
              
              if let percentage = model.buraq_percentage {
                  totalCost = totalCost + ((totalCost * percentage) / 100.0)
              }
                  
            
            // labelEstimatePrice.text = "Est \("currency".localizedString) \(String(totalCost).getTwoDecimalFloat())"
              
              let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
              switch template {
              case .GoMove:
                  labelEstimatePrice.text = ""
                  break
                
            
                  
              default:
                  labelEstimatePrice.text = "Est \(/UDSingleton.shared.appSettings?.appSettings?.currency) \(String(totalCost).getTwoDecimalFloat())"
              }
                              
                
                break
                
            default:
                labelEstimatePrice.text = " "
                labelEstimateTime.text = "No Driver"
            }
            
            
            
//            if labelEstimateTime.text == "No Driver"
//            {
//                labelEstimatePrice.text = ""
//                labelEstimateTime.text =  ""
//                lblwait.text = ""
//
////                labelEstimatePrice.isHidden = true
////                labelEstimateTime.isHidden = true
////                lblwait.isHidden = true
//                
//            }
//            else
//            {
//                labelEstimatePrice.isHidden = false
//                labelEstimateTime.isHidden = false
//                lblwait.isHidden = false
//            }
            
            
            labelEstimatePrice.text = ""
            labelEstimateTime.text =  ""
            lblwait.text = ""
            
           
            
            
        } else {
            
            let pickUpLocation = CLLocation(latitude: /request?.latitudeDest, longitude: /request?.longitudeDest)
            let DropOffLocation = CLLocation(latitude: /filteredDriver?.latitude, longitude: /filteredDriver?.longitude)
            print(/request?.distance)
            print(/request?.duration)
            if /filteredDriver?.latitude > 0.0 && /filteredDriver?.longitude > 0.0 &&  /request?.latitudeDest > 0.0 && /request?.longitudeDest > 0.0{
            let distanceInMeter = GoogleMapsDataSource.getDistance(newPosition: DropOffLocation, previous: pickUpLocation)
            let timeInMins = (distanceInMeter/60.0) // 60 kmph - speed of vehicle
            let timeInSeconds = Int(ceil(/timeInMins)) * 60
            let time = Utility.shared.convertToReadableTimeDuration(seconds: Int(timeInSeconds))
            labelEstimateTime.text = time
                 
            }
            else{
                
                labelEstimateTime.text = ""
            }
           
            
            if let pricePPD = product?.pricePerDistance {
             totalCost = totalCost + (pricePPD * /estimatedDistanceInKm)
            }
            
//            if let pricePPM = product?.price_per_hr {
//                totalCost = totalCost + (pricePPM * Float(/estimatedTimeinMins))
//            }
            
            totalCost = totalCost + Float(/product?.alphaPrice) // Base price
            
            if let percentage = model.buraq_percentage {
                totalCost = totalCost + ((totalCost * percentage) / 100.0)
            }
            
            
            let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
            switch template {
            case .GoMove:
                labelEstimatePrice.text = ""
                break
                
            default:
                 labelEstimatePrice.text = "Est \(/UDSingleton.shared.appSettings?.appSettings?.currency) \(String(totalCost).getTwoDecimalFloat())"
            }
            
            
            labelEstimatePrice.text = ""
            labelEstimateTime.text =  ""
            lblwait.text = ""
            
           // labelEstimatePrice.text = "Est \("currency".localizedString) \(String(totalCost).getTwoDecimalFloat())"
           
            
        }
    }
    
    func markProductSelected(selected : Bool , model : ProductCab) {
        
        viewImgBrand.cornerRadius(radius: viewImgBrand.bounds.width/2)

      let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
             switch template {
                    
                case .DeliverSome?:
                  let colorValue  = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.secondary_colour ?? DefaultColor.color.rawValue)
                  selected ? viewImgBrand.addBorder(width: 2, color: colorValue) :  viewImgBrand.addBorder(width: 0, color: .gray)
                 break
                 
                default:
                  let colorValue  = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.secondary_colour ?? DefaultColor.color.rawValue)
                  selected ? viewImgBrand.addBorder(width: 2, color: colorValue) :  viewImgBrand.addBorder(width: 0, color: .gray)
                 
             }
        
       // imgViewBrand.addBorder(width: 0, color: .colorDefaultSkyBlue)
        
      //  imgViewBrand.image = #imageLiteral(resourceName: "tonImg")
        //        imgViewBrand.sd_setImage(with: model.imageURL, completed: nil)
        lblBrandName.text = /model.productName
        lblBrandName.textColor =  selected ? .colorDefaultSkyBlue : .colorDarkGrayPopUp
    }
    
}


