//
//  LocationTableViewCell.swift
//  Trava
//
//  Created by Dhan Guru Nanak on 11/6/19.
//  Copyright © 2019 CodeBrewLabs. All rights reserved.
//

import UIKit
import GooglePlaces

class LocationTableViewCell: UITableViewCell {
    
    //MARK:- Outlets
    
    @IBOutlet weak var imageViewLocation: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var heightConstForLblSavedAddress: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func assignData(item :AddressCab ) {
        
        lblTitle.text = item.addressName
        
        if item.isSavedAdd == ""
            {
            //lblTitle.text = item.addressName
            heightConstForLblSavedAddress.constant = 0
        }
        else
        {
            
            heightConstForLblSavedAddress.constant = 20
//            let str = "SAVED ADDRESS" + "\n\n" + (item.addressName ?? "")
//            lblTitle.attributedText = str.changeColor(strMain: str, strSub: "SAVED ADDRESS")
        }
        
        lblSubtitle.text = item.address
    }
    
}


extension String
{
    func changeColor(strMain:String,strSub:String) -> NSAttributedString
    {
        let main_string = strMain
            let string_to_color = strSub

            let range = (main_string as NSString).range(of: string_to_color)

            let attribute = NSMutableAttributedString.init(string: main_string)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue , range: range)
        return attribute

           
    }
}
