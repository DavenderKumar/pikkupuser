//
//  BookingLocationCell.swift
//  Buraq24
//
//  Created by MANINDER on 10/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class BookingLocationCell: UITableViewCell {

    @IBOutlet var lblDropOffLocationName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
