//
//  APIConstantsCab.swift
//  Buraq24
//
//  Created by MANINDER on 16/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import Foundation

enum AppScheme {
    case shipusnow
    case deliversome
    case mexicar
    case sales
}


internal struct APIBasePath {
    
    
    static func setAppScheme() {
        
        #if SHIPUSNOW
        appScheme = .shipusnow
        secretDBKey = "b25dcb3ed072a432e0b76634d9d7ae4d"
        #elseif DELIVERSOME
        appScheme = .deliversome
        secretDBKey = "7882e69b3f898ba08aca4b703f1b1133e53956be99a692cc7772903f791b8c14"
        #elseif MEXICAR
        appScheme = .mexicar
        secretDBKey = "1e16c998d6a69a8eecd30cbdf51adc60"
        #else
        appScheme = .sales
        #endif
        
    }
    
    static var isShipUsNow:Bool{
        return secretDBKey == "b25dcb3ed072a432e0b76634d9d7ae4d"
    }
    
    static var isIgot4U:Bool{
        return secretDBKey == "456454ae09ad1d988be2677c9d477385"
    }
    
    static var hasSocialLogin:Bool{
        return secretDBKey == "0035690c91fbcffda0bb1df570e8cb98"
    }
    
    static var isDiva:Bool{
        return secretDBKey == "df258aa6db00e79bc182bc5d9ce5e4f6"
    }
    
    static var isMedc2u: Bool {
        return secretDBKey == "8969983867fe9a873b1b39d290ffa25c"
    }
    
    
    static var admin_base_url:String{
        return UDSingleton.shared.appSettings?.appSettings?.admin_base_url ?? "https://salesdemo-devadmin.royorides.io"
    }
    
    static var appScheme:AppScheme = .sales
    
  
    static var PrivacyPolicy:String{
        return "\(admin_base_url)/privacy_policy/user/" + BundleLocalization.sharedInstance().language
    }
    static var AboutUs:String{
        return "\(admin_base_url)/about_us/user/" + BundleLocalization.sharedInstance().language
    }
    
    static var TermsConditions:String{
        return "\(admin_base_url)/terms_conditions/user/" + BundleLocalization.sharedInstance().language
    }
    
    static var FaQ:String{
        
        return "\(admin_base_url)/faq"
       // return "https://pikkup-devadmin.royorides.io/faq"
    }
   
    static let categoryImageBasePath = "https://cdn-royorides.imgix.net/BuraqExpress/public/images/" //dev or test or production
    
    static let AppStoreURL = "https://itunes.apple.com"
    static let botChatToken = ""
    static let chatBotPath = "https://dialogflow.googleapis.com/v2/projects/my-project-1571850000978/agent/sessions/1234:detectIntent/"
    
    
    //Make changes here
    static var isPikkup: Bool{
       // return secretDBKey == "d190fc6d825399e5276673ee8467d415cebf52a7d89e2a2f532b4d8d1268fbc7"
        return secretDBKey == "d190fc6d825399e5276673ee8467d415af6dc774ffe7005ea59b3d5b6a2f0314"
        
    }
    
    //static var secretDBKey = "d190fc6d825399e5276673ee8467d415cebf52a7d89e2a2f532b4d8d1268fbc7"
    static var secretDBKey = "d190fc6d825399e5276673ee8467d415af6dc774ffe7005ea59b3d5b6a2f0314"
    
    
    //apidev
//    static let basePathwithoutv1 = "https://apidev.royorides.io"
//    static let basePath = "https://apidev.royorides.io/v1"
//    static let socketServerPath = "https://apidev.royorides.io"
//    static let baseImagePath = "https://apidev.royorides.io/v1/images/"
   // static let imageBasePathWithoutVersion =  "https://cdn-royorides.imgix.net/images/"
    
    //apitest
//   static let basePath = "https://apitest.royorides.io/v1" //
//   static let baseImagePath = "https://apitest.royorides.io/v1/images/"
//   static let socketServerPath = "https://apitest.royorides.io" //
   
    //production
    static let basePathwithoutv1 = "https://api.royorides.io"
    static let basePath = "https://api.royorides.io/v1"
    static let socketServerPath = "https://api.royorides.io"
    static let baseImagePath = "https://api.royorides.io/v1/images/"
    static let imageBasePathWithoutVersion = "https://cdn-royorides.imgix.net/prodimages/"
        
    
    static var paymentCheck:String{
        return basePath.replacingOccurrences(of: "/v1", with: "")
    }
    
    
    static let GoogleClientID = "691462131454-17h0t5h3pm8h5snij5cdg63t15ohtffd.apps.googleusercontent.com"
    
    static func paymentGateway(orderid:String,amount:String) -> String{
        return "https://salesdemo-devadmin.royorides.io/Payment-Method-new.php?amount=\(amount)&order_id=\(orderid)"
    }
    
    
    
    static func benefitPaymentUrl(amount:String?,order_id:Int?) -> String?{
        
        if let amt = amount,let id = order_id{
            let total = /Double(amt)?.rounded(toPlaces: 2)
           // return "https://pikkup-devadmin.royorides.io/benefit_payment_munira/sample/request.php?amount=\(total)&order_id=\(id)"
            let n = Int.random(in: 1...1000000)
            return "https://pikkupapp.com/benefit_payment_munira/sample/request.php?amount=\(total)&order_id=\(id)_\(n)"
        }
        return nil
    }
    
    
    
    static let StoryBaseUrl = "https://salesdemo-devadmin.royorides.io/BuraqExpress/public/images/"
    
    //static let benefitSuccessUrl = "https://pikkup-devadmin.royorides.io/admin/benefit_payment_save/00/"
    static let benefitSuccessUrl = "https://pikkupapp.com/admin/benefit_payment_save/00/"

    

}

internal struct APITypes {
    
    
    static let appSetting = "appSettings"
    
    //Login Signup
    
    static let sendOtp = "sendOtp"
    static let verifyOTP = "verifyOTP"
    static let addName = "addName"
    static let logOut = "logout"
    static let updateData = "updateData"
    static let eContacts = "eContacts"
    static let contactUs = "contactus"
    static let editProfile = "profileUpdate"
    static let changeNotification = "settingUpdate"
    static let checkuserExists = "checkuserExists"
    static let socailLogin = "socailLogin"
    static let emailLogin = "emailLogin"
    static let privateCooperationListing = "privateCooperationListing"
    static let privateCooperationRegForum = "privateCooperationRegForum"
    static let addEmergencyContact = "addEmergencyContacts"
    static let removeEmergencyContact = "removeEmergencyContacts"
    static let userEmergencyContact = "emergencyContactListing"
    
    //Service APIs
    static let getAllZones = "getAllZones"
    static let storyApi = "getStories"
    static let homeAPI = "homeApi"
    static let terminologyAPI = "terminology"
    static let requestAPI = "Request"
    static let cancelRequestAPI = "Cancel"
    static let ongoingRequestAPI = "Ongoing"
    static let rate = "Rate"
    static let packageListing = "packageListing"
    static let scanQrCode = "scanQrCode"
    static let coupons = "coupons"
    static let checkCoupons = "checkCoupons"
    static let halfWayStop = "halfWayStop"
    static let breakdownRequest = "breakdownRequest"
    static let shareRide = "shareRide"
    static let cancelShareRide = "cancelShareRide"
    static let cancelHalfWayStop = "cancelHalfWayStop"
    static let cancelVehicleBreakDown = "cancelVehicleBreakDown"
    static let getCreditPoints = "getCreditPoints"
    static let panic = "panic"
    static let addStops = "addStops"
    static let addAddress = "addAddress"
    static let editAddress = "editAddress"
    static let bannerAndServices = "bannerAndServices"
    static let addCard = "addCard"
    static let getCard = "getCardList"
    static let removeCard = "removeCard"
    static let payPendingAmount = "payPendingAmount"
    static let notification = "notificationListing"
    
    //Buy EToken
    static let eTokens = "ETokens"
    static let paginate = "ETokens/Paginate"
    static let buyEToken = "eToken/buy"
    static let eTokenDetails = "payment/details"
    static let bookingHistory = "order/history"
    static let orderDetails = "order/details"
    static let companyList = "companies/list"
    static let etokensList = "etokens/list"
    static let etokenPurchasedList = "purchases/list"
    static let etokenPurchase = "etoken/purchase"
    static let etokenConfirmReject   = "confirm/order"
    static let editCheckList = "editCheckList"
    static let removeCheckListItem = "removeCheckListItem"
    static let walletLogs = "walletLogs"
    static let walletTransfer = "walletTransfer"
    static let addTip = "Tip"
    static let getWalletBalance = "getWalletBalance"
    
    static let chatList   = "chatlogs"
    static let getConversationList   = "getChat"
    static let uploadImge = "mediafileUpload"
    static let addWalletAmount = "addMoneyInWallet"
    static let addUserCard = "addUserCard"
    static let razorPayReturnUrl = "razorPayReturnUrl"
    static let paytabReturnUrl = "payTabReturnUrl"
    //gift
    static let sentGiftList = "order/gift_list"
    static let receivedGiftRequest = "order/gift_requested_list"
    static let giftRequestResponse = "order/gift_requested/action"
    
    //common
    static let fileUpload = "fileUpload"
    
    
    
    
    //Paymaya
    static let getPaymayaUrl = "paymayaReturnUrl"
}


internal struct Routes{
    static let user = "/user/"
    static let commonRoutes = "/common/"
    static let service = "/service/"
    
    struct SubRoute {
        static let service = "service/"
        static let other = "other/"
        static let water = "water/"
        static let order = "order/"
    }
}

enum APIConstantsCab:String {
    case success = "success"
    case message = "msg"
    case accessToken = "accessToken"
    case statusCode = "statusCode"
}

enum SocialLoginType : String {
    
    case facebook = "Facebook"
    case google = "Google"
}

enum Keys : String {
    case card_type
    case isGifted = "isGifted"
    case access_token = "access_token"
    case receiver_id = "receiver_id"
    case language_id = "language_id"
    case phone_code = "phone_code"
    case phone_number = "phone_number"
    case timezone = "timezone"
    case latitude = "latitude"
    case longitude = "longitude"
    case socket_id = "socket_id"
    case fcm_id = "fcm_id"
    case device_type = "device_type"
    case otp = "otp"
    case name = "name"
    case distance = "distance"
    case airport_charges
    case category_id = "category_id"
    case breakdown_latitude
    case breakdown_longitude
    case payment_type = "payment_type"
    case category_brand_id = "category_brand_id"
    case category_brand_product_id = "category_brand_product_id"
    case product_quantity = "product_quantity"
    case pickup_address = "pickup_address"
    case pickup_latitude = "pickup_latitude"
    case pickup_longitude = "pickup_longitude"
    
    case dropoff_address = "dropoff_address"
    case dropoff_latitude = "dropoff_latitude"
    case dropoff_longitude = "dropoff_longitude"
    
    case order_timings = "order_timings"
    case future = "future"
    case orderId = "order_id"
    case transaction_id
    case cancelReason = "cancel_reason"
    case ratings = "ratings"
    case comments = "comments"
    case take = "take"
    case organisation_coupon_id = "organisation_coupon_id"
    case organisation_coupon_user_id = "organisation_coupon_user_id"
    case coupon_user_id = "coupon_user_id"
    case skip = "skip"
    case type = "type"
    case message = "message"
    case profile_pic = "profile_pic"
    case notifications = "notifications"
    case order_images = "order_images"
    case photo_proof_address = "photo_proof_address"
    case identification_school_or_work = "identification_school_or_work"
    case product_weight = "product_weight"
    case details = "details"
    case material_details = "material_details"
    case order_distance = "order_distance"
    case status = "status"
    
    // water modules
    
    case organisation_id = "organisation_id"
    case buraq_percentage = "buraq_percentage"
    case bottle_returned_value = "bottle_returned_value"
    case bottle_charge = "bottle_charge"
    case quantity = "quantity"
    case price  = "price"
    case eToken_quantity = "eToken_quantity"
    
    case address = "address"
    case  address_latitude = "address_latitude"
    case  address_longitude  = "address_longitude"
    
    case pickupPersonName = "pickup_person_name"
    case pickupPersonPhone = "pickup_person_phone"
    case invoiceNumber = "invoice_number"
    case deliveryPersonName = "delivery_person_name"
    
    case brandName = "brand_name"
    case firstName = "firstName"
    case lastName = "lastName"
    case gender = "gender"
    case f_name
    case f_number
    case n_name
    case n_number
    case finalCharge = "final_charge"
    case email
    case iso
    case package_id
    case distance_price_fixed
    case price_per_min
    case time_fixed_price
    case price_per_km
    case booking_type
    case friend_name
    case friend_phone_number
    case friend_phone_code
    case user_id
    case driver_id
    case coupon_id
    case coupon_code
    case code
    case half_way_stop_reason
    case reason
    case shareWith
    case cancellation_charges
    case stops
    case user_address_id
    case category
    case address_name
    case user_card_id
    case amount
    case credit_point_used
    case description
    case elevator_pickup
    case elevator_dropoff
    case pickup_level
    case dropoff_level
    case fragile
    case signup_as
    case social_key
    case login_as
    case password
    case items
    case cooperation_type
    case cooperation_id
    case identification_number
    case document
    case user_type_id
    case check_lists
    case check_list_ids
    case token_id
    case gateway_unique_id
    case card_number
    case exp_year
    case exp_month
    case cvc
    case limit
    case tip = "tip"
    case card_holder_name
    case card_brand
    case referral_code
    case emergencyContacts
    case emergency_contact_id
    case nationalId
    case start_dt = "start_dt"
    case end_dt = "end_dt"
    case order_time
     case payment_id
    case cancellation_payment_id
    case numberOfRiders = "numberOfRiders"
    case exact_path
    case nonce
    case relation
    case is_children
    case action
    case product_detail
    //Paymaya
    case currency
    case userId
    case successUrl
    case failureUrl
}

enum ValidateCab : String {
    
    case none
    case success = "1"
    case successCode = "200"
    case failure = "0"
    case invalidAccessToken = "401"
    case fbLogin = "3"
    case validation = "400"
    case apiError = "500"
    case lastRideCharges = "301"
    
    func map(response message : String?) -> String? {
        
        switch self {
        case .success:
            return message
        case .failure :
            return message
        case .invalidAccessToken :
            return message
        default:
            return nil
        }
    }
}

enum ResponseCab {
    case success(AnyObject?)
    case failure(String?)
}

typealias OptionalDictionary = [String : Any]?

let reasonString = "CANCELLING_WHILE_REQUESTING"

struct Parameters {
    
    //User Login SignUp Routes
    static let sendOtp : [Keys] = [.language_id , .phone_code , .phone_number , .timezone , .latitude , .longitude , .socket_id , .fcm_id, .device_type, .iso, .social_key, .signup_as, .email, .password ]
    static let socailLogin : [Keys] = [.language_id , .timezone , .latitude , .longitude , .socket_id , .fcm_id, .device_type, .social_key, .login_as]
    static let emailLogin : [Keys] = [.language_id , .timezone , .latitude , .longitude , .socket_id , .fcm_id, .device_type, .login_as, .email, .password ]
    static let privateCooperationRegForum: [Keys] = [.language_id , .timezone , .latitude , .longitude , .socket_id , .fcm_id, .device_type, .cooperation_id, .identification_number, .email, .phone_code, .iso, .phone_number, .password, .user_type_id]
    
    static let checkuserExists: [Keys] = [.social_key, .login_as]
    static let privateCooperationListing : [Keys] = [.items, .cooperation_type]
    static let addEmergrncyContact : [Keys] = [.emergencyContacts]
    static let removeEmergencyContact: [Keys] = [.emergency_contact_id]
    
    static let verifyOTP : [Keys] = [.otp ]
    static let addName : [Keys] = [.name,.firstName,.lastName,.gender,.address, .email, .referral_code, .nationalId,.f_name,.f_number,.n_name,.n_number]
    static let logout : [Keys] = []
    static let homeApi : [Keys] = [ .category_id  , .latitude , .longitude ,.distance ]
    static let braintreeCheckout : [Keys] = [ .amount  , .nonce, .orderId ]
    static let terminologyAPI : [Keys] = [.category_id]
    static let requestAPi : [Keys] = [
        .category_id,
        .category_brand_id,
        .category_brand_product_id,
        .product_quantity,
        .dropoff_address,
        .dropoff_latitude,
        .dropoff_longitude,
        .pickup_address,
        .pickup_latitude,
        .pickup_longitude,
        .order_timings,
        .future,
        .payment_type,
        .distance,
        .organisation_coupon_user_id,
        .material_details,
        .product_weight,
        .details,
        .order_distance,
        .pickupPersonName,
        .pickupPersonPhone,
        .invoiceNumber,
        .deliveryPersonName,
        .brandName,
        .finalCharge,
        .package_id,
        .distance_price_fixed,
        .price_per_min,
        .time_fixed_price,
        .price_per_km,
        .booking_type,
        .friend_name,
        .friend_phone_number,
        .friend_phone_code,
        .driver_id,
        .coupon_code,
        .cancellation_charges,
        .stops,
        .address_name,
        .user_card_id,
        .credit_point_used,
        .description,
        .elevator_pickup,
        .elevator_dropoff,
        .pickup_level,
        .dropoff_level,
        .fragile,
        .check_lists,
        .gender,
        .order_time,
        .numberOfRiders,
        .airport_charges,
        .exact_path,
        .cancellation_payment_id,
        .is_children,
        .relation,
        .isGifted,
        .product_detail,
        .breakdown_latitude,
        .breakdown_longitude,
        .card_type
    ]
    
    static let addStops: [Keys] = [.orderId, .stops, .dropoff_address, .dropoff_latitude, .dropoff_longitude]
    static let cancelRequestApi : [Keys] = [ .orderId , .cancelReason]
    static let ongoingApi : [Keys] = []
    static let rateDriver : [Keys] = [.orderId , .ratings , .comments]
    static let eTokens : [Keys] = [.category_id , .latitude , .longitude ,.distance ,.take , .order_timings]
    static let eTokenDetails : [Keys] = [.category_id , .latitude , .longitude ,.distance ,.take , .order_timings]
    static let buyETokens : [Keys] = [.organisation_coupon_id ]
    static let updateData : [Keys] = [.timezone , .latitude , .longitude , .fcm_id]
    static let history : [Keys] = [.skip , .take , .type, .start_dt, .end_dt]
    static let contactUs : [Keys] = [.message]
    static let editProfile : [Keys] = [.name, .email, .phone_code, .phone_number, .iso]
    static let orderDetail : [Keys] = [.orderId]
    static let changeNotification : [Keys] = [.notifications]
    static let getCompanyListWater :[Keys] = [.type, .latitude, .longitude, .category_brand_id, .category_brand_product_id,.take,.skip]
    static let getCompanyTokenList :[Keys] = [.organisation_id,.category_brand_id, .skip, .take]
    static let getTokenPurchaseList:[Keys] = [.skip, .take]
    
    static let purchaseEToken :[Keys] = [.organisation_coupon_id,.buraq_percentage, .bottle_returned_value, .bottle_charge, .quantity, .payment_type, .price, .eToken_quantity,.address, .address_latitude, .address_longitude]
    
    static let EtokenOrderAcceptReject :[Keys] = [.orderId, .status]
    static let scanQrCode: [Keys] = [.user_id]
    static let checkCoupons: [Keys] = [.code]
    static let halfWayStop: [Keys] = [.orderId, .latitude, .longitude, .order_distance, .half_way_stop_reason, .payment_type]
    static let breakdownRequest: [Keys] = [.orderId, .latitude, .longitude, .order_distance, .reason]
    static let shareRide: [Keys] = [.shareWith, .orderId]
    static let cancelShareRide: [Keys] = [.orderId]
    static let cancelHalfWayStop: [Keys] = [.orderId]
    static let cancelVehicleBreakDown: [Keys] = [.orderId]
    static let addAddress: [Keys] = [.address, .address_latitude, .address_longitude, .category, .address_name]
    static let editAddress: [Keys] = [.address, .address_latitude, .address_longitude, .user_address_id, .category, .address_name]
    static let removeCard: [Keys] = [.user_card_id]
    static let removeCardWithPaymentId: [Keys] = [.user_card_id,.gateway_unique_id]
    static let payPendingAmount: [Keys] = [.user_card_id, .amount]
    static let eContacts:[Keys] = [.phone_code]
    static let editCheckList:[Keys] = [.check_lists]
    static let removeCheckListItem:[Keys] = [.check_list_ids]
    static let addStripCard:[Keys] = [.token_id,.gateway_unique_id]
    static let addEpaycoCard:[Keys] = [.card_number,.exp_year,.exp_month,.cvc,.gateway_unique_id]
    static let walletLogs:[Keys] = [.skip, .limit]
    static let walletTransfer:[Keys] = [.amount, .phone_code, .phone_number]
    static let addTip : [Keys] = [.tip , .orderId, .gateway_unique_id]
    static let getWalletBalance: [Keys] = []
    static let addCard: [Keys] = [.card_holder_name, .card_number, .exp_year, .cvc, .gateway_unique_id, .card_brand, .exp_month]
    static let addWalletAmount: [Keys] = [.amount,.user_card_id, .gateway_unique_id]
    static let getchatList :[Keys] = [.language_id,.limit,.skip]
    static let pssChatList :[Keys] = [.language_id,.receiver_id,.limit,.skip]
    static let razorPayReturnUrl:[Keys] = [.orderId, .payment_id]
    static let payTabReturnUrl:[Keys] = [.orderId, .transaction_id]
    
    static let gift : [Keys] = [.skip , .take]
    static let giftRequest : [Keys] = [.orderId , .action]
    
    //Paymaya
    static let getPaymayaUrl:[Keys] = [.amount, .currency,  .successUrl,  .failureUrl]
     
}

struct Headers {
    
    //User Login SignUp Routes
    static let headerBasic : [Keys] = [.language_id]
    static let headerUserInfo : [Keys] = [.language_id , .access_token  ]
    
}

