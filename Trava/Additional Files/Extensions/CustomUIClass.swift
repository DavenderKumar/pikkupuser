//
//  CustomUIClass.swift
//  Untap
//
//  Created by OSX on 17/02/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import Foundation
import UIKit
import IBAnimatable

@IBDesignable class CustomUITextField:SkyFloatingLabelTextField{
  
  override func layoutSubviews() {
    super.layoutSubviews()
//    self.titleFont = R.font.colfaxBold(size: 16)!
    self.autocorrectionType = .no
    self.textColor = UIColor.black
  }
}

@IBDesignable class DisabledSelectOptionTextField:SkyFloatingLabelTextField{
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) || action ==  #selector(UIResponderStandardEditActions.copy(_:)) || action ==  #selector(UIResponderStandardEditActions.cut(_:)) || action ==  #selector(UIResponderStandardEditActions.select(_:)) || action ==  #selector(UIResponderStandardEditActions.selectAll(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}

class CustomGradientButtonView:AnimatableView{
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    self.startColor = UIColor.colorDefaultDarkPink()
    self.endColor = UIColor.colorDefaultPink()
    self.startPoint = .left
  }
}

class CustomUILabel:UILabel{
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.numberOfLines = 0
    }
}

@IBDesignable class CustomUISwitch:UISwitch{
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
    self.layer.cornerRadius = 15
    self.layer.masksToBounds = true
  }
}
@IBDesignable class CustomUITextView: UITextView{
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        self.textContainerInset = UIEdgeInsets(top: 0, left: -4, bottom: 0, right: 0)
    }
}

extension UIView {
  
  
   func slideAnimate(){
    
    let originalTransform = self.transform
    let scaledTransform = originalTransform.scaledBy(x: 0.8, y: 0.8)
    self.transform = scaledTransform.translatedBy(x: 0.0, y: 500)
    
    UIView.animate(withDuration: 1.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseOut, animations: {
      self.transform = originalTransform.scaledBy(x: 0.8, y: 0.8)
    }, completion: { (value) in
      
      self.transform = originalTransform.scaledBy(x: 1, y: 1)
    })
    
    UIView.animate(withDuration: 0.7, animations: {
      self.transform = originalTransform
    })
    
  }

  func loginHeaderAnimate(){
    
    let originalTransform = self.transform
    let scaledTransform = originalTransform.scaledBy(x: 1, y: 1)
    self.transform = scaledTransform.translatedBy(x: 0.0, y: -200)
   
    UIView.animate(withDuration: 0.5) {
      self.transform = originalTransform
    }
    
    
  }
  
    func changeViewAnimation(firstView:UIView,secondView:UIView,isSelected:Bool){
        
        UIView.animate(withDuration: 0.5, animations: {
            
            firstView.alpha = isSelected ? 0 : 1
            secondView.alpha = isSelected ? 0 : 1
            
        }, completion: { (value) in
            
            UIView.animate(withDuration: 0.3, animations: {
                
                firstView.alpha = isSelected ? 0 : 1
                secondView.alpha = isSelected ? 1 : 0
            })
            
        })
        
    }
  
}


@IBDesignable
class SlantColorView: AnimatableView {
  
  private let gradientLayer = CAGradientLayer()
  
  @IBInspectable var color1: UIColor = .white { didSet { updateColors() } }
  @IBInspectable var color2: UIColor = UIColor.init(hexString: "F8FBFF")  { didSet { updateColors() } }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    configureGradient()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    configureGradient()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    gradientLayer.frame = bounds
  }
  
  private func configureGradient() {
    
    gradientLayer.locations = [0.5,0.35]
    gradientLayer.startPoint = CGPoint(x: 0, y: 1)
    gradientLayer.endPoint = CGPoint(x: 1, y: 0)
    updateColors()
    layer.insertSublayer(gradientLayer, at: 0)
  }
  
  private func updateColors() {
    gradientLayer.colors = [color1.cgColor, color2.cgColor]
  }
  
}
