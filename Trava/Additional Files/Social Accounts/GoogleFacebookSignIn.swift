//
//  GoogleFacebookSignIn.swift
//  RoyoConsultant
//
//  Created by Sandeep Kumar on 12/05/20.
//  Copyright © 2020 SandsHellCreations. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit

//-------------------------------------------------------------------------------------------------------------------
//MARK:- GoogleSigin Class
class GoogleSignIn: NSObject, GIDSignInDelegate {
    
    typealias SuccessCallBack = ((_ userData: GoogleFBUserData?) -> ())
    var successCallBack: SuccessCallBack?
    
    static let shared = GoogleSignIn()
    
    override init() {
        super.init()
      //  GIDSignIn.sharedInstance()?.clientID = SDK.GoogleSignInKey.rawValue
      //  GIDSignIn.sharedInstance()?.delegate = self
      //  GIDSignIn.sharedInstance()?.presentingViewController = ez.topMostVC ?? UIViewController()
    }
    
    func openGoogleSigin(success: SuccessCallBack?) {
        
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = ez.topMostVC ?? UIViewController()
        
        GIDSignIn.sharedInstance()?.signOut()
        GIDSignIn.sharedInstance()?.signIn()
        successCallBack = success
    }
    
    //MARK:- GIDSignInDelegate Method
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if user != nil {
            successCallBack?(GoogleFBUserData.init(user.profile.name, user.userID, user.profile.email, user.profile.imageURL(withDimension: 120), user.authentication.idToken))
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
}

//-------------------------------------------------------------------------------------------------------------------
//MARK:- Facebook Login Class
class FBLogin: NSObject {
    
    static let shared = FBLogin()
    
    typealias SuccessCallBack = ((_ userData: GoogleFBUserData?) -> ())
    var successCallBack: SuccessCallBack?
    
    func login(_ success: SuccessCallBack?) {
        successCallBack = success
        AccessToken.current = nil
        Profile.current = nil
        LoginManager().logOut()
        LoginManager().logIn(permissions: ["email", "public_profile"], from: ez.topMostVC ?? UIViewController()) { (result, err) in
            if err != nil {
                print("failed to start graph request: \(String(describing: err))")
                return
            }
            self.getEmailNameIdImageFromFB()
        }
    }
    
    fileprivate func getEmailNameIdImageFromFB() {
        GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, picture.width(480).height(480)"]).start { [weak self] (connection, result, err) -> Void in
            if err != nil {
                print("failed to start graph request: \(String(describing: err))")
                return
            }
            print(result ?? "")
            let fbData = GoogleFBUserData(result: result as AnyObject?)
            self?.successCallBack?(fbData)
        }
    }
}

//MARK:- Google and Facebook Model
//-------------------------------------------------------------------------------------------------------------------
class GoogleFBUserData {
    var name: String?
    var id: String?
    var email: String?
    var imageURL: URL?
    var isThroughGoogle = false
    var accessToken: String?
    
    init(_ _name: String?, _ _id: String?, _ _email: String?, _ _imageURL: URL?, _ _accessToken: String?) {
        name = _name
        id = _id
        email = _email
        imageURL = _imageURL
        isThroughGoogle = true
        accessToken = _accessToken
    }
    
    init(result : AnyObject?) {
        guard let fbResult = result else { return }
        id = fbResult.value(forKey: "id") as? String
        name = fbResult.value(forKey: "name") as? String
        email = fbResult.value(forKey: "email") as? String
        imageURL = URL.init(string: "https://graph.facebook.com/".appending(/AccessToken.current?.userID).appending("/picture?type=large"))
        isThroughGoogle = false
        accessToken = /AccessToken.current?.tokenString
    }
}
//-------------------------------------------------------------------------------------------------------------------
