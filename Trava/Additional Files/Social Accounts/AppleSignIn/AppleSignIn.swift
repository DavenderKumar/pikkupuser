//
//  AppleSignIn.swift
//  AppleSignInDemo
//
//  Created by Ankush on 17/05/20.
//  Copyright © 2020 Ankush. All rights reserved.
//

import Foundation
import UIKit
import AuthenticationServices


class AppleSignIn: NSObject {
    
    public var didCompletedSignIn: ((_ user: AppleUser) -> Void)?
    
    static let shared = AppleSignIn()
    
}

@available(iOS 13.0, *)
extension AppleSignIn {
    
    func setUpSignInAppleButton(stackView: UIStackView) {
        let authorizationButton = ASAuthorizationAppleIDButton(authorizationButtonType: .continue, authorizationButtonStyle: .whiteOutline)
        
        authorizationButton.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
        authorizationButton.cornerRadius = 10
        //Add button on some view or stack
        stackView.addArrangedSubview(authorizationButton)
    }
    
    @objc func handleAppleIdRequest() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
}

@available(iOS 13.0, *)
extension AppleSignIn : ASAuthorizationControllerDelegate {
    
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        switch authorization.credential {
        case let credentials as ASAuthorizationAppleIDCredential:
            didCompletedSignIn?(AppleUser(credentials.user, credentials.fullName?.givenName, credentials.fullName?.familyName, credentials.email, nil))
            
        case let passwordCredential as ASPasswordCredential:
            didCompletedSignIn?(AppleUser(passwordCredential.user, nil, nil, nil, passwordCredential.password))
            
            
        default:
            break
        }
        
        /*  if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
         let userIdentifier = appleIDCredential.user
         let fullName = appleIDCredential.fullName
         let email = appleIDCredential.email
         print("User id is \(userIdentifier) \n Full Name is \(String(describing: fullName)) \n Email id is \(String(describing: email))") } */
    }
    
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        
        debugPrint("didCompleteWithAuthorization")
        
        let alertBox = UIAlertController.init(title: "Apple Sign in failed", message: error.localizedDescription, preferredStyle: .alert)
        alertBox.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
        ez.topMostVC?.present(alertBox, animated: true, completion: nil)
    }
    
}


public class AppleUser {
    var id: String?
    var firstName: String?
    var lastName: String?
    var email: String?
    var password: String?
    
    init(_ _id: String?, _ _firstName: String?, _ _lastName: String?, _ _email: String?, _ _password: String?) {
        id = _id
        firstName = _firstName
        lastName = _lastName
        email = _email
        password = _password
    }
}
