//
//  TableViewDataSourceWithHeader.swift
//  Auttle
//
//  Created by CodeBrew on 8/30/17.
//  Copyright © 2017 CodeBrew. All rights reserved.
//

import UIKit

class TableViewDataSourceWithHeader: TableViewDataSource {
    var viewforHeaderInSection: ViewForHeaderInSection?
    var viewForFooterInSection: ViewForHeaderInSection?
//    var willDisplayCell: WillDisplayCell?
    var headerHeight: CGFloat?
    
    required init (items: Array<Any>? , tableView: UITableView? , cellIdentifier: String?,cellHeight:CGFloat?,headerHeight:CGFloat?,configureCellBlock: ListCellConfigureBlock?,viewForHeader:ViewForHeaderInSection?,viewForFooter:ViewForHeaderInSection?, aRowSelectedListener: DidSelectedRow?,willDisplayCell: WillDisplayCell?) {
        
        self.viewforHeaderInSection = viewForHeader
        self.headerHeight = headerHeight
//        self.willDisplayCell = willDisplayCell
        self.viewForFooterInSection = viewForFooter
        
        super.init(items: items, tableView: tableView, cellIdentifier: cellIdentifier, cellHeight: cellHeight, configureCellBlock: configureCellBlock, aRowSelectedListener: aRowSelectedListener)
    }
    
    override init() {
        super.init()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let block = viewforHeaderInSection else { return nil }
        return block(section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight ?? 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let block = viewForFooterInSection else { return nil }
        return block(section)
    }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 0
  }
    

  
}
