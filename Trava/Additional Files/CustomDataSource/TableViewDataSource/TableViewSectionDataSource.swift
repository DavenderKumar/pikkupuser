//
//  TableViewSectionDataSource.swift
//  RoyoRide
//
//  Created by Prashant on 27/08/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit

typealias  SwipeAtIndex = (_ indexPath: IndexPath) -> ()

class TableViewSectionDataSource: NSObject  {
    var items: Array<Any>?
    var cellIdentifier: String?
    var tableView : UITableView?
    var configureCellBlock: ListCellConfigureBlockCab?
    var aRowSelectedListener: DidSelectedRowCab?
    var scrollViewScroll:  ScrollViewScroll?
    var willDisplayCell:WillDisplayCell?
    var cellHeight: CGFloat?
    var sectionHeight: CGFloat?
    var identifier1: Identifier?
    var swipeAtIndex:SwipeAtIndex?
    var swipAction:[String]?
    var canEdit = false
    
    
    init (items: Array<Any>? , tableView: UITableView? , cellIdentifier: String?, cellHeight:CGFloat? ) {
        self.tableView = tableView
        self.items = items
        self.cellIdentifier = cellIdentifier
        self.cellHeight = cellHeight
        
    }
    
    
    override init() {
        super.init()
    }
}

extension TableViewSectionDataSource: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        guard let identifier = cellIdentifier else {
//            fatalError("Cell identifier not provided")
//        }
        
        let ident = self.identifier1?(indexPath)
        
        
        let identifier = (ident != nil) ? /ident : /cellIdentifier
        
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        if let block = self.configureCellBlock , let item: Any = self.items?[indexPath.section] {
            block(cell , item , indexPath)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let block = self.aRowSelectedListener, let item: Any = self.items?[indexPath.row], case let cell as Any = tableView.cellForRow(at: indexPath) {
            
            block(indexPath , cell , item)

        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.items?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return   /cellHeight
    }
  
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return /sectionHeight
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 5.0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return canEdit
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        
        return "Remove"
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
            if let swipeAtIndex = self.swipeAtIndex{
                swipeAtIndex(indexPath)
            }
            }
        }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//
//        return canEdit
//    }
//
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//
//        let removeActions = ["remove","delete"]
//        var actions:[UITableViewRowAction]?
//        if let swipAction = swipAction{
//
//            for action in swipAction{
//
//                let action1 = UITableViewRowAction(style: .normal, title: action) { (actn, indexPath) in
//
//                    if let swipeAtIndex = self.swipeAtIndex{
//                        swipeAtIndex(indexPath)
//                    }
//                }
//
//                if removeActions.contains(action.lowercased()){
//
//                    action1.backgroundColor = UIColor.red
//                } else{
//
//                    // action1.style = UITableViewRowAction.Style.default
//                }
//
//                actions?.append(action1)
//
//
//            }
//
//            return actions
//        }
//
//        return nil
//    }
  
    
    
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    
    if let block = willDisplayCell{
      block(indexPath,cell)
    }
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    self.scrollViewScroll?(scrollView)
  }
    


}
