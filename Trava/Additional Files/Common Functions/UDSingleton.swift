//
//  UserDefaultsManager.swift
//  InTheNight
//
//  Created by OSX on 19/02/18.
//  Copyright © 2018 InTheNight. All rights reserved.
//

import UIKit
import ObjectMapper
import CoreLocation



 class UDSingleton: NSObject {
    var home : HomeCab?
    static let shared = UDSingleton()
    var order :Payment?
    private var loginDetail:LoginDetail?
    private var settings: AppSettingModel?
    private var terminology: AppTerminologyModel?
    
    
    //MARK:- User Data
    var userData:LoginDetail? {
        get{
            
            if let obj = self.loginDetail {
                return obj
            }
            
            if let userString = UserDefaults.standard.object(forKey: UDKeys.user) as? String {
                let userJson = Mapper<LoginDetail>.parseJSONStringIntoDictionary(JSONString: userString)
                self.loginDetail = Mapper<LoginDetail>().map(JSON: userJson ?? [:])
                return self.loginDetail
            }
            return nil
        }
        set{
            if let newVal = newValue {
                self.loginDetail = newVal
                let userString:String = /(newVal.toJSONString())
                UserDefaults.standard.set(userString, forKey: UDKeys.user)
               
                
            }else{
                UserDefaults.standard.removeObject(forKey: UDKeys.user)
            }
        }
    }
    
    // App Settings
    var appSettings: AppSettingModel? {
        get{
            
            if let obj = self.settings {
                return obj
            }
            
            if let settingString = UserDefaults.standard.object(forKey: UDKeys.appSetting) as? String {
                let settingJson = Mapper<AppSettingModel>.parseJSONStringIntoDictionary(JSONString: settingString)
                self.settings = Mapper<AppSettingModel>().map(JSON: settingJson ?? [:])
                return self.settings
            }
            return nil
        }
        set{
            if let newVal = newValue {
                self.settings = newVal
                let settingString:String = /(newVal.toJSONString())
                UserDefaults.standard.set(settingString, forKey: UDKeys.appSetting)
                
            }else{
                UserDefaults.standard.removeObject(forKey: UDKeys.appSetting)
            }
        }
    }
    
    // App Terminology
    var appTerminology: AppTerminologyModel? {
        get{
            
            if let obj = self.terminology {
                return obj
            }
            
            if let termString = UserDefaults.standard.object(forKey: UDKeys.appTerminology) as? String {
                let termJson = Mapper<AppTerminologyModel>.parseJSONStringIntoDictionary(JSONString: termString)
                self.terminology = Mapper<AppTerminologyModel>().map(JSON: termJson ?? [:])
                return self.terminology
            }
            return nil
        }
        set{
            if let newVal = newValue {
                self.terminology = newVal
                let termString:String = /(newVal.toJSONString())
                UserDefaults.standard.set(termString, forKey: UDKeys.appTerminology)
                
            }else{
                UserDefaults.standard.removeObject(forKey: UDKeys.appTerminology)
            }
        }
    }
    
    var is_multiple_requests:Bool{
        let count = Int(/appSettings?.appSettings?.multiple_request) ?? 0
        return count > 1
    }
    
    var isRidecaddie:Bool{
        return appSettings?.appSettings?.is_ridecaddie?.lowercased() == "true"
    }
    
    var hasTravelPackage:Bool{
        return appSettings?.appSettings?.travel_packages?.lowercased() == "true"
    }
    
    var hasSocialLogin:Bool{
        get{
            return appSettings?.appSettings?.hasSocialLogin?.lowercased() == "true"
        }
    }
    
    
    //MARK: - Static Methods
    var isOnBoardingDone: Bool {
        
        set{
            UserDefaults.standard.set(newValue, forKey: UDKeys.onboardingRun)
        }
        get{
            return (UserDefaults.standard.value(forKey: UDKeys.onboardingRun) as? Bool)  ?? false
        }
    }
    
    static var deviceToken: String? {
        get {
            return UserDefaults.standard.value(forKey: UDKeys.deviceToken) as? String
        }
        set {
            if let value = newValue {
                UserDefaults.standard.set(value, forKey: UDKeys.deviceToken)
            }
            else {
                UserDefaults.standard.removeObject(forKey: UDKeys.deviceToken)
            }
        }
    }
    
    
    
    
    
    //MARK:- ======== Functions ========
    func clearData() {
        
        //    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    }
    
    func tokenExpired() {
        
        removeAppData()
        
    }
    
    
    func removeAppData() {
        // needs to unregister notifcations
        self.loginDetail = nil
        UserDefaults.standard.removeObject(forKey: UDKeys.user)
        UserDefaults.standard.synchronize()
        SocketIOManagerCab.shared.closeConnection()
        UDSingleton.shared.clearData()
        
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
        
        switch template {
        case .Moby?:
            guard let vc = R.storyboard.newTemplateLoginSignUp.ntLoginSignupTypeViewController() else {return}
            AppDelegate.shared().setLoginAsRootVC(vc: vc)
            
        case .DeliverSome:
            guard let vc = R.storyboard.template1_Design.landingAndPhoneInputVCTemplate1() else { return }
            AppDelegate.shared().setLoginAsRootVC(vc: vc)
            
        default:
            guard UDSingleton.shared.hasSocialLogin else {
                guard let vc = R.storyboard.mainCab.landingAndPhoneInputVC() else {return}
                AppDelegate.shared().setLoginAsRootVC(vc: vc)
                return
            }
            guard let vc = R.storyboard.newTemplateLoginSignUp.ntLoginSignupTypeViewController() else {return}
            vc.template = .Default
            AppDelegate.shared().setLoginAsRootVC(vc: vc)
        }
    }
    
    
    //MARK: - Class Methods
    class func isGuestUser()->Bool {
        return /(UserDefaults.standard.value(forKey:UDKeys.user) as? String) == ""
    }
    
    func getService(categoryId : Int?) -> Service? {
        
        guard let services = userData?.services else{ return nil}
        let objCategory = services.filter{($0.serviceCategoryId == categoryId)}
        if !objCategory.isEmpty{
            return  objCategory.first
        }
        return nil
    }
}
