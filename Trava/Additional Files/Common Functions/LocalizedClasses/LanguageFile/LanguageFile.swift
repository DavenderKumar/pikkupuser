//
//  LanguageFile.swift
//  Buraq24
//
//  Created by MANINDER on 12/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit




class LanguageFile: NSObject {
    
    static let shared = LanguageFile()
   
    func setLanguage(languageID : Int,languageCode:String="en") {
        
        switch languageCode {
            
        case "en":
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            BundleLocalization.sharedInstance().language = Languages.English
            UserDefaultsManager.localeIdentifierCustom = "en_US"
        case "hi":
            BundleLocalization.sharedInstance().language = Languages.Hindi
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UserDefaultsManager.localeIdentifierCustom = "hi_IN"
            
        case "ur":
            BundleLocalization.sharedInstance().language = Languages.Urdu
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UserDefaultsManager.localeIdentifierCustom = "ur"
            
        case "zh":
            BundleLocalization.sharedInstance().language = Languages.Chinese
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UserDefaultsManager.localeIdentifierCustom = "zh_Hans_SG"
            
            
        case "ar":
            BundleLocalization.sharedInstance().language = Languages.Arabic
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UserDefaultsManager.localeIdentifierCustom = "ar_OM"
            
        case "es":
            BundleLocalization.sharedInstance().language = Languages.Spanish
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UserDefaultsManager.localeIdentifierCustom = "es"
        case "fr":
          BundleLocalization.sharedInstance().language = Languages.Franch
          UIView.appearance().semanticContentAttribute = .forceLeftToRight
          UserDefaultsManager.localeIdentifierCustom = "fr"
            
            
        default:
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            BundleLocalization.sharedInstance().language = Languages.English
            UserDefaultsManager.localeIdentifierCustom = "en_US"
            
        }
        UserDefaultsManager.languageId = "\(languageID)"
        UserDefaultsManager.languageCode = "\(languageCode)"
        UserDefaults.standard.set(BundleLocalization.sharedInstance().language, forKey: "language")
    }
    
    
    func getLanguage() -> String {
        
        if  let languageCode = UserDefaultsManager.languageId{
          return languageCode
        }
           return LanguageCode.English.rawValue
    }
    
    func isLanguageRightSemantic() -> Bool {
        
        if let languageCode = UserDefaultsManager.languageCode {
            if languageCode == "ar" ||  languageCode == "ur" {
               return true
            }
        }
        return false
    }
    
}



extension UIButton{
    func setAlignment() {
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu{
            self.contentHorizontalAlignment = .right
        }else {
            self.contentHorizontalAlignment = .left
        }
    }
    
    func setLeftAlign(){
        self.contentHorizontalAlignment = .left
    }
}

extension UILabel{
    func setAlignment() {
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu{
            self.textAlignment = .right
        }else {
            self.textAlignment = .left
        }
    }
    
    func setLeftAlign(){
        self.textAlignment = .left
    }
}
