//
//  AppDelegate.swift
//  Buraq24
//
//  Created by Maninder on 30/07/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import SideMenu
import Firebase
import UserNotifications
import Fabric
import Crashlytics
import Firebase
import FirebaseMessaging
import Alamofire
import FBSDKCoreKit
import GoogleSignIn
import Intents
import Braintree


//@UIApplicationMain
var userStatus:Bool = false
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {
    
    var window: UIWindow?
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    
    var brainTreeUrlScheme:String {
        get {
            if let path = Bundle.main.path(forResource: "Info", ofType: "plist") {
                
                if let dict = NSDictionary(contentsOfFile: path) as? [String: Any],
                    let urlTypes = dict["CFBundleURLTypes"] as? [[String:Any]],
                    let brainTreeScheme = urlTypes.first(where: { /($0["CFBundleURLName"] as? String) == "brainTree" }) {
                    debugPrint(/(brainTreeScheme["CFBundleURLSchemes"] as? [String])?.first)
                    
                    return /(brainTreeScheme["CFBundleURLSchemes"] as? [String])?.first
                }
            }
            return ""
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        NewRelic.start(withApplicationToken:"AA2e240acd665c8ff7cd77079736872febf9111a68-NRMA")
//        registerForPushNotifications()
        FirebaseApp.configure()

        
        // For facebook
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
                
        setUpLanguage()
        setUpApiKeys()
       // siriPermission()
        
       IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
        IQKeyboardManager.shared.enable = true
        
        

//        LocationManager.shared.updateUserLocation()
        
        
        
        self.listenForReachability()
        
        APIBasePath.setAppScheme()
        
       
        clearNotifications()
        
        onLoad()
        Fabric.with([Crashlytics.self])
        Fabric.sharedSDK().debug = true
        BTAppSwitch.setReturnURLScheme(brainTreeUrlScheme)
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            Messaging.messaging().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        application.registerForRemoteNotifications()
        
        // Firebase Setup
        // Override point for customization after application launch.
        return true
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        NotificationCenter.default.post(name: Notification.Name(rawValue:RemoteNotificationType.DriverAccepted.rawValue), object: nil)
        // Print full message.
        print("tap on on forground app",userInfo)
      
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        center.removeAllPendingNotificationRequests()
        
        if  UDSingleton.shared.userData == nil{
            
            return
        }
        if let payload = userInfo  as? [String : Any] {
            if let type = payload["type"] as? String {

                switch type {
                case RemoteNotificationType.WalletRecharge.rawValue:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "WalletVCViewController") as! WalletVCViewController
                        if let topVC = UIApplication.getTopViewController() {
                           topVC.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    break
                case RemoteNotificationType.Chat.rawValue:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        let storyboard = UIStoryboard(name: "MainCab", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                        vc.otherUserId = payload["sender_id"] as? String
                        vc.otherUserDetailId = payload["sender_user_detail_id"] as? String
//                        vc.otherUserDetailId = "\(/UDSingleton.shared.userData?.userDetails?.userDetailId)"
                        vc.profilePic = payload["profile_pic"] as? String ?? ""
                        vc.name = payload["name"] as? String ?? ""
                        vc.order_id = Int(payload["order_id"] as? String ?? "0")
                        vc.orderId = payload["order_id"] as? String ?? ""
                        print("Other User Id", payload["sender_id"] as? String)
                        if let topVC = UIApplication.getTopViewController(),!topVC.isKind(of: ChatVC.self) {
                           topVC.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                break
                    
                default:
                break
                }
            } else {
                if let type = payload["gcm.notification.type"] as? String {
                    switch type {
                        case RemoteNotificationType.WalletRecharge.rawValue:
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "WalletVCViewController") as! WalletVCViewController
                                if let topVC = UIApplication.getTopViewController() {
                                   topVC.navigationController?.pushViewController(vc, animated: false)
                                }
                            }
                            
                        break
                       
                        case RemoteNotificationType.Chat.rawValue:
                           DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            let storyboard = UIStoryboard(name: "MainCab", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                            vc.otherUserId = payload["sender_id"] as? String
                            vc.otherUserDetailId = payload["sender_user_detail_id"] as? String
    //                        vc.otherUserDetailId = "\(/UDSingleton.shared.userData?.userDetails?.userDetailId)"
                            vc.profilePic = payload["profile_pic"] as? String ?? ""
                            vc.name = payload["name"] as? String ?? ""
                            vc.order_id = Int(payload["order_id"] as? String ?? "0")
                            vc.orderId = payload["order_id"] as? String ?? ""
                            print("Other User Id", payload["sender_id"] as? String)
                            if let topVC = UIApplication.getTopViewController(),!topVC.isKind(of: ChatVC.self) {
                               topVC.navigationController?.pushViewController(vc, animated: false)
                            }
                           }
                        break
                       
                        default:
                        break
                   }
                }
            }
            
        }
        
        completionHandler()
    }
    
    
    func siriPermission(){
        
        INPreferences.requestSiriAuthorization { status in
          if status == .authorized {
            print("Hey, Siri!")
          } else {
            print("Nay, Siri!")
          }
        }
    }
    
    func listenForReachability() {
        self.reachabilityManager?.listener = { status in
            print("Network Status Changed: \(status)")
            switch status {
            case .notReachable, .unknown:
                
                NotificationCenter.default.post(name: Notification.Name(rawValue:LocalNotifications.InternetDisconnected.rawValue), object: nil)
                
            //Show error state
            case .reachable(_):
                NotificationCenter.default.post(name: Notification.Name(rawValue:LocalNotifications.InternetConnected.rawValue), object: nil)
                //Hide error state
            }
        }
        
        self.reachabilityManager?.startListening()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disab le timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        clearNotifications()
        
        let userInfo = UDSingleton.shared
        if (userInfo.userData != nil) {
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: LocalNotifications.AppInForground.rawValue), object: nil)
            SocketIOManagerCab.shared.establishConnection()
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue:RemoteNotificationType.DriverAccepted.rawValue), object: nil)
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        clearNotifications()
        SocketIOManagerCab.shared.establishConnection()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    
    func application(_ app: UIApplication,open url: URL,options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let sourceApplication: String? = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
        
        let isFBOpenUrl = ApplicationDelegate.shared.application(app, open: url, sourceApplication: sourceApplication, annotation: nil)
        let isGoogleOpenUrl = GIDSignIn.sharedInstance().handle(url)
        
        if isFBOpenUrl { return true }
        if isGoogleOpenUrl { return true }
        
        return false
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Buraq24")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

extension AppDelegate {
    
    func registerForPushNotifications(succcess: (() -> Void)? = nil) {
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                
                guard granted else { return }
                succcess?()
                self.getNotificationSettings()
            }
            
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                switch settings.authorizationStatus {
                case .authorized:
                    succcess?()
                default:
                    break
                }
            }
        } else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            ez.runThisInMainThread {
                UIApplication.shared.registerUserNotificationSettings(settings)
            }
        }
    }
    
    func getNotificationSettings() {
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            guard settings.authorizationStatus == .authorized else { return }
            ez.runThisInMainThread {
                
                UIApplication.shared.registerForRemoteNotifications()
                
            }
        }
    }
    
    func unregisterForpushNotification() {
        ez.runThisInMainThread {
            
            UIApplication.shared.unregisterForRemoteNotifications()
            
        }
    }
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        //        InstanceID.instanceID().instanceID { (result, error) in
        //            if let error = error {
        //                debugPrint("Error fetching remote instange ID: \(error)")
        //            } else if let result = result {
        //                debugPrint(result.token)
        //                UserDefaultsManager.fcmId = result.token
        //            }
        //        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        debugPrint("Failed to register: \(error)")
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        print("fcm token")
           print(fcmToken)
        UserDefaultsManager.fcmId = fcmToken
    }
    
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Refreshing the token called")
        print("fcm token")
                  print(fcmToken)
        UserDefaultsManager.fcmId = fcmToken
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        debugPrint(userInfo)
        
        NotificationCenter.default.post(name: Notification.Name(rawValue:RemoteNotificationType.DriverAccepted.rawValue), object: nil)
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        center.removeAllPendingNotificationRequests()
        
        if  UDSingleton.shared.userData == nil{
            
            return
        }
        if let payload = userInfo  as? [String : Any] {
            if let type = payload["type"] as? String {

                switch type {
                case RemoteNotificationType.WalletRecharge.rawValue:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "WalletVCViewController") as! WalletVCViewController
                        if let topVC = UIApplication.getTopViewController() {
                           topVC.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    break
                case RemoteNotificationType.Chat.rawValue:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        let storyboard = UIStoryboard(name: "MainCab", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                        vc.otherUserId = payload["sender_id"] as? String
                        vc.otherUserDetailId = payload["sender_user_detail_id"] as? String
//                        vc.otherUserDetailId = "\(/UDSingleton.shared.userData?.userDetails?.userDetailId)"
                        vc.profilePic = payload["profile_pic"] as? String ?? ""
                        vc.name = payload["name"] as? String ?? ""
                        vc.order_id = Int(payload["order_id"] as? String ?? "0")
                        vc.orderId = payload["order_id"] as? String ?? ""
                        print("Other User Id", payload["sender_id"] as? String)
                        if let topVC = UIApplication.getTopViewController(),!topVC.isKind(of: ChatVC.self) {
                           topVC.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                break
                    
                default:
                break
                }
            } else {
                if let type = payload["gcm.notification.type"] as? String {
                    switch type {
                        case RemoteNotificationType.WalletRecharge.rawValue:
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "WalletVCViewController") as! WalletVCViewController
                                if let topVC = UIApplication.getTopViewController() {
                                   topVC.navigationController?.pushViewController(vc, animated: false)
                                }
                            }
                            
                        break
                       
                        case RemoteNotificationType.Chat.rawValue:
                           DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                
                            let storyboard = UIStoryboard(name: "MainCab", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                            vc.otherUserId = payload["sender_id"] as? String
                            vc.otherUserDetailId = payload["sender_user_detail_id"] as? String
    //                        vc.otherUserDetailId = "\(/UDSingleton.shared.userData?.userDetails?.userDetailId)"
                            vc.profilePic = payload["profile_pic"] as? String ?? ""
                            vc.name = payload["name"] as? String ?? ""
                            vc.order_id = Int(payload["order_id"] as? String ?? "0")
                            vc.orderId = payload["order_id"] as? String ?? ""
                            print("Other User Id", payload["sender_id"] as? String)
                            if let topVC = UIApplication.getTopViewController(),!topVC.isKind(of: ChatVC.self) {
                               topVC.navigationController?.pushViewController(vc, animated: false)
                            }
                           }
                        break
                       
                        default:
                        break
                   }
                }
            }
            
        }
        
        
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        NotificationCenter.default.post(name: Notification.Name(rawValue:RemoteNotificationType.DriverAccepted.rawValue), object: nil)
        if let payload  = notification.request.content.userInfo  as? [String : Any] {
            
            print("Driver Payload",payload)
            
            guard let type = payload["type"] as? String else { return }
            print(type)
            
            switch type {
                
            case RemoteNotificationType.DriverAccepted.rawValue:
                NotificationCenter.default.post(name: Notification.Name(rawValue:RemoteNotificationType.DriverAccepted.rawValue), object: nil)
                completionHandler([.alert,.sound, .badge])
            case RemoteNotificationType.DriverArrive.rawValue:
                NotificationCenter.default.post(name: Notification.Name(rawValue:RemoteNotificationType.DriverAccepted.rawValue), object: nil)
                completionHandler([.alert,.sound, .badge])
            case RemoteNotificationType.DriverStartRide.rawValue:
                NotificationCenter.default.post(name: Notification.Name(rawValue:RemoteNotificationType.DriverAccepted.rawValue), object: nil)
                completionHandler([.alert,.sound, .badge])
            case RemoteNotificationType.DriverComplete.rawValue:
                userStatus = true
                NotificationCenter.default.post(name: Notification.Name(rawValue:RemoteNotificationType.DriverAccepted.rawValue), object: nil)
                completionHandler([.alert,.sound, .badge])
                
            default:
                break
            }
        }
   
        
        completionHandler([.alert,.sound, .badge])
        NSLog("Userinfo %@",notification.request.content.userInfo);
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            debugPrint("Message ID: \(messageID)")
        //        }
        
        // Print full message.
        NotificationCenter.default.post(name: Notification.Name(rawValue:RemoteNotificationType.DriverAccepted.rawValue), object: nil)
        print("UserInfo", userInfo)
        
        if  UDSingleton.shared.userData == nil{
            
            return
        }
        if let payload = userInfo  as? [String : Any] {
            if let type = payload["type"] as? String {

                switch type {
                case RemoteNotificationType.WalletRecharge.rawValue:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "WalletVCViewController") as! WalletVCViewController
                        if let topVC = UIApplication.getTopViewController() {
                           topVC.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    break
                case RemoteNotificationType.Chat.rawValue:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        let storyboard = UIStoryboard(name: "MainCab", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                        vc.otherUserId = payload["sender_id"] as? String
                        vc.otherUserDetailId = payload["sender_user_detail_id"] as? String
//                        vc.otherUserDetailId = "\(/UDSingleton.shared.userData?.userDetails?.userDetailId)"
                        vc.profilePic = payload["profile_pic"] as? String ?? ""
                        vc.name = payload["name"] as? String ?? ""
                        vc.order_id = payload["order_id"] as? Int ?? 0
                        vc.orderId = payload["order_id"] as? String ?? ""
                        print("Other User Id",payload["sender_id"] as? String)
                        if let topVC = UIApplication.getTopViewController(),!topVC.isKind(of: ChatVC.self) {
                           topVC.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                break
                    
                default:
                break
                }
            } else {
                if let type = payload["gcm.notification.type"] as? String {
                    switch type {
                        case RemoteNotificationType.WalletRecharge.rawValue:
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "WalletVCViewController") as! WalletVCViewController
                                if let topVC = UIApplication.getTopViewController() {
                                   topVC.navigationController?.pushViewController(vc, animated: false)
                                }
                            }
                            
                        break
                       
                        case RemoteNotificationType.Chat.rawValue:
                           DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                
                            let storyboard = UIStoryboard(name: "MainCab", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                            vc.otherUserId = payload["sender_id"] as? String
                            vc.otherUserDetailId = payload["sender_user_detail_id"] as? String
    //                        vc.otherUserDetailId = "\(/UDSingleton.shared.userData?.userDetails?.userDetailId)"
                            vc.profilePic = payload["profile_pic"] as? String ?? ""
                            vc.name = payload["name"] as? String ?? ""
                            vc.order_id = Int(payload["order_id"] as? String ?? "0")
                            vc.orderId = payload["order_id"] as? String ?? ""
                            print("Other User Id", payload["sender_id"] as? String)
                            if let topVC = UIApplication.getTopViewController(),!topVC.isKind(of: ChatVC.self) {
                               topVC.navigationController?.pushViewController(vc, animated: false)
                            }
                           }
                        break
                       
                        default:
                        break
                   }
                }
            }
            
        }
        
        debugPrint(userInfo)
    }
    
}
extension AppDelegate {
    
    class func shared() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func setUpApiKeys() {
        //google api setup
        
//        if /UDSingleton.shared.appSettings?.appSettings?.ios_google_api == "" {
//           print("Google Place API")
//           GMSServices.provideAPIKey( /UDSingleton.shared.appSettings?.appSettings?.google_place_api)
//           GMSPlacesClient.provideAPIKey(/UDSingleton.shared.appSettings?.appSettings?.google_place_api)
//       } else {
//           GMSServices.provideAPIKey( /UDSingleton.shared.appSettings?.appSettings?.ios_google_api)
//           GMSPlacesClient.provideAPIKey(/UDSingleton.shared.appSettings?.appSettings?.ios_google_api)
//       }
                       
        
//        GMSServices.provideAPIKey( /UDSingleton.shared.appSettings?.appSettings?.ios_google_api)
//        GMSPlacesClient.provideAPIKey(/UDSingleton.shared.appSettings?.appSettings?.ios_google_api)
        GIDSignIn.sharedInstance().clientID = APIBasePath.GoogleClientID
    }
    
    
    
    func clearNotifications() {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
            center.removeAllDeliveredNotifications() // To remove all delivered notifications
        }
    }
    
    
    func  setUpLanguage() {
        
        var code = 1
        if  let languageCode = UserDefaultsManager.languageId{
            guard let intVal = Int(languageCode) else {return}
            code = intVal
        }else {
            // let language = Bundle.main.preferredLocalizations.first
        }
        
        LanguageFile.shared.setLanguage(languageID: code,languageCode: UserDefaultsManager.languageCode ?? "en")
    }
    
    func setHomeAsRootVC(categoryID: Int? = nil) {
        
        guard let vc = R.storyboard.bookService.homeVC() else { return }
        let navigation = UINavigationController(rootViewController: vc)
        navigation.isNavigationBarHidden = true
        self.window?.rootViewController = navigation
        UIViewController().stopAnimating()
    }
    
    func setWalkThroughAsRootVC() {
        
        guard let vc = R.storyboard.mainCab.landingAndPhoneInputVC() else { return }
        let navigation = UINavigationController(rootViewController: vc)
        navigation.isNavigationBarHidden = true
        self.window?.rootViewController = navigation
        UIViewController().stopAnimating()
    }
    
    
    func setInitialAsRootVC() {
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "MainCab", bundle:nil)
//        let vc = storyBoard.instantiateViewController(withIdentifier: "OnboardingController") as! OnboardingController
        
        guard let vc = R.storyboard.mainCab.initiallViewController() else { return }
        
        if  let navigation = self.window?.rootViewController as? UINavigationController {
            navigation.navigationBar.isTranslucent = false
            navigation.navigationBar.isHidden = true
            navigation.navigationBar.barTintColor = UIColor.clear
            navigation.isNavigationBarHidden = true
            navigation.setViewControllers([vc], animated: false)
            self.window?.rootViewController = navigation
        }else  {
            let navigation = UINavigationController(rootViewController: vc)
            navigation.navigationBar.isTranslucent = false
            navigation.navigationBar.isHidden = true
            navigation.navigationBar.barTintColor = UIColor.clear
            self.window?.rootViewController = navigation
        }
        
        UIViewController().stopAnimating()
    }
    
    func setLoginAsRootVC(vc: UIViewController) {
        
        //   guard let vc = R.storyboard.mainCab.landingAndPhoneInputVC() else { return }
        
        if  let navigation = self.window?.rootViewController as? UINavigationController {
            navigation.navigationBar.isTranslucent = false
            navigation.navigationBar.isHidden = true
            navigation.navigationBar.barTintColor = UIColor.clear
            navigation.isNavigationBarHidden = true
            navigation.viewControllers[0] = vc
            self.window?.rootViewController = navigation
        }else  {
            let navigation = UINavigationController(rootViewController: vc)
            navigation.navigationBar.isTranslucent = false
            navigation.navigationBar.isHidden = true
            navigation.navigationBar.barTintColor = UIColor.clear
            self.window?.rootViewController = navigation
        }
        
        UIViewController().stopAnimating()
    }
    
    var appDelegate : AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    func onLoad() {
        
    }
}


public extension UIApplication {
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)

        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)

        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}


