//
//  EmptyStateView.swift
//  RoyoRideDriver
//
//  Created by Sandeep Kumar on 04/09/20.
//  Copyright © 2020 OSX. All rights reserved.
//

import UIKit
extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

enum NoDataTitle: String {
    case NO_PAST_BOOKINS
    case NO_UPCOMING
    case NO_ACTIVE
    case NO_NOTIFICATIONS
    case NO_TRANSACTIONS
    case NO_EMPERGENCY_CONTACTS
    case NO_SENT_GIFT
    case NO_RECEIVED_GIFT
    
    var localized: String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

class EmptyStateView: UIView {
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var imgView: UIImageView!
    
    func setData(type: NoDataTitle) {
        lblDesc.text = type.localized
    }
}
