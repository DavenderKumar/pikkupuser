//
//  Bubbler
//
//  Created by Zivato Limited on 17/04/2018.
//  Copyright © 2018 Ben Swift. All rights reserved.
//

import UIKit

extension UIView {
    func rotate360Degrees(duration: CFTimeInterval = 14.0, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI * 2.0)
        rotateAnimation.duration = duration
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as! CAAnimationDelegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
}

class OnboardingController: UIViewController {
    
        
    @IBOutlet var cars1: UIImageView!
    @IBOutlet var wheel: UIImageView!
    @IBOutlet var appDriver: UIButton!
    @IBOutlet var clouds: UIImageView!
    @IBOutlet var van: UIImageView!
    @IBOutlet var buttonView: UIStackView!
    @IBOutlet var backgroundImage: UIImageView!
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var defaultKM = 48 //48KM = 30MLS
    
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.setNavigationBarHidden(true, animated: false)
    
   // self.buttonView.alpha = 0
    self.appDriver.alpha = 0
    self.wheel.rotate360Degrees()
    buttonView.isHidden = false
    
    UIView.animate(withDuration: 8, animations: {
        self.cars1.layer.frame.origin.x += 1000
        self.van.layer.frame.origin.x -= 800
        self.clouds.frame.origin.x -= 50

    }, completion: nil)
    
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
        //self.locationManager.requestAlwaysAuthorization()
    }
    
 }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedOpeningBefore")
        if launchedBefore  {
            print("Not first launch.")
            getAppSettings()
            self.buttonView.alpha = 0
            
        } else {
            print("First launch, setting UserDefault.")
            UserDefaults.standard.set(true, forKey: "launchedOpeningBefore")
            
           // self.buttonView.alpha = 1
            self.appDriver.alpha = 1
        }
       
    }
  

    @IBAction func btnGetStarted(_ sender: UIButton) {
        getAppSettings()
    }
    
    
    
    
    
    func getAppSettings() {
        
        let getSettings = LoginEndpoint.appSetting
        getSettings.request( header: ["language_id" : LanguageFile.shared.getLanguage()]) {[weak self] (response) in
            switch response {
                
            case .success(let data):
                
                debugPrint(data as Any)
                
                guard let model = data as? AppSettingModel, let _ = model.appSettings else { return }
                
                UDSingleton.shared.appSettings = model
                let userInfo = UDSingleton.shared
                
                let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
                switch template {
                case .Moby?:
                    
                    if (userInfo.userData != nil) {
                         ez.runThisInMainThread {
                            AppDelegate.shared().setHomeAsRootVC()
                         }
                      }else  {
                       
                        ez.runThisInMainThread {
                            guard let vc = R.storyboard.newTemplateLoginSignUp.ntLoginSignupTypeViewController() else {return}
                            self?.pushVC(vc)
                        }
                    }
                    
                case .DeliverSome:
                    
                    if (userInfo.userData != nil) {
                        ez.runThisInMainThread {
                           AppDelegate.shared().setHomeAsRootVC()
                        }
                    }else  {
                        
                        ez.runThisInMainThread {
                           guard let loginTemplate1 = R.storyboard.template1_Design.landingAndPhoneInputVCTemplate1() else { return }
                           self?.pushVC(loginTemplate1)
                       }
                    }
                
                    
                default:
                    if (userInfo.userData != nil) {
                         ez.runThisInMainThread {
                            AppDelegate.shared().setHomeAsRootVC()
                         }
                      }else  {
                       
                        ez.runThisInMainThread {
                            guard let vc = R.storyboard.mainCab.landingAndPhoneInputVC() else {return}
                            self?.pushVC(vc)
                        }
                    }
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
                //Toast.show(text: strError, type: .error)
            }
        }
    }
    
 
        
}
