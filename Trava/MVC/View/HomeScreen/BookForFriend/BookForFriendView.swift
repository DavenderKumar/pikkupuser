//
//  BookForFriendView.swift
//  Trava
//
//  Created by Apple on 27/11/19.
//  Copyright © 2019 CodeBrewLabs. All rights reserved.
//

import Foundation
import UIKit
import IBAnimatable

class BookForFriendView: UIView {
    
    //MARK:- Outlets
    //MARK:-
    
    @IBOutlet weak var textfieldPhoneNumber: UITextField!
    @IBOutlet weak var textfieldFullName: AnimatableTextField!
    
    @IBOutlet var imgViewCountryCode: UIImageView!
    @IBOutlet var lblCountryCode: UILabel!
    @IBOutlet weak var buttonContinue: UIButton!
    @IBOutlet weak var btnCountryCode: UIButton!
    
    @IBOutlet weak var txtfldRelationship: UITextField!
    @IBOutlet weak var stckVwRelation: UIStackView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnContact: UIButton!
    //MARK:- Properties
    //MARK:-
    var ISO: String?
    var viewSuper : UIView?
    var isAdded : Bool = false
    var request : ServiceRequest = ServiceRequest()
    var frameHeight : CGFloat = 420
    var delegate : BookRequestDelegate?
    var isGift : Bool = false
    //MARK:- Actions
    //MARK:-
    
    @IBAction func actionBtnBookPressed(_ sender: UIButton) {
        
        validateFields()
        
        /*  //        if /request.serviceSelected?.serviceCategoryId != 1 {
         //           alertBoxOk(message:"work_in_progress".localizedString , title: "AppName".localizedString, ok: {
         //            })
         //            return
         //        }else{
         
         minimizeConfirmPickUPView()
         delegate?.didClickConfirmPickup()
         
         // Ankush self.delegate?.didSelectNext(type: .SubmitOrder)
         // } */
        
    }
    
    @IBAction func actionBtnCountryCode(_ sender: UIButton) {
        
        guard let countryPicker = R.storyboard.mainCab.countryCodeSearchViewController() else{return}
        countryPicker.delegate = self
        ez.topMostVC?.presentVC(countryPicker)
    }
    
    @IBAction func actionBtnCancel(_ sender: Any) {
        minimizeBookForFriendView()
    }
    
    @IBAction func actionBtnContact(_ sender: Any) {
        
        self.delegate?.didSelectContactForGift()
    }
    //MARK:- Functions
    //MARK:-
    
    func minimizeBookForFriendView() {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: /self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            
        }, completion: { (done) in
        })
        
        clearTextfields()
    }
    
    func maximizeBookForFriendView() {
        
        
        btnCountryCode.setButtonWithTitleAndBorderColorSecondary()
        textfieldPhoneNumber.setBorderColorSecondary()
        textfieldPhoneNumber.addLeftTextPadding(10)
        textfieldPhoneNumber.addShadowToTextFieldColorSecondary()
        
        textfieldFullName.setBorderColorSecondary()
        textfieldFullName.addLeftTextPadding(10)
        textfieldFullName.addShadowToTextFieldColorSecondary()
        
        
        txtfldRelationship.setBorderColorSecondary()
        txtfldRelationship.addLeftTextPadding(10)
        txtfldRelationship.addShadowToTextFieldColorSecondary()
        
        btnCountryCode.setButtonWithTitleAndBorderColorSecondary()
        
        if  isGift {
            frameHeight = UIDevice.current.iPhoneX ? 342 + 34 : 342
        } else {
            frameHeight = UIDevice.current.iPhoneX ? 420 + 34 : 420
        }
        
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            
            // Ankush   self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - CGFloat(BookingPopUpFrames.PaddingX) - /self?.frameHeight , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            
            // 10 - for hide bottom corner radius
            self?.frame = CGRect(x: 0, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - /self?.frameHeight + 10 , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            self?.layoutIfNeeded()
            self?.setupUI()
            self?.assignData()
        }, completion: { (done) in
            
        })
    }
    
    func clearTextfields() {
        
        textfieldFullName.text = ""
        textfieldPhoneNumber.text = ""
        
    }
    
    func setupUI() {
        buttonContinue.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        btnCancel.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        btnContact.setButtonWithTitleColorTheme()
        stckVwRelation.isHidden = isGift
        
    }
    
    func showBookForFriendView(superView : UIView, requestPara: ServiceRequest) {
        request = requestPara
        viewSuper = superView
        
        if !isAdded {
            
            // Ankush frameHeight =  superView.frame.size.width*70/100
            viewSuper = superView
            self.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (superView.frame.origin.y + superView.frame.size.height) , width: BookingPopUpFrames.WidthPopUp, height: frameHeight)
            superView.addSubview(self)
            isAdded = true
        }
        if requestPara.serviceSelected?.serviceCategoryId == 12 {
            lblTitle.text = requestPara.serviceSelected?.serviceName
            btnCancel.isHidden = false
            isGift = true
            frameHeight = 342
        }else{
            btnCancel.isHidden = true
            lblTitle.text = "Book for a friend"
            isGift = false
            frameHeight = 420
        }
        maximizeBookForFriendView()
    }
    
    func assignData() {
        
        lblCountryCode.text = UDSingleton.shared.appSettings?.appSettings?.default_country_code ?? DefaultCountry.countryCode.rawValue
        ISO = UDSingleton.shared.appSettings?.appSettings?.iso_code ?? DefaultCountry.ISO.rawValue
        debugPrint("\(/ISO?.lowercased()).png")
        imgViewCountryCode.image = UIImage(named: "\(/ISO?.lowercased()).png")
        
        /*  guard let user = UDSingleton.shared.userData?.userDetails?.user else{return}
         txtFieldFullName.text = user.name
         textFieldEmail.text = user.email
         textfieldPhoneNumber.text = String(/user.phoneNumber)
         
         lblCountryCode.text = user.countryCode
         ISO = user.iso
         
         imgViewCountryCode.image = UIImage(named: /ISO?.lowercased())
         
         if let urlImage = UDSingleton.shared.userData?.userDetails?.profilePic {
         imgViewUser.sd_setImage(with: URL(string : urlImage), placeholderImage: #imageLiteral(resourceName: "ic_user"), options: .refreshCached, progress: nil, completed: nil)
         } */
    }
    
    func validateFields() {
        
        if Validations.sharedInstance.validateUserName(userName: /textfieldFullName.text) && Validations.sharedInstance.validatePhoneNumber(phone: /textfieldPhoneNumber.text) {
            
            request.booking_type = "Friend"
            request.friend_name = /textfieldFullName.text
            request.friend_phone_number = /textfieldPhoneNumber.text
            request.friend_phone_code = /lblCountryCode.text
            request.relation = /txtfldRelationship.text
            delegate?.didClickContinueToBookforFriend(request: request)
            
            
            if isGift {
                
                request.isGifted = 1
                delegate?.didClickContinueToBookforFriend(request: request)
                //minimizeBookForFriendView()
            }else{
                request.isGifted = 0
                delegate?.didClickContinueToBookforFriend(request: request)
            }
        }
    }
}
//MARK: - Country Picker Delegates

extension BookForFriendView: CountryCodeSearchDelegate {
    
    func didTap(onCode detail: [AnyHashable : Any]!) {
        
        ISO = /(detail["code"] as? String)
        
        imgViewCountryCode.image = UIImage(named: /ISO?.lowercased())
        lblCountryCode.text = /(detail["dial_code"] as? String)
        
    }
    
    func didSuccessOnOtpVerification(){
        
    }
    
    
}
