//
//  ChooseAddress.swift
//  Trava
//
//  Created by Apple on 08/01/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit

class ChooseAddress: UIView {

    @IBOutlet var viewOuter: UIView!
    @IBOutlet weak var labelLocationNickName: UILabel!
    @IBOutlet weak var labelLocationName: UILabel!
    @IBOutlet weak var btnPickUpAddress: UIButton!
    @IBOutlet weak var btnDropAddress: UIButton!
    
    @IBOutlet weak var btnAddStopAddress: UIButton!
    
    @IBOutlet weak var heightConstForBtnAddStop: NSLayoutConstraint!
    @IBOutlet weak var lblOr: UILabel!
    @IBOutlet weak var heightConstForChooseView: NSLayoutConstraint!
    var delegate: BookRequestDelegate?
    var address: AddressCab?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let firstTouch = touches.first {
            let hitView = self.hitTest(firstTouch.location(in: self), with: event)
            if hitView === viewOuter {
                removeFromSuperview()
            }
        }
        
    }
    
    
    func showView(address: AddressCab?,count:Int?) {
        
        self.address = address
        setupUI()
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
           
            self?.frame = UIScreen.main.bounds
            ez.topMostVC?.view.addSubview(self ?? UIView())
            
            }, completion: { (done) in
                
        })
        
        
        labelLocationName.text = address?.address
        labelLocationNickName.text = address?.addressName
        
        if count == 0 
            {
            heightConstForChooseView.constant = 290
            heightConstForBtnAddStop.constant = 0
            lblOr.isHidden = true
        }
        else
        {
            heightConstForChooseView.constant = 340
            heightConstForBtnAddStop.constant = 48
            lblOr.isHidden = false
            
        }
    }
    
    
    
    func setupUI() {
        btnPickUpAddress.setButtonWithBackgroundColorSecondaryAndTitleColorBtnText()
        btnDropAddress.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        btnAddStopAddress.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        labelLocationName.setAlignment()
        
        
    }
    

    @IBAction func buttonClicked(_ sender: UIButton) {
        
        // 1- Pickup, 2- Dropoff
        switch /sender.tag {
        
        case 1:
            delegate?.didChooseAddressFromRecent(place: address, isPickup: true ,isFromStop:false,stopIndex:0 )
            removeFromSuperview()
            
        case 2:
            delegate?.didChooseAddressFromRecent(place: address, isPickup: false,isFromStop:false,stopIndex:0)
            removeFromSuperview()
            
        case 3:
            delegate?.didChooseAddressFromRecent(place: address, isPickup: false,isFromStop:true,stopIndex:0)
            removeFromSuperview()
            
        default:
            break
        }
        
    }
}

