//
//  SelectServiceView.swift
//  Buraq24
//
//  Created by MANINDER on 28/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

var arrTitle: [AppForum]?

class SelectServiceView: UIView, iCarouselDataSource, iCarouselDelegate {
    
    //MARK:- Outlets
    @IBOutlet var viewCrousal: iCarousel!
    @IBOutlet weak var crousalHeightCOnstant: NSLayoutConstraint!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var btnSelectLocation: RoundShadowButton!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    //MARK:- Properties
    var previousIndex : Int = -1
    
    var collectionViewDataSource : CollectionViewDataSourceCab?
    var services : [Service] = [Service]()
    var heightPopUp : CGFloat = 320
    var viewSuper : UIView?
    var request = ServiceRequest()
    var delegate : BookRequestDelegate?
    let reuseIdentifier = "ServiceCellCab"
  var selectedIndex = 0

    lazy var serviceLocal = [ServiceTypeCab]()
    
    var firstTime = false
    var isAdded : Bool = false
    
    
    override func awakeFromNib() {
        
        btnSelectLocation.setButtonWithBorderColorSecondary()
        btnSelectLocation.addShadowToViewColorSecondary()
        let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
        switch template {
        case .GoMove:
            btnSelectLocation.setTitle("ServiceView.Where_you_want_to_deliver".localizedString + ".", for: .normal)
            break
            
        case .DeliverSome:
            print("Deliver Some")
            btnSelectLocation.setTitle("ServiceView.Where_you_want_to_deliver".localizedString + "?", for: .normal)
            break
            
        case .Corsa:
            break
            
        default:
            if APIBasePath.isPikkup {
                btnSelectLocation.setTitle("Choose your pickup" , for: .normal)
            }
            if APIBasePath.isIgot4U
            {
                btnSelectLocation.setTitle("ServiceView.Where_you_want_to_deliver".localizedString + "?", for: .normal)
            }
            print("Defualt")
        }
    }
    

    //MARK:- Actions
    @IBAction func actionBtnSelectLocation(_ sender: UIButton) {
        
        let idCateGory = /request.serviceSelected?.serviceCategoryId
        if idCateGory == 5  {
            Alerts.shared.show(alert: "AppName".localizedString, message: "coming_soon".localizedString , type: .error )
        }else{
            let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
            switch template {
       
            case .Mover?:
                self.delegate?.didSelectNext(type: .SelectingFreightBrand)
                
            default:
                self.delegate?.didSelectNext(type: .SelectLocation)
                
            //arpit
                
            }
            
            minimizeSelectServiceView()
        }
    }
    
    //MARK:- Functions
    /// Configure Service CollectionView
    func configureServiceCollectionView() {
        
        viewCrousal.delegate = self
        viewCrousal.dataSource = self
        viewCrousal.type = .linear
        viewCrousal.isScrollEnabled = false
        viewCrousal.isPagingEnabled = false
        let index = 0
        viewCrousal.currentItemIndex = index
        

    }
    
    func minimizeSelectServiceView() {
        
        let heightPopUp = UIDevice.current.iPhoneX ? self.heightPopUp + 34 : self.heightPopUp
        
        UIView.animate(withDuration: 0.3, animations: {
            [weak self] in
            self?.frame = CGRect(x: 0 , y: /self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height  + 20, width: BookingPopUpFrames.WidthFull, height: heightPopUp)
            }, completion: { (done) in
        })
    }
    
    func maximizeSelectServiceView() {
        
        
        if services.count == 1 {
            viewCrousal.isHidden = true
        } else {
            viewCrousal.isHidden = false
        }
        btnSelectLocation.isHidden = APIBasePath.isMedc2u ? true: false
        lblSubTitle.setAlignment()
        btnSelectLocation.setAlignment()
        viewCrousal.transform = .init(translationX: -60, y: 0)
        
        let heightPopUp = UIDevice.current.iPhoneX ? self.heightPopUp + 34 : self.heightPopUp
        UIView.animate(withDuration: 0.3, animations: {
            [weak self] in
            self?.frame = CGRect(x: 0, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - (UIDevice.current.iPhoneX ? heightPopUp + 40 : heightPopUp + 0) , width: BookingPopUpFrames.WidthFull, height: heightPopUp)
            }, completion: {
                (done) in
        })
    }
    
    func showSelectServiceView(superView: UIView, moveType: MoveType, requestPara: ServiceRequest, service: [Service],fromPackage:Bool = false) {
        
        labelName.text = "Hi ".localizedString + /UDSingleton.shared.userData?.userDetails?.user?.name
        lblSubTitle.text = "Choose your service"
        request = requestPara
        
        services = service
        
       /* layer.cornerRadius = 13.0
        layer.backgroundColor = UIColor.white.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.16).cgColor
        layer.shadowOpacity = 1
        layer.shadowRadius = 21 */
            
        if !isAdded {
            viewSuper = superView
            self.frame = CGRect(x: 0, y: (superView.frame.origin.y + superView.frame.size.height) , width: BookingPopUpFrames.WidthFull, height: heightPopUp)
            superView.addSubview(self)
            if fromPackage{
                self.createPackageService()
            }else{
                self.createServices()
            }
            configureServiceCollectionView()
            isAdded = true
        }
        
        if firstTime == false {
            firstTime = true
        }
        maximizeSelectServiceView()

    }
    
    func createPackageService(){
        serviceLocal.removeAll()
        var id = 0
        for service in services{
            id = service.serviceCategoryId ?? 0
        }
        switch id {
        case 4:
            if let objS = services.filter({$0.serviceCategoryId == 4}).first{
                  serviceLocal.append(ServiceTypeCab(serviceName: /objS.serviceName , serviceImageSelected: R.image.freightActive() ?? UIImage(), serviceImageUnSelected: R.image.freightActive() ?? UIImage(), objService: objS))
            }
            
            case 7:
                if let objS = services.filter({$0.serviceCategoryId == 7}).first {
                                     serviceLocal.append(ServiceTypeCab(serviceName: /objS.serviceName , serviceImageSelected: R.image.ic_car_1() ?? UIImage(), serviceImageUnSelected: R.image.ic_car_0() ?? UIImage(), objService: objS))
            }
            
            case 10:
                if let objS = services.filter({$0.serviceCategoryId == 10}).first {
                                     serviceLocal.append(ServiceTypeCab(serviceName: /objS.serviceName , serviceImageSelected: R.image.ambulanceOutline() ?? UIImage(), serviceImageUnSelected: R.image.ambulanceNormal() ?? UIImage(), objService: objS))
            }
          
        default:
            break
        }
    }
    
    
    func createServices() {
        
        serviceLocal.removeAll()
        
        
        for index in 1...services.count {
          
            print("Service Name 33",/services[index - 1].serviceName)
            
            switch /services[index - 1].serviceName {
            case "Ambulance":
                 serviceLocal.append(ServiceTypeCab(serviceName: /services[index - 1].serviceName , serviceImageSelected: R.image.ambulanceOutline() ?? UIImage(), serviceImageUnSelected: R.image.ambulanceNormal() ?? UIImage(), objService:  services[index - 1]))
                break
                
            case "Pickup Delivery":
                 serviceLocal.append(ServiceTypeCab(serviceName: /services[index - 1].serviceName , serviceImageSelected: R.image.towTruckActive() ?? UIImage(), serviceImageUnSelected: R.image.towTruck() ?? UIImage(), objService:  services[index - 1]))
                break
                
            case "Cab Booking":
                 serviceLocal.append(ServiceTypeCab(serviceName: /services[index - 1].serviceName , serviceImageSelected: R.image.ic_car_1() ?? UIImage(), serviceImageUnSelected: R.image.ic_car_0() ?? UIImage(), objService:  services[index - 1]))
                break
                
            default:
                print("Defual")
                 serviceLocal.append(ServiceTypeCab(serviceName: /services[index - 1].serviceName , serviceImageSelected: R.image.ic_car_1() ?? UIImage(), serviceImageUnSelected: R.image.ic_car_0() ?? UIImage(), objService:  services[index - 1]))
            }
        }
    }
    
    
    func getServiceIndex(id:Int)->Int?{
        
        if let index = self.serviceLocal.firstIndex(where: {/$0.objService.serviceCategoryId == id}){
            viewCrousal.scrollToItem(at: index, animated: true)
            return index
        }
        return nil
    }
    
    //MARK:- iCrousal  Delegate & Data Source
    func numberOfItems(in carousel: iCarousel) -> Int {
        return self.serviceLocal.count//*10000
    }
    

    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
        
            var containerView : UIView
            var itemView = UIImageView()
            itemView.contentMode = .center
            
            var lblTitle = UILabel()
            lblTitle.adjustsFontSizeToFitWidth = true
            lblTitle.minimumScaleFactor = 0.5
            lblTitle.textAlignment = .center
            
            let indexnew = index % self.serviceLocal.count
            let localObj = self.serviceLocal[indexnew]
        itemView.setViewBorderColorSecondary()
            itemView.setCornerRadius(radius: 35)
            if let view = view as? UIView {
                containerView = view
                
                for subView in view.subviews{
                    
                    if subView.isKind(of: UIImageView.self){
                        
                        itemView = (subView as! UIImageView)
                        
                    }
                    else if subView.isKind(of: UIImageView.self){
                        
                        lblTitle = (subView as! UILabel)
                    }
                }
                
                
            } else {
                if   UIDevice.current.userInterfaceIdiom == .pad {
                    containerView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 130))
                    itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
                    lblTitle = UILabel(frame: CGRect(x: 0, y: 105, width: 100, height: 20))
                    containerView.addSubview(itemView)
                           containerView.addSubview(lblTitle)
                } else {
                    containerView = UIView(frame: CGRect(x: 0, y: 0, width: 120, height: 100))
                     itemView = UIImageView(frame: CGRect(x: 25, y: 0, width: 70, height: 70))
                     lblTitle = UILabel(frame: CGRect(x: 0, y: 75, width: 120, height: 25))
                        lblTitle.adjustsFontSizeToFitWidth = true
                        lblTitle.minimumScaleFactor = 1
                           lblTitle.textAlignment = .center
                    lblTitle.font = .systemFont(ofSize: 13, weight: .semibold)
                    containerView.addSubview(itemView)
                           containerView.addSubview(lblTitle)
                }
            }
        itemView.setCornerRadius(radius: 35)
            itemView.sd_setImage(with: URL(string : APIBasePath.categoryImageBasePath + /localObj.objService.image),placeholderImage: UIImage(), options: .refreshCached, progress: nil, completed: nil)
            
            lblTitle.text = localObj.serviceName
            lblTitle.textColor = UIColor.black
            print("Label3 :\(localObj.serviceName)")
            
            return containerView
    }


    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return 1.2
        }
        return value
    }

    func carousel(_ carousel: iCarousel, shouldSelectItemAt index: Int) -> Bool {
        let localObj = self.serviceLocal[index]
        request.serviceSelected = localObj.objService
        delegate?.didSelectServiceType(localObj.objService)
        self.delegate?.didSelectNext(type: .SelectLocation)
        minimizeSelectServiceView()
        return false
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
   


}

