//
//  OrderPricing.swift
//  Buraq24
//
//  Created by MANINDER on 27/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import GoogleMaps

public var orderprice = ""

class OrderPricing: UIView {
    
    //MARK:- Outlets
    //MARK:-
    @IBOutlet weak var poolSwitch: UISwitch!
    @IBOutlet var lblBrandName: UILabel!
    @IBOutlet var lblOrderDetails: UILabel!
    @IBOutlet var lblFinalPrice: UILabel!
    
    @IBOutlet weak var txtInfoTotal: UILabel!
    @IBOutlet var btnBookService: UIButton!
    
    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet var imgViewPaymentType: UIImageView!
    @IBOutlet var imgViewPromoCodeType: UIImageView!

    @IBOutlet weak var lblCreatedHeading: UILabel!
    @IBOutlet weak var paymentSelectionStack: UIStackView!
    @IBOutlet weak var buttonApplyPromo: UIButton!
    @IBOutlet var lblPaymentType: UILabel!
    @IBOutlet var lblPromoCodeType: UILabel!
    
    @IBOutlet weak var labelSeatCapacity: UILabel!
    
    @IBOutlet weak var textFieldPromo: UITextField!
    @IBOutlet weak var imageViewVehicle: UIImageView!
    @IBOutlet weak var labelCreditPoints: UILabel!
    
    @IBOutlet weak var buttonCreditCard: UIButton!
    @IBOutlet weak var creditPointView: UIStackView!
    @IBOutlet weak var paymentStack:UIStackView!
    @IBOutlet weak var buttonCash: UIButton!
    @IBOutlet weak var switchCreditPoints: UISwitch!
    
    @IBOutlet weak var goMovePricingView: UIView!
    @IBOutlet weak var heightCreditConstraint: NSLayoutConstraint!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var genderStack: UIStackView!
    @IBOutlet weak var carpoolStack: UIStackView!
    
    @IBOutlet weak var lblNewPrice: UILabel!
    @IBOutlet weak var lblNewCurrency: UILabel!

    @IBOutlet weak var sepView: UIView!
    
    
    @IBOutlet weak var lblTotalAmountTitle: UILabel!
    @IBOutlet weak var btnDecrease: UIButton!
    @IBOutlet weak var btnIncrease: UIButton!
    @IBOutlet weak var lblSeatCOunt: UILabel!
    @IBOutlet weak var lblTotalPassengerAmount: UILabel!
    @IBOutlet weak var lblTotalPassenger: UILabel!
    @IBOutlet weak var viewPoolAmount: UIView!
    @IBOutlet weak var viewPoolSwitch: UIStackView!
    @IBOutlet weak var lblAirportCharge: UILabel!
    
    
    @IBOutlet weak var stckVwPikkupPayment: UIStackView!
    
    @IBOutlet weak var btnCredimax: UIButton!
    @IBOutlet weak var btnBenefitPay: UIButton!
    
    @IBOutlet weak var btnCash: UIButton!
    @IBOutlet weak var btnCorporate: UIButton!
    
    
    //MARK:- Properties
    //MARK:-
    var finalPriceStr = "0.0"
    var viewSuper : UIView?
    var isAdded : Bool = false
    var request : ServiceRequest = ServiceRequest()
    var modalPackages: TravelPackages?
    var frameHeight : CGFloat = 350
    var delegate : BookRequestDelegate?
    var CalculatedPriceBeforePromo: Float?
    var selectedPackageProduct: ProductCab?
    var isAppliedPromo: Bool = false
    var promoAmount = 0.0
    var couponID: Int?
    var couponCode:String?
    var gender = "Male"
    var minimumAmount = 0
    var walletAmount = 0
    var zoneArray = [ZoneModel]()
    var pickupZone:ZoneModel? = nil
    var dropoffZone:ZoneModel? = nil
    var airportCharge = 0.0
    var creditPointStr = "0.0"
    var totalCost:Float = 0.0
    var initialPrice:Float = 0
    var bookingCharges:Float = 0
    
    var card_type:Int?
    
    var isCashPaymentEnabled:Bool{
        get{
            return  UDSingleton.shared.appSettings?.appSettings?.is_cash_on_Delivery == "true" && UDSingleton.shared.appSettings?.appSettings?.is_cash_payment_enabled == "true"
        }
    }
    
    var selectedPoolSeat = 1{
        
        didSet{
            
            calculatePoolPrice()
        }
    }
    
    func findDateDiff(time1Str: String, time2Str: String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "hh:mm a"

        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }

        //You can directly use from here if you have two dates

        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let second = (interval.truncatingRemainder(dividingBy: 3600) / 60) / 60
        let intervalInt = Int(interval)
        
        return "\(Int(minute))"
    }
    
    //MARK:- Actions
    //MARK:-
    
    @IBAction func actionBtnBookPressed(_ sender: UIButton) {
        
        if request.requestType == .Future
        {
            
            let todayDate = Date().toLocalDateAcTOLocale()
            let requetedDate = request.orderDateTime.toLocalDateAcTOLocale()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEE,dd MMM"
           
            let firstDate = dateFormatter.date(from:requetedDate)
            let secondDate = dateFormatter.date(from:todayDate)
            
            
            if firstDate?.compare(secondDate!) == .orderedSame || firstDate?.compare(secondDate!) == .orderedAscending{
                print("Both dates are same ")
                print("first is less than scecond")
                
                let todayTime = Date().addHours(hoursToAdd: 1).toLocalTimeAcTOLocale()
                let requetedTime = request.orderDateTime.toLocalTimeAcTOLocale()
                
                switch requetedTime.compare(todayTime)
                {
                    case .orderedDescending  , .orderedSame    :   print("Date A is earlier than date B")
                        
                        orderprice = self.lblFinalPrice.text ?? ""
                        if /self.request.isPool{
                            self.request.numberOfRiders = "\(self.selectedPoolSeat)"
                        }
                        
                        
                        
                        self.request.finalPrice = self.finalPriceStr
                        self.request.distance_price_fixed = String(/self.selectedPackageProduct?.distance_price_fixed)
                        self.request.price_per_min = String(/self.selectedPackageProduct?.price_per_min)
                        self.request.time_fixed_price = String(/self.selectedPackageProduct?.time_fixed_price)
                        self.request.price_per_km = String(/self.selectedPackageProduct?.price_per_km)
                        self.request.coupon_id = self.couponID
                        self.request.promoAmount = self.promoAmount
                        self.request.isPromo = self.isAppliedPromo
                        self.request.coupon_code = self.couponCode
                        self.request.gender = self.gender
                        self.request.airport_charges =  String(/self.airportCharge)
                        self.request.card_type = self.card_type
                        self.request.selectedProduct = self.request.selectedBrand?.products?.first
                        if serId == 15 && self.request.paymentMode == .Cash{
                            Alerts.shared.show(alert: "AppName".localizedString, message: "Please select payment type.".localizedString , type: .error )
                        }else{
                            self.delegate?.didClickConfirmBooking(request: self.request)
                        }
                        
                  
                        
                case .orderedAscending  :
                        
                        let dateDiff = findDateDiff(time1Str:todayTime, time2Str:requetedTime)
                        
                        print(dateDiff)
                        
                        var  dateDiff1 = Int(dateDiff)
                        if (dateDiff1 ?? 0) < 0 {
                            dateDiff1 = -(dateDiff1 ?? 0)
                        }
                        
                        
                        self.request.orderDateTime = Date().addHours(hoursToAdd: 1)
                        
                        let strOrderType = "schedule".localizedString
                        let strDate = strOrderType + "\n" + self.request.orderDateTime.toLocalDateAcTOLocale() + ", " + self.request.orderDateTime.toLocalTimeAcTOLocale()
                        self.btnBookService.setTitle( strDate, for: .normal)
                       
                        //"Minimum scheduling time should be 1 hours, kindly confirm again."
                        
                        let alert = UIAlertController(title: "Alert", message: self.btnBookService.title(for: .normal), preferredStyle: .alert)
                                // Create the actions
                        let okAction = UIAlertAction(title: "Ok", style:
                            UIAlertAction.Style.default) {
                               UIAlertAction in
                               print("Yes Pressed")
                            
                           
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                orderprice = self.lblFinalPrice.text ?? ""
                                if /self.request.isPool{
                                    self.request.numberOfRiders = "\(self.selectedPoolSeat)"
                                }
                                
                                
                                
                                self.request.finalPrice = self.finalPriceStr
                                self.request.distance_price_fixed = String(/self.selectedPackageProduct?.distance_price_fixed)
                                self.request.price_per_min = String(/self.selectedPackageProduct?.price_per_min)
                                self.request.time_fixed_price = String(/self.selectedPackageProduct?.time_fixed_price)
                                self.request.price_per_km = String(/self.selectedPackageProduct?.price_per_km)
                                self.request.coupon_id = self.couponID
                                self.request.promoAmount = self.promoAmount
                                self.request.isPromo = self.isAppliedPromo
                                self.request.coupon_code = self.couponCode
                                self.request.gender = self.gender
                                self.request.airport_charges =  String(/self.airportCharge)
                                self.request.card_type = self.card_type
                                self.request.selectedProduct = self.request.selectedBrand?.products?.first
                                if serId == 15 && self.request.paymentMode == .Cash{
                                    Alerts.shared.show(alert: "AppName".localizedString, message: "Please select payment type.".localizedString , type: .error )
                                }else{
                                    self.delegate?.didClickConfirmBooking(request: self.request)
                                }
                            }
                            
                            
                        }
                        
                        alert.addAction(okAction)
                    
                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)

                       // window.present(alert, animated: true, completion: nil)
                        
 //                        if (dateDiff1 ?? 0)*60 < 30 {
                            //Alerts.shared.show(alert: "Alert".localizedString, message: "Diff is less than 1/2 min".localizedString , type: .error )
 //                        }
 //                        else
 //                        {
 //                            Alerts.shared.show(alert: "Alert".localizedString, message: "Schedule time must be alteast 1 hour.".localizedString , type: .error )
 //                            return
 //                        }
                        
                      
                }
                       
            }
            
            else
            {
                //second date is bigger than first
                         // strDateValidate = "no"
                
                orderprice = self.lblFinalPrice.text ?? ""
                if /self.request.isPool{
                    self.request.numberOfRiders = "\(self.selectedPoolSeat)"
                }
                
                
                
                self.request.finalPrice = self.finalPriceStr
                self.request.distance_price_fixed = String(/self.selectedPackageProduct?.distance_price_fixed)
                self.request.price_per_min = String(/self.selectedPackageProduct?.price_per_min)
                self.request.time_fixed_price = String(/self.selectedPackageProduct?.time_fixed_price)
                self.request.price_per_km = String(/self.selectedPackageProduct?.price_per_km)
                self.request.coupon_id = self.couponID
                self.request.promoAmount = self.promoAmount
                self.request.isPromo = self.isAppliedPromo
                self.request.coupon_code = self.couponCode
                self.request.gender = self.gender
                self.request.airport_charges =  String(/self.airportCharge)
                self.request.card_type = self.card_type
                self.request.selectedProduct = self.request.selectedBrand?.products?.first
                if serId == 15 && self.request.paymentMode == .Cash{
                    Alerts.shared.show(alert: "AppName".localizedString, message: "Please select payment type.".localizedString , type: .error )
                }else{
                    self.delegate?.didClickConfirmBooking(request: self.request)
                }
            }
            
       
//
//            let todayTime = Date().addHours(hoursToAdd: 1).toLocalTimeAcTOLocale()
//            let requetedTime = request.orderDateTime.toLocalTimeAcTOLocale()
//
//            switch request.orderDateTime.compare(Date())
//            {
//            case .orderedAscending :
//
//                //req date is less than current date
//
//               switch todayTime.compare(requetedTime)
//               {
//                   case .orderedAscending     :   print("Date A is earlier than date B")
//
//                       orderprice = self.lblFinalPrice.text ?? ""
//                       if /self.request.isPool{
//                           self.request.numberOfRiders = "\(self.selectedPoolSeat)"
//                       }
//
//
//
//                       self.request.finalPrice = self.finalPriceStr
//                       self.request.distance_price_fixed = String(/self.selectedPackageProduct?.distance_price_fixed)
//                       self.request.price_per_min = String(/self.selectedPackageProduct?.price_per_min)
//                       self.request.time_fixed_price = String(/self.selectedPackageProduct?.time_fixed_price)
//                       self.request.price_per_km = String(/self.selectedPackageProduct?.price_per_km)
//                       self.request.coupon_id = self.couponID
//                       self.request.promoAmount = self.promoAmount
//                       self.request.isPromo = self.isAppliedPromo
//                       self.request.coupon_code = self.couponCode
//                       self.request.gender = self.gender
//                       self.request.airport_charges =  String(/self.airportCharge)
//                       self.request.card_type = self.card_type
//                       self.request.selectedProduct = self.request.selectedBrand?.products?.first
//                       if serId == 15 && self.request.paymentMode == .Cash{
//                           Alerts.shared.show(alert: "AppName".localizedString, message: "Please select payment type.".localizedString , type: .error )
//                       }else{
//                           self.delegate?.didClickConfirmBooking(request: self.request)
//                       }
//
//                   case .orderedSame          :   print("The two dates are the same")
//
//                       orderprice = self.lblFinalPrice.text ?? ""
//                       if /self.request.isPool{
//                           self.request.numberOfRiders = "\(self.selectedPoolSeat)"
//                       }
//
//
//
//                       self.request.finalPrice = self.finalPriceStr
//                       self.request.distance_price_fixed = String(/self.selectedPackageProduct?.distance_price_fixed)
//                       self.request.price_per_min = String(/self.selectedPackageProduct?.price_per_min)
//                       self.request.time_fixed_price = String(/self.selectedPackageProduct?.time_fixed_price)
//                       self.request.price_per_km = String(/self.selectedPackageProduct?.price_per_km)
//                       self.request.coupon_id = self.couponID
//                       self.request.promoAmount = self.promoAmount
//                       self.request.isPromo = self.isAppliedPromo
//                       self.request.coupon_code = self.couponCode
//                       self.request.gender = self.gender
//                       self.request.airport_charges =  String(/self.airportCharge)
//                       self.request.card_type = self.card_type
//                       self.request.selectedProduct = self.request.selectedBrand?.products?.first
//                       if serId == 15 && self.request.paymentMode == .Cash{
//                           Alerts.shared.show(alert: "AppName".localizedString, message: "Please select payment type.".localizedString , type: .error )
//                       }else{
//                           self.delegate?.didClickConfirmBooking(request: self.request)
//                       }
//
//                   case .orderedDescending    :   print("Date A is later than date B")
//
//                       let dateDiff = findDateDiff(time1Str:todayTime, time2Str:requetedTime)
//
//                       print(dateDiff)
//
//                       var  dateDiff1 = Int(dateDiff)
//                       if (dateDiff1 ?? 0) < 0 {
//                           dateDiff1 = -(dateDiff1 ?? 0)
//                       }
//
//
//                       self.request.orderDateTime = Date().addHours(hoursToAdd: 1)
//
//                       let strOrderType = "schedule".localizedString
//                       let strDate = strOrderType + "\n" + self.request.orderDateTime.toLocalDateAcTOLocale() + ", " + self.request.orderDateTime.toLocalTimeAcTOLocale()
//                       self.btnBookService.setTitle( strDate, for: .normal)
//
//                       //"Minimum scheduling time should be 1 hours, kindly confirm again."
//
//                       let alert = UIAlertController(title: "Alert", message: self.btnBookService.title(for: .normal), preferredStyle: .alert)
//                               // Create the actions
//                       let okAction = UIAlertAction(title: "Ok", style:
//                           UIAlertAction.Style.default) {
//                              UIAlertAction in
//                              print("Yes Pressed")
//
//
//
//                           DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                               orderprice = self.lblFinalPrice.text ?? ""
//                               if /self.request.isPool{
//                                   self.request.numberOfRiders = "\(self.selectedPoolSeat)"
//                               }
//
//
//
//                               self.request.finalPrice = self.finalPriceStr
//                               self.request.distance_price_fixed = String(/self.selectedPackageProduct?.distance_price_fixed)
//                               self.request.price_per_min = String(/self.selectedPackageProduct?.price_per_min)
//                               self.request.time_fixed_price = String(/self.selectedPackageProduct?.time_fixed_price)
//                               self.request.price_per_km = String(/self.selectedPackageProduct?.price_per_km)
//                               self.request.coupon_id = self.couponID
//                               self.request.promoAmount = self.promoAmount
//                               self.request.isPromo = self.isAppliedPromo
//                               self.request.coupon_code = self.couponCode
//                               self.request.gender = self.gender
//                               self.request.airport_charges =  String(/self.airportCharge)
//                               self.request.card_type = self.card_type
//                               self.request.selectedProduct = self.request.selectedBrand?.products?.first
//                               if serId == 15 && self.request.paymentMode == .Cash{
//                                   Alerts.shared.show(alert: "AppName".localizedString, message: "Please select payment type.".localizedString , type: .error )
//                               }else{
//                                   self.delegate?.didClickConfirmBooking(request: self.request)
//                               }
//                           }
//
//
//                       }
//
//                       alert.addAction(okAction)
//
//                       self.window?.rootViewController?.present(alert, animated: true, completion: nil)
//
//                      // window.present(alert, animated: true, completion: nil)
//
////                        if (dateDiff1 ?? 0)*60 < 30 {
//                           //Alerts.shared.show(alert: "Alert".localizedString, message: "Diff is less than 1/2 min".localizedString , type: .error )
////                        }
////                        else
////                        {
////                            Alerts.shared.show(alert: "Alert".localizedString, message: "Schedule time must be alteast 1 hour.".localizedString , type: .error )
////                            return
////                        }
//
//
//               }
//
//            case .orderedSame :
//
//               switch todayTime.compare(requetedTime)
//                {
//                    case .orderedAscending     :   print("Date A is earlier than date B")
//
//                        orderprice = self.lblFinalPrice.text ?? ""
//                        if /self.request.isPool{
//                            self.request.numberOfRiders = "\(self.selectedPoolSeat)"
//                        }
//
//
//
//                        self.request.finalPrice = self.finalPriceStr
//                        self.request.distance_price_fixed = String(/self.selectedPackageProduct?.distance_price_fixed)
//                        self.request.price_per_min = String(/self.selectedPackageProduct?.price_per_min)
//                        self.request.time_fixed_price = String(/self.selectedPackageProduct?.time_fixed_price)
//                        self.request.price_per_km = String(/self.selectedPackageProduct?.price_per_km)
//                        self.request.coupon_id = self.couponID
//                        self.request.promoAmount = self.promoAmount
//                        self.request.isPromo = self.isAppliedPromo
//                        self.request.coupon_code = self.couponCode
//                        self.request.gender = self.gender
//                        self.request.airport_charges =  String(/self.airportCharge)
//                        self.request.card_type = self.card_type
//                        self.request.selectedProduct = self.request.selectedBrand?.products?.first
//                        if serId == 15 && self.request.paymentMode == .Cash{
//                            Alerts.shared.show(alert: "AppName".localizedString, message: "Please select payment type.".localizedString , type: .error )
//                        }else{
//                            self.delegate?.didClickConfirmBooking(request: self.request)
//                        }
//
//                    case .orderedSame          :   print("The two dates are the same")
//
//                        orderprice = self.lblFinalPrice.text ?? ""
//                        if /self.request.isPool{
//                            self.request.numberOfRiders = "\(self.selectedPoolSeat)"
//                        }
//
//
//
//                        self.request.finalPrice = self.finalPriceStr
//                        self.request.distance_price_fixed = String(/self.selectedPackageProduct?.distance_price_fixed)
//                        self.request.price_per_min = String(/self.selectedPackageProduct?.price_per_min)
//                        self.request.time_fixed_price = String(/self.selectedPackageProduct?.time_fixed_price)
//                        self.request.price_per_km = String(/self.selectedPackageProduct?.price_per_km)
//                        self.request.coupon_id = self.couponID
//                        self.request.promoAmount = self.promoAmount
//                        self.request.isPromo = self.isAppliedPromo
//                        self.request.coupon_code = self.couponCode
//                        self.request.gender = self.gender
//                        self.request.airport_charges =  String(/self.airportCharge)
//                        self.request.card_type = self.card_type
//                        self.request.selectedProduct = self.request.selectedBrand?.products?.first
//                        if serId == 15 && self.request.paymentMode == .Cash{
//                            Alerts.shared.show(alert: "AppName".localizedString, message: "Please select payment type.".localizedString , type: .error )
//                        }else{
//                            self.delegate?.didClickConfirmBooking(request: self.request)
//                        }
//
//                    case .orderedDescending    :   print("Date A is later than date B")
//
//                        let dateDiff = findDateDiff(time1Str:todayTime, time2Str:requetedTime)
//
//                        print(dateDiff)
//
//                        var  dateDiff1 = Int(dateDiff)
//                        if (dateDiff1 ?? 0) < 0 {
//                            dateDiff1 = -(dateDiff1 ?? 0)
//                        }
//
//
//                        self.request.orderDateTime = Date().addHours(hoursToAdd: 1)
//
//                        let strOrderType = "schedule".localizedString
//                        let strDate = strOrderType + "\n" + self.request.orderDateTime.toLocalDateAcTOLocale() + ", " + self.request.orderDateTime.toLocalTimeAcTOLocale()
//                        self.btnBookService.setTitle( strDate, for: .normal)
//
//                        //"Minimum scheduling time should be 1 hours, kindly confirm again."
//
//                        let alert = UIAlertController(title: "Alert", message: self.btnBookService.title(for: .normal), preferredStyle: .alert)
//                                // Create the actions
//                        let okAction = UIAlertAction(title: "Ok", style:
//                            UIAlertAction.Style.default) {
//                               UIAlertAction in
//                               print("Yes Pressed")
//
//
//
//                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                                orderprice = self.lblFinalPrice.text ?? ""
//                                if /self.request.isPool{
//                                    self.request.numberOfRiders = "\(self.selectedPoolSeat)"
//                                }
//
//
//
//                                self.request.finalPrice = self.finalPriceStr
//                                self.request.distance_price_fixed = String(/self.selectedPackageProduct?.distance_price_fixed)
//                                self.request.price_per_min = String(/self.selectedPackageProduct?.price_per_min)
//                                self.request.time_fixed_price = String(/self.selectedPackageProduct?.time_fixed_price)
//                                self.request.price_per_km = String(/self.selectedPackageProduct?.price_per_km)
//                                self.request.coupon_id = self.couponID
//                                self.request.promoAmount = self.promoAmount
//                                self.request.isPromo = self.isAppliedPromo
//                                self.request.coupon_code = self.couponCode
//                                self.request.gender = self.gender
//                                self.request.airport_charges =  String(/self.airportCharge)
//                                self.request.card_type = self.card_type
//                                self.request.selectedProduct = self.request.selectedBrand?.products?.first
//                                if serId == 15 && self.request.paymentMode == .Cash{
//                                    Alerts.shared.show(alert: "AppName".localizedString, message: "Please select payment type.".localizedString , type: .error )
//                                }else{
//                                    self.delegate?.didClickConfirmBooking(request: self.request)
//                                }
//                            }
//
//
//                        }
//
//                        alert.addAction(okAction)
//
//                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
//
//                       // window.present(alert, animated: true, completion: nil)
//
////                        if (dateDiff1 ?? 0)*60 < 30 {
//                            //Alerts.shared.show(alert: "Alert".localizedString, message: "Diff is less than 1/2 min".localizedString , type: .error )
////                        }
////                        else
////                        {
////                            Alerts.shared.show(alert: "Alert".localizedString, message: "Schedule time must be alteast 1 hour.".localizedString , type: .error )
////                            return
////                        }
//
//
//                }
//
//            case .orderedDescending :
//
//                orderprice = self.lblFinalPrice.text ?? ""
//                if /self.request.isPool{
//                    self.request.numberOfRiders = "\(self.selectedPoolSeat)"
//                }
//
//
//
//                self.request.finalPrice = self.finalPriceStr
//                self.request.distance_price_fixed = String(/self.selectedPackageProduct?.distance_price_fixed)
//                self.request.price_per_min = String(/self.selectedPackageProduct?.price_per_min)
//                self.request.time_fixed_price = String(/self.selectedPackageProduct?.time_fixed_price)
//                self.request.price_per_km = String(/self.selectedPackageProduct?.price_per_km)
//                self.request.coupon_id = self.couponID
//                self.request.promoAmount = self.promoAmount
//                self.request.isPromo = self.isAppliedPromo
//                self.request.coupon_code = self.couponCode
//                self.request.gender = self.gender
//                self.request.airport_charges =  String(/self.airportCharge)
//                self.request.card_type = self.card_type
//                self.request.selectedProduct = self.request.selectedBrand?.products?.first
//                if serId == 15 && self.request.paymentMode == .Cash{
//                    Alerts.shared.show(alert: "AppName".localizedString, message: "Please select payment type.".localizedString , type: .error )
//                }else{
//                    self.delegate?.didClickConfirmBooking(request: self.request)
//                }
//            }
            

        }
        else
        {
            orderprice = lblFinalPrice.text ?? ""
            if /request.isPool{
                request.numberOfRiders = "\(selectedPoolSeat)"
            }
            
            
            
            request.finalPrice = finalPriceStr
            request.distance_price_fixed = String(/selectedPackageProduct?.distance_price_fixed)
            request.price_per_min = String(/selectedPackageProduct?.price_per_min)
            request.time_fixed_price = String(/selectedPackageProduct?.time_fixed_price)
            request.price_per_km = String(/selectedPackageProduct?.price_per_km)
            request.coupon_id = couponID
            request.promoAmount = promoAmount
            request.isPromo = isAppliedPromo
            request.coupon_code = couponCode
            request.gender = gender
            request.airport_charges =  String(/airportCharge)
            request.card_type = card_type
            request.selectedProduct = request.selectedBrand?.products?.first
            if serId == 15 && request.paymentMode == .Cash{
                Alerts.shared.show(alert: "AppName".localizedString, message: "Please select payment type.".localizedString , type: .error )
            }else{
                delegate?.didClickConfirmBooking(request: request)
            }
        }
    
       
    }
    
    @IBAction func switchCarPoolAction(_ sender: UISwitch) {
        
        request.booking_type = sender.isOn ? "CarPool" : ""
        request.isPool = sender.isOn
        viewPoolAmount.isHidden = !sender.isOn
        
        if !sender.isOn{
             selectedPoolSeat = 1
            
        }
        changeHeight()
        sender.isOn ? calculatePriceForPoolBooking() : calculatePriceForNormalBooking()
    }
    
    @IBAction func btnDecreaseAction(_ sender: Any) {
        
        if selectedPoolSeat > 1{
            
            selectedPoolSeat -= 1
        }
        
        calculatePriceForPoolBooking()
        
        
    }
    
 
    
    @IBAction func btnIncreaseAction(_ sender: Any) {
        
        if selectedPoolSeat < (/request.selectedProduct?.seating_capacity){
              
              selectedPoolSeat += 1
          }
        
         calculatePriceForPoolBooking()
    }
    
    
     func calculatePoolPrice(){
         
         lblSeatCOunt.text = "\(selectedPoolSeat)"
     }
    
    @IBAction func actionBtnPaymentType(_ sender: UIButton) {
        
        // 1- Credit card, 2- Cash
        
        
        switch sender.tag {
            
        case 1,4:
            
            guard let _ = UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id else {
               Alerts.shared.show(alert: "AppName".localizedString, message: "Payment, gateway unique id is empty." , type: .error)
               return
            }
            
            
            
            let paymentGateway = PaymentGateway(rawValue:(/UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id))
                        
            switch paymentGateway {
            case .paystack,.payku,.braintree,.wipay, .paymaya,.qpaypro , .benefit,.credimax:
                print("Not navigate to card list..")
                
                [btnCorporate,btnBenefitPay,btnCredimax,btnCash].forEach { btn in
                    btn?.layer.borderColor = UIColor.lightGray.cgColor
                    btn?.tintColor =  UIColor.lightGray
                    btn?.backgroundColor = .white
                    btn?.setTitleColor(.lightGray, for: .normal)
                }
                card_type = 2
                btnBenefitPay.setSelectedButton()
                request.paymentMode = .Card
                break
                
            default:
                guard let vc = R.storyboard.sideMenu.cardListViewController() else {return}
                vc.delegate = ez.topMostVC as? HomeVC
                (ez.topMostVC as? HomeVC)?.pushVC(vc)
            }
            
        case 2,5:
            
            [btnCorporate,btnBenefitPay,btnCredimax,btnCash].forEach { btn in
                btn?.layer.borderColor = UIColor.lightGray.cgColor
                btn?.tintColor =  UIColor.lightGray
                btn?.backgroundColor = .white
                btn?.setTitleColor(.lightGray, for: .normal)
            }
            card_type = nil
            if sender.tag == 2{
                btnCash.setSelectedButton()
                delegate?.didPaymentModeChanged(paymentMode: .Cash, selectedCard: nil)
                request.paymentMode = .Cash
            }else{
                btnCorporate.setSelectedButton()
                delegate?.didPaymentModeChanged(paymentMode: .cooporate, selectedCard: nil)
                request.paymentMode = .cooporate
            }
            
        case 3:
            [btnCorporate,btnBenefitPay,btnCredimax,btnCash].forEach { btn in
                btn?.layer.borderColor = UIColor.lightGray.cgColor
                btn?.tintColor =  UIColor.lightGray
                btn?.backgroundColor = .white
                btn?.setTitleColor(.lightGray, for: .normal)
            }
            btnCredimax.setSelectedButton()
            request.paymentMode = .Card
            card_type = 1
            guard let vc = R.storyboard.sideMenu.cardListViewController() else {return}
            vc.delegate = ez.topMostVC as? HomeVC
            (ez.topMostVC as? HomeVC)?.pushVC(vc)
            
        default:
            break
        }
        
        changeHeight()
        
    }
    
    @IBAction func actionBtnPromoCodePressed(_ sender: UIButton) {

        if isAppliedPromo {
            textFieldPromo.text = nil
            couponID = nil
            couponCode = nil
            promoAmount = 0.0
            buttonApplyPromo.setTitle("APPLY PROMO CODE".localizedString, for: .normal)
            buttonApplyPromo.setTitleColor(R.color.appPurple(), for: .normal)
            
            isAppliedPromo = false
//            lblFinalPrice.text =  String(/CalculatedPriceBeforePromo).getTwoDecimalFloat()
            let currency = (/UDSingleton.shared.appSettings?.appSettings?.currency)
            
            lblFinalPrice.text =  "Initial Fare - \(String(initialPrice).getTwoDecimalFloat())\nTotal - \(String(totalCost).getTwoDecimalFloat()) \(currency)"
            let text = "Included charges \(currency)"
            lblCurrency.text = text
//            lblNewCurrency.text = text
            finalPriceStr = String(/CalculatedPriceBeforePromo).getTwoDecimalFloat()
            lblNewPrice.text =  String(/CalculatedPriceBeforePromo).getTwoDecimalFloat()
            
            let minPrice = /CalculatedPriceBeforePromo - (/CalculatedPriceBeforePromo * 10.0/100.0)
            let maxPrice = /CalculatedPriceBeforePromo + (/CalculatedPriceBeforePromo * 10.0/100.0)
            
            lblFinalPrice.text = "Total - " + String(minPrice).getTwoDecimalFloat() + " - " + String(maxPrice).getTwoDecimalFloat()
            
        if APIBasePath.isPikkup {
            labelSeatCapacity.text = "Total - " + String(minPrice).getTwoDecimalFloat() + " - " + String(maxPrice).getTwoDecimalFloat() + " \(currency)"
            lblFinalPrice.text = ""
            lblCurrency.text = ""
        }
        } else {
            guard let vc = R.storyboard.bookService.promoCodeViewController() else {return}
            vc.modalPresentationStyle = .overFullScreen
            vc.currentrequest = request
            vc.delegate = (ez.topMostVC as? HomeVC)
            (ez.topMostVC as? HomeVC)?.presentVC(vc)
        }
        
       
        
      //  self.delegate?.didSelectNext(type: .Payment)

       // Alerts.shared.show(alert: "AppName".localizedString, message: "coming_soon".localizedString , type: .error )
    }
    
    @IBAction func buttonInfoClicked(_ sender: Any) {
        
        guard let vc = R.storyboard.bookService.fareBreakdownViewController() else {return}
        vc.product = request.selectedProduct
        (ez.topMostVC as? HomeVC)?.presentVC(vc)
        
    }
    
    @IBAction func switchClicked(_ sender: UISwitch) {
        
        if sender.isOn {
           
            if Bool(/UDSingleton.shared.appSettings?.appSettings?.is_wallet) ?? false {
                
                if walletAmount <= 0{
                    
                    alertBoxOk(message: "Please recharge your wallet first.".localizedString, title: "AppName".localizedString, ok: {})
                    sender.isOn = false
                    
                }
                
                else if walletAmount < minimumAmount && Float(walletAmount) <= (/CalculatedPriceBeforePromo - Float(promoAmount)) {
                    alertBoxOk(message: "Your balance is less than minimum required amount, you may not be able to book ride".localizedString, title: "AppName".localizedString, ok: {})
                    sender.isOn = false
                } else {
                    request.paymentMode = .Wallet
                    self.paymentStack.isHidden = true
                    //self.frame.size.height = frameHeight - 54
                    self.sepView.isHidden = true
      
                }
               
                
            } else {
                if request.paymentMode == .Cash {
                       alertBoxOk(message: "can't_use_credit_point".localizedString, title: "AppName".localizedString, ok: {})
                       switchCreditPoints.setOn(false, animated: true)
                       return
                   }
                   
                   
                   if sender.isOn {
                       request.credit_point_used = creditPointStr //labelCreditPoints.text
                   } else {
                       request.credit_point_used = nil
                   }
            }
            
        } else {
            self.paymentStack.isHidden = false
            //self.frame.size.height = frameHeight
             self.sepView.isHidden = true
            
            request.paymentMode = .Cash
            
        }
        
        changeHeight()
                
    }
    
    
    @IBAction func gnderSelectionSementType(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
           {
           case 0:
               gender = "Male"
           case 1:
               gender = "Female"
           default:
               break
           }
        
    }
    
    
    //MARK:- Functions
    
    func minimizeOrderPricingView() {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: /self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            
            }, completion: { (done) in
        })
    }
    
    func maximizeOrderPricingView() {
        
       
        card_type = nil
        let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
        //lblCurrency.text = (/UDSingleton.shared.appSettings?.appSettings?.currency)
        let isPool = /request.selectedProduct?.pool == "true" ? true : false
        viewPoolSwitch.isHidden = !isPool
        switch template {
        case .DeliverSome:
            genderStack.isHidden = true
//            heightCreditConstraint.constant = 0
            creditPointView.isHidden = true
            goMovePricingView.isHidden = true
           // frameHeight =  UIDevice.current.iPhoneX ? 300 + 34  : 300
            frameHeight =  isPool ? (UIDevice.current.iPhoneX ? 420 + 34  : 420) : (UIDevice.current.iPhoneX ? 300 + 34  : 300)
            break
            
        case .GoMove:
             //carpoolStack.isHidden = true
             buttonCreditCard.setButtonWithTitleAndBorderColorSecondary()
             buttonCreditCard.setButtonWithTintColorSecondary()
             buttonCash.isHidden = true
             genderStack.isHidden = true
             goMovePricingView.isHidden = false
            // frameHeight =  UIDevice.current.iPhoneX ? 350 + 34  : 350
             frameHeight =  isPool ? (UIDevice.current.iPhoneX ? 350 + 34  : 350) :  (UIDevice.current.iPhoneX ? 350 + 34  : 350)
             
             break
            
        default:
            genderStack.isHidden = false
            goMovePricingView.isHidden = true
            creditPointView.isHidden = false
           // frameHeight =  UIDevice.current.iPhoneX ? 380 + 34  : 380
            var poolContainerHeight = CGFloat((/request.selectedProduct?.pool == "true" ? true : false) ? 0 : -50)
            
            if UDSingleton.shared.isRidecaddie{
                poolContainerHeight -= 50
                genderStack.isHidden = true
                buttonCash.isHidden = true
            }
            
            let isGenderEnabled = UDSingleton.shared.appSettings?.appSettings?.is_gender_selection_enabled == "true"
            
            if !isGenderEnabled{
                poolContainerHeight -= 50
                genderStack.isHidden = true
            }
            var isCash = false
            let isCashOnDeliveryEnabled = UDSingleton.shared.appSettings?.appSettings?.is_cash_on_Delivery == "true"
            let isCashEnabled = UDSingleton.shared.appSettings?.appSettings?.is_cash_payment_enabled == "true"
            if isCashEnabled && isCashOnDeliveryEnabled{
                isCash = true
                buttonCash.isHidden = false
            }else {
                buttonCash.isHidden = true
            }
            
            let isCardEnabled = UDSingleton.shared.appSettings?.appSettings?.is_card_payment_enabled == "true"
            
            buttonCreditCard.isHidden = !isCardEnabled
            
            if !isCash && !isCardEnabled{
                poolContainerHeight -= 50
            }
            
            poolContainerHeight += 30
            
            if APIBasePath.isDiva {
                buttonCreditCard.isHidden = /UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id != "stripe" ? true:false
                buttonCash.isHidden = false
            }
            let poolHeight = CGFloat(/request.isPool ? 450 : 390) + poolContainerHeight
            frameHeight =  isPool ? (UIDevice.current.iPhoneX ? poolHeight + 34  : poolHeight + 34) : (UIDevice.current.iPhoneX ? poolHeight + 34  : poolHeight + 34)
            
            
            if APIBasePath.isPikkup {
                stckVwPikkupPayment.isHidden = false
                paymentSelectionStack.isHidden = true
            } else{
                stckVwPikkupPayment.isHidden = true
                paymentSelectionStack.isHidden = false
                
            }
            break
        }
        
        
         //lblNewCurrency.text = /UDSingleton.shared.appSettings?.appSettings?.currency
        
        if modalPackages != nil {
            frameHeight = frameHeight - 26 // Height of promo StackView
        }
        
        if /UDSingleton.shared.userData?.userDetails?.user?.vendor_id > 0{
            btnCorporate.isHidden = false
        }else if serId == 15{
            btnCorporate.isHidden = true
            //btnBenefitPay.isHidden = true
            
            btnBenefitPay.isHidden = false
            btnCredimax.isHidden = false
            btnCash.isHidden = true
        }else{
            btnCorporate.isHidden = true
            btnCash.isHidden = !isCashPaymentEnabled
            btnBenefitPay.isHidden = false
            btnCredimax.isHidden = false
            
        }
       
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
           
         // Ankush   self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - CGFloat(BookingPopUpFrames.PaddingX) - /self?.frameHeight , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            
            
            self?.frame = CGRect(x: 0, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - /self?.frameHeight + 10 , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            
            self?.layoutIfNeeded()
            self?.setupUI()
            
            }, completion: { (done) in
                
        })
        
        
        
    }
    
    
    func changeHeight(){
        
        let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
        let isPool = /request.selectedProduct?.pool == "true" ? true : false
        viewPoolSwitch.isHidden = !isPool
        switch template {
        case .DeliverSome:
            genderStack.isHidden = true
//            heightCreditConstraint.constant = 0
            creditPointView.isHidden = true
            goMovePricingView.isHidden = true
           // frameHeight =  UIDevice.current.iPhoneX ? 300 + 34  : 300
            frameHeight =  isPool ? (UIDevice.current.iPhoneX ? 420 + 34  : 420) : (UIDevice.current.iPhoneX ? 300 + 34  : 300)
            break
            
        case .GoMove:
             //carpoolStack.isHidden = true
             buttonCreditCard.setButtonWithTitleAndBorderColorSecondary()
             buttonCreditCard.setButtonWithTintColorSecondary()
             buttonCash.isHidden = true
             genderStack.isHidden = true
             goMovePricingView.isHidden = false
            // frameHeight =  UIDevice.current.iPhoneX ? 350 + 34  : 350
             frameHeight =  isPool ? (UIDevice.current.iPhoneX ? 350 + 34  : 350) :  (UIDevice.current.iPhoneX ? 350 + 34  : 350)
             
             break
            
        default:
            genderStack.isHidden = false
            goMovePricingView.isHidden = true
            creditPointView.isHidden = false
           // frameHeight =  UIDevice.current.iPhoneX ? 380 + 34  : 380
            var poolContainerHeight = CGFloat((/request.selectedProduct?.pool == "true" ? true : false) ? 0 : -50)
            var poolHeight = CGFloat(/request.isPool ? 450 : 390) + poolContainerHeight
            poolHeight += self.paymentStack.isHidden ? -50 : 0
            
            if APIBasePath.isShipUsNow{
                poolHeight -= 50
                genderStack.isHidden = true
            }
            
            let isGenderEnabled = UDSingleton.shared.appSettings?.appSettings?.is_gender_selection_enabled == "true"
            
            if !isGenderEnabled{
                poolContainerHeight -= 50
                genderStack.isHidden = true
            }
            
            var isCash = false
            let isCashOnDeliveryEnabled = UDSingleton.shared.appSettings?.appSettings?.is_cash_on_Delivery == "true"
            let isCashEnabled = UDSingleton.shared.appSettings?.appSettings?.is_cash_payment_enabled == "true"
            if isCashEnabled && isCashOnDeliveryEnabled{
                isCash = true
                buttonCash.isHidden = false
            }else {
                buttonCash.isHidden = true
            }
            
            let isCardEnabled = UDSingleton.shared.appSettings?.appSettings?.is_card_payment_enabled == "1"
            
            buttonCreditCard.isHidden = !isCardEnabled
            
            if !isCash && !isCardEnabled{
                poolContainerHeight -= 50
            }

            poolContainerHeight += 30
            
            if APIBasePath.isIgot4U {
                genderStack.isHidden = true
                buttonCash.isHidden = true
            }
            if APIBasePath.isDiva {
                buttonCreditCard.isHidden = /UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id != "stripe" ? true:false
                buttonCash.isHidden = false
            }
            
            if APIBasePath.isPikkup {
                stckVwPikkupPayment.isHidden = false
                paymentSelectionStack.isHidden = true
            } else{
                stckVwPikkupPayment.isHidden = true
                paymentSelectionStack.isHidden = false
                
            }
            
            frameHeight =  isPool ? (UIDevice.current.iPhoneX ? poolHeight + 34  : poolHeight + 34) : (UIDevice.current.iPhoneX ? poolHeight + 34  : poolHeight + 34)
            break
        }
        
    
        
        if modalPackages != nil {
            frameHeight = frameHeight - 26 // Height of promo StackView
        }
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
           
         // Ankush   self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - CGFloat(BookingPopUpFrames.PaddingX) - /self?.frameHeight , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            
            
            self?.frame = CGRect(x: 0, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - /self?.frameHeight + 10 , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            
            self?.layoutIfNeeded()
       
            
            }, completion: { (done) in
                
        })
    }
    
    func setupUI() {
        
        //        let gatewayType = /UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id
        //
        //       switch gatewayType {
        //       case GatewayType.Paystack.rawValue:
        //        paymentSelectionStack.isHidden = true
        //        break
        //
        //       default:
        //          paymentSelectionStack.isHidden = false
        //       }
        
        
        btnBookService.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        buttonCash.setButtonWithTitleAndBorderColorSecondary()
        buttonCash.setButtonWithTintColorSecondary()
        
        buttonCreditCard.setButtonBorderTitleAndTintColor()
        buttonCash.setButtonBorderTitleAndTintColor()
        
        
        
        //btnCash.setButtonBorderTitleAndTintColor()
        
        //btnCredimax.setButtonBorderTitleAndTintColor()
        //btnBenefitPay.setButtonBorderTitleAndTintColor()
        
        
    
        [btnCorporate,btnBenefitPay,btnCredimax,btnCash].forEach { btn in
            btn?.layer.borderColor = UIColor.lightGray.cgColor
            btn?.tintColor =  UIColor.lightGray
            btn?.backgroundColor = .white
            btn?.setTitleColor(.lightGray, for: .normal)
        }
        
        if /UDSingleton.shared.userData?.userDetails?.user?.vendor_id > 0{
            btnCorporate.setSelectedButton()
        }else{
            btnCash.setSelectedButton()
        }
        // buttonCreditCard.setButtonWithTintColorSecondary()
        
        buttonApplyPromo.setButtonWithTitleColorSecondary()
        switchCreditPoints.setSwitchTintColorSecondary()
        
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentController.setTitleTextAttributes(titleTextAttributes, for: .selected)
        if #available(iOS 13.0, *) {
            segmentController.selectedSegmentTintColor = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.secondary_colour ?? DefaultColor.color.rawValue)
        } else {
            // Fallback on earlier versions
        }
        
        switch request.paymentMode {
        case .Card:
            
            buttonCreditCard.setButtonWithTitleAndBorderColorSecondary()
            buttonCreditCard.setButtonWithTintColorSecondary()
            
            //buttonCreditCard.layer.borderColor = R.color.appNewRedBorder()?.cgColor
            //buttonCreditCard.setImage(R.image.ic_card_active(), for: .normal)
            //buttonCreditCard.setTitleColor(R.color.appPurple(), for: .normal)
            
            buttonCash.layer.borderColor = UIColor.lightGray.cgColor
            buttonCash.tintColor =  UIColor.lightGray
            buttonCash.setTitleColor(.lightGray, for: .normal)
            
            
            
        case .Cash:
            
            buttonCash.setButtonWithTitleAndBorderColorSecondary()
            buttonCash.setButtonWithTintColorSecondary()
            
            //buttonCash.layer.borderColor = R.color.appNewRedBorder()?.cgColor
            //buttonCash.setImage(R.image.ic_cash_active(), for: .normal)
            //buttonCash.setTitleColor(R.color.appPurple(), for: .normal)
            let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
            switch template {
            case .GoMove:
                buttonCreditCard.setButtonWithTitleAndBorderColorSecondary()
                buttonCreditCard.setButtonWithTintColorSecondary()
                break
                

            default:
                buttonCreditCard.layer.borderColor = UIColor.lightGray.cgColor
                buttonCreditCard.tintColor =  UIColor.lightGray
                buttonCreditCard.setTitleColor(.lightGray, for: .normal)
                buttonCreditCard.setTitle("Credit Card".localizedString , for: .normal)
                
            }
            
            
            
            request.credit_point_used = nil
            switchCreditPoints.setOn(false, animated: true)
            
        default:
            break
            
        }
        
        let paymentGateway = PaymentGateway(rawValue:(/UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id))
        
        switch paymentGateway {
        case .paystack,.payku, .braintree, .paymaya:
            
            buttonCreditCard.setTitle("Pay Online", for: .normal)
            break
            
        default:
            buttonCreditCard.setTitle("Credit Card".localizedString , for: .normal)
        }
        
        
        
        btnBenefitPay.setTitle("Debit Card", for: .normal)
        btnCredimax.setTitle("Credit Card", for: .normal)
        

    }
    
    func setWalletData(wallet: WalletDetails?) {
        lblCreatedHeading.text = "Use your wallet"
        minimumAmount = Int(/wallet?.details?.minBalance) ?? 0
        walletAmount = /wallet?.amount
        
        if /wallet?.amount == 0 {
            labelCreditPoints.text = "Current Wallet Balance" + ": " +  String(/wallet?.amount)
            creditPointStr =  String(/wallet?.amount)
           // switchCreditPoints.isUserInteractionEnabled = false
            
        } else {
           // switchCreditPoints.isUserInteractionEnabled = true
            creditPointView.isHidden = false
            labelCreditPoints.text = "Current Wallet Balance" + ": " +   String(/wallet?.amount)
            creditPointStr =  String(/wallet?.amount)
        }
    }
    
    func setCreditPoints(points: Float?) {
         lblCreatedHeading.text = "Use Credit Amount"
        labelCreditPoints.text =  String(/points)
        creditPointStr =  String(/points)
    }
    
    
    
    func showOrderPricingView(superView : UIView , moveType : MoveType ,requestPara : ServiceRequest, modalPackages: TravelPackages? = nil,zoneArray:[ZoneModel]? = nil ) {
        request = requestPara
        self.modalPackages = modalPackages
        
        isAppliedPromo = false
        couponID = nil
        couponCode = nil
        promoAmount = 0.0
        airportCharge = 0.0
        request.paymentMode = .Cash
        self.paymentStack.isHidden = false
        updatePaymentMode(service: request)
        if !(/request.isPool){
            
             viewPoolAmount.isHidden = true
            poolSwitch.isOn = false
            selectedPoolSeat = 1
        }
        
        if let zone = zoneArray{
            
            self.zoneArray = zone
        }
        
        switchCreditPoints.setOn(false, animated: true)
        request.credit_point_used = nil
        
      /*  buttonCash.layer.borderColor = R.color.appNewRedBorder()?.cgColor
        buttonCash.setImage(R.image.ic_cash_active(), for: .normal)
        buttonCash.setTitleColor(R.color.appPurple(), for: .normal)
                   
        buttonCreditCard.layer.borderColor = UIColor.lightGray.cgColor
        buttonCreditCard.setImage(R.image.ic_card_inactive(), for: .normal)
        buttonCreditCard.setTitleColor(.lightGray, for: .normal)
        
        request.paymentMode = .Cash */
        
        if !isAdded {
            
           // Ankush frameHeight =  superView.frame.size.width*70/100
            viewSuper = superView
            self.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (superView.frame.origin.y + superView.frame.size.height) , width: BookingPopUpFrames.WidthPopUp, height: frameHeight)
            superView.addSubview(self)
            isAdded = true
        }
         assignData()
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
         
        if template == .Corsa{
            
            buttonApplyPromo.isHidden = true
            textFieldPromo.isHidden = true
            if let coupon = requestPara.coupon{
                
                setupDataAfterPromoApplied(object: requestPara.coupon)
            }
            
        }
        
         maximizeOrderPricingView()
        
        if serId == 15{
            btnCash.isHidden = true
        }else{
            btnCash.isHidden = !isCashPaymentEnabled
        }
    }
    
    
    func assignData() {
        
        
      /* Ankush  if    BundleLocalization.sharedInstance().language == Languages.English{
            if request.serviceSelected?.serviceCategoryId == 3{
                txtInfoTotal.text = "Base price"
            }
            else{
                txtInfoTotal.text = "Total"
            }
        }
        
         lblBrandName.text =  /request.serviceSelected?.serviceCategoryId != 2 ?   /request.serviceSelected?.serviceName : /request.selectedBrand?.brandName
        
        
        
        if  /request.serviceSelected?.serviceCategoryId == 4 {
            
             lblOrderDetails.text =   /request.selectedProduct?.productName
            lblBrandName.text =   /request.selectedBrand?.brandName
            
        } else if /request.serviceSelected?.serviceCategoryId == 7  {
            
            lblBrandName.text =   /request.serviceSelected?.serviceName
            lblOrderDetails.text =  /request.selectedBrand?.brandName + " : "  + /request.selectedProduct?.productName
            
        } else{
            
            lblBrandName.text =   /request.serviceSelected?.serviceName
               lblOrderDetails.text =   /request.productName + " × " +  String(request.quantity)
        } */

        textFieldPromo.isHidden = modalPackages != nil
        buttonApplyPromo.isHidden = modalPackages != nil
        btnBookService.titleLabel?.textAlignment = .center
        
       // imageViewVehicle?.image = request.selectedBrand?.categoryBrandId == 20 ? R.image.ic_micro_inactive() : R.image.ic_bike_inactive()
      //  imageViewVehicle.sd_setImage(with: request.selectedBrand?.imageURL, completed: nil)
       // imageViewVehicle.sd_setImage(with: request.selectedProduct?.imageURL, completed: nil)
        
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
               
        
        lblBrandName.text =  /request.selectedProduct?.productName // Ankush
        
        switch template {
        case .DeliverSome?:
             labelSeatCapacity.text = ""
             buttonCreditCard.isHidden = false
             buttonCash.isHidden = true
             imageViewVehicle.sd_setImage(with: request.selectedBrand?.imageURL, completed: nil)
            
        case .Corsa: imageViewVehicle.sd_setImage(with: request.selectedProduct?.imageURL, completed: nil)
            
        default:
            
             imageViewVehicle.sd_setImage(with: request.selectedBrand?.imageURL, completed: nil)
            if request.selectedBrand?.categoryId == 4 {
                labelSeatCapacity.text = ""
            } else {
                labelSeatCapacity.text = "\("Seating capacity".localizedString) : \(/request.selectedProduct?.seating_capacity)"
            }
            
        }
        
        
        
        
        textFieldPromo.text = nil
        
        buttonApplyPromo.setTitle("APPLY PROMO CODE".localizedString, for: .normal)
        buttonApplyPromo.setButtonWithTitleColorSecondary()
        
        lblAirportCharge.isHidden = true
      
        if template == .Corsa{
            
            pickupZone = checkForZone(location: CLLocationCoordinate2DMake(/request.latitude, /request.longitude))
            dropoffZone = checkForZone(location: CLLocationCoordinate2DMake(/request.latitudeDest, /request.longitudeDest))
            
//            pickupZone = checkForZone(location: CLLocationCoordinate2DMake(33.50991566370255, -112.37271485732421))
//            dropoffZone = checkForZone(location: CLLocationCoordinate2DMake(33.50991566370255, -212.37271485732421))
            
            if /pickupZone?.id != dropoffZone?.id{
                
                if /pickupZone?.airport_charges == "yes"{
                    
                    airportCharge += Double(pickupZone?.airport_charge_fee ?? "0.0") ?? 0.0
                }
                
                if /dropoffZone?.airport_charges == "yes"{
                    
                    airportCharge += Double(dropoffZone?.airport_charge_fee ?? "0.0") ?? 0.0
                }
                
            }
            
            if airportCharge > 0.0{
                
                lblAirportCharge.isHidden = false
                lblAirportCharge.text = String(format:"Airport charges is applied on this ride %0.2f %@",airportCharge,(/UDSingleton.shared.appSettings?.appSettings?.currency))
            }
            
            
        }
        
        
        if modalPackages == nil {
            
          /*  var totalCost : Float = 0.0
            
            //Price based on distance
            
            if let pricePPD = request.selectedProduct?.pricePerDistance {
                totalCost = totalCost + (pricePPD * request.distance)
            }
            
            //Price based on time
            if let pricePPM = request.selectedProduct?.price_per_hr {
                totalCost = totalCost + (pricePPM * /request.duration)
            }
            
            /* Ankush  if let pricePPQ = request.selectedProduct?.pricePerQuantity{
             totalCost = totalCost + (pricePPQ * Float(request.quantity))
             } */
            
           
            totalCost = totalCost + Float(/request.selectedProduct?.alphaPrice) // Base price
                        
            if /UDSingleton.shared.appSettings?.appSettings?.schedule_fee == "true" && request.requestType == .Future{
                
                let type = /self.request.selectedProduct?.schedule_charge_type
                
                if type == "value"{
                    
                    totalCost = totalCost + Float(/self.request.selectedProduct?.schedule_charge)
                }
                else{
                    
                    totalCost = totalCost + ((totalCost * Float(/self.request.selectedProduct?.schedule_charge)) / 100.0)
                }
            }
            
            // Ankush  totalCost = totalCost + totalCost.getBuraqShare(percent: /request.serviceSelected?.buraqPercentage)
            // Ankush  lblFinalPrice.text =  String(totalCost).getTwoDecimalFloat() + " " + "currency".localizedString
            if let percentage = request.selectedBrand?.buraq_percentage {
                totalCost = totalCost + ((totalCost * percentage) / 100.0)
            }
            
                            
            
            //Time require to level charge -> calculated the price value
//           let appTemp  = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
//            switch appTemp {
//                case .GoMove:
//                    totalCost = calculateGomovePricing()
//                    break
//           
//                default:
//                    break
//               
//           }
            
            
            //Adding Surchange Value
//            if Bool(/UDSingleton.shared.appSettings?.appSettings?.surCharge) ?? false {
//                let surchargePercentage = Float(/UDSingleton.shared.appSettings?.appSettings?.surChargePercentage)
//                totalCost = totalCost + ((totalCost * /surchargePercentage) / 100.0)
//            }
            
            

            //Time require to level charge -> calculated the price value
           let appTemp  = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
            switch appTemp {
                case .GoMove:
                    totalCost = calculateGomovePricing()
                    break
           
                default:
                    break
               
           }
            
            if Bool(/UDSingleton.shared.appSettings?.appSettings?.surCharge) ?? false {
                let surchargeCalculatedAmount = calculateSurchargeWithTimeSlot(totalCost: totalCost)
                totalCost = totalCost + surchargeCalculatedAmount
            }
            CalculatedPriceBeforePromo = totalCost
            lblFinalPrice.text =  String(totalCost).getTwoDecimalFloat()
            lblNewPrice.text = String(totalCost).getTwoDecimalFloat()*/
            
            if /request.isPool{
                
                calculatePriceForPoolBooking()
            }
            else{
                
                calculatePriceForNormalBooking()
            }
            
            
        } else {
            
            guard let selectedBrand = modalPackages?.package?.pricingData?.categoryBrands?.filter({$0.categoryBrandId == request.selectedBrand?.categoryBrandId}).first,
                  let selectedProduct = selectedBrand.products?.filter({$0.productBrandId == request.selectedProduct?.productBrandId}).first else {return}

            self.selectedPackageProduct = selectedProduct
            CalculatedPriceBeforePromo = /selectedProduct.distance_price_fixed
            lblFinalPrice.text = String(/selectedProduct.distance_price_fixed).getTwoDecimalFloat()
            finalPriceStr = String(/selectedProduct.distance_price_fixed).getTwoDecimalFloat()
            lblNewPrice.text =  String(/selectedProduct.distance_price_fixed).getTwoDecimalFloat()
            let appTemp  = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
            switch appTemp {
            case .Corsa:
                
                let minPrice = /selectedProduct.distance_price_fixed - (/selectedProduct.distance_price_fixed * 10.0/100.0)
                let maxPrice = /selectedProduct.distance_price_fixed + (/selectedProduct.distance_price_fixed * 10.0/100.0)
                
                lblFinalPrice.text =  String(minPrice).getTwoDecimalFloat() + " - " + String(maxPrice).getTwoDecimalFloat()
                break
                
            default:
                break
                
            }
        }
        
       
        
        if request.requestType == .Future {
            
            let todayDate = Date().toLocalDateAcTOLocale()
            let requetedDate = request.orderDateTime.toLocalDateAcTOLocale()
            
            let todayTime = Date().addHours(hoursToAdd: 1).toLocalTimeAcTOLocale()
            let requetedTime = request.orderDateTime.toLocalTimeAcTOLocale()
            
            switch requetedDate.compare(requetedTime)
            {
                case .orderedAscending:
                    switch todayTime.compare(requetedTime) {
                        case .orderedAscending     :   print("Date A is earlier than date B")
                           
                            let strOrderType = "schedule".localizedString
                            let strDate = strOrderType + "\n" + Date().toLocalDateAcTOLocale() + ", " + request.orderDateTime.toLocalTimeAcTOLocale()
                            btnBookService.setTitle( strDate, for: .normal)
                            
                        case .orderedDescending    :   print("Date A is later than date B")
                            
                            let strOrderType = "schedule".localizedString
                            let strDate = strOrderType + "\n" + Date().toLocalDateAcTOLocale() + ", " + Date().addHours(hoursToAdd: 1).toLocalTimeAcTOLocale()
                            btnBookService.setTitle( strDate, for: .normal)
                            
                        case .orderedSame          :   print("The two dates are the same")
                            let strOrderType = "schedule".localizedString
                            let strDate = strOrderType + "\n" + request.orderDateTime.toLocalDateAcTOLocale() + ", " + request.orderDateTime.toLocalTimeAcTOLocale()
                            btnBookService.setTitle( strDate, for: .normal)
                    }
            case .orderedSame:
            
                switch todayTime.compare(requetedTime) {
                    case .orderedAscending     :   print("Date A is earlier than date B")
                       
                        let strOrderType = "schedule".localizedString
                        let strDate = strOrderType + "\n" + Date().toLocalDateAcTOLocale() + ", " + request.orderDateTime.toLocalTimeAcTOLocale()
                        btnBookService.setTitle( strDate, for: .normal)
                        
                    case .orderedDescending    :   print("Date A is later than date B")
                        
                        let strOrderType = "schedule".localizedString
                        let strDate = strOrderType + "\n" + Date().toLocalDateAcTOLocale() + ", " + Date().addHours(hoursToAdd: 1).toLocalTimeAcTOLocale()
                        btnBookService.setTitle( strDate, for: .normal)
                        
                    case .orderedSame          :   print("The two dates are the same")
                        let strOrderType = "schedule".localizedString
                        let strDate = strOrderType + "\n" + request.orderDateTime.toLocalDateAcTOLocale() + ", " + request.orderDateTime.toLocalTimeAcTOLocale()
                        btnBookService.setTitle( strDate, for: .normal)
                }
            case .orderedDescending:
            
                let strOrderType = "schedule".localizedString
                let strDate = strOrderType + "\n" + request.orderDateTime.toLocalDateAcTOLocale() + ", " + request.orderDateTime.toLocalTimeAcTOLocale()
                btnBookService.setTitle( strDate, for: .normal)
            }
            
//            if  requetedDate <= todayDate {
//                switch todayTime.compare(requetedTime) {
//                    case .orderedAscending     :   print("Date A is earlier than date B")
//
//                        let strOrderType = "schedule".localizedString
//                        let strDate = strOrderType + "\n" + Date().toLocalDateAcTOLocale() + ", " + request.orderDateTime.toLocalTimeAcTOLocale()
//                        btnBookService.setTitle( strDate, for: .normal)
//
//                    case .orderedDescending    :   print("Date A is later than date B")
//
//                        let strOrderType = "schedule".localizedString
//                        let strDate = strOrderType + "\n" + Date().toLocalDateAcTOLocale() + ", " + Date().addHours(hoursToAdd: 1).toLocalTimeAcTOLocale()
//                        btnBookService.setTitle( strDate, for: .normal)
//
//                    case .orderedSame          :   print("The two dates are the same")
//                        let strOrderType = "schedule".localizedString
//                        let strDate = strOrderType + "\n" + request.orderDateTime.toLocalDateAcTOLocale() + ", " + request.orderDateTime.toLocalTimeAcTOLocale()
//                        btnBookService.setTitle( strDate, for: .normal)
//                }
//            }
//            else
//            {
//                let strOrderType = "schedule".localizedString
//                let strDate = strOrderType + "\n" + request.orderDateTime.toLocalDateAcTOLocale() + ", " + request.orderDateTime.toLocalTimeAcTOLocale()
//                btnBookService.setTitle( strDate, for: .normal)
//            }
           
           
            
        }else{
            
            let strOrderType = "booking".localizedString
            btnBookService.setTitle( strOrderType, for: .normal)
            
           /* if BundleLocalization.sharedInstance().language == Languages.English{
                
                if request.serviceSelected?.serviceCategoryId == 2{
                    btnBookService.setTitle("Order Now", for: .normal)
                }
                else{
                    btnBookService.setTitle("Book Now", for: .normal)
                }
            }
            else{
                btnBookService.setTitle( "book_now".localizedString, for: .normal)
            } */
            
        }
      
    }
    
    func coordinatesLiesInZone(location:CLLocationCoordinate2D,zoneArray:[CLLocationCoordinate2D])->Bool{
        
        let polygone = GMSMutablePath()
        
        for point in zoneArray{
            
            polygone.add(point)
        }
        
        return GMSGeometryContainsLocation(location, polygone, true)
    }
    
    
    func checkForZone(location:CLLocationCoordinate2D)->ZoneModel?{
        
        for zone in zoneArray{
            
            let zoneCoordinates = zone.coordinatesArray
            
            if coordinatesLiesInZone(location: location, zoneArray: zoneCoordinates){
                
                return zone
                 
                break
            }
            
        }
        
        return nil
        
        
    }
        
    
    
    
    func calculatePriceForNormalBooking(){
        
        totalCost = 0.0
        
        //Price based on distance
        
        if let pricePPD = request.selectedProduct?.pricePerDistance {
            totalCost = totalCost + (pricePPD * request.distance)
        }
        
        //Price based on time
        if let pricePPM = request.selectedProduct?.price_per_hr {
            totalCost = totalCost + (pricePPM * /request.duration)
        }
        
        /* Ankush  if let pricePPQ = request.selectedProduct?.pricePerQuantity{
         totalCost = totalCost + (pricePPQ * Float(request.quantity))
         } */
        
        
        totalCost = totalCost + Float(/request.selectedProduct?.alphaPrice) // Base price
        
        initialPrice = totalCost
        
        bookingCharges = 0
        totalCost += Float(airportCharge)
        
        if /UDSingleton.shared.appSettings?.appSettings?.schedule_fee == "true" && request.requestType == .Future{
            
            let type = /self.request.selectedProduct?.schedule_charge_type
            
            if type == "value"{
                
                totalCost = totalCost + Float(/self.request.selectedProduct?.schedule_charge)
            }
            else{
                
                totalCost = totalCost + ((totalCost * Float(/self.request.selectedProduct?.schedule_charge)) / 100.0)
            }
        }
        
        // Ankush  totalCost = totalCost + totalCost.getBuraqShare(percent: /request.serviceSelected?.buraqPercentage)
        // Ankush  lblFinalPrice.text =  String(totalCost).getTwoDecimalFloat() + " " + "currency".localizedString
        if let percentage = request.selectedBrand?.buraq_percentage {
            totalCost = totalCost + ((totalCost * percentage) / 100.0)
        }
        
        
        
        //Time require to level charge -> calculated the price value
        //           let appTemp  = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
        //            switch appTemp {
        //                case .GoMove:
        //                    totalCost = calculateGomovePricing()
        //                    break
        //
        //                default:
        //                    break
        //
        //           }
        
        
        //Adding Surchange Value
        //            if Bool(/UDSingleton.shared.appSettings?.appSettings?.surCharge) ?? false {
        //                let surchargePercentage = Float(/UDSingleton.shared.appSettings?.appSettings?.surChargePercentage)
        //                totalCost = totalCost + ((totalCost * /surchargePercentage) / 100.0)
        //            }
        
        
        
        //Time require to level charge -> calculated the price value
        let appTemp  = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
        switch appTemp {
        case .GoMove:
            totalCost = calculateGomovePricing()
            break
            
        default:
            break
            
        }
        
        if Bool(/UDSingleton.shared.appSettings?.appSettings?.surCharge) ?? false {
            let surchargeCalculatedAmount = calculateSurchargeWithTimeSlot(totalCost: totalCost)
            totalCost = totalCost + surchargeCalculatedAmount
        }
        
        bookingCharges = totalCost - initialPrice
        
        CalculatedPriceBeforePromo = totalCost
        
        let currency = (/UDSingleton.shared.appSettings?.appSettings?.currency)
        
        lblFinalPrice.text =  "Initial Fare - \(String(initialPrice).getTwoDecimalFloat())\nTotal - \(String(totalCost).getTwoDecimalFloat())"
        let text = "Included charges \(currency)"
//        lblCurrency.text = text
//        lblNewCurrency.text = text
        finalPriceStr =  String(totalCost).getTwoDecimalFloat()
        lblNewPrice.text = String(totalCost).getTwoDecimalFloat()
        
   
            let minPrice = totalCost - (totalCost * 10.0/100.0)
            let maxPrice = totalCost + (totalCost * 10.0/100.0)
            
            lblFinalPrice.text = "Total - " + String(minPrice).getTwoDecimalFloat() + " - " + String(maxPrice).getTwoDecimalFloat()
            
        if APIBasePath.isPikkup {
            labelSeatCapacity.text = "Total - " + String(minPrice).getTwoDecimalFloat() + " - " + String(maxPrice).getTwoDecimalFloat() + " \(currency)"
            lblCurrency.text = ""
            lblNewCurrency.text = ""
            lblFinalPrice.text = ""
        }
    }
    
    
    func calculatePriceForPoolBooking(){
        
        totalCost = 0.0
        
        //Price based on distance
        var poolPricePerDistance = Float()
        var poolPricePerHour = Float()
        
        if let pricePPD = request.selectedProduct?.pool_price_per_distance {
            poolPricePerDistance =  (pricePPD * request.distance)
        }
        
        //Price based on time
        if let pricePPM = request.selectedProduct?.pool_price_per_hr {
            poolPricePerHour = (pricePPM * /request.duration)
        }
        
        /* Ankush  if let pricePPQ = request.selectedProduct?.pricePerQuantity{
         totalCost = totalCost + (pricePPQ * Float(request.quantity))
         } */
        
        
        totalCost = totalCost + Float(/request.selectedProduct?.pool_alpha_price) // Base price
        
        
        
        let priceBySeat = ((poolPricePerDistance + poolPricePerHour) * Float(selectedPoolSeat))
        
        totalCost += priceBySeat
        
        initialPrice = totalCost
        
        totalCost += Float(airportCharge)
        
        if /UDSingleton.shared.appSettings?.appSettings?.schedule_fee == "true" && request.requestType == .Future{
            
            let type = /self.request.selectedProduct?.schedule_charge_type
            
            if type == "value"{
                
                totalCost = totalCost + Float(/self.request.selectedProduct?.schedule_charge)
            }
            else{
                
                totalCost = totalCost + ((totalCost * Float(/self.request.selectedProduct?.schedule_charge)) / 100.0)
            }
        }
        
        // Ankush  totalCost = totalCost + totalCost.getBuraqShare(percent: /request.serviceSelected?.buraqPercentage)
        // Ankush  lblFinalPrice.text =  String(totalCost).getTwoDecimalFloat() + " " + "currency".localizedString
        if let percentage = request.selectedBrand?.buraq_percentage {
            totalCost = totalCost + ((totalCost * percentage) / 100.0)
        }
        
        
        
        //Time require to level charge -> calculated the price value
        //           let appTemp  = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
        //            switch appTemp {
        //                case .GoMove:
        //                    totalCost = calculateGomovePricing()
        //                    break
        //
        //                default:
        //                    break
        //
        //           }
        
        
        //Adding Surchange Value
        //            if Bool(/UDSingleton.shared.appSettings?.appSettings?.surCharge) ?? false {
        //                let surchargePercentage = Float(/UDSingleton.shared.appSettings?.appSettings?.surChargePercentage)
        //                totalCost = totalCost + ((totalCost * /surchargePercentage) / 100.0)
        //            }
        
        
        
        //Time require to level charge -> calculated the price value
        let appTemp  = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
        switch appTemp {
        case .GoMove:
            totalCost = calculateGomovePricing()
            break
            
        default:
            break
            
        }
        
        if Bool(/UDSingleton.shared.appSettings?.appSettings?.surCharge) ?? false {
            let surchargeCalculatedAmount = calculateSurchargeWithTimeSlot(totalCost: totalCost)
            totalCost = totalCost + surchargeCalculatedAmount
        }
        CalculatedPriceBeforePromo = totalCost
        bookingCharges = totalCost - initialPrice
        
        let currency = (/UDSingleton.shared.appSettings?.appSettings?.currency)
        
        lblFinalPrice.text =  "Initial Fare - \(String(initialPrice).getTwoDecimalFloat())\nTotal - \(String(totalCost).getTwoDecimalFloat())"
        let text = "Included charges \(currency)"
//        lblCurrency.text = text
//        lblNewCurrency.text = text
//        lblFinalPrice.text =  String(totalCost).getTwoDecimalFloat()
        finalPriceStr =  String(totalCost).getTwoDecimalFloat()
        lblNewPrice.text = String(totalCost).getTwoDecimalFloat()
        
        let minPrice = totalCost - (totalCost * 10.0/100.0)
        let maxPrice = totalCost + (totalCost * 10.0/100.0)
        
        lblFinalPrice.text = "Total - " + String(minPrice).getTwoDecimalFloat() + " - " + String(maxPrice).getTwoDecimalFloat()
        
    if APIBasePath.isPikkup {
        labelSeatCapacity.text = "Total - " + String(minPrice).getTwoDecimalFloat() + " - " + String(maxPrice).getTwoDecimalFloat()
        lblFinalPrice.text = ""
    }
    }
    
    
    func calculateSurchargeWithTimeSlot(totalCost:Float)->Float{
        
      
        var surchargeAmount:Float = 0.0
        
        if let surChargeArray =  request.selectedProduct?.surChargeAdmin{
            
              var surChargeAdmin:SurChargeAdmin?
            
            for surcharge in surChargeArray{
                
                let dateStr = Date().dateToString(format: "dd MMM yyyy")
                let startTimeStr =  dateStr + " " + /surcharge.startTime
                let endTimeStr =   dateStr + " " + /surcharge.endTime
                
                guard let startTime = startTimeStr.toLocalDate(format: "dd MMM yyyy HH:mm:ss") else{break}
                guard let endTime = endTimeStr.toLocalDate(format: "dd MMM yyyy HH:mm:ss") else{break}
                
                
                if (startTime.equalToDate(dateToCompare: Date()) || startTime.isLessThanDate(dateToCompare: Date())) &&   (endTime.equalToDate(dateToCompare: Date()) || endTime.isGreaterThanDate(dateToCompare: Date())){
                    
                    surChargeAdmin = surcharge
                    break
                }
                                
            }
            
            if let surChargeAdmin = surChargeAdmin{
                
                if /surChargeAdmin.status == 1{
                    
                    if /surChargeAdmin.type == "value"{
                        
                        if let surchargeAdminAmount = Float(/surChargeAdmin.value){
                            
                            surchargeAmount = surchargeAdminAmount
                        }
                    }
                    else{
                        if let percentage = Int(/surChargeAdmin.value){
                            
                            surchargeAmount = totalCost * Float(Float(percentage) / 100.0)
                        }
                        
                    }
                }
            }
            
        }
        else{
            
            let surchargePercentage = Float(/UDSingleton.shared.appSettings?.appSettings?.surChargePercentage)
            surchargeAmount = ((totalCost * /surchargePercentage) / 100.0)
        }
        
        return surchargeAmount
        
    }
    

       func calculateEstimatePrice()->Double {
           let totalMinutes = calculateEstimateTotalTime()
           //let hours = totalMinutes / 60
           return totalMinutes * Double(/request.selectedProduct?.price_per_hr)
       }
       
       
       func calculateEstimateTotalTime()->Double{
        
            print("unloadTime",Float(unloadTime()))
            print("loadTimeTime",Float(loadTime()))
            print("request.duration",request.duration)
            print("selectedProduct?.actualPrice",Float(/request.selectedProduct?.actualPrice))
        
            
            let estTime = Float(unloadTime()) + Float(loadTime()) + request.duration +  Float(/request.selectedProduct?.actualPrice)
        
            return Double(estTime)
       }
       
       
       func unloadTime() -> Int {
    
           var unloadTime = 0
           var loadUploadTime =  0
           var level = 0
           
           let lvlPercentage = /UDSingleton.shared.appSettings?.appSettings?.level_percentage?.toInt()
           let elivatorPercentage = /UDSingleton.shared.appSettings?.appSettings?.elevator_percentage?.toInt()
        
            print("lvlPercentage ====>",lvlPercentage)
            print("elivatorPercentage ====>",elivatorPercentage)
            print("load_unload_time  ===>", /request.selectedProduct?.load_unload_time)
            print("request.elevator_dropoff ===>", request.elevator_dropoff)
                      
           if let t = Double(/request.selectedProduct?.load_unload_time){
               loadUploadTime = Int(t)
           }
           
          
           if let lvl = Int(/request.dropoff_level){
               level = lvl
           }
           
           if request.elevator_dropoff != "1" {
                unloadTime = loadUploadTime + (loadUploadTime * level * lvlPercentage)
           }
           else{
                unloadTime = loadUploadTime + (loadUploadTime * level * elivatorPercentage)
           }
        
        return unloadTime
        
       }
       
       
       func loadTime()-> Int {
           
           var loadTime = 0
           
           var loadUploadTime =  0
           
           let lvlPercentage = /UDSingleton.shared.appSettings?.appSettings?.level_percentage?.toInt()
           let elivatorPercentage = /UDSingleton.shared.appSettings?.appSettings?.elevator_percentage?.toInt()
        
           print("Load lvlPercentage ====>",lvlPercentage)
           print("load elivatorPercentage ====>",elivatorPercentage)
        
           print("load elivatorPercentage ====>",/request.selectedProduct?.load_unload_time)
           print("load request.pickup_level ====>",/request.pickup_level)
        print("load request.elevator_pickup ====>",/request.elevator_pickup)
           
           
           if let t = Double(/request.selectedProduct?.load_unload_time){
                loadUploadTime = Int(t)
           }
           
            var level = 0
            if let lvl = Int(/request.pickup_level){
               
               level = lvl
           }
           
          
           
           if request.elevator_pickup != "1"{
                loadTime = loadUploadTime + (loadUploadTime * level * lvlPercentage)
           }
           else{
                loadTime = loadUploadTime + (loadUploadTime * level * elivatorPercentage)
           }
        
            return loadTime
        
       }

    func calculateGomovePricing() -> Float {
        var isPickupElevator: Bool = false
        var isDroppElevator: Bool = false
        var loadTime: Double? = 0.0
        var unloadTime: Double? = 0.0
        
        loadTime =  /Double(/request.selectedProduct?.load_unload_time)
        unloadTime = /Double(/request.selectedProduct?.load_unload_time)
        
        
        var pickLevel: Int? = 0
        pickLevel = /Int(/request.pickup_level)
        var dropLevel: Int? = 0
        dropLevel = /Int(/request.dropoff_level)
        
        
        var pickuprateVAlue: String? = ""
        var dropRateValue = "0"
        
        if /request.elevator_pickup == "true" {
            isPickupElevator = true
        }
        
        if /request.elevator_dropoff == "true" {
            isDroppElevator = true
        }
        
        
        if isPickupElevator {
            pickuprateVAlue = /UDSingleton.shared.appSettings?.appSettings?.elevator_percentage
        } else {
            pickuprateVAlue = /UDSingleton.shared.appSettings?.appSettings?.level_percentage
        }
        
        if isDroppElevator {
            dropRateValue = /UDSingleton.shared.appSettings?.appSettings?.elevator_percentage
        } else {
            dropRateValue = /UDSingleton.shared.appSettings?.appSettings?.level_percentage
        }
        
        
        var timeLoading: Double = 0
        var timeUnloading: Double = 0
        
        let timeLoadingValue  = /loadTime * Double(/pickLevel) * /Double(/pickuprateVAlue)
        timeLoading = /loadTime + timeLoadingValue
        print("Time Loading", timeLoading)
        
        
        let timeUnloadingValue  = /unloadTime * Double(/dropLevel)  * /Double(/dropRateValue)
        timeUnloading = /unloadTime  + timeUnloadingValue
        print("timeUnloading", timeUnloading)

        let estimationTime = request.duration
        let ET = timeLoading + timeUnloading + /Double(estimationTime)
        let estimatePriceHourly = (Float(ET) * /request.selectedProduct?.price_per_hr) + Float(/request.selectedProduct?.alphaPrice)
        
        return estimatePriceHourly
        
    }

    // Call After promo applied
    func setupDataAfterPromoApplied (object: Coupon?) {
        
        isAppliedPromo = true
        couponID = object?.couponId
        couponCode = object?.code
        textFieldPromo.text = object?.code
       
        textFieldPromo.textColor = R.color.appPurple()
        
        buttonApplyPromo.setTitle("REMOVE", for: .normal)
        buttonApplyPromo.setTitleColor(R.color.appRed(), for: .normal)
        
        let price = initialPrice
        let currency = (/UDSingleton.shared.appSettings?.appSettings?.currency)
        if /object?.couponType == "Value" {
            
            let finalPrice = (/price - /Float(/object?.amountValue)) + bookingCharges
            lblFinalPrice.text = String(finalPrice).getTwoDecimalFloat()
            lblFinalPrice.text =  "Initial Fare - \(String(price).getTwoDecimalFloat())\nTotal - \(String(finalPrice).getTwoDecimalFloat())"
//            lblCurrency.text = "Included charges \(currency)"
            finalPriceStr = String(finalPrice).getTwoDecimalFloat()
            lblNewPrice.text =  String(finalPrice).getTwoDecimalFloat()
            promoAmount = /Double(/object?.amountValue)
            
            let minPrice = finalPrice - (finalPrice * 10.0/100.0)
            let maxPrice = finalPrice + (finalPrice * 10.0/100.0)
            
            lblFinalPrice.text = "Total - " + String(minPrice).getTwoDecimalFloat() + " - " + String(maxPrice).getTwoDecimalFloat()
            
        if APIBasePath.isPikkup {
            labelSeatCapacity.text = "Total - " + String(minPrice).getTwoDecimalFloat() + " - " + String(maxPrice).getTwoDecimalFloat()
            lblFinalPrice.text = ""
        }
        } else {
            promoAmount = (Double(/price) * /Double(/object?.amountValue))/100.0
            let finalPrice = (/price - (/price * /Float(/object?.amountValue))/100.0) + bookingCharges
            lblFinalPrice.text = String(finalPrice).getTwoDecimalFloat()
            lblFinalPrice.text =  "Initial Fare - \(String(price).getTwoDecimalFloat())\nTotal - \(String(finalPrice).getTwoDecimalFloat())"
            finalPriceStr = String(finalPrice).getTwoDecimalFloat()
            lblNewPrice.text =  String(finalPrice).getTwoDecimalFloat()
            let text = "Included charges \(currency)"
//            lblCurrency.text = text
//            lblNewCurrency.text = text
            
            let minPrice = finalPrice - (finalPrice * 10.0/100.0)
            let maxPrice = finalPrice + (finalPrice * 10.0/100.0)
            
            lblFinalPrice.text = "Total - " + String(minPrice).getTwoDecimalFloat() + " - " + String(maxPrice).getTwoDecimalFloat()
            
        if APIBasePath.isPikkup {
            labelSeatCapacity.text = "Total - " + String(minPrice).getTwoDecimalFloat() + " - " + String(maxPrice).getTwoDecimalFloat()
            lblFinalPrice.text = ""
        }
        }
    }
    
    
    func updatePaymentMode(service : ServiceRequest) {
          let paymentGateway = PaymentGateway(rawValue:(/UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id))
        request = service
        
        switch service.paymentMode {
        case .Card:
            
            btnCredimax.setButtonWithTitleAndBorderColorSecondary()
            btnCredimax.setButtonWithTintColorSecondary()
            btnCredimax.setTitleColor(.white, for: .normal)
            buttonCash.tintColor =  UIColor.lightGray
            buttonCash.layer.borderColor = UIColor.lightGray.cgColor
            buttonCash.setImage(R.image.ic_cash_inactive(), for: .normal)
            buttonCash.setTitleColor(.lightGray, for: .normal)
            switch paymentGateway {
            case .stripe:
                buttonCreditCard.setTitle("**** **** **** \(/request.selectedCard?.lastDigit)" , for: .normal)
                
            case .peach:
                buttonCreditCard.setTitle("**** **** **** \(/request.selectedCard?.last4)" , for: .normal)
                
                
            case .epayco:
                buttonCreditCard.setTitle("\(/request.selectedCard?.last4)" , for: .normal)
                
            default:
                btnCredimax.setTitle(/request.selectedCard?.cardNo , for: .normal)
            }

        case .Cash:
            
            buttonCash.setButtonWithTitleAndBorderColorSecondary()
            buttonCash.setButtonWithTintColorSecondary()
            btnCredimax.layer.borderColor = UIColor.lightGray.cgColor
                       buttonCreditCard.setImage(R.image.ic_card_inactive(), for: .normal)
                       buttonCreditCard.setTitleColor(.lightGray, for: .normal)
                       buttonCreditCard.tintColor =  UIColor.lightGray
                       buttonCreditCard.setTitle("Credit Card".localizedString , for: .normal)
            //  buttonCreditCard.setTitle("Credit Card".localizedString , for: .normal)
            let paymentGateway = PaymentGateway(rawValue:(/UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id))
            
            switch paymentGateway {
            case .paystack,.payku,.braintree, .paymaya:
                
                buttonCreditCard.setTitle("Pay Online", for: .normal)
                break
                
            default:
                btnCredimax.setTitle("Credimax".localizedString , for: .normal)
            }
            
            request.credit_point_used = nil
            switchCreditPoints.setOn(false, animated: true)

            
        default:
            break
            
        }
    }
    
    
    
}
