//
//  InvoiceView.swift
//  Buraq24
//
//  Created by MANINDER on 28/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class InvoiceView: UIView {
    
    
    //MARK:- IBOutlets
    //MARK:-
    @IBOutlet weak var previousChargesStackView: UIStackView!
    @IBOutlet weak var baseFareHeadingLabel: UILabel!
    @IBOutlet weak var lblPreviousCharges: UILabel!
    @IBOutlet weak var lblWaitingCharges: UILabel!
    @IBOutlet var lblBrandName: UILabel!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblBaseFairValue: UILabel!
    @IBOutlet var labelTime: UILabel!
    @IBOutlet var labelTimeValue: UILabel!
    @IBOutlet var labelDistance: UILabel!
    @IBOutlet var labelDistanceValue: UILabel!
    @IBOutlet var labelNormalFareValue: UILabel!
    @IBOutlet var labelBookingFeeValue: UILabel!
    @IBOutlet var labelServiceChargeValue: UILabel!
    @IBOutlet var labelSubTotalValue: UILabel!
    @IBOutlet var lblFinalAmount: UILabel!
    @IBOutlet weak var buttonFinish: UIButton!
    @IBOutlet weak var stackTip: UIStackView!
    @IBOutlet weak var lblSurCharge: UILabel!
    @IBOutlet weak var lblCheckListCharge: UILabel!
    @IBOutlet weak var labelWaitingTime: UILabel!
    @IBOutlet weak var labelWaitingTimeValue: UILabel!
    @IBOutlet weak var viewAirport: UIStackView!
    @IBOutlet weak var viewZone: UIStackView!
    @IBOutlet weak var viewTollParking: UIStackView!
    @IBOutlet weak var lblAirport: UILabel!
    @IBOutlet weak var lblAirportValue: UILabel!
    @IBOutlet weak var lblZone: UILabel!
    @IBOutlet weak var lblZoneValue: UILabel!
    @IBOutlet weak var lblTollParking: UILabel!
    @IBOutlet weak var lblTollParkingvalue: UILabel!
     @IBOutlet weak var couponLabel: UILabel!
    
    @IBOutlet weak var viewSurchage: UIStackView!
    @IBOutlet weak var viewServiceCharge: UIStackView!
    @IBOutlet weak var viewWaitingCharge: UIStackView!
    @IBOutlet weak var viewBookingFee: UIStackView!
    
    
    @IBOutlet weak var lblCancellationChargesValue: UILabel!
    @IBOutlet weak var lblTipsAmount: UILabel!
    
    
    
    
    //MARK:- Properties
    //MARK:-
    var viewSuper : UIView?
    var delegate : BookRequestDelegate?
    var isAdded : Bool = false
    var orderDone : OrderCab?
    var frameHeight : CGFloat = 342
    let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
    
    
    
    //MARK:- Actions
    //MARK:-
    @IBAction func actionBtnFinishOrder(_ sender: Any) {
        self.minimizeInvoiceView()
        self.delegate?.didSelectNext(type: .DoneInvoice)
    }
    
    @IBAction func showCheckListData(_ sender: Any) {
        self.delegate?.didShowCheckList(order: orderDone)
    }
    
    //MARK:- Functions
    //MARK:-
    
    func minimizeInvoiceView() {
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: /self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            }, completion: { (done) in
        })
    }
    
    func maximizeInvoiceView() {
        
        if orderDone?.payment?.paymentType == PaymentType.Card.rawValue  || /UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id == "stripe" || /UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id == "epayco" {
            frameHeight = UIDevice.current.iPhoneX ? 416 + 34 : 416  //410
        } else {
            frameHeight = UIDevice.current.iPhoneX ? 400 + 34 : 400   //342
        }
        
        
        // 10 - to hide bottom corner radius
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: 0, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - /self?.frameHeight + 10 , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            
            self?.layoutIfNeeded()
            self?.setupUI()
            
            }, completion: { (done) in
        })
    }
    
    func setupUI() {
        
        
        print("======>>>>>>>> Payment Type", orderDone?.payment?.paymentType)
        
        
        if orderDone?.payment?.paymentType == PaymentType.Card.rawValue  || /UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id == "stripe" || /UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id == "epayco"{
            stackTip.isHidden = false
        } else {
            stackTip.isHidden = true
        }
        
        if orderDone?.payment?.paymentType == PaymentType.Cash.rawValue{
            stackTip.isHidden = true
        }
        
        buttonFinish.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
    }
    
    func showInvoiceView(superView : UIView,serviceRequest:ServiceRequest? ,order : OrderCab ) {
        orderDone = order
        if !isAdded {
            //frameHeight =  superView.frame.size.width*76/100
            viewSuper = superView
            self.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (superView.frame.origin.y + superView.frame.size.height) , width: BookingPopUpFrames.WidthPopUp, height: frameHeight)
            superView.addSubview(self)
            isAdded = true
        }
        
        assignPopUpData(serviceRequest:serviceRequest)
        maximizeInvoiceView()
    }
    
    
    func assignPopUpData(serviceRequest:ServiceRequest?) {
        
        guard let currentOrd = orderDone else{return}
        
        let brand = currentOrd.orderProductDetail
        let currency = (/UDSingleton.shared.appSettings?.appSettings?.currency) + " "
        lblBrandName.text = brand?.productBrandName
        lblProductName.text = brand?.productName
        let payment = currentOrd.payment
        let previousCharges = Float(payment?.previous_charges ?? "") ?? 0
        if previousCharges == 0 {
            previousChargesStackView.isHidden = true
            lblPreviousCharges.text = ""
        }else{
            lblPreviousCharges.text =  String(format:"%@ %0.2f",currency,previousCharges)
            //currency +  /payment?.previous_charges
            previousChargesStackView.isHidden = false
        }
        viewAirport.isHidden = true
        viewZone.isHidden = true
        viewTollParking.isHidden = true
        if  /UDSingleton.shared.appSettings?.appSettings?.currency_decimal_places == "3"{
            if currentOrd.booking_type == "Package" {
               
                
                let paymentInfo = currentOrd.payment
                let currency = /UDSingleton.shared.appSettings?.appSettings?.currency
                let baseFare = Double(/Float(/paymentInfo?.distance_price_fixed))
                lblBaseFairValue.text = "\(currency) \(String(format: "%.3f",baseFare))"
                baseFareHeadingLabel.text = "Package Price"
                //Time
                let orderTime = Double(paymentInfo?.extra_time ?? "") ?? 0.0
                let timePerHourCharges = Double(/Float(/paymentInfo?.price_per_min))
                let timeCharges = orderTime*timePerHourCharges
                labelTime.text = "Extra Time(\((Int(paymentInfo?.extra_time ?? "") ?? 0)) min)"
                labelTimeValue.text = "\(currency) \(String(format: "%.3f",timeCharges))"
                
                //Distance
                let orderDistance = Double(paymentInfo?.extra_distance ?? "") ?? 0.0
                let distancePerHourCharges = Double(/Float(/paymentInfo?.price_per_km))
                let distanceCharges = orderDistance * distancePerHourCharges
                labelDistance.text = "Extra Distance (\(String(format: "%.3f",orderDistance)) KM)"
                labelDistanceValue.text = "\(currency) \(String(format: "%.3f",distanceCharges))"
                
                //Normal Charges.
                let normalCharges = (baseFare + timeCharges + distanceCharges)
                // let actualValue = Double(paymentInfo?.product_actual_value ?? "") ?? 0.0
                labelNormalFareValue.text = "\(currency) \(String(format: "%.3f",normalCharges))"
                
                let serviceCharges = 0.0
                let bookingFee = 0.0
                
                labelBookingFeeValue.text = "\(currency) \(String(format: "%.3f",bookingFee))"
                labelServiceChargeValue.text = "\(currency) \(String(format: "%.3f",serviceCharges))"
                
                //checklist charge
    //            lblCheckListTotal.text = "\(/UDSingleton.shared.appSettings?.appSettings?.currency) \(/completedRequest?.result?.check_list_total)"
                
                //SubTotal
               // labelSubTotalValue.text = "\(currency) \(String(format: "%.3f",(normalCharges + bookingFee + serviceCharges)))"
                
                //Total
                labelSubTotalValue.text = "\(currency) \(String(format: "%.3f",(Double(paymentInfo?.finalCharge ?? "") ?? 0.0)))"
                lblFinalAmount.text = "\(currency) \(String(format: "%.3f",(Double(paymentInfo?.finalCharge ?? "") ?? 0.0)))"
                
                lblTipsAmount.text = "\(currency) \(String(format: "%.3f",(Double(paymentInfo?.tipCharge ?? "") ?? 0.0)))"
                lblCancellationChargesValue.text = "\(currency) \(String(format: "%.3f",(Double(paymentInfo?.cancellation_charges ?? "") ?? 0.0)))"
                
            }else{
                
                let baseFareValue = /Float(/currentOrd.payment?.productAplhaCharge)
                let timeValue = (/Float(/currentOrd.payment?.order_time)) * (/Float(/currentOrd.payment?.product_per_hr_charge))
                
                
                var waitingValue:Float = 0.0
                
                if template == .Corsa{
                    
                    waitingValue = (/Float(/currentOrd.payment?.waiting_charges))
                    
                } else{
                    
                    //waitingValue = (/Float(/currentOrd.payment?.waiting_charges)) * (/Float(/currentOrd.payment?.waiting_time))
                    waitingValue = (/Float(/currentOrd.payment?.waiting_charges))
                    
                }
                
                
                let distanceValue = (/Float(/currentOrd.payment?.orderDistance)) * (/Float(/currentOrd.payment?.product_per_distance_charge))
                
                lblBaseFairValue.text = currency + "\(baseFareValue)".getThreeDecimalFloat()
                
                let time = "\(/currentOrd.payment?.order_time)".getThreeDecimalFloat()
                labelTime.text = "Time(\(time) min)"
                labelTimeValue.text = currency + "\(timeValue)".getThreeDecimalFloat()
                
                labelTime.isHidden = true
                labelTimeValue.isHidden = true
                
                let countRideStops = /currentOrd.ride_stops?.count
                
                //var waitingTime = 0.000
                
//                if countRideStops != 0 {
//                    for i in 0..<countRideStops
//                    {
//                        let waitTime =  /currentOrd.ride_stops?[i].waiting_time
//
//                        waitingTime = waitingTime + (Double(waitTime) ?? 0.0)
//
//                    }
//                }
//
//                let waitingTime1 = "\(/currentOrd.waiting_time)".getTwoDecimalFloat()
//
//                waitingTime = waitingTime + (Double(waitingTime1) ?? 0.000)
                
//                var waitingTime = 0.000
//                let waitingTimePayment = "\(/currentOrd.payment?.waiting_time)".getThreeDecimalFloat()
//                let minimum_waiting_time = (/UDSingleton.shared.appSettings?.appSettings?.minimum_waiting_time)
//                let minimum_waiting_time1 = (Double(minimum_waiting_time.getThreeDecimalFloat()) ?? 0.000)
//
//                let waitingTimeOuter = (Double("\(/currentOrd.waiting_time)".getThreeDecimalFloat()) ?? 0.000)
//
//                var balanceWaitingTime = 0.000
//                if waitingTimeOuter > minimum_waiting_time1{
//                    balanceWaitingTime = waitingTimeOuter - minimum_waiting_time1
//                }
//
//                waitingTime = balanceWaitingTime + (Double(waitingTimePayment) ?? 0.0)
                
                let waitingTime = "\(/currentOrd.payment?.waiting_time)".getThreeDecimalFloat()
                
                if template == .Corsa{
                    
                    labelWaitingTime.text = "Waiting Charges"
                    labelWaitingTimeValue.text = currency + "\(waitingValue)".getThreeDecimalFloat()
                    
                } else{
                    //labelWaitingTime.text = "Waiting Charges"
                    labelWaitingTime.text = "Waiting Time(\(waitingTime) min)"
                    labelWaitingTimeValue.text = currency + "\(waitingValue)".getThreeDecimalFloat()
                }
                
               // viewWaitingCharge.isHidden = waitingValue == 0.0
                    
                    
                
                
                
                let distance = "\(/currentOrd.payment?.orderDistance)".getThreeDecimalFloat()
                labelDistance.text = "Distance(\(distance) km)"
                labelDistanceValue.text = currency + "\(distanceValue)".getThreeDecimalFloat()
                
                var normalValue = baseFareValue + timeValue + distanceValue
                labelNormalFareValue.text = currency + "\(normalValue)".getThreeDecimalFloat()
                
                let airportCharge = Float(Double(currentOrd.payment?.airport_charges ?? "") ?? 0.0)
                let zoneCharge = Float(Double(currentOrd.payment?.zone_charges ?? "") ?? 0.0)
                let tollCharge = Float(Double(currentOrd.payment?.toll_charges ?? "") ?? 0.0)
                let parkingCharge = Float(Double(currentOrd.payment?.parking_charges ?? "") ?? 0.0)
                
                
                if airportCharge > 0.0{
                    
                    viewAirport.isHidden = false
                    lblAirportValue.text = currency +  "\(airportCharge)".getThreeDecimalFloat()
                }
                
                if zoneCharge > 0.0{
                    
                    viewZone.isHidden = false
                    lblZoneValue.text =  currency +  "\(zoneCharge)".getThreeDecimalFloat()
                }
                
                if (tollCharge + parkingCharge) > 0.0{
                    
                    viewTollParking.isHidden = false
                    lblTollParkingvalue.text =  currency +  "\(tollCharge + parkingCharge)".getThreeDecimalFloat()
                    
                }
                
                normalValue = normalValue + airportCharge + zoneCharge + tollCharge + parkingCharge + waitingValue
                var bookingCharge: Float
                var serviceCharges: Float
                
                // let actualValue = /brand?.productActualPrice
                let actualValue = (/Float(/currentOrd.payment?.productActualValue))
                let totalPercentage = ((/currentOrd.payment?.buraqPercentage)/100)
                let exactValue = normalValue + previousCharges
                if exactValue > actualValue {
                    bookingCharge = normalValue * totalPercentage
                    serviceCharges = 0.0
                }else {
                    bookingCharge = actualValue * totalPercentage
                    if exactValue > actualValue {
                        serviceCharges = exactValue - actualValue
                    }else{
                        serviceCharges = actualValue - exactValue
                    }
                }
                
                lblCheckListCharge.text = currency + "\(/currentOrd.check_list_total)"
    //            labelBookingFeeValue.text = currency + "\(bookingCharge)".getTwoDecimalFloat()
                labelBookingFeeValue.text = currency + "\(/currentOrd.payment?.adminCharge)".getThreeDecimalFloat()
                
                viewBookingFee.isHidden = bookingCharge == 0.0
                
                labelServiceChargeValue.text = currency + "\(serviceCharges)".getThreeDecimalFloat()
                
                viewServiceCharge.isHidden = serviceCharges == 0.0
                
                let subtotal = normalValue + bookingCharge + serviceCharges  + (Float(currentOrd.payment?.sur_charge ?? "0.0") ?? 0.0) + previousCharges
                lblSurCharge.text = currency + "\(/currentOrd.payment?.sur_charge)".getThreeDecimalFloat()
                
                viewSurchage.isHidden = /(Double(currentOrd.payment?.sur_charge ?? "0.0")) == 0.0
                
    //            labelSubTotalValue.text = currency + "\(subtotal)".getTwoDecimalFloat()
                labelSubTotalValue.text = currency + "\(/currentOrd.payment?.finalCharge)".getThreeDecimalFloat()
                lblFinalAmount.text =  currency + (/currentOrd.payment?.finalCharge).getThreeDecimalFloat()
                
                lblTipsAmount.text = currency + (/currentOrd.payment?.tipCharge).getThreeDecimalFloat()
                lblCancellationChargesValue.text = currency + (/currentOrd.payment?.cancellation_charges).getThreeDecimalFloat()
                
            }
        }else{
            if currentOrd.booking_type == "Package" {
               
                
                let paymentInfo = currentOrd.payment
                let currency = /UDSingleton.shared.appSettings?.appSettings?.currency
                let baseFare = Double(/Float(/paymentInfo?.distance_price_fixed))
                lblBaseFairValue.text = "\(currency) \(String(format: "%.2f",baseFare))"
                baseFareHeadingLabel.text = "Package Price"
                //Time
                let orderTime = Double(paymentInfo?.extra_time ?? "") ?? 0.0
                let timePerHourCharges = Double(/Float(/paymentInfo?.price_per_min))
                let timeCharges = orderTime*timePerHourCharges
                labelTime.text = "Extra Time(\((Int(paymentInfo?.extra_time ?? "") ?? 0)) min)"
                labelTimeValue.text = "\(currency) \(String(format: "%.2f",timeCharges))"
                
                //Distance
                let orderDistance = Double(paymentInfo?.extra_distance ?? "") ?? 0.0
                let distancePerHourCharges = Double(/Float(/paymentInfo?.price_per_km))
                let distanceCharges = orderDistance * distancePerHourCharges
                labelDistance.text = "Extra Distance (\(String(format: "%.2f",orderDistance)) KM)"
                labelDistanceValue.text = "\(currency) \(String(format: "%.2f",distanceCharges))"
                
                //Normal Charges.
                let normalCharges = (baseFare + timeCharges + distanceCharges)
                // let actualValue = Double(paymentInfo?.product_actual_value ?? "") ?? 0.0
                labelNormalFareValue.text = "\(currency) \(String(format: "%.2f",normalCharges))"
                
                let serviceCharges = 0.0
                let bookingFee = 0.0
                
                labelBookingFeeValue.text = "\(currency) \(String(format: "%.2f",bookingFee))"
                labelServiceChargeValue.text = "\(currency) \(String(format: "%.2f",serviceCharges))"
                
                //checklist charge
    //            lblCheckListTotal.text = "\(/UDSingleton.shared.appSettings?.appSettings?.currency) \(/completedRequest?.result?.check_list_total)"
                
                //SubTotal
               // labelSubTotalValue.text = "\(currency) \(String(format: "%.2f",(normalCharges + bookingFee + serviceCharges)))"
                
                //Total
                labelSubTotalValue.text = "\(currency) \(String(format: "%.2f",(normalCharges + bookingFee + serviceCharges)))"
                lblFinalAmount.text = "\(currency) \(String(format: "%.2f",(Double(paymentInfo?.finalCharge ?? "") ?? 0.0)))"
                
                lblTipsAmount.text = "\(currency) \(String(format: "%.3f",(Double(paymentInfo?.tipCharge ?? "") ?? 0.0)))"
                lblCancellationChargesValue.text = "\(currency) \(String(format: "%.3f",(Double(paymentInfo?.cancellation_charges ?? "") ?? 0.0)))"
                
            }else{
                
                let baseFareValue = /Float(/currentOrd.payment?.productAplhaCharge)
                
                let timeValue = (/Float(/currentOrd.payment?.order_time)) * (/Float(/currentOrd.payment?.product_per_hr_charge))
                
                
                var waitingValue:Float = 0.0
                
                if template == .Corsa{
                    
                    waitingValue = (/Float(/currentOrd.payment?.waiting_charges))
                    
                }
//                else{
//                    waitingValue = (/Float(/currentOrd.payment?.waiting_time)) * (/Float(/currentOrd.payment?.waiting_charges))
//                }
              
                else{

                    waitingValue = (/Float(/currentOrd.payment?.waiting_charges))

                }
                
                
                let distanceValue = (/Float(/currentOrd.payment?.orderDistance)) * (/Float(/currentOrd.payment?.product_per_distance_charge))
                
                lblBaseFairValue.text = currency + "\(baseFareValue)".getTwoDecimalFloat()
                
                let time = "\(/currentOrd.payment?.order_time)".getTwoDecimalFloat()
                labelTime.text = "Time(\(time) min)"
                labelTimeValue.text = currency + "\(timeValue)".getTwoDecimalFloat()
                
                labelTime.isHidden = true
                labelTimeValue.isHidden = true
                
                
                //let waitingTime = "\(/currentOrd.payment?.waiting_time)".getTwoDecimalFloat()
                
                let countRideStops = /currentOrd.ride_stops?.count
                
//                var waitingTime = 0.000
//
//                if countRideStops != 0 {
//                    for i in 0..<countRideStops
//                    {
//                        let waitTime =  /currentOrd.ride_stops?[i].waiting_time
//
//                        waitingTime = waitingTime + (Double(waitTime) ?? 0.0)
//
//                    }
//                }
//
//                let waitingTime1 = "\(/currentOrd.waiting_time)".getTwoDecimalFloat()
//
//                waitingTime = waitingTime + (Double(waitingTime1) ?? 0.000)
//                
//                var waitingTime = 0.000
//                let waitingTimePayment = "\(/currentOrd.payment?.waiting_time)".getThreeDecimalFloat()
//                let minimum_waiting_time = (/UDSingleton.shared.appSettings?.appSettings?.minimum_waiting_time)
//                let minimum_waiting_time1 = (Double(minimum_waiting_time.getThreeDecimalFloat()) ?? 0.000)
//               
//                let waitingTimeOuter = (Double("\(/currentOrd.waiting_time)".getThreeDecimalFloat()) ?? 0.000)
//                
//                var balanceWaitingTime = 0.000
//                if waitingTimeOuter > minimum_waiting_time1{
//                    balanceWaitingTime = waitingTimeOuter - minimum_waiting_time1
//                }
//                
//                waitingTime = balanceWaitingTime + (Double(waitingTimePayment) ?? 0.0)
                
             
                let waitingTime = "\(/currentOrd.payment?.waiting_time)".getThreeDecimalFloat()
                
                if template == .Corsa{
                    
                    labelWaitingTime.text = "Waiting Charges"
                    labelWaitingTimeValue.text = currency + "\(waitingValue)".getTwoDecimalFloat()
                    
                } else{
                   // labelWaitingTime.text = "Waiting Charges"
                    labelWaitingTime.text = "Waiting Time(\(waitingTime) min)"
                    labelWaitingTimeValue.text = currency + "\(waitingValue)".getTwoDecimalFloat()
                }
                
             //   viewWaitingCharge.isHidden = waitingValue == 0.0
                
                let distance = "\(/currentOrd.payment?.orderDistance)".getTwoDecimalFloat()
                labelDistance.text = "Distance(\(distance) km)"
                labelDistanceValue.text = currency + "\(distanceValue)".getTwoDecimalFloat()
                
                var normalValue = baseFareValue + timeValue + distanceValue
                labelNormalFareValue.text = currency + "\(normalValue)".getTwoDecimalFloat()
                
                let airportCharge = Float(Double(currentOrd.payment?.airport_charges ?? "") ?? 0.0)
                let zoneCharge = Float(Double(currentOrd.payment?.zone_charges ?? "") ?? 0.0)
                let tollCharge = Float(Double(currentOrd.payment?.toll_charges ?? "") ?? 0.0)
                let parkingCharge = Float(Double(currentOrd.payment?.parking_charges ?? "") ?? 0.0)
                
                
                if airportCharge > 0.0{
                    
                    viewAirport.isHidden = false
                    lblAirportValue.text = currency +  "\(airportCharge)".getTwoDecimalFloat()
                }
                
                if zoneCharge > 0.0{
                    
                    viewZone.isHidden = false
                    lblZoneValue.text =  currency +  "\(zoneCharge)".getTwoDecimalFloat()
                }
                
                if (tollCharge + parkingCharge) > 0.0{
                    
                    viewTollParking.isHidden = false
                    lblTollParkingvalue.text =  currency +  "\(tollCharge + parkingCharge)".getTwoDecimalFloat()
                    
                }
                
                normalValue = normalValue + airportCharge + zoneCharge + tollCharge + parkingCharge + waitingValue
                var bookingCharge: Float
                var serviceCharges: Float
                
                // let actualValue = /brand?.productActualPrice
                let actualValue = (/Float(/currentOrd.payment?.productActualValue))
                let totalPercentage = ((/currentOrd.payment?.buraqPercentage)/100)
                let exactValue = normalValue + previousCharges
                if exactValue > actualValue {
                    bookingCharge = normalValue * totalPercentage
                    serviceCharges = 0.0
                }else {
                    bookingCharge = actualValue * totalPercentage
                    if exactValue > actualValue {
                        serviceCharges = exactValue - actualValue
                    }else{
                        serviceCharges = actualValue - exactValue
                    }
                }
                
                lblCheckListCharge.text = currency + "\(/currentOrd.check_list_total)"
    //            labelBookingFeeValue.text = currency + "\(bookingCharge)".getTwoDecimalFloat()
                labelBookingFeeValue.text = currency + "\(/currentOrd.payment?.adminCharge)".getTwoDecimalFloat()
                
                viewBookingFee.isHidden = bookingCharge == 0.0
                
                labelServiceChargeValue.text = currency + "\(serviceCharges)".getTwoDecimalFloat()
                
                viewServiceCharge.isHidden = serviceCharges == 0.0
                
                let subtotal = normalValue + bookingCharge + serviceCharges  + (Float(currentOrd.payment?.sur_charge ?? "0.0") ?? 0.0) + previousCharges
                lblSurCharge.text = currency + "\(/currentOrd.payment?.sur_charge)".getTwoDecimalFloat()
                
                viewSurchage.isHidden = /(Double(currentOrd.payment?.sur_charge ?? "0.0")) == 0.0
                
    //            labelSubTotalValue.text = currency + "\(subtotal)".getTwoDecimalFloat()
                labelSubTotalValue.text = currency + "\(/currentOrd.payment?.finalCharge)".getTwoDecimalFloat()
                lblFinalAmount.text =  currency + (/currentOrd.payment?.finalCharge).getTwoDecimalFloat()
                
                var tipAmount = ""
                if /currentOrd.payment?.tip == 0
                    {
                    tipAmount = "0.000"
                }
                else
                {
                    tipAmount = "\(currentOrd.payment?.tip ?? 0)".getThreeDecimalFloat()
                }
                
                lblTipsAmount.text = currency + tipAmount
                
                //lblTipsAmount.text = currency + (/currentOrd.payment?.tip).getTwoDecimalFloat()
                lblCancellationChargesValue.text = currency + (/currentOrd.payment?.cancellation_charges).getTwoDecimalFloat()
                
            }
            
        }
  
        // lblWaitingCharges.text = currency + (currentOrd.payment?.waiting_charges ?? "")
        
        if serviceRequest?.isPool ?? false{
            viewSurchage.isHidden = true
        }
        
        if (currentOrd.payment?.coupon_charge ?? 0.0) > 0.0 {
            if  let couponType = currentOrd.coupon_detail?.couponType{
                let code = currentOrd.coupon_detail?.code ?? ""
                let value = Double(currentOrd.coupon_detail?.amountValue ?? "") ?? 0
                if couponType == "Value"{
                    couponLabel.text = "Promocode applied of \(code) value of \(/UDSingleton.shared.appSettings?.appSettings?.currency)\(value)"
                }else{
                    couponLabel.text = "Promocode applied of \(code) value of \(value)%"
                }
            }else{
                couponLabel.text = ""
            }
            
        }else{
            couponLabel.minimumScaleFactor = 0.5
                couponLabel.text = "Promo cannot be applied because ride price is less "
        }
        
        if APIBasePath.isShipUsNow {
            stackTip.isHidden = true
            viewSurchage.isHidden = true
            viewWaitingCharge.isHidden = true
        }
        
        
        /*  guard let service = UDSingleton.shared.getService(categoryId: currentOrd.serviceId) else {return}
         guard let payment = currentOrd.payment else{return}
         
         
         if /service.serviceCategoryId == 2 || /service.serviceCategoryId == 4 {
         lblBrandName.text = /currentOrd.orderProductDetail?.productBrandName
         }else{
         lblBrandName.text = /service.serviceName
         }
         
         
         let productName  = /service.serviceCategoryId > 3 ? (/currentOrd.orderProductDetail?.productName)   : (/currentOrd.orderProductDetail?.productName + " × " +  String(/payment.productQuantity)) */
        
        
        
        
        
        
        //        lblBaseFairValue.text =  (/payment.initalCharge).getTwoDecimalFloat() + " " + "currency".localizedString
        //        lblTaxValue.text = (/payment.adminCharge).getTwoDecimalFloat() + " " + "currency".localizedString
        //        lblFinalAmount.text =  (/payment.finalCharge).getTwoDecimalFloat() + " " + "currency".localizedString
        
        /* var total : Double = 0.0
         
         if let initalCharge = Double(/payment.initalCharge) {
         total =  initalCharge
         }
         
         if let adminCharge = Double(/payment.adminCharge) {
         total =  total + adminCharge
         } */
        
        
        
    }
}


extension InvoiceView {
    
    @IBAction func showTip(_ sender : UIButton) {
        delegate?.didSelectAddTip(order : self.orderDone!)
    }
    
}
