//
//  Service.swift
//  Buraq24
//
//  Created by MANINDER on 17/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import ObjectMapper

class Service: Mappable {
    
    var buraqPercentage : Float?
    var serviceDescription : String?
    var brands : [Brand]?
    var serviceCategoryId : Int?
    var serviceName : String?
    var serviceCategoryType : String?
    var defaultBrands : String?
    var brandName:[String]?
    var brandDetails : [BrandDetail]?
     var booking_flow: String?
    var image:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        serviceDescription <- map["description"]
        brands <- map["brands"]
        serviceCategoryId <- map["category_id"]
        serviceName <- map["name"]
        serviceCategoryType <- map["category_type"]
        defaultBrands <- map["default_brands"]
        buraqPercentage <- map["buraq_percentage"]
        booking_flow <- map["booking_flow"]
        image <- map["image"]
        
        
//        var buraqPercent : Float?
//        orderstatus <- map["order_status"]
//         buraqPercent <- map["buraq_percentage"]
//
//
//
//        if

        brandName = brands?.map({/$0.brandName })
    }
}
