//
//  Payment.swift
//  Buraq24
//
//  Created by MANINDER on 31/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import ObjectMapper

class Payment: Mappable {
    
    
    var paymentType : String?
    var productWeight : String?
    var productPerQuantityCharge : String?
    var refundStatus : String?
    var initalCharge : String?
    var adminCharge : String?
    var bottleCharge : String?
    var refundId : String?
    var transactionId: String?
    var paymentId : Int?
    var productActualValue : String?
    var orderDistance : String?
    var productPerWeightCharge :String?
    var bottleReturnedValue : Int?
    var finalCharge : String?
    var bankCharge : String?
    var productPerDistanceCharge :String?
    var productStatus : String?
    var productQuantity : Int?
    var productAplhaCharge : String?
    var buraqPercentage : Float?
    var order_time: String?
    var waiting_time: String?
    var waiting_charges:String?
    var previous_charges:String?
    var price_per_km : String?
    var price_per_min : String?
    var extra_distance : String?
    var extra_time : String?
    var distance_price_fixed : String?
    var product_per_distance_charge : String?
    var product_per_hr_charge : String?
    var paymentBody: String?
    var tipCharge : String?
    var sur_charge: String?
    var tip: Int?
    var parking_charges:String?
    var parking_images:[String]?
    var toll_charges:String?
    var toll_images:[String]?
    var tollImageStr:String?
    var parkingImageStr:String?
    var airport_charges:String?
    var zone_charges:String?
    var previous_driver_charges:String?
    var coupon_charge:Float?
    var product_detail:[Product]?
    var product_details:String?
    var driverSignature:String?
    var driverProductImage:String?
    var credit_point_used:String?
    var payment_status:String?
    var card_type:String?
    var cancellation_charges:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        var strType : String?
        payment_status <- map["payment_status"]
        paymentType <- map["payment_type"]
        //guard let type = strType else {return}
        card_type <- map["card_type"]
        
        if card_type == nil{
            var num:Int?
            num <- map["card_type"]
            if let num = num{
                card_type = String(num)
            }
        }
        
        var charge:String?
        tipCharge <- map["tipCharge"]
        tip <- map["tip"]
        cancellation_charges <- map["cancellation_charges"]
        credit_point_used <- map["credit_point_used"]
        charge <- map["coupon_charge"]
        product_detail <- map["product_detail"]
        coupon_charge = Float(/charge)
        // paymentType = PaymentType(rawValue: type)!
        airport_charges <- map["airport_charges"]
        zone_charges <- map["zone_charges"]
        previous_driver_charges <- map["previous_driver_charges"]
        parkingImageStr <- map["parking_images"]
        tollImageStr <- map["toll_images"]
        toll_charges <- map["toll_charges"]
        parking_charges <- map["parking_charges"]
        paymentBody <- map["payment_body"]
        productWeight <- map["product_weight"]
        productPerQuantityCharge <- map["product_per_quantity_charge"]
        refundStatus <- map["refund_status"]
        initalCharge <- map["initial_charge"]
        adminCharge <- map["admin_charge"]
        bottleCharge <- map["bottle_charge"]
        refundId <- map["refund_id"]
        transactionId <- map["transaction_id"]
        paymentId <- map["payment_id"]
        productActualValue <- map["product_actual_value"]
        orderDistance <- map["order_distance"]
        productPerWeightCharge <- map["product_per_weight_charge"]
        bottleReturnedValue <- map["bottle_returned_value"]
        finalCharge <- map["final_charge"]
        bankCharge <- map["bank_charge"]
        productPerDistanceCharge <- map["product_per_distance_charge"]
        productStatus <- map["payment_status"]
        productQuantity <- map["product_quantity"]
        productAplhaCharge <- map["product_alpha_charge"]
        buraqPercentage <- map["buraq_percentage"]
        order_time <- map["order_time"]
        price_per_km <- map["price_per_km"]
        price_per_min <- map["price_per_min"]
        extra_distance <- map["extra_distance"]
        extra_time <- map["extra_time"]
        distance_price_fixed <- map["distance_price_fixed"]
        waiting_charges <- map["waiting_charges"]
        previous_charges <- map["previous_charges"]
        waiting_time <- map["waiting_time"]
        product_per_hr_charge <- map["product_per_hr_charge"]
        product_per_distance_charge <- map["product_per_distance_charge"]
        sur_charge <- map["sur_charge"]
        driverSignature <- map["sur_charge"]
        driverProductImage <- map["sur_charge"]
        
        toll_images = convertToStringArray(text: /tollImageStr) as? [String]
        parking_images = convertToStringArray(text: /parkingImageStr) as? [String]
        
        
        product_details <- map["product_detail"]
        
        if let data = product_details?.data(using: .utf8){
            do {
                product_detail = try JSONDecoder().decode(Array<Product>.self, from: data)
            } catch {
                print(error)
            }

        }
        
    }
}


func convertToStringArray(text: String) -> Any? {

   let data = text.data(using: .utf8)!
    do {
        if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .fragmentsAllowed) as? [String]
        {
           print(jsonArray)
            return jsonArray
        } else {
            print("bad json")
            return nil
        }
    } catch let error as NSError {
        print(error)
    }
     return nil

}

class Product:Decodable {
//    required init?(map: Map) {
//
//    }
    
    
    var address: String?
    var product_weight: String?
    var url_image: String?
    var product_name: String?
    var receiver_name: String?
    var additional_info: String?
    var addressType: String?
    var receiver_number: String?
    var store_name:String?
    var store_number:String?
//
//
//
//    func mapping(map: Map) {
//        address <- map["address"]
//        product_weight <- map["product_weight"]
//        url_image <- map["url_image"]
//        product_name <- map["product_name"]
//        receiver_name <- map["receiver_name"]
//        additional_info <- map["additional_info"]
//        addressType <- map["addressType"]
//        receiver_number <- map["receiver_number"]
//
//    }
//
}
