//
//  OrderCab.swift
//  Buraq24
//
//  Created by MANINDER on 18/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import ObjectMapper


enum OrderTurn : String {
    case MyTurn = "1"
    case OtherCustomerTurn = "0"
    
}


class RatingCab : Mappable {
    
    
    var comment  : String?
    var ratingGiven : Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        comment <- map["comments"]
        ratingGiven <- map["ratings"]
    }
}

class OrderDates : Mappable{
    
    
    var startedDate : Date?
    var completedDate : Date?
    

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        var strAccepted : String?
        strAccepted <- map["accepted_at"]
        
        var strCompleted : String?
        strCompleted <- map["updated_at"]
        guard let utcAccDateStr = strAccepted else{return}
        startedDate = utcAccDateStr.getLocalDate()
        guard let utcCompletedDateStr = strCompleted else{return}
        completedDate = utcCompletedDateStr.getLocalDate()
    }
}

class Address:Mappable{
    required init?(map: Map) {
        
    }
    
    
    var landmark_info:String?
    var receiver_number:String?
    var product_weight:String?
    var latitude:String?
    var address:String?
    var receiver_name:String?
    var longitude:String?
    var additional_info:String?
    var product_name:String?
    var addressType:String?
    var url_image:String?
    
    
    func mapping(map: Map) {
        landmark_info <- map["landmark_info"]
        receiver_number <- map["receiver_number"]
        product_weight <- map["product_weight"]
        latitude <- map["latitude"]
        address <- map["address"]
        receiver_name <- map["receiver_name"]
        longitude <- map["longitude"]
        additional_info <- map["additional_info"]
        product_name <- map["product_name"]
        addressType <- map["addressType"]
        url_image <- map["url_image"]
    }
    
}


class OrderCab: Mappable {
    
    var orderId : Int?
    var customerOrganisationId : Int?
    var refundStatus : String?
    var initialCharge : String?
    var continuousOrderId : Int?
    var material_details: String?
    var details: String?
    var bottleCharge : String?
    var bottleReturned : String?
    var userType : UserType = .Customer
    var bottleReturnedValue : Int?
    // var paymentStatus : PaymentStatus = .Pending
    
    var serviceId: Int?
    var serviceBrandId : Int?
    var categoryBrandProductId : Int?
    
    var userCardId : Int?
    var bookingType : BookingType = .Present
    var paymentType : PaymentType = .Cash
    var orderStatus : OrderStatus = .Searching
    
    var pickUpLongitude : Double?
    var pickUpLatitude : Double?
    var pickUpAddress : String?
    var order_images_url: [imagesArray]?
    
    var dropOffAddress  : String?
    var dropOffLatitude : Double?
    var dropOffLongitude : Double?
    var breakdown_latitude : Double?
    var breakdown_longitude : Double?
    
    
    var orderToken : String?
    var orderTimings : String?
    var orderLocalDate : Date?
    var orderDate : Date?
    var cancelReason : String?
    
    var finalCharge : String?
    var productQuantity :  Int?
    var orderProductDetail : OrderProductDetail?
    var driverAssigned : Driver?
    var payment : Payment?
    var rating : RatingCab?
    var delivery_person_name:String?
    var pickup_person_name: String?
    var orderDates : OrderDates?
    var pickup_person_phone: String?
    var myTurn : OrderTurn = .MyTurn
    var isDirectSearching = false
    var product_weight: String?
    var organisationCouponUserId :Int?
    var booking_type : String?
    
    var isContinueFromBreakdown: Bool?
    var shareWith : [ContactNumberModal]?
    var updated_at: String?
    var accepted_at: String?
    
    var orderTurn : OrderTurn  = .MyTurn
    var statusCode: Int? // to check pending balance for last ride
    
    var cancellation_charges: Float?
    var payment_id: Int?
    var ride_stops: [Stops]?
    var check_lists:[CheckLists]?
    var check_list_total: Int?
    var exactPath:String?
    var order_images: [imagesArray]?
    var coupon_detail:Coupon?
    var url: String?
    
    var friend_phone_number: String?
    var friend_user_id: Int?
    var friend_name: String?
    var friend_order_status:String?
    var customer: Customer?
    var delivery_proof1:String?
    var delivery_proof2:String?
    var customer_signature:String?
    var waiting_time : String?
    
    var driver_current_latitude : Double?
    var driver_current_longitude : Double?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        driver_current_latitude <- map["driver_current_latitude"]
        driver_current_longitude <- map["driver_current_longitude"]
        
        
        waiting_time <- map["waiting_time"]
        exactPath <- map["exact_path"]
        url <- map["url"]
        order_images_url <- map["order_images_url"]
        
         order_images <- map["order_images"]
        
        if order_images_url == nil{
            
            order_images_url = order_images
        }
        breakdown_latitude <- map["breakdown_latitude"]
        breakdown_longitude <- map["breakdown_longitude"]
        serviceBrandId <- map["category_brand_id"]
        categoryBrandProductId <- map["category_brand_product_id"]
        details <- map["details"]
        product_weight <- map["product_weight"]
        pickup_person_phone <- map["pickup_person_phone"]
        pickup_person_name <- map["pickup_person_name"]
        delivery_person_name <- map["delivery_person_name"]
        orderId <- map["order_id"]
        material_details <- map["material_details"]
        customerOrganisationId <- map["customer_organisation_id"]
        refundStatus <- map["refund_status"]
        initialCharge <- map["initial_charge"]
        continuousOrderId <- map["continuous_order_id"]
        serviceId <- map["category_id"]
        bottleCharge <- map["bottle_charge"]
        bottleReturned <- map["bottle_returned"]
        bottleReturnedValue <- map["bottle_returned_value"]
        productQuantity <- map["product_quantity"]
        cancelReason <- map["cancel_reason"]
        orderTimings <- map["order_timings"]
        shareWith <- map["shareWith"]
        updated_at <- map["updated_at"]
        accepted_at <- map["accepted_at"]
        statusCode <- map["statusCode"]
        cancellation_charges <- map["cancellation_charges"]
        payment_id <- map["payment_id"]
        ride_stops <- map["ride_stops"]
       
        
        guard let utcDateStr = orderTimings else{return}
        
        orderLocalDate = utcDateStr.getLocalDate()
        
        finalCharge <- map["final_charge"]
        orderToken <- map["order_token"]
        userCardId <- map["user_card_id"]
        
        organisationCouponUserId <- map["organisation_coupon_user_id"]

        pickUpLongitude <- map["pickup_longitude"]
        pickUpLatitude <- map["pickup_latitude"]
        pickUpAddress <- map["pickup_address"]
        dropOffAddress  <- map["dropoff_address"]
        dropOffLatitude <- map["dropoff_latitude"]
        dropOffLongitude <- map["dropoff_longitude"]
        orderProductDetail <- map["brand"]
        driverAssigned <- map["driver"]
        payment <- map["payment"]
        rating <- map["ratingByUser"]
        booking_type <- map["booking_type"]
        check_lists <- map["check_lists"]
        
        var paymentTypeStr:String?
        paymentTypeStr <- map["payment_type"]
        
        paymentType = PaymentType(rawValue: /paymentTypeStr) ?? .Cash
        
        var orderstatus : String?
        orderstatus <- map["order_status"]
        
        guard let status = orderstatus else{return}
        orderStatus = OrderStatus(rawValue: /status) ?? .Searching
        
        var orderTurn : String?
        orderTurn  <- map["my_turn"]
        
        guard let turn = orderTurn else{return}
        myTurn = OrderTurn(rawValue: /turn)!
        
        orderDates  <- map["cRequest"]
        
        
        var strFuture : String?
        strFuture <- map["future"]
         guard let future = strFuture else{return}
        bookingType = BookingType(rawValue: /future)!
        coupon_detail  <- map["coupon_detail"]
        
        customer <- map["Customer"]
        friend_phone_number <- map["friend_phone_number"]
        friend_user_id <- map["friend_user_id"]
        friend_name <- map["friend_name"]
        friend_order_status <- map["friend_order_status"]
        delivery_proof2 <- map["delivery_proof2"]
        delivery_proof1 <- map["delivery_proof1"]
        customer_signature <- map["customer_signature"]
        
    }
    
}
class Customer: Mappable {
    
    var phone_number:Int?
    var name:String?
    
    required init?(map: Map){ }
    
    func mapping(map: Map) {
      
        name <- map["name"]
        phone_number <- map["phone_number"]
       
        
    }
}
class CheckLists: NSObject, Mappable {
    
    var after_item_price: String?
    var before_item_price: String?
    var check_list_id: Int?
    var created_at: String?
    var item_name: String?
    var order_id: String?
    var updated_at: String?
    var user_detail_id: String?
    var tax: Int?
    var price: String?
    
    required convenience init?(map: Map) {
           self.init()
       }
    
    func mapping(map: Map) {
        after_item_price <- map["after_item_price"]
        before_item_price <- map["before_item_price"]
        check_list_id <- map["check_list_id"]
        created_at <- map["created_at"]
        item_name <- map["item_name"]
        order_id <- map["order_id"]
        updated_at <- map["updated_at"]
        user_detail_id <- map["user_detail_id"]
        tax <- map["tax"]
        price <- map["price"]
    }
    
}


class CheckListModel {
      var after_item_price: String?
      var before_item_price: String?
      var check_list_id: Int?
      var created_at: String?
      var item_name: String?
      var order_id: String?
      var updated_at: String?
      var user_detail_id: String?
      var tax: Int?
      var price: String?
    
    
    init(afteritemprice: String?, beforeitemprice: String?, checklistid: Int?, createdat: String?, itemname: String?, orderid: String?, updatedat: String?, userdetailid: String?, tax: Int?, price: String?) {
        self.after_item_price = afteritemprice
        self.before_item_price = beforeitemprice
        self.check_list_id = checklistid
        self.created_at = createdat
        self.item_name = itemname
        self.order_id = orderid
        self.updated_at = updatedat
        self.user_detail_id = userdetailid
        self.tax = tax
        self.price = price
    }
}


class Stops: NSObject, Mappable {
    
    var latitude: Double?
    var longitude: Double?
    var priority: Int?
    var address: String?
    var added_with_ride: String?
    var stop_status: String?
    var ride_stop_id : Int?
    var delivery_proof1:String?
    var delivery_proof2:String?
    var waiting_time : String?
    required init?(map: Map) {}
       
    func mapping(map: Map) {
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        priority <- map["priority"]
        address <- map["address"]
        stop_status <- map["stop_status"]
        added_with_ride <- map["added_with_ride"]
        ride_stop_id <- map["ride_stop_id"]
        delivery_proof1 <- map["delivery_proof1"]
        delivery_proof2 <- map["delivery_proof2"]
        waiting_time <- map["waiting_time"]
    }
    
    
    init(latitude: Double?, longitude: Double?, priority: Int?, address: String?) {
        self.latitude = latitude
        self.longitude = longitude
        self.priority = priority
        self.address = address
        self.stop_status = ""
        self.ride_stop_id = nil
        self.added_with_ride = ""
    }
}

class ShareWith : NSObject, Mappable{

    var name : String?
    var phoneCode : String?
    var phoneNumber : String?


    class func newInstance(map: Map) -> Mappable?{
        return ShareWith()
    }
    private override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
    {
        name <- map["name"]
        phoneCode <- map["phone_code"]
        phoneNumber <- map["phone_number"]
        
    }
}


class OrderProductDetail : Mappable {
    
    var productName : String?
    var productDescription : String?
    var pricePerWeight : Float?
    var productCategoryId : Int?
    var pricePerDistance : Float?
    var productBrandId : Int?
    var productPricePerQuantity : Float?
    var productActualPrice : Float?
    var productAlphaPrice : Float?
    var productId : Int?
    var productBrandName : String?
    var category_name :String?
    var price_per_hr: Float?
    var details:String?
    
    var breakdown:String?
    var half_way_stop:String?
    var is_check_list:String?
    var is_seating_capacity:String?
    var panic:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        productName <- map["name"]
        productDescription <- map["description"]
        pricePerWeight <- map["price_per_weight"]
        productCategoryId <- map["category_id"]
        pricePerDistance <- map["price_per_distance"]
        productBrandId <- map["category_brand_id"]
        productPricePerQuantity <- map["price_per_quantity"]
        productActualPrice <- map["actual_value"]
        productAlphaPrice <- map["alpha_price"]
        productId <- map["category_brand_product_id"]
        productBrandName <- map["brand_name"]
        category_name <- map["category_name"]
        price_per_hr <- map["price_per_hr"]
        details <- map["details"]
        
        breakdown <- map["breakdown"]
        half_way_stop <- map["half_way_stop"]
        is_check_list <- map["is_check_list"]
        is_seating_capacity <- map["is_seating_capacity"]
        panic <- map["panic"]
    }
}

class imagesArray:NSObject,Mappable{
    
    var image:String?
    var order_id:Int?
    var image_url:String?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        image <- map["image"]
        order_id <- map["order_id"]
        image_url <- map["image_url"]
    }
}
