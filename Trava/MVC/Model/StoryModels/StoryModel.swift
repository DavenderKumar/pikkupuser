//
//  StoryModel.swift
//  RoyoRide
//
//  Created by Prashant on 07/07/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import Foundation
import ObjectMapper


class StoryModel: Mappable {
    var duration : Int?
    var thumbnail : String?
    var video_url: String?
    var created_at : String?
    var name : String?
    var updated_at : String?
    var image_url: String?
    var category_id: Int?
    var id : Int?
    
    required init?(map: Map) {
           
       }
       
    func mapping(map: Map) {
        duration <- map["duration"]
        thumbnail <- map["thumbnail"]
        video_url <- map["video_url"]
        created_at <- map["created_at"]
        name <- map["name"]
        updated_at <- map["updated_at"]
        image_url <- map["image_url"]
        category_id <- map["category_id"]
        id <- map["id"]
        
        if /video_url != ""{
            video_url = APIBasePath.StoryBaseUrl + /video_url
        }
        if /image_url != ""{
            image_url = APIBasePath.StoryBaseUrl + /image_url
        }
    }
}

