//
//  ZoneModel.swift
//  RoyoRide
//
//  Created by Prashant on 14/08/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

class ZoneModel: NSObject,Mappable {
    
    var airport_charge_fee : String?
    var airport_charges : String?
    var toll_charges_fee : String?
    var toll_charges : String?
    var zone_charges : String?
    var zone_charges_fee : String?
    var zone_name: String?
    var zoom_level: Int?
    var id:Int?
    var coordinates:String?
    var coordinatesArray = [CLLocationCoordinate2D]()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        airport_charge_fee <- map["airport_charge_fee"]
        airport_charges <- map["airport_charges"]
        toll_charges_fee <- map["toll_charges_fee"]
        toll_charges <- map["toll_charges"]
        zone_charges <- map["zone_charges"]
        zone_charges_fee <- map["zone_charges_fee"]
        zone_name <- map["zone_name"]
        zoom_level <- map["zoom_level"]
        coordinates <- map["coordinates"]
        

               
            
                  
        coordinates = coordinates?.replacingOccurrences(of: "\"[", with: "[")
        coordinates = coordinates?.replacingOccurrences(of: "]\"", with: "]")
        
       // coordinatesArray =  coordinates?.parseJSONString as? [[String:Double]]
        
       // print(coordinatesArray)
        


        if let coordinatesStr = coordinates {
             
            if let jsonAray = convertToDictionary(text: coordinatesStr) as? [[String:Double]] {
                
                for dict in jsonAray{
                    
                    if let lat = dict["lat"], let lng = dict["lng"] {
                        
                        let location = CLLocationCoordinate2DMake(lat, lng)
                        coordinatesArray.append(location)
                    }
                    
                }
            }
        }
        
    }
    
}

func convertToDictionary(text: String) -> Any? {

   let data = text.data(using: .utf8)!
    do {
        if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .fragmentsAllowed) as? [Dictionary<String,Any>]
        {
           print(jsonArray)
            return jsonArray
        } else {
            print("bad json")
            return nil
        }
    } catch let error as NSError {
        print(error)
    }
     return nil

}
