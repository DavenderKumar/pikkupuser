//
//  CreditCardModel.swift
//  RoyoRide
//
//  Created by Prashant on 04/06/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import Foundation

class CreditCard {
    
    var name:String?
    var cardNumber:String?
    var expMonth:UInt?
    var expYear:UInt?
    var expMonthStr:String?
    var expYearStr:String?
    var cvv:String?
    var token:String?
}
