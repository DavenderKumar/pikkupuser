//
//  AddLocationFromMapViewController.swift
//  RoyoRide
//
//  Created by Ankush on 13/05/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit
import GoogleMaps
import IBAnimatable
import Mapbox

import GooglePlaces
import Alamofire
import CoreLocation

protocol AddLocationFromMapViewControllerDelegate: class {
    func locationSelectedFromMap(address: String?, name: String?, latitude: Double?, longitude: Double?, locationType: Int,isFromStop:Bool)
    func getDataFromAnotherVC(address: String?, name: String?, latitude: Double?, longitude: Double?, getCountry:String,getLocality:String,getSublocality:String)
    func getDataFromAnotherVC1(address: String?, name: String?, latitude: Double?, longitude: Double?, getCountry:String,getLocality:String,getSublocality:String,strComingFrom:String?)
}



class AddLocationFromMapViewController: UIViewController {
    
    // MARK:- Outlet
    @IBOutlet var viewMapContainer: UIView!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var imgViewPickingLocation: UIImageView!
    @IBOutlet var viewConfirmPickUp: ConfirmPickupView!
    @IBOutlet weak var textFieldPickUpLocation: AnimatableTextField!
    @IBOutlet weak var buttonDone: UIButton!
    
    @IBOutlet weak var viewNavigation: UIView?
    @IBOutlet weak var viewStatusBar: UIView?
    @IBOutlet var buttonTitle: UIButton?
    
     @IBOutlet var btnNewBack: UIButton?
    @IBOutlet weak var mapBoxView: MGLMapView!
    
    var comingFrom = ""
    
    //MARK:- Properties
    var isMapLoaded : Bool = false
    var mapDataSource:GoogleMapsDataSource?
    
    var request : ServiceRequest?
    var locationType: Int!
    
    var address: String?
    var name: String?
    var latitude: Double?
    var longitude: Double?
    var isFromStop = false
    var isCurrentLocationUpdated: Bool?
    var countryName : String?
    var locality : String?
    var sublocality : String?
    
    var getAddressIds : Int?
    var getNickName : String = ""
    
    weak var delegate: AddLocationFromMapViewControllerDelegate?
    var mapType =  UDSingleton.shared.appSettings?.appSettings?.mapType
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if comingFrom != ""
            {
            
            let latvalue = latitude ?? 0.0
            let longValue = longitude ?? 0.0
            let currentLocation = GMSCameraPosition.camera(withLatitude:latitude ?? 0.0,longitude:longitude ?? 0.0,zoom: 14.0)
            
            if latvalue != 0.0  &&  longValue != 0.0{
                self.mapView.camera = currentLocation
                
                if !isMapLoaded {
                    configureMapView()
                    isMapLoaded = !isMapLoaded
                }
            }
        }
        else{
            if !isMapLoaded {
                configureMapView()
                isMapLoaded = !isMapLoaded
              
            }
        }
        
       
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
               
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
               switch template {
               case .Moby?:
                   return .lightContent
                   
               default:
                   return .default
               }
       }
    
    
    @IBAction func action_openLocationPlaceHolder(_ sender: UIButton) {
//        if comingFrom != ""
//        {
//            
//        }
        
        autocompleteClicked()
        
    }
    
    
    func autocompleteClicked() {
        
        GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
            
//            self?.txtDropOffLocation.text = place?.formattedAddress
//            self?.serviceRequest.locationNickName = place?.name
//            self?.serviceRequest.locationName = place?.formattedAddress
//            self?.serviceRequest.latitude =  /place?.coordinate.latitude
//            self?.serviceRequest.longitude =  /place?.coordinate.longitude
            
            //  self?.setCameraGoogleMap(latitude: /place?.coordinate.latitude, longitude: /place?.coordinate.longitude)
            
            let lat = place?.coordinate.latitude
            let lon = place?.coordinate.longitude
            print("lat lon",lat,lon)
        
        
            self?.latitude = lat
            self?.longitude = lon
        
        DispatchQueue.main.async {
            let latvalue = self?.latitude ?? 0.0
            let longValue = self?.longitude ?? 0.0
            let currentLocation = GMSCameraPosition.camera(withLatitude:self?.latitude ?? 0.0,longitude:self?.longitude ?? 0.0,zoom: 14.0)

            if latvalue != 0.0  &&  longValue != 0.0{
                self?.mapView.camera = currentLocation

                if !(self?.isMapLoaded ?? false) {
                    self?.configureMapView()
                    self?.isMapLoaded = !(self?.isMapLoaded ?? false)
                }
            }
        }
            
            
//            if place?.name != nil {
//                self?.tfAddressNickname.text = "\(String(describing:place?.name))"
//            }
//
//            if place?.formattedAddress != nil {
//                self?.tfArea.text = "\(String(describing: place?.formattedAddress))"
//            }
//
//
//            let lat = place?.coordinate.latitude
//            let lon = place?.coordinate.longitude
//            print("lat lon",lat,lon)
//
//
//            self?.latValue = lat ?? 0.0
//            self?.longvalue = lon ?? 0.0
        }
        
        
        
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//
//        // Specify the place data types to return.
//        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
//          UInt(GMSPlaceField.placeID.rawValue) |
//            UInt(GMSPlaceField.coordinate.rawValue) |
//            GMSPlaceField.addressComponents.rawValue |
//            GMSPlaceField.formattedAddress.rawValue)!
//        autocompleteController.placeFields = fields
//
//        //let text = "Search Address"
////        let views = autocompleteController.view.subviews
////                    let subviewsOfSubview = views.first!.subviews
////                    let subOfNavTransitionView = subviewsOfSubview[1].subviews
////                    let subOfContentView = subOfNavTransitionView[2].subviews
////
////                    if let searchBar = subOfContentView[0] as? UISearchBar {
////                        searchBar.text = text
////                        searchBar.delegate?.searchBar?(searchBar, textDidChange: text)
////                    } else {
////                        let searchBar: UISearchBar? = autocompleteController.view.getViewWithConcreteType()
////                        searchBar?.text = text
////                        searchBar.map { $0.delegate?.searchBar?($0, textDidChange: text) }
////                    }
//
//
//        if let searchBar = (autocompleteController.view.subviews
//           .flatMap { $0.subviews }
//           .flatMap { $0.subviews }
//           .flatMap { $0.subviews }
//           .filter { $0 == $0 as? UISearchBar}).first as? UISearchBar {
//                    searchBar.text = "Search Address"
//                    searchBar.delegate?.searchBar?(searchBar, textDidChange: "Search Address") // to get the autoComplete Response
//
//            }
//
//        // Specify a filter.
//        let filter = GMSAutocompleteFilter()
//        filter.type = .address
//        autocompleteController.autocompleteFilter = filter
//
//        // Display the autocomplete view controller.
//        present(autocompleteController, animated: true, completion: nil)
      }
    
    
    
    
}

//MARK:- Function
extension AddLocationFromMapViewController {
    
    func initialSetup() {
        
        setupUI()
    }
    
    func setupUI() {
        
        
        if self.mapType == .google{
            
            mapView.isHidden = false
            mapBoxView.isHidden = true
            
        } else{
            
            mapView.isHidden = true
            mapBoxView.isHidden = false
        }
        
        buttonDone.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        
        
       
        
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
        switch template {
        case .Moby?:
            viewNavigation?.isHidden = false
            viewStatusBar?.setViewBackgroundColorHeader()
            
        case .DeliverSome?:
            viewNavigation?.isHidden = false
            viewStatusBar?.setViewBackgroundColorHeader()
            
        default:
            viewNavigation?.isHidden = true
            viewStatusBar?.isHidden = true 
        }
        
        buttonTitle?.setButtonWithTitleColorHeaderText()
        viewNavigation?.setViewBackgroundColorHeader()
        
        
        btnNewBack?.setButtonWithTintColorHeaderText()
    }
    
    func configureMapView() {
        
        let didUpdateCurrentLocation : DidUpdatecurrentLocation
        
        if comingFrom != "" {
            didUpdateCurrentLocation  = {[weak self] (manager,locations) in  //current location closure
                
                if self?.mapType == .google{
                    
                    let currentLocation = GMSCameraPosition.camera(withLatitude: self?.latitude ?? 0.0,
                                                                   longitude: self?.longitude ?? 0.0,
                                                                   zoom: 14.0)
                    if !(/self?.isCurrentLocationUpdated) {
                        
                        self?.mapView.camera = currentLocation
                        self?.isCurrentLocationUpdated = true
                        
                        
                        self?.mapDataSource?.getAddressFromlatLong(lat: self?.latitude ?? 0.0, long: self?.longitude ?? 0.0, completion: {  [weak self](strLocationName, country, name,locality , sublocality)  in
                            
                            self?.setUpdatedAddress(location: strLocationName, name: name, coordinate: manager.location?.coordinate,getCountry:country,getLocality: locality,getSublocality: sublocality)
                            
                            
                        })
                    }
                    
                } else{
                    
                    if !(/self?.isCurrentLocationUpdated) {
                        
                        self?.isCurrentLocationUpdated = true
                        
                        
                        self?.mapDataSource?.getAddressFromlatLong(lat: self?.latitude ?? 0.0, long: self?.longitude ?? 0.0, completion: {  [weak self](strLocationName, country, name,locality , sublocality)  in
                            
                            self?.mapBoxView.setCenter(CLLocationCoordinate2DMake(self?.latitude ?? 0.0, self?.longitude ?? 0.0), animated: true)
                        })
                    }
                    
                }
            }
        }
        else
        {
            didUpdateCurrentLocation  = {[weak self] (manager,locations) in  //current location closure
                
                if self?.mapType == .google{
                    
                    let currentLocation = GMSCameraPosition.camera(withLatitude: /manager.location?.coordinate.latitude,
                                                                   longitude: /manager.location?.coordinate.longitude,
                                                                   zoom: 14.0)
                    if !(/self?.isCurrentLocationUpdated) {
                        
                        self?.mapView.camera = currentLocation
                        self?.isCurrentLocationUpdated = true
                        
                        
                        self?.mapDataSource?.getAddressFromlatLong(lat: /manager.location?.coordinate.latitude, long: /manager.location?.coordinate.longitude, completion: {  [weak self](strLocationName, country, name,locality , sublocality)  in
                            
                            self?.setUpdatedAddress(location: strLocationName, name: name, coordinate: manager.location?.coordinate,getCountry:country,getLocality: locality,getSublocality: sublocality)
                            
                            
                        })
                    }
                    
                } else{
                    
                    if !(/self?.isCurrentLocationUpdated) {
                        
                        self?.isCurrentLocationUpdated = true
                        
                        
                        self?.mapDataSource?.getAddressFromlatLong(lat: /manager.location?.coordinate.latitude, long: /manager.location?.coordinate.longitude, completion: {  [weak self](strLocationName, country, name,locality , sublocality)  in
                            
                            self?.mapBoxView.setCenter(CLLocationCoordinate2DMake(/manager.location?.coordinate.latitude, /manager.location?.coordinate.longitude), animated: true)
                        })
                    }
                    
                }
            }
        }
       
        
        let didStopPosition : MapsDidStopMoving = { [weak self]  (position) in
            
            // let newLocation = CLLocation(latitude: /position.latitude, longitude: /position.longitude)
            
            
            self?.mapDataSource?.getAddressFromlatLong(lat: /position.latitude , long:  /position.longitude , completion: { (strLocationName, country, name,locality , sublocality) in
                self?.setUpdatedAddress(location: strLocationName, name: name, coordinate: position,getCountry:country,getLocality: locality,getSublocality: sublocality)
            })
        }
        
        
        if self.mapType == .google{
            
            mapDataSource = GoogleMapsDataSource.init(mapStyleJSON: "MapStyle", mapView: mapView)
            mapView.delegate = mapDataSource
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = false
            mapView.settings.tiltGestures = false
            mapView.isBuildingsEnabled = false
            mapView.setMinZoom(2, maxZoom: 20)
            mapView.animate(toZoom: 6)
            mapDataSource?.didUpdateCurrentLocation = didUpdateCurrentLocation
            //  mapDataSource?.didChangePosition = didChangePosition
            mapDataSource?.mapStopScroll = didStopPosition
            
            do {
                if /UDSingleton.shared.appSettings?.appSettings?.is_darkMap == "true" {
                    // Set the map style by passing the URL of the local file.
                    if let styleURL = Bundle.main.url(forResource: "MapStyleNight", withExtension: "json") {
                        mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                        
                    } else {
                        debugPrint("Unable to find style.json")
                    }
                } else {
                // Set the map style by passing the URL of the local file.
                if let styleURL = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
                    mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                    
                } else {
                    debugPrint("Unable to find style.json")
                }
            }
            } catch {
                debugPrint("One or more of the map styles failed to load. \(error)")
            }
            
        } else{
            mapDataSource = GoogleMapsDataSource(mapStyleJSON: nil)
            mapDataSource?.didUpdateCurrentLocation = didUpdateCurrentLocation
             configureMapBox()
        }
        
        
       
        
        // mapBoxView.isHidden = true
    }
    
    
    func configureMapBox(){
        let url = URL(string: "mapbox://styles/mapbox/streets-v11")
        mapBoxView.styleURL = url
        mapBoxView.minimumZoomLevel = 10
        mapBoxView.maximumZoomLevel = 25
        mapBoxView.zoomLevel = 15
        mapBoxView.showsUserLocation = true
        mapBoxView.delegate = self
       
    }
    
    func setUpdatedAddress(location : String , name: String?, coordinate : CLLocationCoordinate2D?,getCountry:String?,getLocality:String?,getSublocality:String?) {
        
        
        textFieldPickUpLocation.text = location
        
        address = location
        self.name = name
        self.countryName = getCountry
        self.locality = getLocality
        self.sublocality = getSublocality
        
        latitude = Double(/coordinate?.latitude)
        longitude = Double(/coordinate?.longitude)
        
    }
    
    func setCameraGoogleMap( latitude : Double , longitude : Double) {
        
        let newLocation = GMSCameraPosition.camera(withLatitude: latitude ,
                                                   longitude: longitude ,
                                                   zoom: 14)
        mapView.animate(to: newLocation)
    }
    
    
}


//MARK:- Button Selectors

extension AddLocationFromMapViewController: GMSAutocompleteViewControllerDelegate {
//
//    internal func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        let views = viewController.view.subviews
//        let subviewsOfSubview = views.first!.subviews
//        let subOfNavTransitionView = subviewsOfSubview[1].subviews
//        let subOfContentView = subOfNavTransitionView[2].subviews
//        let searchBar = subOfContentView[0] as! UISearchBar
//        searchBar.text = "YOUR_TEXT"
//    }

  // Handle the user's selection.
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//    print("Place name: \(place.name)")
//    print("Place ID: \(place.placeID)")
//    print("Place attributions: \(place.attributions)")
    
        let lat = place.coordinate.latitude
        let lon = place.coordinate.longitude
        print("lat lon",lat,lon)
    
    
        self.latitude = lat
        self.longitude = lon
    
    DispatchQueue.main.async {
        let latvalue = self.latitude ?? 0.0
        let longValue = self.longitude ?? 0.0
        let currentLocation = GMSCameraPosition.camera(withLatitude:self.latitude ?? 0.0,longitude:self.longitude ?? 0.0,zoom: 14.0)

        if latvalue != 0.0  &&  longValue != 0.0{
            self.mapView.camera = currentLocation

            if !self.isMapLoaded {
                self.configureMapView()
                self.isMapLoaded = !self.isMapLoaded
            }
        }
    }
    
   
    
//    if comingFrom != ""
//        {
//
//
//    }
//    else{
//        if !isMapLoaded {
//            configureMapView()
//            isMapLoaded = !isMapLoaded
//        }
//    }
    
    self.dismiss(animated: true, completion: nil)
    
  
    
    
    
  
    
//
//    if place.name != nil {
//        self.tfAddressNickname.text = "\(String(describing: place.name!))"
//    }
//
//    if place.formattedAddress != nil {
//        self.tfArea.text = "\(String(describing: place.formattedAddress!))"
//    }
//
//

    
    //let cityCoords = CLLocation(latitude: place.coordinate.latitude , longitude: place.coordinate.longitude)
    
    //self.dismiss(animated: true, completion: nil)
    
//
//    guard let vc = R.storyboard.bookService.addLocationFromMapViewController() else{return}
//    vc.modalPresentationStyle = .overFullScreen
//    vc.comingFrom = "AddNewAddress"
//    vc.latitude = self.latValue
//    vc.longitude = self.longvalue
//    vc.delegate = self
//    presentVC(vc, false)
    
    //self.getAdressName(coords: cityCoords)
    
  }

  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
  }

  // User canceled the operation.
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    dismiss(animated: true, completion: nil)
  }

  // Turn the network activity indicator on and off again.
  func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }

  func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }

}

extension UIView {
    func getViewWithConcreteType<T: UIView>()-> T? {
        if let concreteSubview = self as? T {
            return concreteSubview
        }

        for subview in subviews {
            let concreteView: T? = subview.getViewWithConcreteType()
            if concreteView != nil {
                return concreteView!
            }
        }

        return nil
    }
}

extension AddLocationFromMapViewController {
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        if comingFrom ==  "SavedAddressForEdit1"{
            self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
        }
        else
        {
            dismiss(animated: false, completion: nil)
        }
        
    }
    
    @IBAction func buttonDonePressed(_ sender: Any) {
        
        if comingFrom == "SavedAddress" || comingFrom == "SavedAddress1"
            {
            
            //delegate?.getDataFromAnotherVC1(address: address, name: name, latitude: latitude, longitude: longitude, getCountry:self.countryName ?? "",getLocality:self.locality ?? "",getSublocality:self.sublocality ?? "",strComingFrom: "SavedAddress")
            
            let strybord = UIStoryboard(name: "MainCab", bundle: nil)
            let bookingVC = strybord.instantiateViewController(withIdentifier: "AddNewAddressVC") as! AddNewAddressVC
            bookingVC.comingFrom = "SavedAddress1"
            
            
            bookingVC.areavalue = address ?? ""
            bookingVC.street = self.sublocality ?? ""
            bookingVC.block = self.locality ?? ""
            bookingVC.country = self.countryName ?? ""
            bookingVC.latValue = latitude ?? 0.0
            bookingVC.longvalue = longitude ?? 0.0
           // self.navigationController?.pushViewController(bookingVC, animated: false)
           
            if self.presentedViewController==nil{
                self.present(bookingVC, animated: false)
            }else{
                self.presentedViewController!.present(bookingVC, animated: false)
            }
           
        }
        else if comingFrom == "SavedAddressForEdit" || comingFrom == "SavedAddressForEdit1"
            {
            
            let strybord = UIStoryboard(name: "MainCab", bundle: nil)
            let bookingVC = strybord.instantiateViewController(withIdentifier: "AddNewAddressVC") as! AddNewAddressVC
            bookingVC.comingFrom = "SavedAddressForEdit11"
            
            bookingVC.getAddressId = self.getAddressIds
            bookingVC.sendNicName = self.getNickName
            
            bookingVC.areavalue = address ?? ""
            bookingVC.street = self.sublocality ?? ""
            bookingVC.block = self.locality ?? ""
            bookingVC.country = self.countryName ?? ""
            bookingVC.latValue = latitude ?? 0.0
            bookingVC.longvalue = longitude ?? 0.0
           // self.navigationController?.pushViewController(bookingVC, animated: false)
           
            if self.presentedViewController==nil{
                self.present(bookingVC, animated: false)
            }else{
                self.presentedViewController!.present(bookingVC, animated: false)
            }
        }
        else if comingFrom == ""
            {
            delegate?.locationSelectedFromMap(address: address, name: name, latitude: latitude, longitude: longitude, locationType: locationType,isFromStop: isFromStop)
            delegate?.getDataFromAnotherVC(address: address, name: name, latitude: latitude, longitude: longitude, getCountry:self.countryName ?? "",getLocality:self.locality ?? "",getSublocality:self.sublocality ?? "")
            dismiss(animated: false, completion: nil)
        }
        else
        {
            delegate?.getDataFromAnotherVC(address: address, name: name, latitude: latitude, longitude: longitude, getCountry:self.countryName ?? "",getLocality:self.locality ?? "",getSublocality:self.sublocality ?? "")
            
            dismiss(animated: false, completion: nil)
        }
        
        //delegate?.locationSelectedFromMap(address: address, name: name, latitude: latitude, longitude: longitude, locationType: locationType,isFromStop: isFromStop)
        
        
        
//        if comingFrom == ""
//            {
//            delegate?.locationSelectedFromMap(address: address, name: name, latitude: latitude, longitude: longitude, locationType: locationType,isFromStop: isFromStop)
//            delegate?.getDataFromAnotherVC(address: address, name: name, latitude: latitude, longitude: longitude, getCountry:self.countryName ?? "",getLocality:self.locality ?? "",getSublocality:self.sublocality ?? "")
//        }
//        else
//        {
//            delegate?.getDataFromAnotherVC(address: address, name: name, latitude: latitude, longitude: longitude, getCountry:self.countryName ?? "",getLocality:self.locality ?? "",getSublocality:self.sublocality ?? "")
//        }
       
        
    }
    
    @IBAction func buttonCurrentLocationPressed(_ sender: Any) {
     
        if self.mapType == .google{
            
            let lat = /mapView.myLocation?.coordinate.latitude
            let long = /mapView.myLocation?.coordinate.longitude
            
            
            let currentLocation = GMSCameraPosition.camera(withLatitude: lat,
                                                           longitude: long,
                                                           zoom: 14)
            mapView.animate(to: currentLocation)
            let mapLocation = self.mapView.myLocation?.coordinate
            
            mapDataSource?.getAddressFromlatLong(lat: lat, long: long, completion: {  [weak self](strLocationName, country, name,locality , sublocality) in
                
                self?.setUpdatedAddress(location: strLocationName, name: name, coordinate: mapLocation,getCountry:country,getLocality: locality,getSublocality: sublocality)
            })
   
        } else{
            
            let lat = /mapBoxView.userLocation?.location?.coordinate.latitude
            let long = /mapBoxView.userLocation?.location?.coordinate.longitude
            
            let  mapLocation = mapBoxView.userLocation?.location?.coordinate ?? CLLocationCoordinate2DMake(0.0, 0.0)
            
            let camera = MGLMapCamera(lookingAtCenter: mapBoxView.userLocation?.location?.coordinate ?? CLLocationCoordinate2DMake(0.0, 0.0), altitude: mapBoxView.camera.altitude, pitch: mapBoxView.camera.pitch, heading: mapBoxView.camera.heading)
            
            // Animate the camera movement over 5 seconds.
            mapBoxView.setCamera(camera, withDuration: 1, animationTimingFunction: CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut))
            
            mapDataSource?.getAddressFromlatLong(lat: lat, long: long, completion: {  [weak self](strLocationName, country, name,locality , sublocality) in
                
                self?.setUpdatedAddress(location: strLocationName, name: name, coordinate: mapLocation,getCountry:country,getLocality: locality,getSublocality: sublocality)
            })
            
        }
    }
}


extension AddLocationFromMapViewController:MGLMapViewDelegate{
    
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        
        print(#function)
    }
    
    
    func mapViewDidFinishRenderingFrame(_ mapView: MGLMapView, fullyRendered: Bool) {
         print(#function)
    }
    
    func mapViewDidBecomeIdle(_ mapView: MGLMapView) {
        
         print(#function)
        
        self.mapDataSource?.getAddressFromlatLong(lat: mapView.centerCoordinate.latitude , long:   mapView.centerCoordinate.longitude , completion: { (strLocationName, country, name,locality , sublocality) in
            self.setUpdatedAddress(location: strLocationName, name: name, coordinate: mapView.centerCoordinate,getCountry:country,getLocality: locality,getSublocality: sublocality)
        })
    }
    
}
