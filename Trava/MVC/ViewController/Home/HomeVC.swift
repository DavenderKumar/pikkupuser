//
//  HomeVC.swift
//  Buraq24
//
//  Created by MANINDER on 31/07/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//


import UIKit
import GoogleMaps
import GooglePlaces
import DropDown
import SideMenu
import Crashlytics
import Fabric
import NVActivityIndicatorView
import ObjectMapper
import Razorpay
import Mapbox
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections
import IBAnimatable
import EPContactsPicker
import AVFoundation
import Alamofire

var arrlocationGlobal = NSMutableArray()
var serId:Int = 1

typealias reloadApiBlock = (_ isTrue: Bool?) -> ()
var reloadApi: reloadApiBlock?

protocol BookRequestDelegate {
    
    func didSelectServiceType(_ serviceSelected : Service)
    
    func didSelectBrandProduct(_ brand : Brand ,_ product : ProductCab)
    
    func didSelectQuantity( count : Int)
    
    func didSelectNext( type : ActionType)
    
    func didSelectSchedulingDate(date : Date, minDate: Date)
    
    func didGetRequestDetails(request : ServiceRequest )
    func didGetRequestDetails(product_detail : String)
    
    
    func didGetRequestDetailWithScheduling(request : ServiceRequest)
    
    func didRequestTimeout()
    
    func didSelectAddTip(order : OrderCab)
    
    func didPaymentModeChanged(paymentMode: PaymentType, selectedCard: CardCab?)
    
    func didRatingSubmit(ratingValue : Int, comment : String)
    func didSkipRating()
    
    func didBuyEToken(brandId :Int, brandProductId:Int)
    
    //Selecting Freight Service Order
    
    func didSelectedFreightBrand(brand : Brand,isPromo:Bool,coupon:Coupon?)
    func didSelectedFreightProduct(product : ProductCab, isSchedule: Bool)
    
    func didClickConfirmBooking(request: ServiceRequest)
    func didClickInvoiceInfoButton()
    
    func didClickContinueToBookforFriend(request: ServiceRequest)
    
    func didScanRoadPickUPQRCode(object: RoadPickupModal?)
    
    func optionApplyCouponCodeClicked(object: Coupon?)
    func didPayOutstanding(request: ServiceRequest)
    func didChooseAddressFromRecent(place: AddressCab?, isPickup: Bool? ,isFromStop:Bool?,stopIndex:Int)
    
    func didAddLocationFrom(buttonTag: Int, isFromSearch: Bool?,isFromStop:Bool)
    
    func didAddLocationFromSavedAddress(buttonTag: Int, isFromSearch: Bool?,isFromStop:Bool)
    
    func didShowCheckList(order: OrderCab?)
    
    func didSelectContactForGift()
    func didSelectGiftNext(type: ActionType, serviceSelected: ServiceRequest)
    
    func didBackFromSlot(type : MoveType)
    func didSelectScription()
    func reloadDrivers1(_ serviceSelected : Service)
    func reloadDrivers(_ serviceSelected : Service)
}


extension HomeVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        serviceRequest.stops.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StopTableViewCell", for: indexPath) as! StopTableViewCell
        cell.stop = serviceRequest.stops[indexPath.row]
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(buttonRemoveStopClicked(_:)), for:.touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        48
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showAddressSelector(index: indexPath.row)
    }
    
    
    func showAddressSelector(index:Int){
        let isMapSelectionEnabled = UDSingleton.shared.appSettings?.appSettings?.is_location_selection_alert == "true"
        
        if isMapSelectionEnabled{
            self.viewAddLocationFrom.showView(locationButtonTag: index,isFromStop:true)
        }else{
            self.didAddLocationFrom(buttonTag: index, isFromSearch: true,isFromStop:true)
        }
    }
    
}

class HomeVC: UIViewController, SavedAddressVCDelegate_Delegate {
    
    
    
    
    //MARK:- OUTLETS
    
    //  @IBOutlet var constraintNavigationLocationBar: NSLayoutConstraint!
    
    
    var arrData = NSArray()
    
    let myGroup = DispatchGroup()
    
    @IBOutlet weak var viewForSavedAddress: UIView!
    
    
    
    var arrDrawPath : [CLLocationCoordinate2D] = []
   
    var trackOrder:OrderCab?
    var isOnHome:Bool = true
    @IBOutlet weak var stopsTableView: UITableView!
    @IBOutlet weak var btnBookings: UIButton!
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var constraintYAddLocationView: NSLayoutConstraint!
    @IBOutlet weak var constraintNavigationBasicBar: NSLayoutConstraint!
    //  @IBOutlet weak var constraintHeightPickUpView: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightNavigationBar: NSLayoutConstraint!
    // @IBOutlet var constraintOnlyBackButtonTop: NSLayoutConstraint!
    @IBOutlet var constraintTopSegmentedView: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightViewEnterPickDrop: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomTablelocationsearch: NSLayoutConstraint!
    
    
    @IBOutlet var viewTopBarBasicHome: UIView!
    @IBOutlet weak var viewTitlePackageBooking: UIView!
    @IBOutlet var viewTopBarAddLocation: UIView!
    @IBOutlet var viewLocationTableContainer: UIView!
    //  @IBOutlet var viewPickUpLocation: UIView!
    @IBOutlet weak var viewStop1: UIView!
    @IBOutlet weak var viewStop2: UIView!
    @IBOutlet weak var viewStop3: UIView!
    @IBOutlet var viewMapContainer: UIView!
    @IBOutlet var viewBottom: UIView!
    @IBOutlet weak var ViewEnterPickUpDropOff: RoundShadowButton!
    @IBOutlet weak var ViewForfriend: RoundShadowButton!
    @IBOutlet weak var viewStoppageDesc: UIView!
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var viewStatusBar: UIView!
    @IBOutlet weak var viewOnMApNavigation: UIView!
    
    @IBOutlet weak var imageStop1: UIImageView!
    @IBOutlet weak var imageStop2: UIImageView!
    @IBOutlet weak var imageStop3: UIImageView!
    //view Enter Order Details
    
    @IBOutlet var viewSelectService: SelectServiceView!
    @IBOutlet var viewGasService: GasServiceView!
    @IBOutlet var viewScheduler: SchedulerView!
    @IBOutlet var viewDrinkingWaterService: DrinkingWaterView!
    @IBOutlet var viewOrderPricing: OrderPricing!
    // @IBOutlet var viewConfirmPickUp: ConfirmPickupView?
    @IBOutlet var viewUnderProcess: UnderProcessingView!
    @IBOutlet var viewDriverAccepted: RequestAcceptedView!
    @IBOutlet var viewInvoice: InvoiceView!
    @IBOutlet var viewDriverRating: DriverRatingView!
    @IBOutlet var viewNewInvoice: NewInvoiceView!
    @IBOutlet var viewChooseAddress: ChooseAddress!
    @IBOutlet var viewAddLocationFrom: AddLocationFrom!
    
    ///Freight Modeule
    @IBOutlet var viewFreightBrand: SelectBrandView!
    @IBOutlet var viewFreightProduct: SelectProductView!
    @IBOutlet var viewFreightOrder: FreightView!
    @IBOutlet var bottomViewBookforFriend: BookForFriendView!
    
    @IBOutlet var tblLocationSearch: UITableView!
    @IBOutlet var collectionSavedPlaces: UICollectionView?
    @IBOutlet weak var mapBoxView: NavigationMapView!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var segmentedMapType: UISegmentedControl!
    @IBOutlet var stackViewLocation: UIStackView?
    @IBOutlet weak var lblBookTaxi: UILabel!
    
    @IBOutlet weak var btnRoadPickup: RoundShadowButton!
    
    
    // @IBOutlet var btnLocationNext: UIButton!
    //  @IBOutlet var btnLocationBack: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnOnlyBack: UIButton!
    @IBOutlet weak var buttonAddStop: UIButton!
    @IBOutlet var btnRightNavigationBar: UIButton!
    @IBOutlet var btnLeftNavigationBar: UIButton!
    @IBOutlet var lblTitleNavigationBar: UILabel!
    @IBOutlet var btnCurrentLocation: UIButton!
    @IBOutlet weak var buttonContinue: UIButton!
    @IBOutlet var addSavedPlaces: UIButton?
    @IBOutlet weak var buttonHeaderBookingFor: UIButton!
    @IBOutlet weak var buttonEditHomeAddress: UIButton!
    @IBOutlet weak var buttonEditWorkAddress: UIButton!
    @IBOutlet weak var buttonNewSideMenu: UIButton!
    @IBOutlet weak var buttonNewCurrentLocation: UIButton!
    @IBOutlet weak var buttonNewSelectLocation: UIButton!
    @IBOutlet weak var buttonNewBack: UIButton!
    @IBOutlet weak var buttonlNavigationTitle: UIButton!
    
    
    @IBOutlet weak var txtStop3: AnimatableTextField!
    @IBOutlet var txtStop1: UITextField!
    @IBOutlet var txtStop2: UITextField!
    @IBOutlet var txtPickUpLocation: UITextField!
    @IBOutlet var txtDropOffLocation: UITextField!
    
    @IBOutlet weak var labelHomeLocationNickName: UILabel!
    @IBOutlet weak var labelHomeLocationName: UILabel!
    @IBOutlet weak var labelWorkLocationNickName: UILabel!
    @IBOutlet weak var labelWorkLocationName: UILabel!
    
    @IBOutlet var imgViewPickingLocation: UIImageView!
    @IBOutlet weak var imageViewHeaderProfilePic: UIImageView!
    @IBOutlet weak var workLocationView: UIView!
    @IBOutlet weak var imgStaticHome: UIImageView!
    @IBOutlet weak var imgStaticWork: UIImageView!
    @IBOutlet weak var lblRecentLocation: UILabel!
    @IBOutlet weak var imgPickUp: UIImageView!
    @IBOutlet weak var imgDropOff: UIImageView!
    @IBOutlet weak var imgSpacer: UIImageView!
    
    @IBOutlet weak var imgDropIcon: UIImageView!
    @IBOutlet weak var forMeView: UIView!
    @IBOutlet weak var stackForMe: UIStackView!
    @IBOutlet weak var btnWatchEarn: UIButton!
    @IBOutlet weak var vwRecording: UIView!

    
    
    
    
    
    
    //MARK:- PROPERTIES
    var isHomeApiLoading = false
    var alertVw = UIAlertController()
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var mapDataSource:GoogleMapsDataSource?
    //    var initialSetupViewController:PTFWInitialSetupViewController?
    var collectionViewWaterDataSource : CollectionViewDataSourceCab?
    var collectionViewPlacesDataSource : CollectionViewDataSourceCab? {
        didSet {
            collectionSavedPlaces?.delegate = collectionViewPlacesDataSource
            collectionSavedPlaces?.dataSource = collectionViewPlacesDataSource
            collectionSavedPlaces?.reloadData()
            
        }
    }
    
    var isCurrentLocationUpdated : Bool = false
    
    var change : Float = 0.05
    
    ///Searching Location TableView
    lazy var movingDrivers : [MovingVehicle] = [MovingVehicle]()
    var drivers: [Driver]?
    var currentService : Int = 7 // Change here for service
    var tableDataSource : TableViewDataSourceCab?
    var googlePickupLocation : GooglePlaceDataSource?
    var googleDropOffLocation : GooglePlaceDataSource?
    var googleStop1Location : GooglePlaceDataSource?
    var googleStop2Location : GooglePlaceDataSource?
    var updateMapLocation = false
    var chatType: RemoteNotificationType = .Text
    
    var locationEdit : LocationEditing = .DropOff {
        didSet {
            imgViewPickingLocation.image = locationEdit == .DropOff ? #imageLiteral(resourceName: "DropMarker") : #imageLiteral(resourceName: "PickUpMarker")
        }
    }
    
    lazy var recentLocations = [AddressCab]()
    
    var savedLocations = [AddressCab]()
    
    /* var results = [GMSAutocompletePrediction]() {
     didSet {
     // Ankush viewLocationTableContainer.alpha = results.count == 0 ? 0 : 1
     tblLocationSearch.reloadData()
     }
     } */
    lazy var zoneArray = [ZoneModel]()
    let user = UDSingleton.shared.userData
    
    var arraySaveName = ["Home", "Work", "Others"]
    var dataItems : [String] = []
    
    var currentLocation : CLLocation?
    
    var isMapLoaded : Bool = false
    var timerMovingVehicle : Timer?
    var currentSeconds: Float  = 0
    
    var polyPath : GMSMutablePath?
    var polyline : GMSPolyline?
    
    var timer: Timer!
    var i: UInt = 0
    var animationPath = GMSMutablePath()
    var animationPolyline = GMSPolyline()
    
    
    lazy var driverMarker = GMSMarker()
    lazy  var userMarker  = GMSMarker()
    
    
    lazy var driverMapBoxMarker = MGLPointAnnotation()
    lazy var driverMapBoxImageMarker = MGLAnnotationImage()
    
    
    lazy var userMapBoxMarker = MGLPointAnnotation()
    lazy var userMapBoxImageMarker = MGLAnnotationImage()
    
    var durationLeft : String = ""
    
    
    var destination : CLLocationCoordinate2D?
    var source : CLLocationCoordinate2D?
    
    var isDoneCurrentPolyline : Bool = true
    var isStopsShownAfterStartRide : Bool = false
    
    var isCurrent : Bool = false
    var isFocus  : Bool = false
    var lastPoints : String = ""
    var driverBearing : Double?
    var isSearching : Bool = false
    var applePayMerchantID: String = "merchant.1981laundry"
    
    var locationLatest : String = ""
    var latitudeLatest : Double?
    var longitudeLatest : Double?
    var isCheck = false
    
    //MENU Navigation Controller
    var  menuLeftNavigationController : UISideMenuNavigationController?
    var  menuRightNavigationController : UISideMenuNavigationController?
    var markerPickUpLocation : GMSMarker?
    var markerDropOffLocation : GMSMarker?
    lazy var pickupMapBoxMarker = MGLPointAnnotation()
    lazy var pickupMapBoxImageMarker = MGLAnnotationImage()
    
    
    lazy var dropOffMapBoxMarker = MGLPointAnnotation()
    lazy var  dropOffMapBoxImageMarker = MGLAnnotationImage()
    
    
    var tempPickUp : String = ""
    var tempDropOff : String = ""
    // var tempStop1 : String = ""
    // var tempStop2 : String = ""
    
    var serviceRequest : ServiceRequest = ServiceRequest()
    var homeAPI : HomeCab?
    var currentOrder : OrderCab?
    var isFromPackage: Bool = false
    var modalPackages: TravelPackages?
    
    var dic = [String:TrackingModel]()
    
    var isSwitchRiderViewOpened: Bool = false
    var reasonPopupType : ReasonPopuptype?
    var isTransferRequestPopUpShown : Bool = false
    var isLongDistancePopUpShown : Bool = false
    
    var razorpay: RazorpayCheckout?
    var razorpayTestKey = /UDSingleton.shared.appSettings?.appSettings?.razorpaytestkey
    var razorPayment = false
    var order_id = String()
    var routeOptions: NavigationRouteOptions?
    var route: Route?
    var mapType =  UDSingleton.shared.appSettings?.appSettings?.mapType
    var isFirstTimeScription: Bool = true
    var isFromCancellation = false
    
    var screenType : ScreenType = ScreenType(mapMode: .ZeroMode, entryType: .Forward) {
        
        didSet {
            
            let entryType = screenType.entryType
            let mode = screenType.mapMode
            
            switch mode {
            case .ZeroMode:
                showBasicNavigation(updateType: .NormalMode)
                
            case .NormalMode:
                showBasicNavigation(updateType: .NormalMode)
                btnLeftNavigationBar.isHidden = false
                buttonNewSideMenu.isHidden = false
                startMovingVehicleTimer()
                
                if entryType == .Backward && !isFromCancellation{
                    
                    moveToCurrentLocation()
                    clearServiceRequestData()
                    setupInitialUI()
                }
                
                let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
                switch template {
                case .Moby?:
                    break
                case .Mover:
                    serviceRequest.serviceSelected = UDSingleton.shared.appSettings?.services?.first
                    let product = homeAPI?.brands?.first?.products?.first
                    //                    serviceRequest.selectedProduct = product
                    //                    serviceRequest.selectedBrand = UDSingleton.shared.appSettings?.services?.first?.brands?.first
                    //                    if let service = UDSingleton.shared.appSettings?.services?.first{
                    //                        didSelectServiceType(service)
                    //                    }
                    showFreightBrandView(moveType: .Forward)
                default:
                    showSelectServiceView()
                }
                
            case .SelectingLocationMode:
                
                clearMovingVehicles()
                clearMapForIntial()
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    
                    self.clearMapForIntial()
                }
                showBasicNavigation(updateType: .SelectingLocationMode)
                
            case .OrderDetail: // FrightImages  pop uo treated as
                showBasicNavigation(updateType: .OrderDetail)
                showParticularAddOrderView(type: entryType)
                btnLeftNavigationBar.isHidden = true
                buttonNewSideMenu.isHidden = true
                
            case .ScheduleMode :
                
                showBasicNavigation(updateType: .OrderDetail)
                showSchedulerView(moveType: entryType)
                
            case .OrderPricingMode:
                
                serviceRequest.cancellation_charges = nil // click on pay to next driver and cancel on underprocessing view
                apiGetCreditpoints()
                showPricingView(moveType: entryType)
                showBasicNavigation(updateType: .OrderPricingMode)
                
            /* case .ConfirmPickup:
             debugPrint("Need to handle")
             showConfirmPickUpView(moveType: entryType)
             clearMovingVehicles()
             configureConfirmPickUpMapView() */
            
            case .UnderProcessingMode:
                
                showProcessingView(moveType: .Forward)
                
            //showProcessingView(moveType: .Forward)
            
            case .RequestAcceptedMode , .OnTrackMode:
                clearMovingVehicles()
                showBasicNavigation(updateType: .RequestAcceptedMode)
                showDriverView()
                
            case .ServiceDoneMode:
                clearMovingVehicles()
                clearMapForIntial()
                viewUnderProcess.minimizeProcessingView()
                
                //showNewInvoiceView()
                
                showBasicNavigation(updateType: .ServiceDoneMode)
                
                handlePayment()
                
            // Ankush need to handle  showInvoiceView()
            
            case .ServiceFeedBackMode:
                break;
                
            case .SelectingFreightBrand:
                
                showBasicNavigation(updateType: .SelectingFreightBrand)
                
                //                let template = AppTemplate(rawValue: /UDSingleton.shared.appTerminology?.key_value?.template?.toInt())
                let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
                switch template {
                
                case .Default?:
                    dropPickUpAndDropOffPins(pickLat: /serviceRequest.latitudeDest, pickLong: /serviceRequest.longitudeDest, dropLat: /serviceRequest.latitude, dropLong: /serviceRequest.longitude)
                    
                default:
                    dropPickUpAndDropOffPins(pickLat: /serviceRequest.latitudeDest, pickLong: /serviceRequest.longitudeDest, dropLat: /serviceRequest.latitude, dropLong: /serviceRequest.longitude)
                    
                    
                }
                
                showFreightBrandView(moveType: entryType)
                
                
            case .SelectingFreightProduct:
                
                showFreightProductView(moveType: entryType)
                
                showBasicNavigation(updateType: .SelectingFreightProduct)
            }
        }
    }
    
    var locationType : LocationTypeCab = .OnlyDropOff {
        didSet {
            //  viewPickUpLocation.alpha  = locationType == .PickUpDropOff  ? 1 : 0
            //            constraintHeightPickUpView.constant = locationType == .PickUpDropOff  ? 55 : 0
        }
    }
    
    func handlePayment(){
        let paymentGateway = PaymentGateway(rawValue:(/UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id))
        if currentOrder?.payment?.paymentType == "Card" && (paymentGateway == PaymentGateway.paystack || paymentGateway == PaymentGateway.payku || paymentGateway == PaymentGateway.razorpay || paymentGateway == .wipay || paymentGateway == .paytab){
            
            if paymentGateway == PaymentGateway.razorpay {
                
                NotificationCenter.default.addObserver(self, selector: #selector(self.methodRazorpay(notification:)), name: Notification.Name("RazorPaySucccess"), object: nil)
                
                guard let razorPayWaiting = R.storyboard.bookService.razorPayWaitingVC() else { return }
                razorPayWaiting.order = currentOrder
                self.pushVC(razorPayWaiting)
                
            }else if paymentGateway == .paytab{
                showPayTab()
            } else {
                showUrlPaymentView()
            }
            
        }else if currentOrder?.payment?.paymentType == "Card" && paymentGateway == .qpaypro{
            showQPayProPaymentView()
        }
        else if  paymentGateway == PaymentGateway.braintree && /currentOrder?.payment?.paymentType == "Card"  {
            
            self.payViaPayPal()
        }else if  (paymentGateway == PaymentGateway.credimax || paymentGateway == .benefit) &&  /currentOrder?.payment?.paymentType == "Card" && currentOrder?.payment?.card_type != "1" { //pushpinder //PaymentGateway.braintree && /currentOrder?.payment?.paymentType == "Card" {

            self.payViaBenefit()
            
        }else{
            
            showNewInvoiceView()
        }
    }
    
    
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "UpdateSavedAddress1"), object: nil)
        
     
        SocketIOManagerCab.shared.initialiseSocketManager()
        stopsTableView.delegate = self
        stopsTableView.dataSource = self
        let nib = UINib(nibName: "StopTableViewCell", bundle: .main)
        stopsTableView.register(nib, forCellReuseIdentifier: "StopTableViewCell")
        NotificationCenter.default.addObserver(self, selector: #selector(functionName), name: Notification.Name("NewFunctionName"), object: nil)
        
        reloadApi = { (true) in
            self.getUpdatedData()
        }
        
        if self.mapType == MapType.google{
            mapView.isHidden = false
            mapBoxView.isHidden = true
        } else{
            mapView.isHidden = true
            mapBoxView.isHidden = false
        }
        
        razorpay = RazorpayCheckout.initWithKey(razorpayTestKey, andDelegate: self)
        btnWatchEarn.isHidden = true
        
        let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
        
        if template == .Corsa{
            
            getAllzones()
        }
        switch template {
        case .DeliverSome:
            buttonEditHomeAddress.setButtonWithTintColorSecondary()
            buttonEditWorkAddress.setButtonWithTintColorSecondary()
            buttonAddStop.isHidden = true
            imgStaticHome.setImageTintColorSecondary()
            imgStaticWork.image = #imageLiteral(resourceName: "ic_new_Work")
            buttonNewSideMenu.setImage(#imageLiteral(resourceName: "ic_drawr-1").withRenderingMode(.alwaysTemplate), for: .normal)
            buttonNewSideMenu.tintColor = .white
            imgStaticWork.setImageTintColorSecondary()
            lblRecentLocation.text = "Home.Recent_Locations".localizedString
            lblRecentLocation.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            imgPickUp.setImageTintColorSecondary()
            imgDropIcon.image = #imageLiteral(resourceName: "ic_nav")
            forMeView.isHidden = true
            btnRoadPickup.isHidden = true
            lblBookTaxi.text = "Home.Book_a_delivery".localizedString
            stackForMe.isHidden = true
            
            
            break
            
        case .GoMove:
            lblBookTaxi.text = "Home.Book_a_service".localizedString
            btnRoadPickup.isHidden = true
            
            break
        default:
            
            
            stackForMe.isHidden = true
            buttonEditHomeAddress.setButtonWithTintColorSecondary()
            buttonEditWorkAddress.setButtonWithTintColorSecondary()
            btnWatchEarn.setButtonWithBackgroundColorSecondaryAndTitleColorBtnText()
            buttonAddStop.isHidden = false
            
            //            if UDSingleton.shared.appSettings?.dynamicbar?.road_pickup?.lowercased() == "true"{
            //                btnRoadPickup.isHidden = false
            //            }else{
            btnRoadPickup.isHidden = true
        //  }
        
        
        }
        
        
        if !isFromPackage {
            setUpSideMenuPanels()
        }
        
        
        
        SocketIOManagerCab.shared.initialiseSocketManager()
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.nCancelFromBookinScreen(notifcation:)), name: Notification.Name(rawValue: LocalNotifications.nCancelFromBookinScreen.rawValue), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(CallHomeApiUsingNotification), name: Notification.Name(RemoteNotificationType.DriverAccepted.rawValue), object: nil)
        
        
        /* if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
         statusBar.backgroundColor = UIColor.clear
         } */
        
        // self.listeningEvent()
        
        // configureSavePlacesCollectionView()
        
        
        
        
    }
    
    
    @objc func showSpinningWheel(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        
        getUpdatedData()
    }
    
    
    @IBAction func btnBookingsAction(_ sender: Any) {
        guard let bookingVC = R.storyboard.sideMenu.bookingsVC() else{return}
        bookingVC.isRightToLeft = LanguageFile.shared.isLanguageRightSemantic()
        self.navigationController?.pushViewController(bookingVC, animated: true)
    }
    
    @objc func functionName (notification: NSNotification){
        bookService()
    }
    @objc func CallHomeApiUsingNotification (notification: NSNotification){
        
        
        getLocalDriverForParticularService(service: serviceRequest.serviceSelected!)
        
        
        
    }
    
    func clearMapForIntial(){
        
        self.mapView.clear()
        self.cleanPath()
        self.i = 0
        self.animationPath = GMSMutablePath()
        self.animationPolyline.map = nil
    }
    
    @objc func methodRazorpay(notification: Notification) {
        showNewInvoiceView()
    }
    
    @objc func loginSuccess(notification: Notification){
        print(notification.userInfo?["userInfo"] as? [String: Any] ?? [:])
        var driver = notification.userInfo?["userInfo"] as? [String:Any]
        let driver_id = driver?["driver_id"] as? Int
        print(driver_id)
        serviceRequest.driver_id = driver_id
        bookService()
        
        
        //.... code process
    }
    
    deinit {
        NotificationCenter.default
            .removeObserver(self, name:  NSNotification.Name("com.user.login.success"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(RemoteNotificationType.DriverAccepted.rawValue), object: nil)
        
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        updateMapLocation = true
        isHomeApiLoading = false
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(loginSuccess),
                         name: NSNotification.Name ("com.user.login.success"),object: nil)
        checkForLocationPermissoion()
        
        collectionViewPlacesDataSource?.items = dataItems
        collectionSavedPlaces?.reloadData()
        
        showCollectionView()
        if !isMapLoaded {
            configureMapView()
            setUpBasicSettings()
            isMapLoaded = !isMapLoaded
            getUpdatedData()
        }
        
        addForgroundObserver()
        addKeyBoardObserver()
        
        btnBookings.isHidden = !UDSingleton.shared.is_multiple_requests
        btnBookings.setViewBackgroundColorSecondary()
        btnBookings.tintColor = .white
        
        if let urlImage = UDSingleton.shared.userData?.userDetails?.profilePic {
            imageViewHeaderProfilePic.sd_setImage(with: URL(string : urlImage), placeholderImage: #imageLiteral(resourceName: "ic_user"), options: .refreshCached, progress: nil, completed: nil)
            
            //imageViewHeaderProfilePic.set(imageUrl: urlImage)
            imageViewHeaderProfilePic.layer.cornerRadius = 12.5
            imageViewHeaderProfilePic.clipsToBounds = true
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        removeKeyBoardObserver()
        // imgHome.setImageTintColorSecondary()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        updateData()
        
        self.listeningEvent()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if screenType.mapMode == .NormalMode {
            clearMovingVehicles()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
        switch template {
        case .Moby?:
            return .lightContent
            
        default:
            return .default
        }
    }
    
    //MARK:-  ACTIONS
    
    /// Setting action for left and right navigation bar button
    @IBAction func actionBtnMenuPressed(_ sender: UIButton) {
        presentMenu(type: .Left)
    }
    
    @IBAction func btnWatchEarnAction(_ sender: Any) {
        
        // guard let vc = R.storyboard.bookService.chatBotChatVC() else{return}
        
        guard let vc = R.storyboard.bookService.storyVC() else{return}
        self.pushVC(vc)
        
    }
    
    
    @IBAction func actionBtnChangeMaptype(_ sender: UISegmentedControl) {
        
        if let mapType = UserDefaultsManager.shared.mapType {
            guard let mapEnum = BuraqMapType(rawValue: mapType) else { return }
            mapView.mapType = mapEnum == .Hybrid ? .hybrid : .normal
            UserDefaultsManager.shared.mapType = mapEnum == .Hybrid ? BuraqMapType.Satellite.rawValue : BuraqMapType.Hybrid.rawValue
            guard let polylin = self.polyline else{return}
            // Ankush polylin.strokeColor  = mapEnum == .Hybrid ? .white : .darkGray
            
            let color = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.secondary_colour ?? DefaultColor.color.rawValue)
            polylin.strokeColor = color
            
        }
    }
    
    @IBAction func actionBtnCurrentLocation(_ sender: UIButton) {
        
        if sender.isSelected{
            btnCurrentLocation.isSelected = false
            
            let currentLocation = GMSCameraPosition.camera(withLatitude: /mapView.myLocation?.coordinate.latitude,
                                                           longitude: /mapView.myLocation?.coordinate.longitude,
                                                           zoom: 14)
            mapView.animate(to: currentLocation)
            return
        }
        
        if  screenType.mapMode == .RequestAcceptedMode{
            
            guard let path = GMSMutablePath(fromEncodedPath: PolylineCab.points) else { return }
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
            btnCurrentLocation.isSelected = true
            
        }
        else{
            
            if self.mapType == .google{
                let currentLocation = GMSCameraPosition.camera(withLatitude: /mapView.myLocation?.coordinate.latitude,
                                                               longitude: /mapView.myLocation?.coordinate.longitude,
                                                               zoom: 14)
                mapView.animate(to: currentLocation)
            } else{
                
                
                let camera = MGLMapCamera(lookingAtCenter: mapBoxView.userLocation?.location?.coordinate ?? CLLocationCoordinate2DMake(0.0, 0.0), altitude: mapBoxView.camera.altitude, pitch: mapBoxView.camera.pitch, heading: mapBoxView.camera.heading)
                
                // Animate the camera movement over 5 seconds.
                mapBoxView.setCamera(camera, withDuration: 1, animationTimingFunction: CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut))
            }
            //mapBoxView.setTargetCoordinate(mapBoxView.userLocation?.location?.coordinate ?? CLLocationCoordinate2DMake(0.0, 0.0), animated: true, completionHandler: nil)
            
        }
        
        
        
        
        //        if !(self.btnCurrentLocation.isSelected){
        //
        //            guard let path = GMSMutablePath(fromEncodedPath: Polyline.points) else { return }
        //            let bounds = GMSCoordinateBounds(path: path)
        //            self.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
        //            self.btnCurrentLocation.isSelected = true
        //
        //        }
        //        else{
        //                    let currentLocation = GMSCameraPosition.camera(withLatitude: /mapView.myLocation?.coordinate.latitude,
        //                                                                   longitude: /mapView.myLocation?.coordinate.longitude,
        //                                                                   zoom: 14)
        //                    mapView.animate(to: currentLocation)
        //              self.btnCurrentLocation.isSelected = false
        //        }
        
    }
    
    
    
    @IBAction func actionBtnSettingsPressed(_ sender: UIButton) {
        presentMenu(type: .Right)
    }
    
    ///Function to move backward
    
    @IBAction func actionBtnMapBackPressed(_ sender: UIButton) {
        
//        if let block = reloadApi {
//            block(true)
//        }
        
        
        print(sender.tag)
        let mode = screenType.mapMode
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
        switch mode {
        
        case .SelectingLocationMode:
            
            
            //            let template = AppTemplate(rawValue: /UDSingleton.shared.appTerminology?.key_value?.template?.toInt())
            //            let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
            switch template {
            
            case .Mover?:
                screenType = ScreenType(mapMode: .OrderDetail, entryType: .Backward)
                
            default:
                bottomViewBookforFriend.minimizeBookForFriendView()
                // results.removeAll()
                changeLocationType(move: .Backward)
                screenType = ScreenType(mapMode: .NormalMode, entryType: .Backward)
                // moveToCurrentLocation()
                
                viewTitlePackageBooking.isHidden = !isFromPackage
                
            /* if serviceRequest.booking_type == "RoadPickup" {
             clearRoadPickUpdata() // To clear road pick updata
             } */
            
            }
            
        case .SelectingFreightBrand:
            
            arrlocationGlobal = NSMutableArray()
            viewFreightBrand.minimizeSelectBrandView()
            
            //            let template = AppTemplate(rawValue: /UDSingleton.shared.appTerminology?.key_value?.template?.toInt())
            
            switch template {
            
            case .Mover?:
                screenType = ScreenType(mapMode: .NormalMode, entryType: .Backward)
                
            default:
                screenType = ScreenType(mapMode: .SelectingLocationMode, entryType: .Backward)
                
            }
            
            
            
            
        case .SelectingFreightProduct:
            
            //Hide Frieght Product View and show
            viewFreightProduct.minimizeProductView()
            
            screenType = ScreenType(mapMode: .SelectingFreightBrand, entryType: .Backward)
            
            
        case .OrderDetail:
            
            hideParticularOrderView()
            
            //            let template = AppTemplate(rawValue: /UDSingleton.shared.appTerminology?.key_value?.template?.toInt())
            
            switch template {
            case .Mover?:
                
                /*
                 guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{ return }
                 var modeType : MapMode  = serviceID > 3 ? .SelectingFreightProduct : .SelectingLocationMode
                 if serviceID == 4 {
                 modeType =  .SelectingFreightBrand
                 }*/
                
                
                //                let bookingFlow = serviceRequest.serviceSelected?.booking_flow
                //
                //                var modeType : MapMode  = bookingFlow == "1" ? .SelectingFreightProduct : .SelectingLocationMode
                //                if bookingFlow == "1" {
                //                    modeType =  .SelectingFreightProduct
                //                }
                
                screenType = ScreenType(mapMode: .NormalMode, entryType: .Backward)
                
            default:
                /*
                 guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{ return }
                 var modeType : MapMode  = serviceID > 3 ? .SelectingFreightProduct : .SelectingLocationMode
                 if serviceID == 4 {
                 modeType =  .SelectingFreightProduct
                 }*/
                let bookingFlow = serviceRequest.serviceSelected?.booking_flow
                
                var modeType : MapMode  = bookingFlow == "1" ? .SelectingFreightProduct : .SelectingLocationMode
                
                
                
                if bookingFlow == "1" {
                    modeType =  .SelectingFreightProduct
                }
                
                screenType = ScreenType(mapMode: modeType, entryType: .Backward)
                
            }
            
            
        case .ScheduleMode:
            
            viewScheduler.minimizeSchedulerView()
            
            screenType = ScreenType(mapMode: .SelectingFreightProduct, entryType: .Backward)
            
        // Ankush screenType = ScreenType(mapMode: .OrderDetail, entryType: .Backward)
        
        case .OrderPricingMode:
            
            viewOrderPricing.minimizeOrderPricingView()
            
            //            let template = AppTemplate(rawValue: /UDSingleton.shared.appTerminology?.key_value?.template?.toInt())
            
            switch template {
            
            
            case .Mover?:
                let mapmode : MapMode  = serviceRequest.requestType == .Present  ? .SelectingLocationMode : .ScheduleMode
                screenType = ScreenType(mapMode: mapmode, entryType: .Backward)
                
            default:
                
                guard let serviceID = serviceRequest.booking_type == "RoadPickup" ? serviceRequest.category_id : serviceRequest.serviceSelected?.serviceCategoryId else{ return }
                
                if serviceID == 7 || serviceID == 10 || /serviceRequest.serviceSelected?.booking_flow == "2"{
                    
                    if serviceRequest.booking_type == "RoadPickup" {
                        screenType = ScreenType(mapMode: .SelectingLocationMode, entryType: .Backward)
                    } else {
                        viewFreightProduct.minimizeProductView()
                        if serviceRequest.requestType == .Future {
                            
                            screenType = ScreenType(mapMode: .ScheduleMode, entryType: .Backward)
                            
                        } else {
                            
                            screenType = ScreenType(mapMode: .SelectingFreightProduct, entryType: .Backward)
                            
                        }
                    }
                    
                    /* if brandID == 18 || brandID == 19 {
                     screenType = ScreenType(mapMode: .SelectingFreightBrand, entryType: .Backward)
                     } else {
                     screenType = ScreenType(mapMode: .SelectingFreightProduct, entryType: .Backward)
                     } */
                    
                    
                    return
                } else{
                    var mapmode : MapMode  = serviceRequest.requestType == .Present  ? .OrderDetail : .ScheduleMode
                    
                    screenType = ScreenType(mapMode: .OrderDetail, entryType: .Backward)
                    
                }
                
                
            }
            
            
            
        /* case .ConfirmPickup:
         
         viewConfirmPickUp.minimizeConfirmPickUPView()
         dropPickUpAndDropOffPins(pickLat: /serviceRequest.latitudeDest, pickLong: /serviceRequest.longitudeDest, dropLat: /serviceRequest.latitude, dropLong: /serviceRequest.longitude)
         screenType = ScreenType(mapMode: .OrderPricingMode, entryType: .Backward) */
        
        default :
            
            if isFromPackage {
                popVC()
            }
            break;
        }
    }
    
    
    
    @IBAction func addSavedPlaces(_sender : UIButton){
        //        guard let vc = R.storyboard.bookService.saveAddressVC() else {return}
        //        self.pushVC(vc)
        addSavedPlaces?.isSelected = true
        dataItems = arraySaveName
        collectionViewPlacesDataSource?.items = dataItems
        collectionSavedPlaces?.reloadData()
        showCollectionView()
        
    }
    
    
    func changeLocationType(move : MoveType) {
        
        
        
        guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{return}
        if move == .Forward {
            
            serviceRequest.locationNameDest =     serviceID > 3 ?   locationLatest : nil
            
            serviceRequest.latitudeDest =  serviceID > 3 ?   latitudeLatest : nil
            
            serviceRequest.longitudeDest = serviceID > 3 ?   longitudeLatest : nil
            
            
            
            txtPickUpLocation.text =    serviceID > 3 ?  locationLatest : ""
            
            txtDropOffLocation.text =    serviceID > 3 ?  "" : locationLatest
            
            tempDropOff =  /txtDropOffLocation.text
            
            tempPickUp =  /txtPickUpLocation.text
            
            
            
            serviceRequest.locationName =     serviceID > 3 ?   nil : locationLatest
            
            serviceRequest.latitude =   serviceID > 3 ?   nil : latitudeLatest
            
            serviceRequest.longitude = serviceID > 3 ?   nil : longitudeLatest
            
            if serviceID == 15{
                
                serId = serviceID
                
                buttonAddStop.isHidden = true
                //buttonAddStop.isHidden = false
            }
            
            else{
                
                serId = 99
                
                
               // buttonAddStop.isHidden = true
                
            }
            
            
            
        } else {
            
            locationEdit = serviceID > 3 ? .PickUp : .DropOff
            
            
        }
        
    }
    
    func moveToCurrentLocation() {
        
        
        if self.mapType == .google{
            
            let currentLocation = GMSCameraPosition.camera(withLatitude: /mapView.myLocation?.coordinate.latitude,
                                                           longitude: /mapView.myLocation?.coordinate.longitude,
                                                           zoom: 14)
            mapView.animate(to: currentLocation)
        } else{
            
            
            
            let camera = MGLMapCamera(lookingAtCenter: mapBoxView.userLocation?.location?.coordinate ?? CLLocationCoordinate2DMake(0.0, 0.0), altitude: mapBoxView.camera.altitude, pitch: mapBoxView.camera.pitch, heading: mapBoxView.camera.heading)
            
            // Animate the camera movement over 5 seconds.
            mapBoxView.setCamera(camera, withDuration: 1, animationTimingFunction: CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut))
        }
        
    }
    
    @IBAction func buttonAddLocationclicked(_ sender: UIButton) {
        
        // pickup- 1, Stop1- 2, Stop2- 3, drop- 4, 5- add Home, 6- Add Work, 7- Edit Home, 8- Edit Work
        
        switch sender.tag {
        
        case 1, 2, 3, 4, 7, 8,101 :
            

            let isMapSelectionEnabled = UDSingleton.shared.appSettings?.appSettings?.is_location_selection_alert == "true"
            
            if isMapSelectionEnabled{
                self.viewAddLocationFrom.showView(locationButtonTag: sender.tag)
            }else{
                self.didAddLocationFrom(buttonTag: sender.tag, isFromSearch: true,isFromStop:false)
            }

            
        case 5:
            
            if homeAPI?.addresses?.home?.address == nil || /homeAPI?.addresses?.home?.address?.isEmpty {
                viewAddLocationFrom.showView(locationButtonTag: sender.tag)
            } else {
                viewChooseAddress.showView(address: homeAPI?.addresses?.home,count:self.serviceRequest.stops.count)
            }
            
        case 6:
            
            if homeAPI?.addresses?.work?.address == nil || /homeAPI?.addresses?.work?.address?.isEmpty {
                viewAddLocationFrom.showView(locationButtonTag: sender.tag)
            } else {
                viewChooseAddress.showView(address: homeAPI?.addresses?.work,count:self.serviceRequest.stops.count)
            }
            
            
        default:
            break
        }
        
        
        /*  switch sender.tag {
         case 1:
         
         
         
         GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
         
         self?.txtPickUpLocation.text = place?.formattedAddress
         self?.serviceRequest.locationNameDest = place?.formattedAddress
         self?.serviceRequest.latitudeDest =  /place?.coordinate.latitude
         self?.serviceRequest.longitudeDest =  /place?.coordinate.longitude
         
         //  self?.setCameraGoogleMap(latitude: /place?.coordinate.latitude, longitude: /place?.coordinate.longitude)
         
         self?.callLocationSelected()
         }
         
         case 2:
         
         GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
         
         
         if let object = self?.serviceRequest.stops.filter({$0.priority == 1}).first {
         self?.serviceRequest.stops.remove(object: object)
         }
         
         self?.txtStop1.text = place?.formattedAddress
         
         let modal = Stops(latitude: /place?.coordinate.latitude, longitude: /place?.coordinate.longitude, priority: 1, address: place?.formattedAddress)
         self?.serviceRequest.stops.append(modal)
         
         
         debugPrint(self?.serviceRequest.stops as Any)
         }
         
         case 3:
         
         GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
         
         if let object = self?.serviceRequest.stops.filter({$0.priority == 2}).first {
         self?.serviceRequest.stops.remove(object: object)
         }
         
         self?.txtStop2.text = place?.formattedAddress
         let modal = Stops(latitude: /place?.coordinate.latitude, longitude: /place?.coordinate.longitude, priority: 2, address: place?.formattedAddress)
         self?.serviceRequest.stops.append(modal)
         
         debugPrint(self?.serviceRequest.stops as Any)
         }
         
         case 4:
         
         GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
         
         self?.txtDropOffLocation.text = place?.formattedAddress
         self?.serviceRequest.locationNickName = place?.name
         self?.serviceRequest.locationName = place?.formattedAddress
         self?.serviceRequest.latitude =  /place?.coordinate.latitude
         self?.serviceRequest.longitude =  /place?.coordinate.longitude
         
         //  self?.setCameraGoogleMap(latitude: /place?.coordinate.latitude, longitude: /place?.coordinate.longitude)
         
         self?.callLocationSelected()
         }
         
         case 5:
         
         if homeAPI?.addresses?.home?.address == nil || /homeAPI?.addresses?.home?.address?.isEmpty {
         
         GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
         
         debugPrint("Home")
         self?.apiAddAddress(place: place, category: "HOME")
         }
         
         } else {
         
         viewChooseAddress.showView(address: homeAPI?.addresses?.home)
         
         }
         
         case 6:
         
         if homeAPI?.addresses?.work?.address == nil || /homeAPI?.addresses?.work?.address?.isEmpty {
         
         GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
         
         debugPrint("Work")
         self?.apiAddAddress(place: place, category: "WORK")
         }
         } else {
         viewChooseAddress.showView(address: homeAPI?.addresses?.work)
         }
         
         case 7:
         
         GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
         
         debugPrint("Home")
         self?.apiEditAddress(place: place, category: "HOME")
         }
         
         case 8:
         
         GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
         
         debugPrint("WORK")
         self?.apiEditAddress(place: place, category: "WORK")
         }
         
         default:
         break
         } */
        
    }
    func callLocationSelected() {
        
        if (/viewStop1.isHidden && /viewStop2.isHidden) {
            
            let pickupDict = ["address":/serviceRequest.locationNameDest,
                              "lat":/serviceRequest.latitudeDest,
                              "long":/serviceRequest.longitudeDest,
                              "seletedType":"Pickup details",
                              "type": "0"] as [String : Any]
            arrlocationGlobal.add(pickupDict)
            
            let dict = ["address":/serviceRequest.locationName,
                        "lat":/serviceRequest.latitude,
                        "long":/serviceRequest.longitude,
                        "seletedType": /serviceRequest.stops.count > 0 ? "Dropoff location \(/serviceRequest.stops.count+1) details":"Dropoff location",
                        "type": "1"] as [String : Any]
            arrlocationGlobal.add(dict)
            
            
            
            
            if !(/txtDropOffLocation.text?.isEmpty) && txtDropOffLocation.text != nil {
               locationsSelected1()
            }
        }
        else
        {
            
        }
    }
    
    
    
    @IBAction func buttonOptionForMeClicked(_ sender: UIButton) {
        
        /* isSwitchRiderViewOpened = false
         ViewForfriend.isHidden = true
         
         ViewEnterPickUpDropOff.isHidden = false
         imageViewHeaderProfilePic.isHidden = false
         
         buttonHeaderBookingFor.setTitle("For me", for: .normal) */
        
        
        bottomViewBookforFriend.minimizeBookForFriendView()
        isSwitchRiderViewOpened = false
        setupBookForInitialUI(isSwitchRiderViewOpened : isSwitchRiderViewOpened) // No need to Clear serviceReq data as already clear on 'buttonBookingForClicked'
        
    }
    
    @IBAction func buttonBookForFriendClicked(_ sender: Any) {
        debugPrint("Open New UI")
        
        bottomViewBookforFriend.showBookForFriendView(superView: view, requestPara: serviceRequest)
    }
    
    @IBAction func buttonBookingForClicked(_ sender: UIButton) {
        
        view.endEditing(true)
        
        isSwitchRiderViewOpened = !isSwitchRiderViewOpened
        
        bottomViewBookforFriend.minimizeBookForFriendView()
        clearServiceRequestData()
        setupBookForInitialUI(isSwitchRiderViewOpened : isSwitchRiderViewOpened)
        
        serviceRequest.booking_type = nil
        serviceRequest.friend_name = nil
        serviceRequest.friend_phone_number = nil
        serviceRequest.friend_phone_code = nil
        
        bottomViewBookforFriend.clearTextfields()
        
        
        
        var title = ""
        if isSwitchRiderViewOpened{
            title = "Switch Rider"
        }else{
            title = "For me"
        }
        buttonHeaderBookingFor.setTitle(title, for: .normal)
        
        
        
        /*ViewForfriend.isHidden = !isSwitchRiderViewOpened
         ViewEnterPickUpDropOff.isHidden = isSwitchRiderViewOpened
         imageViewHeaderProfilePic.isHidden = isSwitchRiderViewOpened */
        
    }
    
    @IBAction func buttonRoadPickUp(_ sender: Any) {
        
        guard let vc = R.storyboard.bookService.scannerViewController() else {return}
        vc.modalPresentationStyle = .overFullScreen
        vc.delegate = self
        presentVC(vc)
    }
    
    @IBAction func buttonBookByCall(_ sender: Any) {
        callToNumber(number: "+94760111154")
        
    }
    
    @IBAction func buttonAddStopClicked(_ sender: Any) {
        
        view.endEditing(true)
    
        let count = serviceRequest.stops.count
        self.serviceRequest.stops.append(Stops(latitude: nil, longitude: nil, priority: count + 1, address: nil))
        let hasStops = !(serviceRequest.stops.count > 0)
        let numberOfStops =  /Int(/UDSingleton.shared.appSettings?.appSettings?.max_number_of_stops)
        buttonAddStop.isHidden = serviceRequest.stops.count >= numberOfStops
        UIView.animate(withDuration: 0.2) { [weak self] in
            
            self?.constraintHeightViewEnterPickDrop.constant += 48
            self?.constraintYAddLocationView.constant -= 48
            self?.view.layoutIfNeeded()
            
            //self?.viewStoppageDesc.isHidden = hasStops
            
        }
        stopsTableView.reloadData()
        
        
        buttonAddStop.isHidden = true
        
       // viewLocationTableContainer.alpha = 1
    }
    
    @IBAction func buttonRemoveStopClicked(_ sender: UIButton) {
        serviceRequest.stops.remove(at: sender.tag)
        buttonAddStop.isHidden = false
        let hasStops = !(self.serviceRequest.stops.count > 0)
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.constraintHeightViewEnterPickDrop.constant -= 48
            self?.constraintYAddLocationView.constant += 48
            self?.view.layoutIfNeeded()
            //self?.viewStoppageDesc.isHidden = hasStops
            
        }
        // self.buttonRemoveStop.isHidden = true
        stopsTableView.reloadData()
        
        //viewLocationTableContainer.alpha = 1
    }
    
    
    @IBAction func buttonContinueWithStoppageClicked(_ sender: UIButton) {
        print(serviceRequest.latitude)
        let pickupDict = ["address":/serviceRequest.locationNameDest,
                          "lat":/serviceRequest.latitudeDest,
                          "long":/serviceRequest.longitudeDest,
                          "seletedType":"Pickup details",
                          "type": "0"] as [String : Any]
        arrlocationGlobal.add(pickupDict)
        //arrStopsGlob.add(pickupDict)
        // arrFilterStopsG.add(pickupDict)
        
        for i in 0 ..< /serviceRequest.stops.count {
            let dict = ["address":/serviceRequest.stops[i].address,
                        "lat":/serviceRequest.stops[i].latitude,
                        "long":/serviceRequest.stops[i].longitude,
                        "seletedType":"Dropoff location-\(i+1) details",
                        "type": "1"] as [String : Any]
            arrlocationGlobal.add(dict)
            
            //arrFilterStopsG.add(dict)
        }
        let dict = ["address":/serviceRequest.locationName,
                    "lat":/serviceRequest.latitude,
                    "long":/serviceRequest.longitude,
                    "seletedType": /serviceRequest.stops.count > 0 ? "Dropoff location \(/serviceRequest.stops.count+1) details":"Dropoff location",
                    "type": "1"] as [String : Any]
        arrlocationGlobal.add(dict)
        
        print(/serviceRequest.stops.count)
        
        locationsSelected()
    }
    
    
    
    //MARK:- FUNCTIONS
    //MARK:-
    
    
    func addKeyBoardObserver() {
        
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow),name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillhide),name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeKeyBoardObserver() {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            animateTableLocationSearch(true, height: keyboardHeight)
        }
    }
    
    @objc func keyboardWillhide(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            animateTableLocationSearch(false, height: keyboardHeight)
        }
    }
    
    func animateTableLocationSearch(_ isToShown : Bool , height : CGFloat) {
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.constraintBottomTablelocationsearch?.constant =  isToShown ?  (height) :  0
            self?.view.layoutIfNeeded()
        }
    }
    
    func setGradientBackground() {
        
        let colorTop =  UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.05).cgColor
        let colorBottom = UIColor.white.cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = viewBottom.bounds
        //        viewBottom.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    
    func setUpBasicSettings() {
        
        constraintNavigationBasicBar.constant = BookingPopUpFrames.navigationBarHeight + 10
        //   constraintNavigationLocationBar.constant = BookingPopUpFrames.navigationBarHeight
        
        constraintTopSegmentedView.constant = BookingPopUpFrames.paddingX
        
        //  viewPickUpLocation.alpha  = 0  //To SetUP for Only Drop Location
        //        constraintHeightPickUpView.constant = 0
        
        print("Services ===>", UDSingleton.shared.userData?.services)
        
        //DELIVERSOME let selectedService = services.filter({$0.serviceCategoryId == 7}).first
        /*  --- Rohit ---
         if let services = UDSingleton.shared.userData?.services,
         let selectedService = services.filter({$0.serviceCategoryId == 7}).first {
         
         // Change here for service
         // serviceRequest.serviceSelected = services[5] //Ankush services[0]
         serviceRequest.serviceSelected = selectedService
         
         // call terminology API
         getTerminologyData(serviceId: /selectedService.serviceCategoryId)
         }
         */
        
        
        if let services = UDSingleton.shared.userData?.services,
           let selectedService = services.first {
            
            // Change here for service
            // serviceRequest.serviceSelected = services[5] //Ankush services[0]
            serviceRequest.serviceSelected = selectedService
            
            // call terminology API
            getTerminologyData(serviceId: /selectedService.serviceCategoryId)
        }
        
        
        
        
        //constraintOnlyBackButtonTop.constant = BookingPopUpFrames.statusBarHeight
        screenType = ScreenType(mapMode: .ZeroMode, entryType: .Forward)
        
        self.txtDropOffLocation.delegate = self
        self.txtPickUpLocation.delegate = self
        txtStop1.delegate = self
        txtStop2.delegate = self
        
        assignDelegates()
        setGradientBackground()
        
        // btnLocationNext.setImage(#imageLiteral(resourceName: "NextMaterial").setLocalizedImage(), for: .normal)
        //  btnLocationBack.setImage(#imageLiteral(resourceName: "Back").setLocalizedImage(), for: .normal)
        btnOnlyBack.setImage(#imageLiteral(resourceName: "ic_back_arrow_black.png").setLocalizedImage(), for: .normal)
        txtDropOffLocation.setAlignment()
        txtPickUpLocation.setAlignment()
        
        viewTopBarBasicHome.isHidden = isFromPackage
        viewTitlePackageBooking.isHidden = !isFromPackage
        
        
        viewTitlePackageBooking.setViewBackgroundColorTheme()
        buttonContinue.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        
        
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
        switch template {
        case .Moby?:
            viewTopBarBasicHome.isHidden = true
            viewNavigation.isHidden = false
            buttonNewCurrentLocation.isHidden = false
            buttonNewSelectLocation.isHidden = false
            btnCurrentLocation.isHidden = true
            viewStatusBar.setViewBackgroundColorHeader()
            
        case .DeliverSome?:
            viewTopBarBasicHome.isHidden = true
            viewNavigation.isHidden = false
            buttonNewCurrentLocation.isHidden = true
            buttonNewSelectLocation.isHidden = true
            btnCurrentLocation.isHidden = true
            viewStatusBar.setViewBackgroundColorHeader()
            
        case .GoMove?:
            viewStatusBar.backgroundColor = UIColor.clear
            
            
            
        default:
            viewNavigation.isHidden = true
            //buttonNewCurrentLocation.isHidden = true
            buttonNewSelectLocation.isHidden = true
            btnCurrentLocation.isHidden = false
            if isFromPackage {
                viewStatusBar.isHidden = false
                viewStatusBar.setViewBackgroundColorHeader()
            } else {
                viewStatusBar.isHidden = true
            }
            
        }
        
        viewOnMApNavigation.isHidden = true
        viewNavigation.setViewBackgroundColorHeader()
        
        viewOnMApNavigation.setViewBackgroundColorHeader()
        
        buttonNewSideMenu.setButtonWithTintColorHeaderText()
        buttonNewBack.setButtonWithTintColorHeaderText()
        
        buttonlNavigationTitle.setButtonWithTitleColorHeaderText()
    }
    
    func getUpdatedData() {
        var id = Int()
        
        if isFromPackage{
            
            id = /modalPackages?.package?.pricingData?.categories?.first?.categoryId
        }
        
        /*  ------ Rohit -----
         // Change here for service
         guard let services = UDSingleton.shared.userData?.services,
         let selectedService =  services.filter({$0.serviceCategoryId == id}).first else { return }
         
         getLocalDriverForParticularService(service: selectedService)
         */
        
        guard let services = UDSingleton.shared.userData?.services,
              let selectedService =  services.first else { return }
        
        
       
        
        getLocalDriverForParticularService(service: selectedService)
    }
    
    func assignDelegates() {
        
        viewSelectService.delegate = self
        viewGasService.delegate = self
        viewDrinkingWaterService.delegate = self
        viewOrderPricing.delegate = self
        viewUnderProcess.delegate = self
        viewDriverAccepted.delegate = self
        viewInvoice.delegate = self
        viewScheduler.delegate = self
        viewDriverRating.delegate = self
        viewFreightBrand.delegate = self
        viewFreightProduct.delegate = self
        viewFreightOrder.delegate = self
        // viewConfirmPickUp.delegate = self
        viewNewInvoice.delegate = self
        bottomViewBookforFriend.delegate = self
        viewChooseAddress.delegate = self
        viewAddLocationFrom.delegate = self
        
    }
    
    
    func showBasicNavigation(updateType: MapMode) {
        self.view.endEditing(true)
        if updateType == .SelectingLocationMode  {
            
            configureRecentLocationTableView()
        }
        
        UIView.animate(withDuration: 0.4, animations: { [weak self] in
            
            if updateType == .NormalMode {
                
                self?.viewLocationTableContainer.alpha = 0 // Ankush
                
                
                self?.constraintNavigationBasicBar.constant = -(BookingPopUpFrames.navigationBarHeight)
                self?.constraintYAddLocationView.constant = (BookingPopUpFrames.navigationBarHeight + CGFloat(55) + 10) // 55 - Height for navigation View (Book a taxi)
                self?.imgViewPickingLocation.isHidden =  true // false
                self?.mapView.isUserInteractionEnabled = true
                // self?.btnOnlyBack.alpha = 0
                
                self?.btnOnlyBack.alpha = /self?.isFromPackage ? 1 : 0
                
                
                self?.segmentedMapType.isHidden = true
                self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingX
                // self?.btnCurrentLocation.isHidden = false
                self?.viewStoppageDesc.isHidden = true
                //self?.vwFooter.isHidden = false
                self?.viewOnMApNavigation.isHidden = true
                
                let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
                switch template {
                case .Moby?:
                    self?.viewTopBarBasicHome.isHidden = true
                    self?.viewNavigation.isHidden = false
                    self?.buttonNewCurrentLocation.isHidden = false
                    self?.buttonNewSelectLocation.isHidden = false
                    self?.btnCurrentLocation.isHidden = true
                    
                    
                case .DeliverSome?:
                    self?.viewTopBarBasicHome.isHidden = true
                    self?.viewNavigation.isHidden = false
                    self?.buttonNewCurrentLocation.isHidden = true
                    self?.buttonNewSelectLocation.isHidden = true
                    self?.btnCurrentLocation.isHidden = true
                    
                default:
                    self?.viewNavigation.isHidden = true
                    self?.buttonNewCurrentLocation.isHidden = true
                    self?.buttonNewSelectLocation.isHidden = true
                    self?.btnCurrentLocation.isHidden = false
                }
                
                
                
                
            } else if updateType == .SelectingLocationMode {
                
                self?.txtDropOffLocation.becomeFirstResponder()
                self?.viewLocationTableContainer.alpha = 1 // Ankush
                
                self?.segmentedMapType.isHidden = true
                self?.btnCurrentLocation.isHidden = false
                self?.constraintNavigationBasicBar.constant = BookingPopUpFrames.navigationBarHeight + BookingPopUpFrames.navigationTopPadding
                
                self?.imgViewPickingLocation.isHidden = true //false
                
                self?.mapView.isUserInteractionEnabled = true
                self?.btnOnlyBack.alpha = 0
                self?.viewTitlePackageBooking.isHidden = true
                
                guard let selectedService  = self?.serviceRequest.serviceSelected else{return}
                
                //                if  /selectedService.serviceCategoryId > 3 {
                self?.locationType = .PickUpDropOff
                self?.imgViewPickingLocation.image = #imageLiteral(resourceName: "PickUpMarker")
                
                let count = /self?.serviceRequest.stops.count
                let stopsHeight = count * 48
                
                let locationViewHeight = 100 + stopsHeight
                
                self?.constraintYAddLocationView.constant = -(BookingPopUpFrames.navigationBarHeight + CGFloat(locationViewHeight ))
                self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingXPickUpLocation
                self?.viewStoppageDesc.isHidden = !(count > 0)
                
                self?.viewStoppageDesc.isHidden = true
                //                } else{
                //
                //                    self?.constraintYAddLocationView.constant = -(BookingPopUpFrames.navigationBarHeight + CGFloat(55)) // 55 - Height for navigation View (Book a taxi)
                //                    self?.locationType = .OnlyDropOff
                //                    self?.imgViewPickingLocation.image = #imageLiteral(resourceName: "DropMarker")
                //                    self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingXDropOffLocation
                //                }
                
                let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
                switch template {
                case .Moby?:
                    self?.viewTopBarBasicHome.isHidden = true
                    self?.viewNavigation.isHidden = true
                    self?.buttonNewCurrentLocation.isHidden = true
                    self?.buttonNewSelectLocation.isHidden = true
                    self?.btnCurrentLocation.isHidden = true
                    self?.viewOnMApNavigation.isHidden = false
                    self?.buttonlNavigationTitle.setTitle("Address", for: .normal)
                    break
                    
                    
                case .DeliverSome?:
                    self?.viewTopBarBasicHome.isHidden = true
                    self?.viewNavigation.isHidden = true
                    self?.buttonNewCurrentLocation.isHidden = true
                    self?.buttonNewSelectLocation.isHidden = true
                    self?.btnCurrentLocation.isHidden = true
                    self?.viewOnMApNavigation.isHidden = false
                    self?.buttonlNavigationTitle.setTitle("Address", for: .normal)
                    
                    
                default:
                    debugPrint("default")
                }
                
                
                
            }else if updateType == .OrderDetail || updateType == .SelectingFreightBrand || updateType == .SelectingFreightProduct {
                
                self?.buttonlNavigationTitle.setTitle("", for: .normal)
                
                //                let template = AppTemplate(rawValue: /UDSingleton.shared.appTerminology?.key_value?.template?.toInt())
                let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
                switch template {
                
                case .Mover?:
                    self?.viewLocationTableContainer.alpha = 0 // Ankush
                    
                    self?.constraintNavigationBasicBar.constant = -(BookingPopUpFrames.navigationBarHeight)
                    self?.constraintYAddLocationView.constant = (BookingPopUpFrames.navigationBarHeight + CGFloat(55) + 10) // 55 - Height for navigation View (Book a taxi)
                    self?.imgViewPickingLocation.isHidden =  true // false
                    self?.mapView.isUserInteractionEnabled = true
                    //self?.btnOnlyBack.alpha = 0
                    self?.btnOnlyBack.alpha = 1
                    
                    self?.segmentedMapType.isHidden = true
                    self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingX
                    self?.btnCurrentLocation.isHidden = false
                    self?.viewStoppageDesc.isHidden = true
                //self?.vwFooter.isHidden = false
                
                default:
                    self?.viewLocationTableContainer.alpha = 0
                    self?.btnOnlyBack.alpha = 1// Davender
                    break
                    
                }
               
                self?.mapView.isUserInteractionEnabled = true
                self?.btnOnlyBack.alpha = 1
                self?.segmentedMapType.isHidden = true
                self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingX
                self?.btnCurrentLocation.isHidden = true
                self?.constraintNavigationBasicBar.constant = BookingPopUpFrames.navigationBarHeight + BookingPopUpFrames.navigationTopPadding
                
                // self?.serviceRequest.isBookingFromPackage = self?.isFromPackage
                guard let service  = self?.serviceRequest.serviceSelected else{return}
                
                // Ankush self?.imgViewPickingLocation.isHidden  = /service.serviceCategoryId > 3
                
                // Ankush self?.constraintYAddLocationView.constant =  /service.serviceCategoryId > 3 ? (BookingPopUpFrames.navigationBarHeight + CGFloat(115)) : BookingPopUpFrames.navigationBarHeight + CGFloat(55)
                
                // 96 - Pick up and dropoff View size, 55 - Height for navigation View (Book a taxi)
                let locationViewHeight = (!(/self?.viewStop1.isHidden) && !(/self?.viewStop2.isHidden)) ? (96 + 96) : (/self?.viewStop1.isHidden || /self?.viewStop2.isHidden) ? (96 + 48) : 96
                self?.constraintYAddLocationView.constant =  /service.serviceCategoryId > 3 ? (BookingPopUpFrames.navigationBarHeight + CGFloat(locationViewHeight)) : BookingPopUpFrames.navigationBarHeight + CGFloat(55)
                
                
            }else if updateType == .RequestAcceptedMode || updateType == .OnTrackMode {
                
                self?.constraintNavigationBasicBar.constant = -(BookingPopUpFrames.navigationBarHeight)
                
                self?.imgViewPickingLocation.isHidden = true
                self?.mapView.isUserInteractionEnabled = true
                self?.btnOnlyBack.alpha = 0
                self?.segmentedMapType.isHidden = true
                self?.btnCurrentLocation.isHidden = false
                
                guard let currOrder = self?.currentOrder else {return}
                
                if /currOrder.serviceId > 3 {
                    // Ankush self?.constraintYAddLocationView.constant = (BookingPopUpFrames.navigationBarHeight + CGFloat(115) + 10)
                    
                    // 96 - Pick up and dropoff View size
                    let locationViewHeight = (!(/self?.viewStop1.isHidden) && !(/self?.viewStop2.isHidden)) ? (96 + 96) : (!(/self?.viewStop1.isHidden) || !(/self?.viewStop2.isHidden)) ? (96 + 48) : 96
                    self?.constraintYAddLocationView.constant = (BookingPopUpFrames.navigationBarHeight + CGFloat(locationViewHeight) + 10)
                }else{
                    self?.constraintYAddLocationView.constant = (BookingPopUpFrames.navigationBarHeight + CGFloat(55) + 10) //  55 - Height for navigation View (Book a taxi)
                }
                
                let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
                switch template {
                
                case .Moby?:
                    self?.viewOnMApNavigation.isHidden = true
                    self?.viewNavigation.isHidden = true
                    self?.viewOnMApNavigation.isHidden = true
                    self?.buttonNewSelectLocation.isHidden = true
                    self?.buttonNewCurrentLocation.isHidden = true
                    
                default :
                    break
                }
                
                
            } else if updateType == .ServiceDoneMode {
                // To Hide Top View Containing menu button
                self?.constraintNavigationBasicBar.constant =  BookingPopUpFrames.navigationBarHeight + BookingPopUpFrames.navigationTopPadding
                
            } else if updateType == .OrderPricingMode {
                

                if ((self?.isFromCancellation) != nil) && ((self?.isFromCancellation) == true) {
                    let btnNewBackBtn = UIButton()
                    btnNewBackBtn.tag = 1001
                    btnNewBackBtn.frame = (self?.btnOnlyBack.frame)!
                
                    btnNewBackBtn.addTarget(self, action: #selector(self?.pressed(_ :)), for: .touchUpInside)
                    btnNewBackBtn.backgroundColor = UIColor.clear
                    self?.view.addSubview(btnNewBackBtn)
                }
                
                let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
                switch template {
                
                case .Mover?:
                    self?.mapView.isUserInteractionEnabled = true
                    self?.btnOnlyBack.alpha = 1
                    self?.segmentedMapType.isHidden = true
                    self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingX
                    self?.btnCurrentLocation.isHidden = true
                    self?.constraintNavigationBasicBar.constant = BookingPopUpFrames.navigationBarHeight + BookingPopUpFrames.navigationTopPadding
                    
                    // self?.serviceRequest.isBookingFromPackage = self?.isFromPackage
                    guard let service  = self?.serviceRequest.serviceSelected else{return}
                    
                    // Ankush self?.imgViewPickingLocation.isHidden  = /service.serviceCategoryId > 3
                    
                    // Ankush self?.constraintYAddLocationView.constant =  /service.serviceCategoryId > 3 ? (BookingPopUpFrames.navigationBarHeight + CGFloat(115)) : BookingPopUpFrames.navigationBarHeight + CGFloat(55)
                    
                    // 96 - Pick up and dropoff View size, 55 - Height for navigation View (Book a taxi)
                    let locationViewHeight = (!(/self?.viewStop1.isHidden) && !(/self?.viewStop2.isHidden)) ? (96 + 96) : (/self?.viewStop1.isHidden || /self?.viewStop2.isHidden) ? (96 + 48) : 96
                    self?.constraintYAddLocationView.constant =  /service.serviceCategoryId > 3 ? (BookingPopUpFrames.navigationBarHeight + CGFloat(locationViewHeight)) : BookingPopUpFrames.navigationBarHeight + CGFloat(55)
                    
                case .Moby?:
                    self?.viewOnMApNavigation.isHidden = false
                    
                default:
                    
                    if ((self?.isFromCancellation) != nil) && ((self?.isFromCancellation) == true) {
                    }
                    else
                    {
                        self?.mapView.isUserInteractionEnabled = true
                        self?.btnOnlyBack.alpha = 1
                        self?.segmentedMapType.isHidden = true
                        self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingX
                        self?.btnCurrentLocation.isHidden = true
                        self?.constraintNavigationBasicBar.constant = BookingPopUpFrames.navigationBarHeight + BookingPopUpFrames.navigationTopPadding

                        // self?.serviceRequest.isBookingFromPackage = self?.isFromPackage
                        guard let service  = self?.serviceRequest.serviceSelected else{return}

                        let locationViewHeight = (!(/self?.viewStop1.isHidden) && !(/self?.viewStop2.isHidden)) ? (96 + 96) : (/self?.viewStop1.isHidden || /self?.viewStop2.isHidden) ? (96 + 48) : 96
                        self?.constraintYAddLocationView.constant =  /service.serviceCategoryId > 3 ? (BookingPopUpFrames.navigationBarHeight + CGFloat(locationViewHeight)) : BookingPopUpFrames.navigationBarHeight + CGFloat(55)
                    }
                    
                    
                    break
                    
                }
                
                
                
            }
            self?.view.layoutIfNeeded()
        }, completion: { (done) in
        })
    }
    
    
    // Button Action
             @objc func pressed(_ sender: UIButton) {
                    print("Pressed")
                
            
                self.moveToNormalMode()
                
                
//                self.view.subviews.forEach{v in
//                    if v.tag == 1001{
//                        v.removeFromSuperview()
//                    }
//                }
                
                self.view.viewWithTag(1001)?.removeFromSuperview()
                
                self.isFromCancellation = false
              }
    
    @objc func pressed1(_ sender: UIButton) {
           print("Pressed")
      
       self.view.viewWithTag(1002)?.removeFromSuperview()
        
        self.presentingViewController?.dismiss(animated: false, completion:nil)
       
       
     }
    
    
    
    func clearSelectedLocation() {
        view.endEditing(true)
        // results.removeAll()
        // tblLocationSearch.reloadData()
        // viewLocationTableContainer.alpha = 0
    }
    
    
    func showSelectServiceView(moveType : MoveType = .Forward) {
        isSearching = false
        isOnHome = true
        //Rohit-----
        guard let services = UDSingleton.shared.appSettings?.services else {return}
        //guard let services = UDSingleton.shared.appSettings?.services else {return}
        var selectedServices = [Service]()
        if /self.isFromPackage {
            let brands = self.modalPackages?.package?.pricingData?.categoryBrands
            var categoryIds = [Int]()
            for brand in brands ?? []{
                categoryIds.append(brand.categoryId ?? 0)
            }
            for service in services{
                let sBrands = service.brands
                for sBrand in sBrands ?? []{
                    if categoryIds.contains(sBrand.categoryId ?? 0){
                        selectedServices.append(service)
                    }
                }
            }
            viewSelectService.showSelectServiceView(superView: viewMapContainer, moveType: moveType, requestPara: self.serviceRequest, service: selectedServices,fromPackage : true)
        }else{
            
            viewSelectService.showSelectServiceView(superView: viewMapContainer, moveType: moveType, requestPara: self.serviceRequest, service: services)
        }
    }
    
    func showParticularAddOrderView(type : MoveType) {
        
        let bookingFlow = serviceRequest.serviceSelected?.booking_flow
        if bookingFlow == "1" {
            if APIBasePath.isPikkup {
                if type == .Backward{
                    guard let vc = R.storyboard.orderDetails.showOrderDetailsViewController() else{return}
                    vc.delegates = self
                    vc.serviceRequest = serviceRequest
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle = .coverVertical
                    presentVC(vc, false)
                }else{
                    guard let vc = R.storyboard.orderDetails.enterOrderDetailsViewController() else{return}
                    vc.serviceReq = serviceRequest
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle = .coverVertical
                    vc.delegate = self
                    presentVC(vc, true)
                    
                    //viewFreightOrder.showFreightOrderView(supView: viewMapContainer, moveType: type, requestPara: serviceRequest)
                }
            } else{
                viewFreightOrder.showFreightOrderView(supView: viewMapContainer, moveType: type, requestPara: serviceRequest)
            }
            
        }

        
        
    }
    
    
    func hideParticularOrderView() {
        
        let bookingFlow = serviceRequest.serviceSelected?.booking_flow
        
        if bookingFlow == "1" {
            viewFreightOrder.minimizeFreightOrderView()
        }
        
        /*
         guard let serviceId = serviceRequest.serviceSelected?.serviceCategoryId else {return}
         
         switch serviceId {
         case 1,3:
         viewGasService.minimizeGasServiceView()
         case 2:
         viewDrinkingWaterService.minimizeDrinkingWaterView()
         case 4, 7,10:
         viewFreightOrder.minimizeFreightOrderView()
         default:
         break
         }*/
    }
    
    
    func showFreightBrandView(moveType : MoveType) {
        viewNewInvoice.minimizeNewInvoiceView()
              viewInvoice.minimizeInvoiceView()
              viewDriverAccepted.minimizeDriverView()
              viewFreightOrder.minimizeFreightOrderView()
              viewFreightProduct.minimizeProductView()
              viewOrderPricing.minimizeOrderPricingView()
              viewUnderProcess.minimizeProcessingView()
              viewDriverAccepted.minimizeDriverView()
              viewSelectService.minimizeSelectServiceView()
              viewDriverRating.minimizeDriverRatingView()
        
        viewFreightBrand.showBrandView(superView: viewMapContainer, moveType: moveType, requestPara: serviceRequest, drivers: drivers)
    }
    
    func showFreightProductView(moveType : MoveType) {
        viewNewInvoice.minimizeNewInvoiceView()
                viewInvoice.minimizeInvoiceView()
                viewDriverAccepted.minimizeDriverView()
                viewFreightBrand.minimizeSelectBrandView()
                viewFreightOrder.minimizeFreightOrderView()
                viewOrderPricing.minimizeOrderPricingView()
                viewUnderProcess.minimizeProcessingView()
                viewDriverAccepted.minimizeDriverView()
                viewSelectService.minimizeSelectServiceView()
                viewDriverRating.minimizeDriverRatingView()
        viewFreightProduct.showProductView(superView: viewMapContainer, moveType: moveType, requestPara: serviceRequest, modalPackages: modalPackages, drivers: drivers)
    }
    
    func showSchedulerView(moveType : MoveType) {
        self.btnOnlyBack.alpha = 1
        viewScheduler.showSchedulerView(superView: viewMapContainer, moveType: moveType, requestPara: serviceRequest)
    }
    
    
    func showPricingView(moveType : MoveType ) {
        self.btnOnlyBack.alpha = 1
        
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
        switch template {
        
        case .Moby?, .DeliverSome?:
            viewOnMApNavigation.isHidden = false
            
        default:
            break
        }
        
        viewNewInvoice.minimizeNewInvoiceView()
            viewInvoice.minimizeInvoiceView()
            viewDriverAccepted.minimizeDriverView()
            viewFreightBrand.minimizeSelectBrandView()
            viewFreightOrder.minimizeFreightOrderView()
            viewFreightProduct.minimizeProductView()
            viewUnderProcess.minimizeProcessingView()
            viewDriverAccepted.minimizeDriverView()
            viewSelectService.minimizeSelectServiceView()
            viewDriverRating.minimizeDriverRatingView()
        
        let request = serviceRequest
        
//        if request.requestType == .Future
//        {
//            
//            let todayDate = Date().toLocalDateAcTOLocale()
//            let requetedDate = request.orderDateTime.toLocalDateAcTOLocale()
//            
//            let todayTime = Date().addHours(hoursToAdd: 1).toLocalTimeAcTOLocale()
//            let requetedTime = request.orderDateTime.toLocalTimeAcTOLocale()
//            
//            if  requetedDate <= todayDate {
//                switch todayTime.compare(requetedTime) {
//                    case .orderedAscending     :   print("Date A is earlier than date B")
//                    case .orderedSame          :   print("The two dates are the same")
//                        
//                    case .orderedDescending    :   print("Date A is later than date B")
//                        serviceRequest.orderDateTime = Date()
//                }
//            }
//        }
        
        viewOrderPricing.showOrderPricingView(superView: viewMapContainer, moveType: moveType, requestPara: serviceRequest, modalPackages: modalPackages,zoneArray:zoneArray)
        
    }
    
    /* func showConfirmPickUpView(moveType : MoveType ) {
     self.btnOnlyBack.alpha = 1
     // viewConfirmPickUp.showConfirmPickUPView(superView: viewMapContainer, moveType: moveType, requestPara: serviceRequest)
     } */
    
    func showConfirmPickUpController() {
        
        guard let vc = R.storyboard.bookService.confirmPickUpViewController() else{return}
        vc.modalPresentationStyle = .overFullScreen
        vc.request = serviceRequest
        vc.delegate = self
        presentVC(vc, false)
    }
    
    func showAddLocationFromMapController(buttonTag: Int,isFromStop:Bool) {
        
        guard let vc = R.storyboard.bookService.addLocationFromMapViewController() else{return}
        vc.modalPresentationStyle = .overFullScreen
        vc.request = serviceRequest
        vc.locationType = buttonTag
        vc.isFromStop = isFromStop
        vc.delegate = self
        presentVC(vc, false)
    }
    
    func showProcessingView( moveType : MoveType, isHalfWayStop: Bool = false,  isVehicleBreakdown: Bool = false) {
        self.btnOnlyBack.alpha = 0
        viewOnMApNavigation.isHidden = true
        
        guard let current = currentOrder else{return}
        viewNewInvoice.minimizeNewInvoiceView()
               viewInvoice.minimizeInvoiceView()
               viewDriverAccepted.minimizeDriverView()
               viewFreightBrand.minimizeSelectBrandView()
               viewFreightOrder.minimizeFreightOrderView()
               viewFreightProduct.minimizeProductView()
               viewOrderPricing.minimizeOrderPricingView()
               viewDriverAccepted.minimizeDriverView()
               viewSelectService.minimizeSelectServiceView()
               viewDriverRating.minimizeDriverRatingView()
        dropPickUpAndDropOffPins(pickLat: /currentOrder?.pickUpLatitude, pickLong: /currentOrder?.pickUpLongitude, dropLat: /currentOrder?.dropOffLatitude, dropLong: /currentOrder?.dropOffLongitude)
        viewUnderProcess.showWaitingView(superView: viewMapContainer , order: current, isHalfWayStop: isHalfWayStop, isVehicleBreakdown: isVehicleBreakdown,  serviceRequest: serviceRequest)
    }
    
    func showDriverView() {
        guard let current = currentOrder else{return}
        viewNewInvoice.minimizeNewInvoiceView()
             viewInvoice.minimizeInvoiceView()
             viewFreightBrand.minimizeSelectBrandView()
             viewFreightOrder.minimizeFreightOrderView()
             viewFreightProduct.minimizeProductView()
             viewOrderPricing.minimizeOrderPricingView()
             viewUnderProcess.minimizeProcessingView()
             viewDriverAccepted.minimizeDriverView()
             viewSelectService.minimizeSelectServiceView()
             viewDriverRating.minimizeDriverRatingView()
        viewDriverAccepted.showDriverAcceptedView(superView: viewMapContainer, order: current)
    }
    
    func showInvoiceView() {
        guard let current = currentOrder else{return}
        if UDSingleton.shared.is_multiple_requests{
            viewDriverAccepted.minimizeDriverView()
        }
        viewInvoice.showInvoiceView(superView: viewMapContainer, serviceRequest: serviceRequest, order: current)
    }
    
    func showNewInvoiceView() {
        guard let current = currentOrder else{return}
        if UDSingleton.shared.is_multiple_requests{
            viewDriverAccepted.minimizeDriverView()
        }
        
        viewInvoice.minimizeInvoiceView()
        viewDriverAccepted.minimizeDriverView()
        viewFreightBrand.minimizeSelectBrandView()
        viewFreightOrder.minimizeFreightOrderView()
        viewFreightProduct.minimizeProductView()
        viewOrderPricing.minimizeOrderPricingView()
        viewUnderProcess.minimizeProcessingView()
        viewDriverAccepted.minimizeDriverView()
        viewSelectService.minimizeSelectServiceView()
        viewDriverRating.minimizeDriverRatingView()
        viewNewInvoice.showNewInvoiceView(superView: viewMapContainer, order: current)
    }
    
    
    func showQPayProPaymentView(){
        
        if ez.topMostVC?.isKind(of: AddCardViewControllerCab.self) ?? false{
            
            return
        }
        guard let current = currentOrder,let id = current.orderId,let amount = current.payment?.finalCharge else{return}
        guard  let vc = R.storyboard.sideMenu.addCardViewControllerCab() else{return}
        vc.url = APIBasePath.paymentGateway(orderid: "\(id)", amount: amount)
        vc.webPayment = {
            DispatchQueue.main.async {
                self.showNewInvoiceView()
            }
        }
        self.pushVC(vc)
    }
    
    func payViaBenefit() {

        if (ez.topMostVC?.isKind(of: AddCardViewControllerCab.self) ?? false) || ez.topMostVC?.isKind(of: ContactUsVC.self) ?? false{
            return
        }

            guard let current = currentOrder else{return}

            guard  let vc = R.storyboard.sideMenu.addCardViewControllerCab() else{return}
        
            
        
            vc.url = APIBasePath.benefitPaymentUrl(amount: current.payment?.finalCharge, order_id: current.orderId)

            vc.webPayment = {

                self.moveToNormalMode()

                DispatchQueue.main.async {

                    self.showNewInvoiceView()

                }

            }

            

            vc.backCompeletion = {

                DispatchQueue.main.async {

                    self.moveToNormalMode()

                }

            }

            

            self.pushVC(vc)

        }
    
    func showUrlPaymentView(){
        
        if ez.topMostVC?.isKind(of: AddCardViewControllerCab.self) ?? false{
            
            return
        }
        guard let current = currentOrder else{return}
        guard  let vc = R.storyboard.sideMenu.addCardViewControllerCab() else{return}
        vc.url = current.payment?.paymentBody
        vc.webPayment = {
            DispatchQueue.main.async {
                self.showNewInvoiceView()
            }
        }
        self.pushVC(vc)
    }
    
    
    func showDriverRatingView() {
        guard let current = currentOrder else{return}
        viewNewInvoice.minimizeNewInvoiceView()
        viewInvoice.minimizeInvoiceView()
        viewDriverAccepted.minimizeDriverView()
        viewFreightBrand.minimizeSelectBrandView()
        viewFreightOrder.minimizeFreightOrderView()
        viewFreightProduct.minimizeProductView()
        viewOrderPricing.minimizeOrderPricingView()
        viewUnderProcess.minimizeProcessingView()
        viewDriverAccepted.minimizeDriverView()
        viewSelectService.minimizeSelectServiceView()
        viewDriverRating.showDriverRatingView(superView: viewMapContainer, order: current)
    }
    
    func moveToNormalMode() {
        
        self.cleanPath()
        imgViewPickingLocation.isHidden = true //false
        mapView.isUserInteractionEnabled = true
        if self.mapType == .google{
            self.mapView.clear()
        } else{
            self.clearAnnotationWithPath()
        }
        self.cleanPath()
        viewDriverAccepted.minimizeDriverView()
        viewFreightBrand.minimizeSelectBrandView()
        viewFreightOrder.minimizeFreightOrderView()
        viewFreightProduct.minimizeProductView()
        viewOrderPricing.minimizeOrderPricingView()
        viewDriverRating.minimizeDriverRatingView()
        viewUnderProcess.minimizeProcessingView()
        viewDriverAccepted.minimizeDriverView()
        self.screenType = ScreenType(mapMode: .NormalMode, entryType: .Backward)
    }
    
    func driverAcceptedMode(order : OrderCab) {
        self.isHomeApiLoading = false
        self.cleanPath()
        if self.mapType == .google{
            mapView.clear()
        } else{
            self.clearAnnotationWithPath()
        }
        // listeningEvent()
        currentOrder = order
        viewUnderProcess.minimizeProcessingView()
        viewOrderPricing.minimizeOrderPricingView()
        
        imgViewPickingLocation.isHidden = true
        mapView.isUserInteractionEnabled = true
        
        if UDSingleton.shared.is_multiple_requests{
            if trackOrder == nil && !isOnHome {
                moveToNormalMode()
            }else{
                screenType = ScreenType(mapMode: .RequestAcceptedMode, entryType: .Forward)
            }
        }else{
            screenType = ScreenType(mapMode: .RequestAcceptedMode, entryType: .Forward)
        }
        
        // Set
        if /order.serviceId > 3 &&  (order.orderStatus == .Confirmed || order.orderStatus == .reached) {
            destination =   CLLocationCoordinate2D(latitude: CLLocationDegrees(/order.pickUpLatitude) , longitude: CLLocationDegrees(/order.pickUpLongitude))
        }else{
            destination =   CLLocationCoordinate2D(latitude: CLLocationDegrees(/order.dropOffLatitude) , longitude: CLLocationDegrees(/order.dropOffLongitude))
        }
        
        source = CLLocationCoordinate2D(latitude: CLLocationDegrees(/order.driverAssigned?.driverLatitude) , longitude: CLLocationDegrees(/order.driverAssigned?.driverLongitude))
        
       // source = CLLocationCoordinate2D(latitude: CLLocationDegrees(Double(/order.driver_current_latitude) ) , longitude: CLLocationDegrees(Double(/order.driver_current_longitude) ))
        
        let stops = order.orderStatus == .Ongoing ? order.ride_stops : nil
        
        if self.mapType == .google{
            getPolylineRoute(source: source, destination: destination, stops: stops, model: nil)
        } else{
            
            calculateRoute(from: source!, to: destination!, stops: stops,isUserDriverMarker:true)
        }
        
//        getTerminologyData(serviceId: /order.serviceId)
    }
    
    ///To set final price to pricing pop up
    
    
    func calculateImageSize() {
        
        if self.mapType == MapType.google{
            if /mapView.camera.zoom >= minimumZoom {
                
                if previousZoom == /mapView.camera.zoom {return}
                
                previousZoom  = /mapView.camera.zoom
                let widthVal = (Float(vehicleSize.width)*(/mapView.camera.zoom).getZoomPercentage)/100
                let heightVal = (Float(vehicleSize.height)*(/mapView.camera.zoom).getZoomPercentage)/100
                vehicleCurrentSize =  CGSize(width: Double(widthVal), height: Double(heightVal))
                
                for item in movingDrivers {
                    
                    let marker = item.driverMarker
                    let id = /item.driver.driverServiceId
                    let mgVehicle = UIImage().getDriverImage(type: id)
                    marker.icon = mgVehicle.imageWithImage(scaledToSize: vehicleCurrentSize)
                    
                    /* let marker = item.driverMarker
                     let imageView = UIImageView(image: R.image.dropMarker())
                     imageView.kf.setImage(with: URL(string: /item.driver.icon_image_url), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                     marker.iconView = imageView */
                }
            }
        }
    }
    
    func configureMapView() {
        
        let didUpdateCurrentLocation : DidUpdatecurrentLocation = {[weak self] (manager,locations) in  //current location closure
            
            self?.currentLocation =   CLLocation(latitude: /manager.location?.coordinate.latitude, longitude: /manager.location?.coordinate.longitude )
            
            if self?.mapType == MapType.mapbox{
                
                
                if /self?.updateMapLocation {
                    self?.updateMapLocation = false
                    self?.mapBoxView.setCenter(CLLocationCoordinate2DMake(/manager.location?.coordinate.latitude, /manager.location?.coordinate.longitude), animated: true)
                }
                
                
                
            } else{
                
                let currentLocation = GMSCameraPosition.camera(withLatitude: /manager.location?.coordinate.latitude,longitude: /manager.location?.coordinate.longitude,
                                                               zoom: 14.0)
                if /self?.updateMapLocation {
                    self?.updateMapLocation = false
                    self?.mapView.camera = currentLocation
                    
                }
                
                let widthVal = (Float(vehicleSize.width)*(/self?.mapView.camera.zoom).getZoomPercentage)/100
                let heightVal = (Float(vehicleSize.height)*(/self?.mapView.camera.zoom).getZoomPercentage)/100
                vehicleCurrentSize =  CGSize(width: Double(widthVal), height: Double(heightVal))
            }
            
            // if !(/self?.isCurrentLocationUpdated) {
            
            self?.isCurrentLocationUpdated = true
            self?.mapDataSource?.getAddressFromlatLong(lat: /manager.location?.coordinate.latitude, long: /manager.location?.coordinate.longitude, completion: {  [weak self](strLocationName, country, name ,locality , sublocality) in
                
                UDSingleton.shared.userData?.userDetails?.user?.currentCountry = country
                UDSingleton.shared.userData?.userDetails?.user?.currentCountryCode = country?.locale()
                self?.setUpdatedAddress(location: strLocationName, coordinate: manager.location?.coordinate)
            })
            // }
        }
        
        let didChangePosition : DidChangePosition = { [weak self] in
            if self?.screenType.mapMode == .NormalMode {
                self?.calculateImageSize()
            }
        }
        
        let didStopPosition : MapsDidStopMoving = { [weak self]  (position) in
            
            // let newLocation = CLLocation(latitude: /position.latitude, longitude: /position.longitude)
            
            /* if  let currentLocation = self?.currentLocation {
             if let distance = GoogleMapsDataSource.getDistance(newPosition: currentLocation , previous:  newLocation) {
             //                    self?.btnCurrentLocation.isSelected = !(distance > Float(2))
             }
             } */
            
            if self?.screenType.mapMode == .SelectingLocationMode {
                // Ankush self?.viewLocationTableContainer.alpha = 0
                
                // self?.results.removeAll()
                // self?.tblLocationSearch.reloadData()
                
                let newLocation = CLLocation(latitude: /position.latitude, longitude: /position.longitude)
                
                /* self?.mapDataSource?.getAddressFromlatLong(lat: /position.latitude, long: /position.longitude, completion: { (address, country, name) in
                 self?.setUpdatedAddress(location: address, coordinate: position)
                 }) */
            }
        }
        if self.mapType == MapType.google{
            mapDataSource = GoogleMapsDataSource.init(mapStyleJSON: "MapStyle", mapView: mapView)
            mapView.delegate = mapDataSource
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = false
            mapView.settings.tiltGestures = false
            mapView.isBuildingsEnabled = false
            mapView.setMinZoom(2, maxZoom: 20)
            mapView.animate(toZoom: 6)
        } else{
            
            mapDataSource  = GoogleMapsDataSource(mapStyleJSON: nil)
        }
        mapDataSource?.didUpdateCurrentLocation = didUpdateCurrentLocation
        mapDataSource?.didChangePosition = didChangePosition
        mapDataSource?.mapStopScroll = didStopPosition
        
        if self.mapType == MapType.google{
            do {
                if /UDSingleton.shared.appSettings?.appSettings?.is_darkMap == "true" {
                    // Set the map style by passing the URL of the local file.
                    if let styleURL = Bundle.main.url(forResource: "MapStyleNight", withExtension: "json") {
                        mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                        
                    } else {
                        debugPrint("Unable to find style.json")
                    }
                } else
                {
                    // Set the map style by passing the URL of the local file.
                    if let styleURL = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
                        mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                        
                    } else {
                        debugPrint("Unable to find style.json")
                    }
                }
                
            } catch {
                debugPrint("One or more of the map styles failed to load. \(error)")
            }
            
            if let mapType = UserDefaultsManager.shared.mapType {
                
                guard let mapEnum = BuraqMapType(rawValue: mapType) else { return }
                mapView.mapType = mapEnum == .Hybrid ? .normal : .hybrid
                segmentedMapType.selectedSegmentIndex = mapEnum == .Hybrid ? 0 : 1
                
            }
        }
        else{
            configureMapBox()
            
        }
        
        // mapBoxView.isHidden = true
    }
    
    
    func configureMapBox(){
        
        let url = URL(string: "mapbox://styles/mapbox/streets-v11")
        mapBoxView.styleURL = url
        mapBoxView.minimumZoomLevel = 10
        mapBoxView.maximumZoomLevel = 25
        mapBoxView.zoomLevel = 15
        mapBoxView.showsUserLocation = true
        mapBoxView.delegate = self
    }
    
    
    func setUpdatedAddress(location : String , coordinate : CLLocationCoordinate2D?) {
        
        CouponSelectedLocation.latitude = /coordinate?.latitude
        CouponSelectedLocation.longitude = /coordinate?.longitude
        CouponSelectedLocation.selectedAddress = location
        
        locationLatest = location
        latitudeLatest = Double(/coordinate?.latitude)
        longitudeLatest =  Double(/coordinate?.longitude)
        
        if screenType.mapMode == .SelectingLocationMode {
            
            /*  switch locationAddEdit {
             
             case .PickUp:
             
             txtPickUpLocation.text = location
             serviceRequest.locationNameDest = location
             serviceRequest.latitudeDest =  /coordinate?.latitude
             serviceRequest.longitudeDest =  /coordinate?.longitude
             
             case .DropOff:
             
             txtDropOffLocation.text = location
             serviceRequest.locationNickName = location
             serviceRequest.locationName = location
             serviceRequest.latitude =  /coordinate?.latitude
             serviceRequest.longitude =  /coordinate?.longitude
             
             case .Stop1:
             debugPrint("Stop1")
             
             if let object = serviceRequest.stops.filter({$0.priority == 1}).first {
             serviceRequest.stops.remove(object: object)
             }
             
             txtStop1.text = location
             
             let modal = Stops(latitude: /coordinate?.latitude, longitude: /coordinate?.longitude, priority: 1, address: location)
             serviceRequest.stops.append(modal)
             
             case .Stop2:
             debugPrint("Stop2")
             
             if let object = serviceRequest.stops.filter({$0.priority == 2}).first {
             serviceRequest.stops.remove(object: object)
             }
             
             txtStop2.text = location
             let modal = Stops(latitude: /coordinate?.latitude, longitude: /coordinate?.longitude, priority: 2, address: location)
             serviceRequest.stops.append(modal)
             
             debugPrint(serviceRequest.stops as Any)
             
             
             case .Home:
             debugPrint("Home")
             
             
             case .Work:
             debugPrint("Work")
             
             } */
            
            
            
            
            
            
            
            
            /* if txtDropOffLocation.isEditing == true  ||  locationEdit  == .DropOff {
             
             // Ankush Experimental - to not fill out Drop off automatically
             /* txtDropOffLocation.text = location
             serviceRequest.locationName = location
             serviceRequest.longitude =  Double(/coordinate?.longitude)
             serviceRequest.latitude =  Double(/coordinate?.latitude)
             tempDropOff = location */
             
             } else if txtPickUpLocation.isEditing == true ||  locationEdit  == .PickUp {
             
             serviceRequest.locationNameDest = location
             serviceRequest.longitudeDest =  Double(/coordinate?.longitude)
             serviceRequest.latitudeDest =  Double(/coordinate?.latitude)
             txtPickUpLocation.text = location
             tempPickUp = location
             } */
        }
    }
    
//    func clearServiceRequestData() {
//
//           serviceRequest.booking_type = nil
//
//           serviceRequest.category_id = nil
//
//           serviceRequest.category_brand_id = nil
//
//           serviceRequest.selectedProduct = nil
//
//           serviceRequest.driver_id = nil
//
//           serviceRequest.friend_name = nil
//
//           serviceRequest.friend_phone_number = nil
//
//           serviceRequest.friend_phone_code = nil
//
//           serviceRequest.coupon_id = nil
//
//           serviceRequest.cancellation_charges = nil
//
//           serviceRequest.selectedCard = nil
//
//           serviceRequest.stops = []
//
//           stopsTableView.reloadData()
//
//
//
//           // transfer request popup after breakdown
//
//           isTransferRequestPopUpShown = false
//
//           isLongDistancePopUpShown = false
//
//           isStopsShownAfterStartRide = false
//
//           isDoneCurrentPolyline = true
//
//
//
//           isCurrentLocationUpdated = false
//
//          // favAreaBtn?.setTitle("Select Favorite Area", for: .normal)
//
//         //  favLocationBtn?.setTitle("Select Favorite Location", for: .normal)
//
//       }
//
    func clearServiceRequestData() {

        serviceRequest.booking_type = nil
        serviceRequest.category_id = nil
        serviceRequest.category_brand_id = nil
        serviceRequest.selectedProduct = nil
        serviceRequest.driver_id = nil
        serviceRequest.friend_name = nil
        serviceRequest.friend_phone_number = nil
        serviceRequest.friend_phone_code = nil
        serviceRequest.coupon_id = nil
        serviceRequest.cancellation_charges = nil
        serviceRequest.selectedCard = nil
        serviceRequest.stops = []
        stopsTableView.reloadData()
        serviceRequest.addressItems = nil
        serviceRequest.product_detail = nil

        // transfer request popup after breakdown
        isTransferRequestPopUpShown = false
        isLongDistancePopUpShown = false
        isStopsShownAfterStartRide = false
        isDoneCurrentPolyline = true

        isCurrentLocationUpdated = false
    }

    func setupInitialUI() {
        
        viewStop1.isHidden = true
        txtStop1.text = ""
        
        viewStop2.isHidden = true
        txtStop2.text = ""
        
        
        let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
        switch template {
        case .DeliverSome:
            buttonAddStop.isHidden = true
            break
        default:
            buttonAddStop.isHidden = false

        }
        
        
//        let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
//        switch template {
//        case .DeliverSome:
//            buttonAddStop.isHidden = false
//            break
//        default:
//            buttonAddStop.isHidden = true
//
//        }
        
        
        
        constraintYAddLocationView.constant = (BookingPopUpFrames.navigationBarHeight + CGFloat(55) + 10)
        constraintHeightViewEnterPickDrop.constant = 190
        view.layoutIfNeeded()
        
        setupBookForInitialUI(isSwitchRiderViewOpened: false)
    }
    
    func setupBookForInitialUI(isSwitchRiderViewOpened : Bool) {
        
        // isSwitchRiderViewOpened = false
        let title = isSwitchRiderViewOpened ? "Home.Switch_Rider".localizedString : "Home.For_me".localizedString
        buttonHeaderBookingFor.setTitle(title, for: .normal)
        
        ViewForfriend.isHidden = !isSwitchRiderViewOpened
        ViewEnterPickUpDropOff.isHidden = isSwitchRiderViewOpened
        
       // viewForSavedAddress.isHidden = isSwitchRiderViewOpened
        
        imageViewHeaderProfilePic.isHidden = isSwitchRiderViewOpened
        
        /* let heightIfSwitchriderIsNotopened = ( (/viewStop1.isHidden && /viewStop2.isHidden) ? 189 :  ((/viewStop1.isHidden || /viewStop2.isHidden) ? 189 + 48 : 189 + 48 + 48) )
         constraintHeightViewEnterPickDrop.constant = CGFloat(isSwitchRiderViewOpened ? 189 : heightIfSwitchriderIsNotopened)
         
         
         constraintYAddLocationView.constant = ( (/viewStop1.isHidden && /viewStop2.isHidden) ? 189 :  ((/viewStop1.isHidden || /viewStop2.isHidden) ? 189 + 48 : 189 + 48 + 48) ) */
        
        
        bottomViewBookforFriend.clearTextfields()
    }
    
    func setupHomeWorkLocations() {
        
        let home = homeAPI?.addresses?.home
        let work = homeAPI?.addresses?.work
        
        labelHomeLocationNickName.text = home?.addressName == nil || /home?.addressName?.isEmpty ? "Home.Add_Home_Address".localizedString : /home?.addressName
        labelHomeLocationName.text = home?.address == nil || /home?.address?.isEmpty ? "" : /home?.address
        labelHomeLocationName.isHidden = labelHomeLocationName.text == ""
        buttonEditHomeAddress.isHidden = home?.address == nil || /home?.address?.isEmpty
        
        labelWorkLocationNickName.text = work?.addressName == nil || /work?.addressName?.isEmpty ? "Home.Add_Work_Address".localizedString : /work?.addressName
        labelWorkLocationName.text = work?.address == nil || /work?.address?.isEmpty ? "" : /work?.address
        labelWorkLocationName.isHidden = labelWorkLocationName.text == ""
        buttonEditWorkAddress.isHidden = work?.address == nil || /work?.address?.isEmpty
    }
    
    func locationsSelected() {
        
        self.view.endEditing(true)
        
        guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{return}
        guard let addressDropOff = txtDropOffLocation.text else{return}
        
        if serviceID > 3 {
            
            guard let addressPicUp = txtPickUpLocation.text else{return}
            
            if addressPicUp.isEmpty  {
                Alerts.shared.show(alert: "Pikkup", message: "pickup_address_validation_message".localizedString , type: .error )
                return
            }
            
            if (addressDropOff.isEmpty) {

                if (/txtStop1.text).isEmpty && (/txtStop2.text).isEmpty {
                    Alerts.shared.show(alert: "Pikkup", message: "dropoff_address_validation_message".localizedString , type: .error )
                    return
                } else {

                    let lastStop = serviceRequest.stops.last
                    serviceRequest.locationName = lastStop?.address
                    serviceRequest.latitude =  lastStop?.latitude
                    serviceRequest.longitude =  lastStop?.longitude

                    serviceRequest.stops.removeLast()
                }

            }
        } else {
            
            if  addressDropOff.isEmpty  {
                Alerts.shared.show(alert: "Pikkup", message: "dropoff_address_validation_message".localizedString , type: .error )
                return
            }
        }
        
        // results.removeAll()
        
        viewLocationTableContainer.alpha = 0 // Ankush
        viewStoppageDesc.isHidden = true
        // vwFooter.isHidden = false
        
        //        let template = AppTemplate(rawValue: /UDSingleton.shared.appTerminology?.key_value?.template?.toInt())
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
        switch template {
        
        case .Mover?:
            screenType = ScreenType(mapMode: .OrderPricingMode, entryType: .Forward)
            dropPickUpAndDropOffPins(pickLat: /serviceRequest.latitudeDest, pickLong: /serviceRequest.longitudeDest, dropLat: /serviceRequest.latitude, dropLong: /serviceRequest.longitude)
            
        default:
            var modeType : MapMode!
            
            if serviceRequest.booking_type == "RoadPickup" {
                
                showBasicNavigation(updateType: .SelectingFreightBrand)
                dropPickUpAndDropOffPins(pickLat: /serviceRequest.latitudeDest, pickLong: /serviceRequest.longitudeDest, dropLat: /serviceRequest.latitude, dropLong: /serviceRequest.longitude)
                modeType = .OrderPricingMode
            } else {
                modeType = serviceID > 3 ? .SelectingFreightBrand : .OrderDetail
            }
            
            //            if APIBasePath.appScheme == .shipusnow && (serviceRequest.categoryType == .export || serviceRequest.categoryType == .lagos || serviceRequest.categoryType == .moving){
            //                serviceRequest.selectedBrand = serviceRequest.serviceSelected?.brands?.first
            //                serviceRequest.selectedProduct = homeAPI?.brands?.first?.products?.first
            //                modeType = .OrderDetail
            //            }
            //
            //            if UDSingleton.shared.appSettings?.appSettings?.is_vehicle_brand_model_management == "true"{
            //                serviceRequest.selectedBrand = serviceRequest.serviceSelected?.brands?.first
            //                modeType = .SelectingFreightProduct
            //            }
            
            screenType = ScreenType(mapMode: modeType, entryType: .Forward)
            locationEdit  = serviceID > 3 ? .PickUp : .DropOff
            
        }
        
        
        
    }
    
    func locationsSelected1() {
        
        self.view.endEditing(true)
        
        guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{return}
        guard let addressDropOff = txtDropOffLocation.text else{return}
        
        if serviceID > 3 {
            
            guard let addressPicUp = txtPickUpLocation.text else{return}
            
            if addressPicUp.isEmpty  {
                Alerts.shared.show(alert: "Pikkup", message: "pickup_address_validation_message".localizedString , type: .error )
                return
            }
            
            if (addressDropOff.isEmpty) {

                if (/txtStop1.text).isEmpty && (/txtStop2.text).isEmpty {
                    Alerts.shared.show(alert: "Pikkup", message: "dropoff_address_validation_message".localizedString , type: .error )
                    return
                } else {

                    let lastStop = serviceRequest.stops.last
                    serviceRequest.locationName = lastStop?.address
                    serviceRequest.latitude =  lastStop?.latitude
                    serviceRequest.longitude =  lastStop?.longitude

                    serviceRequest.stops.removeLast()
                }

            }
        } else {
            
            if  addressDropOff.isEmpty  {
                Alerts.shared.show(alert: "Pikkup", message: "dropoff_address_validation_message".localizedString , type: .error )
                return
            }
        }
        
    
        
        //        let template = AppTemplate(rawValue: /UDSingleton.shared.appTerminology?.key_value?.template?.toInt())
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
        switch template {
        
        case .Mover?:
            
            print("hi")
            //screenType = ScreenType(mapMode: .OrderPricingMode, entryType: .Forward)
            dropPickUpAndDropOffPins(pickLat: /serviceRequest.latitudeDest, pickLong: /serviceRequest.longitudeDest, dropLat: /serviceRequest.latitude, dropLong: /serviceRequest.longitude)
            
        default:
            var modeType : MapMode!
            
            if serviceRequest.booking_type == "RoadPickup" {
                
//                showBasicNavigation(updateType: .SelectingFreightBrand)
               dropPickUpAndDropOffPins(pickLat: /serviceRequest.latitudeDest, pickLong: /serviceRequest.longitudeDest, dropLat: /serviceRequest.latitude, dropLong: /serviceRequest.longitude)
//                modeType = .OrderPricingMode
            } else {
                modeType = serviceID > 3 ? .SelectingFreightBrand : .OrderDetail
            }
            
            //            if APIBasePath.appScheme == .shipusnow && (serviceRequest.categoryType == .export || serviceRequest.categoryType == .lagos || serviceRequest.categoryType == .moving){
            //                serviceRequest.selectedBrand = serviceRequest.serviceSelected?.brands?.first
            //                serviceRequest.selectedProduct = homeAPI?.brands?.first?.products?.first
            //                modeType = .OrderDetail
            //            }
            //
            //            if UDSingleton.shared.appSettings?.appSettings?.is_vehicle_brand_model_management == "true"{
            //                serviceRequest.selectedBrand = serviceRequest.serviceSelected?.brands?.first
            //                modeType = .SelectingFreightProduct
            //            }
            
            //screenType = ScreenType(mapMode: modeType, entryType: .Forward)
           locationEdit  = serviceID > 3 ? .PickUp : .DropOff
            
        }
        
        
        
    }
    
    
}

//MARK:- API
extension HomeVC {
    
    func orderEventFire() { // WHEN SEARCH REQUEST TIMED OUT. ELSE CASE IN UNDERPROCESSING VIEW
        
        if APIBasePath.isShipUsNow{
            self.alertBoxOk(message: "Your_ride_has_been_accepted_and_its_pending_from_approval.".localizedString, title: "Pikkup") {
                self.viewUnderProcess.minimizeProcessingView()
                
                self.screenType = ScreenType(mapMode: .NormalMode, entryType: .Backward)
                self.moveToNormalMode()
            }
        }else{
            self.viewUnderProcess.minimizeProcessingView()
            let mapMode : MapMode! = (self.isSearching == true || /self.currentOrder?.isContinueFromBreakdown) ? .NormalMode : .OrderPricingMode
            self.screenType = ScreenType(mapMode: mapMode , entryType: .Forward)
        }
        
        guard let order = currentOrder else{return}
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let dict : [String : Any] = [EmitterParams.EmitterType.rawValue : CommonEventType.ParticularOrder.rawValue ,EmitterParams.AccessToken.rawValue  :  token ,EmitterParams.OrderToken.rawValue :   /order.orderToken]
        SocketIOManagerCab.shared.getParticularOrder(dict) { [weak self] (response) in
            
            if let order = response as? OrderCab {
                
                if order.orderStatus == .Confirmed || order.orderStatus == .reached || order.orderStatus == .Ongoing {
                    self?.driverAcceptedMode(order: order)
                    
                } else if order.orderStatus == .ServiceTimeout {
                    self?.viewUnderProcess.minimizeProcessingView()
                    if self?.isSearching == true || /self?.currentOrder?.isContinueFromBreakdown {
                        self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Forward)
                    }else{
                        
                        
                        
                        self?.screenType = ScreenType(mapMode: .OrderPricingMode , entryType: .Forward)
                    }
                } else if order.orderStatus == .CustomerCancel {
                    
                    
                } else if order.orderStatus == .Searching {
                    
                    //  self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Forward)
                }else if order.orderStatus == .Unassigned {
                    self?.setPendingApproval()
                    //  self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Forward)
                }
            }
            
            self?.unreadEvent()
            
        }
    }
    
    
    func unreadEvent(){
        let send_to = /UDSingleton.shared.userData?.userDetails?.userId
        let order_id = /currentOrder?.orderId
        //SocketIOManagerCab.shared.unreadEvent(send_to: send_to, order_id: order_id) { visible in
        // NotificationCenter.default.post(name: .init("unread"), object: visible)
        // }
    }
    
    func checkFinalStatus() {
        
        guard let orderRequested = currentOrder else{return}
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let dict : [String : Any] = [
            EmitterParams.EmitterType.rawValue : CommonEventType.ParticularOrder.rawValue ,
            EmitterParams.AccessToken.rawValue  :  token ,
            EmitterParams.OrderToken.rawValue :   /orderRequested.orderToken
        ]
        
        SocketIOManagerCab.shared.getParticularOrder(dict) { [weak self] (response) in
            
            if let order = response as? OrderCab {
                
                if order.orderStatus ==  .CustomerCancel {
                    //  self?.currentOrder?.orderStatus = order.orderStatus
                    if self?.screenType.mapMode == .RequestAcceptedMode {
                        
                        //                    ez.runThisInMainThread {
                        //
                        //                    self?.driverMarker.map = nil
                        //                    self?.polyline?.map = nil
                        //                    self?.userMarker.map = nil
                        //                    self?.viewDriverAccepted.minimizeDriverView()
                        //                   }
                    }
                }else if order.orderStatus == .Unassigned {
                    self?.setPendingApproval()
                    //  self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Forward)
                }
            }
            self?.unreadEvent()
        }
    }
    
    func listeningEvent() {
        
        SocketIOManagerCab.shared.listenOrderEventConnected { [weak self]( data ,socketType) in
            
            guard let socketType = socketType as? OrderEventType else {return}
            print("Socket data addded", data)
            guard let model = data as? OrderCab else { return }
            switch socketType {
            
            case .DApproved:
                
                guard let model = data as? OrderCab else { return }
                self?.trackOrder = model
                self?.viewSelectService.minimizeSelectServiceView()
                self?.driverAcceptedMode(order: model)
                
            case .ServiceAccepted:
                guard let model = data as? OrderCab else { return }
                if model.bookingType == .Present {
                    if self?.currentOrder?.orderStatus == .CustomerCancel {return}
                }
                
                if self?.serviceRequest.requestType == .Future {
                    self?.viewOrderPricing.minimizeOrderPricingView()
                    self?.viewUnderProcess.minimizeProcessingView()
                    self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Forward)
                    if  self?.isCheck == false {
                        self?.isCheck = true
                        if self?.mapType == .google{
                            self?.mapView.clear()
                        } else{
                            self?.clearAnnotationWithPath()
                        }
                        self?.cleanPath()
                        self?.alertBoxOk(message:"service_booked_successfully".localizedString , title: "congratulations".localizedString, ok: {
                            self?.isCheck = false
                            
                            // To move to Home in case of Package
                            if /self?.isFromPackage {
                                self?.navigationController?.popToRootViewController(animated: true)
                            }
                        })
                    }
                } else {
                    self?.trackOrder = model
                    self?.driverAcceptedMode(order: model)
                    //  self?.checkFinalStatus()
                }
                
            case .SerLongDistance:
                
                if !(/self?.isLongDistancePopUpShown) {
                    self?.alertBoxOption(message: "long_distance_confirmation".localizedString  , title: "Pikkup" , leftAction: "no".localizedString , rightAction: "yes".localizedString , ok: {}, cancel: { [weak self] in
                        
                        self?.viewDriverAccepted.cancelOngoingOrder()
                    })
                    
                    self?.isLongDistancePopUpShown = true
                }
                
            case .ServiceCurrentOrder: // Again and Again
                
                //if self?.currentOrder?.bookingType == .Future {return}
                if UDSingleton.shared.is_multiple_requests{
                    guard (!(self?.isOnHome ?? false) && self?.trackOrder != nil && model.orderId == self?.trackOrder?.orderId) else { return }
                }
                
                
                guard let model = data as? TrackingModel else { return }
                
                AllOrdersOngoing.ordersOngoing["\(/model.orderId)"] = model
                AllOrdersOngoing.order = model
                
                self?.dic["\(/model.orderId)"] = model
                AllOrdersOngoing.ordersOngoing = /self?.dic
                
                print("\(LocalNotifications.ETokenTrackingRefresh.rawValue)\(/model.orderId)")
                
                NotificationCenter.default.post(name: Notification.Name("\(LocalNotifications.ETokenTrackingRefresh.rawValue)\(/model.orderId)"), object: nil, userInfo: nil)
                
                //Driver Updated Lat and Long
                let updatedDriver = CLLocationCoordinate2D(latitude: CLLocationDegrees(/model.driverLatitude) , longitude: CLLocationDegrees(/model.driverLongitude))
                debugPrint(updatedDriver)
                
                
                if (model.orderStatus != .Ongoing && model.orderStatus != .Confirmed) {return}
                self?.currentOrder?.orderStatus = model.orderStatus
                self?.driverBearing = model.bearing
                
                if let order = self?.currentOrder {
                    
                    /*  if /order.serviceId > 3 && (model.orderStatus == .Ongoing || model.orderStatus == .Confirmed) {
                     self?.destination =   CLLocationCoordinate2D(latitude: CLLocationDegrees(/order.dropOffLatitude) , longitude: CLLocationDegrees(/order.dropOffLongitude))
                     } */
                    
                    // Ankush - Moby
                    if /order.serviceId > 3 {
                        if (model.orderStatus == .Ongoing) {
                            self?.destination =   CLLocationCoordinate2D(latitude: CLLocationDegrees(/order.dropOffLatitude) , longitude: CLLocationDegrees(/order.dropOffLongitude))
                        } else if (model.orderStatus == .Confirmed || model.orderStatus == .reached ) {
                            self?.destination =   CLLocationCoordinate2D(latitude: CLLocationDegrees(/order.pickUpLatitude) , longitude: CLLocationDegrees(/order.pickUpLongitude))
                        }
                    }
                }
                
                self?.source = updatedDriver
                
                if /self?.btnCurrentLocation.isSelected{
                    guard let path = GMSMutablePath(fromEncodedPath: /model.polyline) else { return }
                    
                    let bounds = GMSCoordinateBounds(path: path)
                    self?.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                }
                
                if self?.currentOrder?.myTurn == OrderTurn.MyTurn{
                    
                    PolylineCab.points = /model.polyline
                    
                    let stops = self?.currentOrder?.orderStatus == .Ongoing ? self?.currentOrder?.ride_stops : nil
                    if self?.mapType == .google{
                        
                        self?.getPolylineRoute(source: updatedDriver , destination: self?.destination, stops: stops, model: model)
                    } else{
                        
                        self?.calculateRoute(from: updatedDriver, to:  self?.destination, stops: stops, isUserDriverMarker: true, model: model)
                    }
                }
                else {
                    //Experimental
                    PolylineCab.points = /model.polyline
                    
                    let stops = self?.currentOrder?.orderStatus == .Ongoing ? self?.currentOrder?.ride_stops : nil
                    
                    if self?.mapType == .google{
                        self?.getPolylineRoute(source: updatedDriver , destination: self?.destination, stops: stops, model: model)
                    } else{
                        self?.calculateRoute(from: updatedDriver, to:  self?.destination, stops: stops, isUserDriverMarker: true, model: model)
                    }
                }
                
                DispatchQueue.main.async {[weak self] in
                    
                    if self?.screenType.mapMode != .RequestAcceptedMode{
                        self?.viewSelectService.minimizeSelectServiceView()
                        self?.screenType = ScreenType(mapMode: .RequestAcceptedMode, entryType: .Forward)
                    }
                    self?.viewDriverAccepted.setOrderStatus(tracking: model)
                    (ez.topMostVC as? OngoingRideDetailsViewController)?.setOrderStatus(tracking: model)
                }
                
            case .ServiceCompletedByDriver:
                if self?.mapType == .google{
                    self?.clearMapForIntial()
                    
                    self?.mapView.clear()
                    self?.moveToCurrentLocation()
                } else{
                    self?.clearAnnotationWithPath()
                }
                self?.cleanPath()
                
                if UDSingleton.shared.is_multiple_requests{
                    guard (!(self?.isOnHome ?? false) && self?.trackOrder != nil && model.orderId == self?.trackOrder?.orderId) else { return }
                }
                
                
                guard let model = data as? OrderCab else { return }
                self?.checkCancellationPopUp()
                
                //if model.bookingType == .Future {return }
                //                if UDSingleton.shared.appSettings?.appSettings?.is_ride_recording == "true" {
                //
                //                    self?.alertVw.dismiss(animated: true, completion: nil)
                //                    self?.finishRecording(success: true, orderID: /model.orderId)
                //                }
                self?.currentOrder = model
                self?.viewDriverAccepted.minimizeDriverView()
                (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: nil)
                //                if APIBasePath.appScheme == .shipusnow && (self?.serviceRequest.categoryType == .export || self?.serviceRequest.categoryType == .lagos  || self?.serviceRequest.categoryType == .moving ){
                //                    Alerts.shared.showOnTop(alert: "Pikkup", message: "Ongoing order completed by driver", type: .success)
                //                    self?.moveToNormalMode()
                //                }else{
                self?.screenType = ScreenType(mapMode: .ServiceDoneMode , entryType: .Forward)
            //}
            
            
            
            case .ServiceBreakdown, .ServiceBreakDownAccepted:
                if UDSingleton.shared.is_multiple_requests {
                    guard (!(self?.isOnHome ?? false) && self?.trackOrder != nil && model.orderId == self?.trackOrder?.orderId) else { return }
                }
                
                guard let model = data as? OrderCab else { return }
                self?.checkCancellationPopUp()
                
                //if model.bookingType == .Future {return }
                self?.currentOrder = model
                self?.viewDriverAccepted.minimizeDriverView()
                
                if (ez.topMostVC is OngoingRideDetailsViewController) {
                    
                    (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: {[weak self] in
                        if !(/self?.isTransferRequestPopUpShown) {
                            self?.alertBoxOk(message: "transferRequest".localizedString, title: "Pikkup", ok: {})
                            self?.isTransferRequestPopUpShown = true
                        }
                    })
                } else {
                    if !(/self?.isTransferRequestPopUpShown) {
                        self?.alertBoxOk(message: "transferRequest".localizedString, title: "Pikkup", ok: {})
                        self?.isTransferRequestPopUpShown = true
                    }
                }
                
                // self?.homeAPI?.orders.append(model)
                model.isContinueFromBreakdown = true
                self?.currentOrder = model
                
                self?.screenType = ScreenType(mapMode: .UnderProcessingMode , entryType: .Forward)
                
                
            case .SerHalfWayStopRejected, .ServiceBreakDownRejected:
                debugPrint("SerHalfWayStopRejected")
                
                guard let model = data as? OrderCab else { return }
                
                self?.currentOrder = model
                self?.viewUnderProcess.minimizeProcessingView()
                self?.showDriverView()
                
            case .DriverCancelrequest:
                self?.checkCancellationPopUp()
                let mode = self?.screenType.mapMode
                if mode == .RequestAcceptedMode || mode == .OnTrackMode  {
                    if self?.mapType == .google{
                        self?.clearMapForIntial()
                        
                        self?.mapView.clear()
                        self?.moveToCurrentLocation()
                    } else{
                        self?.clearAnnotationWithPath()
                    }
                    self?.cleanPath()
                    
                    self?.viewDriverAccepted.minimizeDriverView()
                    (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: nil)
                    self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Forward)
                    
                    // To move to Home in case of Package
                    if /self?.isFromPackage {
                        self?.navigationController?.popToRootViewController(animated: true)
                    }
                    
                }else if mode == .UnderProcessingMode   {
                    
                    self?.viewUnderProcess.minimizeProcessingView()
                    self?.screenType = ScreenType(mapMode: .OrderPricingMode , entryType: .Forward)
                }
                
            case  .ServiceTimeOut  :
                let template = AppTemplate(rawValue: /UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt())
                switch template {
                case .GoMove:
                    
                    if UIApplication.topViewController()!.isKind(of: UIAlertController.self) {
                        print("Alert already presented by socket....")
                    } else {
                        self?.alertBoxOk(message: "We're sorry, there is no driver around you at the moment available to help you out! Call us at +1 (514) 416-6606 and we'll make sure to appoint someone right away!".localizedString, title: "Pikkup") {
                            self?.viewUnderProcess.minimizeProcessingView()
                            
                            var mapMode : MapMode! = (self?.isSearching == true || /self?.currentOrder?.isContinueFromBreakdown) ? .NormalMode : .OrderPricingMode
                            self?.screenType = ScreenType(mapMode: mapMode , entryType: .Forward)
                        }
                    }
                    
                default:
                    
                    
                    Alerts.shared.show(alert: "Pikkup", message: "No driver found." , type: .info )
                    
                    self?.viewUnderProcess.minimizeProcessingView()
                    
                    var mapMode : MapMode! = (self?.isSearching == true || /self?.currentOrder?.isContinueFromBreakdown) ? .NormalMode : .OrderPricingMode
                    self?.screenType = ScreenType(mapMode: mapMode , entryType: .Forward)
                    
                }
                
            case .ServiceRejectedByAllDriver  :
                
                self?.viewUnderProcess.minimizeProcessingView()
                
                var mapMode : MapMode! = (self?.isSearching == true || /self?.currentOrder?.isContinueFromBreakdown) ? .NormalMode : .OrderPricingMode
                self?.screenType = ScreenType(mapMode: mapMode , entryType: .Forward)
            case .serReached:
                guard let model = data as? OrderCab else { return }
                
                self?.trackOrder = model
                self?.driverAcceptedMode(order: model)
                
                break
            case .ServiceOngoing , .DriverRatedCustomer : // - ServiceOngoing - When driver start ride
                
                //if self?.currentOrder?.bookingType == .Future {return}
                
                if let model = data as? OrderCab {
                    
                    //  let source = CLLocationCoordinate2D(latitude: CLLocationDegrees(/model.pickUpLatitude) , longitude: CLLocationDegrees(/model.pickUpLongitude))
                    //  let destination = CLLocationCoordinate2D(latitude: CLLocationDegrees(/model.dropOffLatitude) , longitude: CLLocationDegrees(/model.dropOffLongitude))
                    
                    //  let stops = model.orderStatus == .Ongoing ? model.ride_stops : nil
                    //  self?.getPolylineRoute(source: source , destination: destination, stops: stops, model: nil)
                    
                    self?.currentOrder = model
                    
                    
                    
                    
                    //For audio recording permission
                    //                    if UDSingleton.shared.appSettings?.appSettings?.is_ride_recording == "true" {
                    //
                    //                        self?.alertVw = UIAlertController(title: "Pikkup", message: "Do you really want to start recording ?", preferredStyle: UIAlertController.Style.alert)
                    //
                    //                        self?.alertVw.addAction(UIAlertAction(title:"Cancel"  , style: UIAlertAction.Style.default , handler: { action in
                    //
                    //                            self?.uploadMediaAction(orderID: Int(/model.orderId) , action: 2)
                    //
                    //                        }))
                    //                        self?.alertVw.addAction(UIAlertAction(title:"Yes"  , style: UIAlertAction.Style.default , handler: { action in
                    //
                    //
                    //                            self?.uploadMediaAction(orderID: Int(/model.orderId) , action: 1)
                    //                        }))
                    //
                    //                        // alert.view.tintColor = Colors.themeColor
                    //                        self?.present(self?.alertVw ?? UIAlertController(), animated: true, completion: nil)
                    //
                    //                    }
                    
                    
                    DispatchQueue.main.async {[weak self] in
                        self?.viewDriverAccepted.setOrderStatus(modal: model)
                        (ez.topMostVC as? OngoingRideDetailsViewController)?.setOrderStatus(modal: model)
                    }
                }
                
                guard let model = data as? TrackingModel else { return }
                
                //Driver Updated Lat and Long
                
                let updatedDriver = CLLocationCoordinate2D(latitude: CLLocationDegrees(/model.driverLatitude) , longitude: CLLocationDegrees(/model.driverLongitude))
                
                if model.orderStatus != .Ongoing {return}
                self?.currentOrder?.orderStatus = model.orderStatus
                self?.driverBearing = model.bearing
                if let order = self?.currentOrder {
                    
                    if /order.serviceId > 3 && model.orderStatus == .Ongoing {
                        self?.destination =   CLLocationCoordinate2D(latitude: CLLocationDegrees(/order.dropOffLatitude) , longitude: CLLocationDegrees(/order.dropOffLongitude))
                    }
                }
                self?.source = updatedDriver
                
                if /self?.btnCurrentLocation.isSelected{
                    guard let path = GMSMutablePath(fromEncodedPath: /model.polyline) else { return }
                    
                    let bounds = GMSCoordinateBounds(path: path)
                    self?.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                }
                
                
                if self?.currentOrder?.myTurn == OrderTurn.MyTurn{
                    
                    PolylineCab.points = /model.polyline
                    
                    let stops = self?.currentOrder?.orderStatus == .Ongoing ? self?.currentOrder?.ride_stops : nil
                    if self?.mapType == .google{
                        self?.getPolylineRoute(source: updatedDriver , destination: self?.destination, stops: stops, model: model)
                    } else{
                        
                        self?.calculateRoute(from: updatedDriver, to:  self?.destination, stops: stops, isUserDriverMarker: true, model: model)
                    }
                }
                else{
                    
                }
                
                DispatchQueue.main.async {[weak self] in
                    self?.viewDriverAccepted.setOrderStatus(tracking: model)
                    (ez.topMostVC as? OngoingRideDetailsViewController)?.setOrderStatus(tracking: model)
                }
                
                
                //  guard let model = data as? OrderCab else { return }
                break
                
            case .EtokenStart:
                
                print("start")
            case .EtokenConfirmed:
                
                print("etoken confirmed")
                
            case .EtokenSerCustPending:
                print("gerg")
                
            case .EtokenCTimeout:
                print("timedout")
                
            case .CardAdded:
                (ez.topMostVC as? AddCardViewControllerCab)?.popVC()
                
            case .SerCheckList:
                print("Checklist data <===")
                
                guard let dataValue = data as? [String: Any] else { return }
                guard  let orderModel = dataValue["order"] as? [String: Any] else{return}
                let order =  Mapper<OrderCab>().map(JSONObject: orderModel)
                self?.currentOrder?.check_lists = order?.check_lists
                UDSingleton.shared.userData?.order?.first?.check_lists = order?.check_lists
                
                
            case .Pending:
                
                if UIApplication.topViewController()!.isKind(of: UIAlertController.self) {
                    print("Alert already presented by socket....")
                } else {
                    self?.alertBoxOk(message: "Your_ride_has_been_accepted_and_its_pending_from_approval.".localizedString, title: "Pikkup") {
                        self?.viewUnderProcess.minimizeProcessingView()
                        
                        self?.screenType = ScreenType(mapMode: .NormalMode, entryType: .Backward)
                        self?.moveToNormalMode()
                    }
                }
                
            case .DApproved:
                // Hit Dashboard API:
                self?.getUpdatedData()
                break
                
                
                
            }
            
            self?.unreadEvent()
            
            
        }
    }
    
    //MARK:- Web Services
    func api_getAddress()
    {
        
        self.recentLocations.removeAll()
        self.savedLocations.removeAll()
        
        myGroup.enter()
       
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
                
        let path = APIBasePath.basePath + Routes.commonRoutes + "getLocation"
        
        print(path)
        Alamofire.request(path, method: .get, parameters: nil,encoding: URLEncoding.default, headers:  ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]).responseJSON { [self]
                    response in
                   
                    switch response.result {
                    case .success:
                        
                        self.savedLocations.removeAll()
                        
                        
                        
                        if let dictSuccess:NSDictionary =  response.value as? NSDictionary
                        {
                            if let arrTemp : NSArray = dictSuccess["result"] as? NSArray {
                                self.arrData = arrTemp.reversed() as NSArray
                                
                                var address1 = ""
                                var address_latitude = 0.0
                                var address_longitude = 0.0
                                var address_name = ""
                                var blocked = "0"
                                var category = "RECENT"
                                var created_at = ""
                                var title = ""
                                var updated_at = ""
                                var user_address_id = ""
                                var user_idq = 0
                                var isSavedAdd = ""
                                
                                for i in 0..<self.arrData.count
                                {
                                    if let tempDic : NSDictionary = self.arrData[i] as? NSDictionary {
                                        
                                        address1 = ""
                                        address_latitude = 0.0
                                        address_longitude = 0.0
                                        address_name = ""
                                        blocked = "0"
                                        category = "RECENT"
                                        created_at = ""
                                        title = ""
                                        updated_at = ""
                                        user_address_id = ""
                                        user_idq = 0
                                        isSavedAdd = ""
                              
                                        
                                        if  let user_id = tempDic["user_id"] as? Int
                                        {
                                            user_idq = user_id
                                        }
                                        
                                        if  let longitude = tempDic["created_at"] as? String
                                        {
                                            created_at = longitude
                                        }
                                        
                                        if  let longitude = tempDic["updated_at"] as? String
                                        {
                                            updated_at = longitude
                                        }
                                        
                                    
                                        if let address : String = tempDic["location_data"] as? String{
                                            
                                            do{
                                                if let json = address.data(using: String.Encoding.utf8){
                                                    if let jsonData = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String:AnyObject]{
                                                       
                                                        if  let name = jsonData["name"] as? String{address_name = name}
                                                        var strAddress = ""
                                                        if  let block = jsonData["block"] as? String{if block != ""{strAddress = block}}
                                                        if  let street = jsonData["street"] as? String{if street != ""{strAddress = strAddress + ", " + street}}
                                                        if  let building = jsonData["building"] as? String{if building != ""{strAddress = strAddress + ", " + building}}
                                                        if  let floor = jsonData["floor"] as? String{if floor != ""{strAddress = strAddress + ", " + floor}}
                                                        if  let apartment_number = jsonData["apartment_number"] as? String{if apartment_number != ""{strAddress = strAddress + ", " + apartment_number}}
                                                        if  let area = jsonData["area"] as? String{if area != ""{strAddress = strAddress + ", " + area}}
                                                        if  let country = jsonData["country"] as? String{if country != ""{strAddress = strAddress + ", " + country}}
                                                        address1 = strAddress
                                                        
                                                        if  let latitide = jsonData["latitide"] as? Double{address_latitude = latitide}
                                                        if  let longitude = jsonData["longitude"] as? Double{address_longitude = longitude}
                                                        
                                                       

                                                    }
                                                }
                                            }catch {
                                                print(error.localizedDescription)

                                            }
                                        }
                                        
                                        if i == 0
                                            {
                                            isSavedAdd = "Yes"
                                        }
                                        else
                                        
                                        {
                                            isSavedAdd = ""
                                        }
                                        
                                        let dic : [String:Any] = [
                                            "address" : address1,
                                            "address_latitude" : address_latitude,
                                            "address_longitude" : address_longitude,
                                            "address_name" : address_name,
                                            "blocked" : 0,
                                            "category" : "RECENT",
                                            "created_at" : created_at,
                                            "title" : "",
                                            "updated_at" : updated_at,
                                            "user_address_id" : "",
                                            "user_id" : user_idq,
                                            "isSavedAdd":isSavedAdd
                                        ]
                                        
                                        let objRef = AddressCab.init(JSON: dic)
                                        self.savedLocations.append(objRef!)
                                    }
                                    
                                    if i == self.arrData.count-1
                                        {
                                        self.myGroup.leave()
                                        
                                        
                                    }
                                    
                                }
                                
                                
                                if self.arrData.count == 0
                                    {
                                    self.myGroup.leave()
                                    
                                    
                                }
                                
                            }
                            
                            
                        }
                        break
                    case .failure(let error):
                        Alerts.shared.show(alert: "Error".localizedString, message: error.localizedDescription , type: .error )
                        
                        print(response)
                        print(error)
                    }
                    
           

                }
    }
    
    func getLocalDriverForParticularService1(service: Service,isScanCode:Bool=false,object: RoadPickupModal?=nil,isFromCancellation:Bool = false) {
        
        //Davender
        if (currentOrder == nil) || (currentOrder?.orderStatus == .ServiceComplete) || (currentOrder?.orderStatus == .ServiceComplete1){
            return
        }
        
        self.isFromCancellation = isFromCancellation
        
        
        if APIManagerCab.shared.isConnectedToNetwork() == false {
            alertBoxOk(message:"Validation.InternetNotWorking".localizedString , title: "Pikkup", ok: { [weak self] in
                self?.getUpdatedData()
            })
            return
        }
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        
        guard !isHomeApiLoading  else {
            return
        }
        
        guard !viewNewInvoice.isShown else {
            return
        }
        
        guard  !viewDriverRating.isShown  else {
            return
        }
        
        
        isHomeApiLoading = true
        let homeAPI =  BookServiceEndPoint.homeApi(categoryID: service.serviceCategoryId)
        homeAPI.request(isLoaderNeeded: false, header: ["language_id" : LanguageFile.shared.getLanguage(), "access_token" :  token]) {
            [weak self] (response) in
            
            switch response {
            
            case .success(let data):
                
                guard let model = data as? HomeCab else { return }
                self?.homeAPI = model
                self?.drivers = model.drivers
                
                UDSingleton.shared.home = model
            
                    
                    self?.setupHomeWorkLocations()
                    self?.recentLocations = model.addresses?.recent ?? []
                    
                    for i in 0..<(self?.savedLocations.count ?? 0)!
                    {
                        self?.recentLocations.append((self?.savedLocations[i])!)
                    }
                    
                
                    guard let orders = self?.homeAPI?.orders else {return}
                    
                    guard let lastCompletedOrder = self?.homeAPI?.orderLastCompleted else {return}
                    
                    if orders.count > 0 {
                        
                        var latestOrder = orders[0]
                        
                        if UDSingleton.shared.is_multiple_requests, let order = self?.trackOrder{
                            if let item = orders.filter({$0.orderId == order.orderId}).first{
                                latestOrder = item
                                self?.isOnHome = false
                            }
                        }
                        
                        if latestOrder.orderStatus == .reached || latestOrder.orderStatus == .Confirmed || latestOrder.orderStatus == .Ongoing {
                            
                            self?.currentOrder =  latestOrder
                            
                            if self?.currentOrder?.bookingType == .Present ||  self?.currentOrder?.bookingType == .Future {
                                //                            self?.trackOrder = latestOrder
                                self?.driverAcceptedMode(order: latestOrder)
                            }else{
                                
                                let isOnline = latestOrder.payment?.paymentType?.lowercased() != "cash"
                                let isPaymentInComplete = latestOrder.payment?.productStatus?.lowercased() != "completed"
                                if isOnline && isPaymentInComplete{
                                    self?.screenType = ScreenType(mapMode: .ServiceDoneMode, entryType: .Forward)
                                }
                                
                                self?.screenType = ScreenType(mapMode: .NormalMode, entryType: .Forward)
                                
                            }
                            
                        }else if latestOrder.orderStatus == .Searching {
                            
                            if  /latestOrder.orderId != /self?.currentOrder?.orderId  { //coming after killing the app
                                self?.currentOrder =  latestOrder
                                self?.isSearching = true
                                self?.showBasicNavigation(updateType: .OrderDetail)
                                self?.screenType = ScreenType(mapMode: .UnderProcessingMode, entryType: .Forward)
                            }
                        }else {
                            print("**************** Screen Type  ************")
                            print(self?.screenType)
    //                        self?.screenType = ScreenType(mapMode: .NormalMode, entryType: .Forward)
                            self?.proceed(lastCompletedOrder: lastCompletedOrder)
                        }
                        
                    }else if lastCompletedOrder.count > 0 {
                        
                        self?.proceed(lastCompletedOrder: lastCompletedOrder)
                        
                        
                    } else{
                        
                        if self?.screenType.mapMode == .UnderProcessingMode{
                            
                            self?.viewUnderProcess.minimizeProcessingView()
                            if self?.isSearching == true || /self?.currentOrder?.isContinueFromBreakdown {
                                self?.screenType = ScreenType(mapMode:  .NormalMode , entryType: .Backward)
                                return
                            }
                            self?.screenType = ScreenType(mapMode:  .OrderPricingMode , entryType: .Backward)
                            
                        } else if  self?.screenType.mapMode == .NormalMode || self?.screenType.mapMode == .ZeroMode {
                            
                            
                            if !isScanCode{
                                
                                self?.viewSelectService.minimizeSelectServiceView()
                                self?.screenType = ScreenType(mapMode: .NormalMode, entryType: .Forward)
                            }
                            
                        }else if self?.screenType.mapMode == .RequestAcceptedMode || self?.screenType.mapMode == .RequestAcceptedMode  {
                            
                            self?.viewDriverAccepted.minimizeDriverView()
                            (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: nil)
                            self?.screenType = ScreenType(mapMode:  .NormalMode , entryType: .Backward)
                            
                            self?.moveToNormalMode()
                            self?.getNearByDrivers()
                            
                        }
                        
                    }
                    
                    
                    if /self?.isFromPackage {
                        
                        self?.serviceRequest.serviceSelected?.brands = []
                        
                        self?.modalPackages?.package?.pricingData?.categoryBrands?.forEach({ (packageBrand) in
                            
                            guard let filteredObject = self?.homeAPI?.brands?.filter({$0.categoryBrandId == packageBrand.categoryBrandId}).first else {return}
                            self?.serviceRequest.serviceSelected?.brands?.append(filteredObject)
                            
                        })
                    } else {
                        self?.serviceRequest.serviceSelected?.brands = self?.homeAPI?.brands
                        
                    }
                    
                    if  self?.screenType.mapMode == .NormalMode || self?.screenType.mapMode == .ZeroMode {
                        
                        
                        if isScanCode{
                            
                            self?.displayScannedService(object:object)
                        }
                        
                    }
                    self?.unreadEvent()
                    if isFromCancellation{
                        if let serviceRequest = self?.serviceRequest{
                            
                            self?.didGetRequestDetails(product_detail: /serviceRequest.product_detail)
                          //  self?.didGetRequestDetails(request: serviceRequest)
                        }
    //                    self?.bookService()
                        
                    }
                
                
            case .failure(let strError):
                //                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
                break
            }
            
            self?.isHomeApiLoading = false
            
            
            //end
        }
    }
    
    func getLocalDriverForParticularService(service: Service,isScanCode:Bool=false,object: RoadPickupModal?=nil,isFromCancellation:Bool = false) {
        
       
        
        self.isFromCancellation = isFromCancellation
        

        DispatchQueue.main.async {
            self.api_getAddress()
        }
        
        
        if APIManagerCab.shared.isConnectedToNetwork() == false {
            alertBoxOk(message:"Validation.InternetNotWorking".localizedString , title: "Pikkup", ok: { [weak self] in
                self?.getUpdatedData()
            })
            return
        }
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        
        guard !isHomeApiLoading  else {
            return
        }
        
        guard !viewNewInvoice.isShown else {
            return
        }
        
        guard  !viewDriverRating.isShown  else {
            return
        }
        
        
        isHomeApiLoading = true
        let homeAPI =  BookServiceEndPoint.homeApi(categoryID: service.serviceCategoryId)
        homeAPI.request(isLoaderNeeded: false, header: ["language_id" : LanguageFile.shared.getLanguage(), "access_token" :  token]) {
            [weak self] (response) in
            
            switch response {
            
            case .success(let data):
                
                guard let model = data as? HomeCab else { return }
                self?.homeAPI = model
                self?.drivers = model.drivers
                
                UDSingleton.shared.home = model
                
               
                
                
                self?.myGroup.notify(queue: .main) {
                        print("Finished all requests.")
                    
                    self?.setupHomeWorkLocations()
                    self?.recentLocations = model.addresses?.recent ?? []
                    
                    for i in 0..<(self?.savedLocations.count ?? 0)!
                    {
                        self?.recentLocations.append((self?.savedLocations[i])!)
                    }
                    
                
                    guard let orders = self?.homeAPI?.orders else {return}
                    
                    guard let lastCompletedOrder = self?.homeAPI?.orderLastCompleted else {return}
                    
                    if orders.count > 0 {
                        
                        var latestOrder = orders[0]
                        
                        if UDSingleton.shared.is_multiple_requests, let order = self?.trackOrder{
                            if let item = orders.filter({$0.orderId == order.orderId}).first{
                                latestOrder = item
                                self?.isOnHome = false
                            }
                        }
                        
                        if latestOrder.orderStatus == .reached || latestOrder.orderStatus == .Confirmed || latestOrder.orderStatus == .Ongoing {
                            
                            self?.currentOrder =  latestOrder
                            
                            if self?.currentOrder?.bookingType == .Present ||  self?.currentOrder?.bookingType == .Future {
                                //                            self?.trackOrder = latestOrder
                                self?.driverAcceptedMode(order: latestOrder)
                            }else{
                                
                                let isOnline = latestOrder.payment?.paymentType?.lowercased() != "cash"
                                let isPaymentInComplete = latestOrder.payment?.productStatus?.lowercased() != "completed"
                                if isOnline && isPaymentInComplete{
                                    self?.screenType = ScreenType(mapMode: .ServiceDoneMode, entryType: .Forward)
                                }
                                
                                self?.screenType = ScreenType(mapMode: .NormalMode, entryType: .Forward)
                                
                            }
                            
                        }else if latestOrder.orderStatus == .Searching {
                            
                            if  /latestOrder.orderId != /self?.currentOrder?.orderId  { //coming after killing the app
                                self?.currentOrder =  latestOrder
                                self?.isSearching = true
                                self?.showBasicNavigation(updateType: .OrderDetail)
                                self?.screenType = ScreenType(mapMode: .UnderProcessingMode, entryType: .Forward)
                            }
                        }else {
                            print("**************** Screen Type  ************")
                            print(self?.screenType)
    //                        self?.screenType = ScreenType(mapMode: .NormalMode, entryType: .Forward)
                            self?.proceed(lastCompletedOrder: lastCompletedOrder)
                        }
                        
                    }else if lastCompletedOrder.count > 0 {
                        
                        self?.proceed(lastCompletedOrder: lastCompletedOrder)
                        
                        
                    } else{
                        
                        if self?.screenType.mapMode == .UnderProcessingMode{
                            
                            self?.viewUnderProcess.minimizeProcessingView()
                            if self?.isSearching == true || /self?.currentOrder?.isContinueFromBreakdown {
                                self?.screenType = ScreenType(mapMode:  .NormalMode , entryType: .Backward)
                                return
                            }
                            self?.screenType = ScreenType(mapMode:  .OrderPricingMode , entryType: .Backward)
                            
                        } else if  self?.screenType.mapMode == .NormalMode || self?.screenType.mapMode == .ZeroMode {
                            
                            
                            if !isScanCode{
                                
                                self?.viewSelectService.minimizeSelectServiceView()
                                self?.screenType = ScreenType(mapMode: .NormalMode, entryType: .Forward)
                            }
                            
                        }else if self?.screenType.mapMode == .RequestAcceptedMode || self?.screenType.mapMode == .RequestAcceptedMode  {
                            
                            self?.viewDriverAccepted.minimizeDriverView()
                            (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: nil)
                            self?.screenType = ScreenType(mapMode:  .NormalMode , entryType: .Backward)
                            
                            self?.moveToNormalMode()
                            self?.getNearByDrivers()
                            
                        }
                        
                    }
                    
                    
                    if /self?.isFromPackage {
                        
                        self?.serviceRequest.serviceSelected?.brands = []
                        
                        self?.modalPackages?.package?.pricingData?.categoryBrands?.forEach({ (packageBrand) in
                            
                            guard let filteredObject = self?.homeAPI?.brands?.filter({$0.categoryBrandId == packageBrand.categoryBrandId}).first else {return}
                            self?.serviceRequest.serviceSelected?.brands?.append(filteredObject)
                            
                        })
                    } else {
                        self?.serviceRequest.serviceSelected?.brands = self?.homeAPI?.brands
                        
                    }
                    
                    if  self?.screenType.mapMode == .NormalMode || self?.screenType.mapMode == .ZeroMode {
                        
                        
                        if isScanCode{
                            
                            self?.displayScannedService(object:object)
                        }
                        
                    }
                    self?.unreadEvent()
                    if isFromCancellation{
                        if let serviceRequest = self?.serviceRequest{
                            
                            self?.didGetRequestDetails(product_detail: /serviceRequest.product_detail)
                          //  self?.didGetRequestDetails(request: serviceRequest)
                        }
    //                    self?.bookService()
                        
                    }
                
                
                
              
                    
                    
                    
                    
                }
            case .failure(let strError):
                //                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
                break
            }
            
            self?.isHomeApiLoading = false
            
            
            //end
        }
    }
    
    
    
    func proceed(lastCompletedOrder:Array<OrderCab>){
            self.viewDriverAccepted.minimizeDriverView()
            print("**************** Screen Type1  ************")
            print(self.screenType)
        

            let order = lastCompletedOrder.first
            
            self.currentOrder = order
            if (order?.orderStatus == .ServiceComplete) && (order?.payment?.paymentType?.lowercased() == "card") && (order?.payment?.payment_status == "Failed" ||
                                                                                                                        order?.payment?.payment_status == "Pending") && order?.payment?.card_type != "1"{
                    self.handlePayment()
            }else if order?.rating?.ratingGiven == nil{
                    self.showNewInvoiceView()
            }else{
                
                if self.screenType.mapMode == .RequestAcceptedMode ||  self.screenType.mapMode == .OnTrackMode {
                    self.viewDriverAccepted.minimizeDriverView()
                    (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: nil)
                }
                
                self.checkCancellationPopUp()
                self.currentOrder = lastCompletedOrder[0]
                self.imgViewPickingLocation.isHidden = true
                self.viewSelectService.minimizeSelectServiceView()
                if self.screenType.mapMode != .ServiceDoneMode  && self.screenType.mapMode != .SelectingFreightBrand && self.screenType.mapMode != .SelectingFreightProduct && self.screenType.mapMode != .OrderPricingMode && self.screenType.mapMode != .SelectingLocationMode{
                    self.moveToNormalMode()
                    
                }
            }
        self.isHomeApiLoading = false
    }
    
    
    
    func getTerminologyData(serviceId: Int) {
        
        if APIManagerCab.shared.isConnectedToNetwork() == false {
            alertBoxOk(message:"Validation.InternetNotWorking".localizedString , title: "Pikkup", ok: { [weak self] in
                self?.getUpdatedData()
            })
            return
        }
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let terminologyAPI =  BookServiceEndPoint.terminologyAPI(categoryID: serviceId)
        terminologyAPI.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage(), "access_token" :  token]) {
            (response) in
            
            switch response {
            
            case .success(let data):
                
                guard let model = data as? AppTerminologyModel else { return }
                UDSingleton.shared.appTerminology = model
                
            case .failure(let strError):
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
            }
        }
    }
    
    
    func bookCancelledService(order:OrderCab){
        serviceRequest.booking_type = order.booking_type
        serviceRequest.category_id = order.serviceId
        serviceRequest.serviceSelected = UDSingleton.shared.appSettings?.services?.filter{ $0.serviceCategoryId == order.serviceId}.first
        
        guard let service = serviceRequest.serviceSelected else { return }
        
        
        
        for brand in homeAPI?.brands ?? []{
            if brand.categoryBrandId == order.serviceBrandId{
                serviceRequest.selectedBrand = brand
                for product in brand.products ?? []{
                    if product.productBrandId == order.categoryBrandProductId{
                        serviceRequest.selectedProduct = product
                    }
                }
            }
        }
        
        
        serviceRequest.quantity = /order.productQuantity
        serviceRequest.locationName = order.dropOffAddress
        serviceRequest.latitude = order.breakdown_latitude
        serviceRequest.longitude = order.breakdown_longitude
        serviceRequest.locationNameDest = order.pickUpAddress
        serviceRequest.latitudeDest = order.pickUpLatitude
        serviceRequest.longitudeDest = order.pickUpLongitude
        serviceRequest.orderDateTime = Date()
        serviceRequest.requestType = .Present
        serviceRequest.paymentMode = order.paymentType
        serviceRequest.materialType = nil
        serviceRequest.weight = Double(/order.product_weight)
        serviceRequest.additionalInfo = nil
        serviceRequest.finalPrice = order.finalCharge
        serviceRequest.distance_price_fixed = "\(/serviceRequest.selectedProduct?.distance_price_fixed)"
        serviceRequest.price_per_min = "\(/serviceRequest.selectedProduct?.price_per_min)"
        serviceRequest.time_fixed_price = "\(/serviceRequest.selectedProduct?.time_fixed_price)"
        serviceRequest.price_per_km = "\(/serviceRequest.selectedProduct?.price_per_km)"
        serviceRequest.booking_type = order.booking_type
        serviceRequest.friend_name = order.friend_name
        serviceRequest.friend_phone_number = order.friend_phone_number
        serviceRequest.coupon_code = order.coupon_detail?.code
        serviceRequest.cancellation_charges = order.cancellation_charges
        serviceRequest.stops = order.ride_stops?.reversed() ?? []
        serviceRequest.locationNickName = order.pickUpAddress
        serviceRequest.selectedCard = nil
        serviceRequest.credit_point_used = order.payment?.credit_point_used
        serviceRequest.description = nil
        serviceRequest.elevator_pickup = nil
        serviceRequest.pickup_level = nil
        serviceRequest.dropoff_level = nil
        serviceRequest.gender = ""
        serviceRequest.product_detail = order.payment?.product_details
        serviceRequest.airport_charges = order.payment?.airport_charges
        serviceRequest.exactPath = order.exactPath
        
        isFromCancellation = true
        
        

        
        getLocalDriverForParticularService(service: service,isFromCancellation: true)
        
    
        
    }
    
    
    func bookService() {
        self.isSearching = false
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        // listeningEvent()
        
        print("Check List Item ",serviceRequest.check_lists?.toJson())
        print(serviceRequest.product_detail)
        ///
        var stops = [Stops]()
        for (index,stop) in serviceRequest.stops.enumerated(){
            if !(stop.address?.isEmpty ?? true){
                stop.priority = stops.count + 1
                stops.append(stop)
            }
        }
        trackOrder = nil
        isOnHome = false
        
//
//        var valueFor_is_manual_assignment = "false"
//        let valueForFuture = /serviceRequest.requestType.rawValue
//        if valueForFuture.trimmed() == "1" {
//            valueFor_is_manual_assignment = "true"
//        }
//        else
//        {
//            valueFor_is_manual_assignment = "false"
//        }
       
        
        let objRequest = BookServiceEndPoint.requestApi(objRequest: serviceRequest,
                                                        categoryId: /serviceRequest.booking_type == "RoadPickup" ? /serviceRequest.category_id : /serviceRequest.serviceSelected?.serviceCategoryId,
                                                        categoryBrandId: /serviceRequest.booking_type == "RoadPickup" ? /serviceRequest.category_brand_id : /serviceRequest.selectedBrand?.categoryBrandId,
                                                        categoryBrandProductId: serviceRequest.selectedProduct?.productBrandId,
                                                        productQuantity: serviceRequest.quantity,
                                                        dropOffAddress:  /serviceRequest.locationName,
                                                        dropOffLatitude: /serviceRequest.latitude?.rounded(toPlaces: 6) ,
                                                        dropOffLongitude:  /serviceRequest.longitude?.rounded(toPlaces: 6),
                                                        pickupAddress: /serviceRequest.locationNameDest,
                                                        pickupLatitude: /serviceRequest.latitudeDest?.rounded(toPlaces: 6),
                                                        pickupLongitude: /serviceRequest.longitudeDest?.rounded(toPlaces: 6),
                                                        orderTimings:  /serviceRequest.orderDateTime.toFormattedDateString(),
                                                        future: /serviceRequest.requestType.rawValue,
                                                        paymentType: /serviceRequest.paymentMode.rawValue,
                                                        distance: 2000,
                                                        organisationCouponUserId: /serviceRequest.eToken?.organisationCouponUserId,
                                                        materialType: /serviceRequest.materialType,
                                                        productWeight: /serviceRequest.weight,
                                                        productDetail:  /serviceRequest.additionalInfo ,
                                                        orderDistance: serviceRequest.distance,
                                                        finalCharge: serviceRequest.finalPrice ?? "0",
                                                        package_id: modalPackages?.packageId,
                                                        distance_price_fixed: serviceRequest.distance_price_fixed,
                                                        price_per_min: serviceRequest.price_per_min,
                                                        time_fixed_price: serviceRequest.time_fixed_price,
                                                        price_per_km: serviceRequest.price_per_km,
                                                        booking_type: serviceRequest.booking_type,
                                                        friend_name: serviceRequest.friend_name,
                                                        friend_phone_number: serviceRequest.friend_phone_number,
                                                        friend_phone_code: serviceRequest.friend_phone_code,
                                                        driver_id: serviceRequest.driver_id,
                                                        coupon_code: serviceRequest.coupon_code,
                                                        cancellation_charges: serviceRequest.cancellation_charges,
                                                        stops: stops.toJSONString(),
                                                        address_name: serviceRequest.locationNickName,
                                                        user_card_id: serviceRequest.selectedCard?.userCardId,
                                                        credit_point_used: serviceRequest.credit_point_used,
                                                        description: serviceRequest.description,
                                                        elevator_pickup: serviceRequest.elevator_pickup,
                                                        elevator_dropoff: serviceRequest.elevator_dropoff,
                                                        pickup_level: serviceRequest.pickup_level,
                                                        dropoff_level: serviceRequest.dropoff_level,
                                                        fragile: serviceRequest.fragile,
                                                        check_lists: serviceRequest.check_lists?.toJson(),
                                                        gender: serviceRequest.gender,
                                                        order_time: serviceRequest.duration,numberOfRiders:
                                                            
                                                            /serviceRequest.numberOfRiders,airportCharge:/serviceRequest.airport_charges,exact_path:/serviceRequest.exactPath,cancelPaymentId:/serviceRequest.paymentId, is_children: /serviceRequest.is_children, relation: /serviceRequest.relation, isGifted: /serviceRequest.isGifted, product_detail: /serviceRequest.product_detail,breakdown_latitude:/serviceRequest.latitude?.rounded(toPlaces: 6),
                                                        breakdown_longitude:/serviceRequest.longitude?.rounded(toPlaces: 6),card_type: serviceRequest.card_type)
        
        
        objRequest.request(isImage: true, images: serviceRequest.orderImages , isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) {[weak self] (response) in
            switch response {
            
            case .success(let data):
                guard let model = data as? OrderCab else { return }
                UDSingleton.shared.userData?.order = [model]
                if /model.orderId == /self?.currentOrder?.orderId && model.orderId != nil && self?.currentOrder?.orderId != nil { //Accepted socket recieved frist
                    return
                } else if model.statusCode == 301 {
                    
                    guard let vc = R.storyboard.bookService.outstandingViewcontroller() else {return}
                    let navigation = UINavigationController(rootViewController: vc)
                    navigation.setNavigationBarHidden(true, animated: false)
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.request = self?.serviceRequest
                    vc.currentOrder = model
                    vc.delegate = self
                    self?.presentVC(navigation)
                    
                } else {
                    self?.viewOrderPricing.minimizeOrderPricingView()
                    self?.homeAPI?.orders.append(model)
                    self?.currentOrder = model
                    if model.orderStatus == .Unassigned {
                        self?.viewFreightOrder.minimizeFreightOrderView()
                        self?.setPendingApproval()
                    }else{
                        self?.screenType = ScreenType(mapMode: .UnderProcessingMode , entryType: .Forward)
                    }
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
            }
            
        }
    }
    
    func setPendingApproval(){
        if UIApplication.topViewController()!.isKind(of: UIAlertController.self) {
            print("Alert already presented by socket....")
        } else {
            self.alertBoxOk(message: "Thank you very much for booking with us! Sit back and relax while our team take care of your needs. If you have questions or you want to cancel, please call us!".localizedString, title: "AppName".localizedString) {
                self.viewUnderProcess.minimizeProcessingView()
                
                self.screenType = ScreenType(mapMode: .NormalMode, entryType: .Backward)
                self.moveToNormalMode()
            }
        }
    }
    
    func cancelRequest(strReason : String , orderID : Int?) {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let objCancel = BookServiceEndPoint.cancelRequest(orderId: /orderID, cancelReason: strReason, orderDistance: 0.0)
        objCancel.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) {  [weak self] (response) in
            
            if self?.isSearching == true || /self?.currentOrder?.isContinueFromBreakdown {
                self?.viewUnderProcess.minimizeProcessingView()
                if self?.mapType == .google{
                    self?.mapView.clear()
                } else{
                    self?.clearAnnotationWithPath()
                }
                self?.cleanPath()
                self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Backward)
                return
            }
            
            switch response {
            case .success(_):
                
                self?.cancelDoneOrder()
                
            case .failure(let strError):
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
            }
        }
    }
    
    @objc func nCancelFromBookinScreen(notifcation : Notification) {
        
        var orderStatus:OrderStatus? = nil
        
        orderStatus = notifcation.userInfo?["orderStatus"] as? OrderStatus ?? nil
        if let orderId = notifcation.object as? String, orderId.toInt() == self.currentOrder?.orderId {
            
            
            cancelDoneOrder(sentOrderStatus:orderStatus)
        }
    }
    
    func cancelDoneOrder(sentOrderStatus:OrderStatus?=nil) {
        self.viewUnderProcess.minimizeProcessingView()
        // viewConfirmPickUp.minimizeConfirmPickUPView()
        
        if self.currentOrder?.orderStatus == .Confirmed || self.currentOrder?.orderStatus == .Ongoing || sentOrderStatus == .Scheduled {
            if self.mapType == .google{
                self.mapView.clear()
            } else{
                self.clearAnnotationWithPath()
            }
            self.cleanPath()
            self.viewDriverAccepted.minimizeDriverView()
            (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: nil)
            self.currentOrder?.orderStatus = .CustomerCancel
            self.screenType = ScreenType(mapMode: .NormalMode , entryType: .Backward)
            
        } else {
            
            if isFromCancellation{
                self.screenType = ScreenType(mapMode: .NormalMode , entryType: .Backward)
            }else{
                self.currentOrder?.orderStatus = .CustomerCancel
                self.screenType = ScreenType(mapMode: .OrderPricingMode , entryType: .Backward)
            }
            
        }
    }
    
    func submitRating(rateValue : Int , comment : String) {
        
        guard let orderId = currentOrder?.orderId else{return}
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let rating = BookServiceEndPoint.rateDriver(orderId: orderId, rating: rateValue, comment: comment)
        
        rating.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) {[weak self] (response) in
            switch response {
            
            case .success(_):
                
                self?.viewDriverRating.minimizeDriverRatingView()
                self?.viewNewInvoice.minimizeNewInvoiceView()
                if self?.mapType == .google{
                    self?.mapView.clear()
                } else{
                    self?.clearAnnotationWithPath()
                }
                self?.cleanPath()
                self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Backward)
                
                // To move to Home in case of Package
                if /self?.isFromPackage {
                    self?.navigationController?.popToRootViewController(animated: true)
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
            }
        }
    }
    
    func updateData() {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let updateDataVC = LoginEndpoint.updateData(fcmID: UserDefaultsManager.fcmId)
        updateDataVC.request(isLoaderNeeded: false, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) { [weak self] (response) in
            switch response {
            
            case .success(let data):
                
                guard let model = data as? HomeCab else { return }
                
                if let userData = UDSingleton.shared.userData {
                    userData.userDetails = model.userDetail
                    userData.services = model.services
                    userData.support = model.support
                    userData.order = model.orders
                    UDSingleton.shared.userData = userData
                }
                
                guard let version = model.version  else { return }
                guard  let forceVersion = version.iOSVersion?.user?.forceVersion  else { return }
                
                self?.checkUpdate(strForce: forceVersion)
                
            case .failure(let strError):
                
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
            }
        }
    }
    
    func cancelHalfWayStop() {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let cancelHalfWayStopObject = BookServiceEndPoint.cancelHalfWayStop(orderId: currentOrder?.orderId)
        cancelHalfWayStopObject.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) { [weak self] (response) in
            switch response {
            
            case .success(let data):
                
                guard let modal = data as? OrderCab else {return}
                
                self?.currentOrder = modal
                self?.viewUnderProcess.minimizeProcessingView()
                self?.showDriverView()
                self?.getUpdatedData()
                
                
            case .failure(let strError):
                
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
            }
        }
    }
    
    func cancelVehicleBreakdown() {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let cancelVehicleBreakdownObject = BookServiceEndPoint.cancelVehicleBreakDown(orderId: currentOrder?.orderId)
        cancelVehicleBreakdownObject.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) { [weak self] (response) in
            switch response {
            
            case .success(let data):
                
                guard let modal = data as? OrderCab else {return}
                
                self?.currentOrder = modal
                self?.viewUnderProcess.minimizeProcessingView()
                self?.showDriverView()
                
            case .failure(let strError):
                
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
            }
        }
    }
    
    func apiGetCreditpoints() {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let getCreditPointsObject = BookServiceEndPoint.getCreditPoints
        let getWalletBalance = BookServiceEndPoint.getWalletBalance
        
        if  Bool(/UDSingleton.shared.appSettings?.appSettings?.is_wallet) ?? false {
            getWalletBalance.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) { [weak self] (response) in
                switch response {
                
                case .success(let data):
                    
                    let walletData = data as? WalletDetails
                    self?.viewOrderPricing.setWalletData(wallet: walletData)
                    
                case .failure(let strError):
                    
                    Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
                }
            }
            
        } else {
            getCreditPointsObject.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) { [weak self] (response) in
                switch response {
                
                case .success(let data):
                    
                    guard let modal = data as? UserDetail else {return}
                    self?.viewOrderPricing.setCreditPoints(points: modal.credit_points)
                    
                case .failure(let strError):
                    
                    Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
                }
            }
        }
        
    }
    
    func apiAddAddress(address: String?, name: String?, latitude: Double?, longitude: Double?, category: String) {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let addAddressObject = BookServiceEndPoint.addAddress(address: address, address_latitude: /latitude, address_longitude:  /longitude, category: category, address_name: name)
        addAddressObject.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) { [weak self] (response) in
            switch response {
            
            case .success(let data):
                debugPrint(data as Any)
                
                guard let modal = data as? AddressCab else {return}
                
                if /modal.category == "HOME" {
                    
                    self?.labelHomeLocationNickName.text = modal.addressName
                    self?.labelHomeLocationName.text = modal.address
                    self?.labelHomeLocationName.isHidden = false
                    self?.buttonEditHomeAddress.isHidden = false
                    
                    self?.homeAPI?.addresses?.home = modal
                    
                } else {
                    
                    self?.labelWorkLocationNickName.text =  modal.addressName
                    self?.labelWorkLocationName.text = modal.address
                    self?.labelWorkLocationName.isHidden = false
                    self?.buttonEditWorkAddress.isHidden = false
                    
                    self?.homeAPI?.addresses?.work = modal
                    
                }
                
            case .failure(let strError):
                
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
            }
        }
    }
    
    func apiEditAddress(address: String?, name: String?, latitude: Double?, longitude: Double?, category: String) {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let addressID = category == "HOME" ? homeAPI?.addresses?.home?.userAddressId : homeAPI?.addresses?.work?.userAddressId
        
        let editAddress = BookServiceEndPoint.editAddress(address: address, address_latitude: latitude, address_longitude: longitude, user_address_id: addressID, category: category, address_name: name)
        
        editAddress.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) { [weak self] (response) in
            switch response {
            
            case .success(let data):
                debugPrint(data as Any)
                
                guard let modal = data as? AddressCab else {return}
                
                if /modal.category == "HOME" {
                    
                    self?.labelHomeLocationNickName.text = modal.addressName
                    self?.labelHomeLocationName.text = modal.address
                    self?.labelHomeLocationName.isHidden = false
                    self?.buttonEditHomeAddress.isHidden = false
                    
                    self?.homeAPI?.addresses?.home = modal
                    
                } else {
                    
                    self?.labelWorkLocationNickName.text =  modal.addressName
                    self?.labelWorkLocationName.text = modal.address
                    self?.labelWorkLocationName.isHidden = false
                    self?.buttonEditWorkAddress.isHidden = false
                    
                    self?.homeAPI?.addresses?.work = modal
                    
                }
                
            case .failure(let strError):
                
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
            }
        }
    }
}

extension HomeVC: GMSMapViewDelegate {
    
    func getNearestPoints(array: [Stops], currentLocation: CLLocationCoordinate2D) -> [Stops]{
        let current = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
        var dictArray = [[String: Any]]()
        for i in 0..<array.count{
            let loc = CLLocation(latitude: array[i].latitude!, longitude: array[i].longitude!)
            let distanceInMeters = current.distance(from: loc)
            let a:[String: Any] = ["distance": distanceInMeters, "coordinate": array[i]]
            dictArray.append(a)
        }
        
        dictArray = dictArray.sorted(by: {($0["distance"] as! CLLocationDistance) < ($1["distance"] as! CLLocationDistance)})
        var sortedArray = [Stops]()
        for i in dictArray{
            sortedArray.append(contentsOf: i["coordinate"] as! [Stops])
        }
        return sortedArray
    }

    
    func  getPolylineRoute(source: CLLocationCoordinate2D? , destination: CLLocationCoordinate2D?, stops: [Stops]? = nil,  model: TrackingModel? ){
        
        if isDoneCurrentPolyline {
            
            isDoneCurrentPolyline = false
            
            // driver giving direction data, and time
            
            if model != nil && model?.polyline != "" {
                if self.currentOrder?.orderStatus == .CustomerCancel  {return}
                self.durationLeft = /model?.etaTime
                // self.updateDriverCustomerMarker(driverLocation: self.source, customerLocation: self.destination , trackString: model?.polyline, stops: stops)
                
                
                if model?.orderStatus == .Ongoing && /stops?.count > 0 && !isStopsShownAfterStartRide {
                    
                } else {
                    self.updateDriverCustomerMarker(driverLocation: source, customerLocation: destination , trackString: model?.polyline, stops: stops)
                    self.isDoneCurrentPolyline = true
                    return
                }
            }
            
//
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            

//
            var urlString  = "https://maps.googleapis.com/maps/api/directions/json?origin=\(/source?.latitude),\(/source?.longitude)&destination=\(/destination?.latitude),\(/destination?.longitude)&sensor=true&mode=driving&key=\( /UDSingleton.shared.appSettings?.appSettings?.ios_google_api)"

            var wayponits = "&waypoints="
            
           
            for stop in stops ?? []{
                if let lat = stop.latitude,let lng = stop.longitude{
                    wayponits = wayponits + "\(/lat),\(/lng)|"
                    let newCordinates = CLLocationCoordinate2DMake(lat, lng)
                    arrDrawPath.append(newCordinates)
                }
            }

            arrDrawPath.append(destination!)
            
            if /stops?.count > 0{
                urlString += wayponits
            }
            
            
            guard let nsString  = NSString.init(string: urlString).addingPercentEscapes(using: String.Encoding.utf8.rawValue),
                  let url = URL(string: nsString) else {return}
            
            print("url Path davender")
            print("**************")
            print(url)
            
            let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
                if error != nil {
                    
                    self.isDoneCurrentPolyline = true
                    debugPrint(error!.localizedDescription)
                    
                } else {
                    do {
                        
                        if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                           // DispatchQueue.global(qos: .background).async { [weak self] () -> Void in
                              //  guard let self = self else{return}
                                guard let routes = json["routes"] as? NSArray else {return}
                                
                                if (routes.count > 0) {
                                    
                                    print(routes)
                                    let overview_polyline = routes[0] as? NSDictionary
                                    let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                    
                                    guard let points = dictPolyline?.object(forKey: "points") as? String else { return }
                                    
                                    guard let legs  = routes[0] as? NSDictionary , let legsJ = legs["legs"] as? NSArray , let lg = legsJ[0] as? NSDictionary else { return }
                                    let duration = lg["duration"] as? NSDictionary
                                    let durationLeft1 = duration?.object(forKey: "text") as? String
                                    
                                    if self.currentOrder?.orderStatus == .CustomerCancel  {return}
                                    
                                    self.durationLeft = /durationLeft1
                                    // self?.updateDriverCustomerMarker(driverLocation: self?.source, customerLocation: self?.destination , trackString: points, stops: stops)
                                    DispatchQueue.main.async {
                                        self.updateDriverCustomerMarker(driverLocation: source, customerLocation: destination , trackString: points, stops: stops)
                                        self.isDoneCurrentPolyline = true
                                        
                                        if model?.orderStatus == .Ongoing {
                                            self.isStopsShownAfterStartRide = true
                                        }
                                    }
                                    
                                } else {
                                    
                                    self.isDoneCurrentPolyline = true
                                }
                        //    }
                        }
                    }catch {
                        debugPrint("error in JSONSerialization")
                    }
                }
            })
            task.resume()
        }
    }
    
    
    func getDotsToDrawRoute(positions : [CLLocationCoordinate2D], completion: @escaping(_ path : GMSPath) -> Void) {
           if positions.count > 1 {
               let origin = positions.first
               let destination = positions.last
               var wayPoints = ""
               for point in positions {
                   wayPoints = wayPoints.count == 0 ? "\(point.latitude),\(point.longitude)" : "\(wayPoints)|\(point.latitude),\(point.longitude)"

               }
            
//            var wayPoint = ""
//                                var wayPoints: [String] = []
//                                for point in positions {
//                                    wayPoint = wayPoint.count == 0 ? "\(point.latitude), \(point.longitude)" : "\(wayPoint)|\(point.latitude), \(point.longitude)"
//
//                                    wayPoints.append(wayPoint)
//                                }
            
//            let request = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin!.latitude),\(origin!.longitude)&destination=\(destination!.latitude),\(destination!.longitude)&wayPoints=\(wayPoints)&key=\(/UDSingleton.shared.appSettings?.appSettings?.ios_google_api)&sensor=false&mode=driving&waypoints"
            

               let request = "https://maps.googleapis.com/maps/api/directions/json"
               let parameters : [String : String] = ["origin" : "\(origin!.latitude),\(origin!.longitude)", "destination" : "\(destination!.latitude),\(destination!.longitude)", "waypoints" : wayPoints,"mode" : "driving","key" :(/UDSingleton.shared.appSettings?.appSettings?.ios_google_api)]
            Alamofire.request(request, method:.get, parameters : parameters).responseJSON(completionHandler: { response in
                   guard let dictionary = response.result.value as? [String : AnyObject]
                       else {
                           return
                   }
                   if let routes = dictionary["routes"] as? [[String : AnyObject]] {
                       if routes.count > 0 {
                           var first = routes.first
                           if let legs = routes.first?["legs"] as? [[String : AnyObject]] {


                            let fullPath : GMSMutablePath = GMSMutablePath()
                            
                               for leg in legs {
                                   if let steps = leg["steps"] as? [[String : AnyObject]] {
                                       for step in steps {
                                           if let polyline = step["polyline"] as? [String : AnyObject] {
                                            
                                            
                                               if let points = polyline["points"] as? String {
                                                self.showPath1(polyStr: points)
                                                
                                            
                                               //fullPath.appendPath(path: GMSMutablePath(fromEncodedPath: points))
                                               }
                                           }
                                       }
                                       completion(fullPath)
                                   }
                               }
                           }
                       }
                   }
               })
           }
       }
    
    
  
     func showPath1(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.map = self.mapView // Your map view
        
       
//        let polyline = GMSPolyline(path: polyStr)
//        polyline.strokeColor = UIColor.blue
//        polyline.strokeWidth = 3.0
//        polyline.map = self.mapView
        
        self.cleanPath()
        self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
        
//        ez.runThisInMainThread { [weak self] in
//            guard let self = self else { return }
//
//        CATransaction.begin()
//            CATransaction.setAnimationDuration(1.0)
//
//
//            guard let path = GMSPath(fromEncodedPath: /polyStr) else { return }
//           // self.polyPath = path
//            self.polyline = GMSPolyline(path: path)
//            self.polyline?.strokeWidth = 3.0
//
//           // guard let path = GMSMutablePath(fromEncodedPath: /polyStr) else { return }
//            let bounds = GMSCoordinateBounds(path: path)
//            self.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
//            CATransaction.commit()
//
//
//            // let path = GMSMutablePath(fromEncodedPath: polyStr) // Path
////            self.polyPath = path
////            self.polyline = GMSPolyline(path: path)
////            self.polyline?.geodesic = true
////            self.polyline?.strokeWidth = 2
//
//            if let mapType = UserDefaultsManager.shared.mapType {
//                guard let mapEnum = BuraqMapType(rawValue: mapType) else { return }
//                // Ankush self.polyline?.strokeColor  = mapEnum == .Hybrid ? .darkGray : .white
//
//                if /UDSingleton.shared.appSettings?.appSettings?.is_darkMap == "true" {
//                    let color = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.Primary_colour ?? DefaultColor.color.rawValue)
//                    self.polyline?.strokeColor = color
//                }else {
//                    let color = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.secondary_colour ?? DefaultColor.color.rawValue)
//                    self.polyline?.strokeColor = color
//                }
//            }
//            self.polyline?.map = self.mapView
//
//            //self.showMovingDrivers(newResponse: self.drivers ?? [])
//
//            self.cleanPath()
//            self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
//
//
//        }
    }
    
    
    func drawRouteWithWaypoint(positions:[CLLocationCoordinate2D]) {
        
        if positions.count >= 2 {
            for i in 0..<positions.count-1
            {
                var arrTemp : [CLLocationCoordinate2D] = []
                arrTemp.append(positions[i])
                arrTemp.append(positions[i+1])
                getDotsToDrawRoute(positions: arrTemp, completion: { path in
                    //self.route.countRouteDistance(p: path)
                    
                    let polyline = GMSPolyline(path: path)
                           polyline.strokeColor = UIColor.blue
                           polyline.strokeWidth = 3.0
                           polyline.map = self.mapView
    //                self.polyline.path = path
    //                self.polyline.strokeColor = UIColor.blue
    //                self.polyline.strokeWidth = 2.0
    //                self.polyline.map = self.mapView
                })
                
            }
        }
        
       
            

           // self.lblDistance.text = String(LiveJob.distance)
        }
    
    func showPath(polyStr :String){
//      drawRouteWithWaypoint(positions: arrDrawPath)
        
//        let path = GMSMutablePath()
//        for i in 0..<arrDrawPath.count
//        {
//            path.add(CLLocationCoordinate2D(latitude:arrDrawPath[i].latitude, longitude:arrDrawPath[i].longitude))
//        }
//
//
//        //Change coordinates
////        path.add(CLLocationCoordinate2D(latitude: -33.85, longitude: 151.20))
////        path.add(CLLocationCoordinate2D(latitude: -33.70, longitude: 151.40))
////        path.add(CLLocationCoordinate2D(latitude: -33.73, longitude: 151.41))
//
//        let path = GMSMutablePath(fromEncodedPath: polyStr) // Path
//
//                let polyline = GMSPolyline(path: path)
//                polyline.strokeColor = UIColor.blue
//                polyline.strokeWidth = 3.0
//                polyline.map = self.mapView

        self.cleanPath()
        self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
        
        ez.runThisInMainThread { [weak self] in
            guard let self = self else { return }

        CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)


            guard let path = GMSPath(fromEncodedPath: /polyStr) else { return }
           // self.polyPath = path
            self.polyline = GMSPolyline(path: path)
            self.polyline?.strokeWidth = 3.0

           // guard let path = GMSMutablePath(fromEncodedPath: /polyStr) else { return }
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
            CATransaction.commit()


            self.polyline = GMSPolyline(path: path)
            self.polyline?.geodesic = true
            self.polyline?.strokeWidth = 2

            if let mapType = UserDefaultsManager.shared.mapType {
                guard let mapEnum = BuraqMapType(rawValue: mapType) else { return }
                // Ankush self.polyline?.strokeColor  = mapEnum == .Hybrid ? .darkGray : .white

                if /UDSingleton.shared.appSettings?.appSettings?.is_darkMap == "true" {
                    let color = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.Primary_colour ?? DefaultColor.color.rawValue)
                    self.polyline?.strokeColor = color
                }else {
                    let color = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.secondary_colour ?? DefaultColor.color.rawValue)
                    self.polyline?.strokeColor = color
                }
            }
            self.polyline?.map = self.mapView

            //self.showMovingDrivers(newResponse: self.drivers ?? [])

            self.cleanPath()
            self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)


        }
    }
    
    func cleanPath() {
        if self.timer != nil {
            self.timer.invalidate()
            self.timer = nil
        }
    }
    
    @objc func animatePolylinePath() {
        
        guard let path = polyPath else { return }
        if (self.i < path.count()) {
            self.animationPath.add(path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            
            self.animationPolyline.strokeColor = .lightGray //R.color.appNewRedBorder()?.withAlphaComponent(0.5) ?? .black
            self.animationPolyline.strokeWidth = 2
            self.animationPolyline.map = self.mapView
            self.i += 1
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }
    
    
    func updatePickupDropoffMarker(pickupLocation:CLLocationCoordinate2D?,dropoffLocation:CLLocationCoordinate2D?){
        
        guard let source = pickupLocation , let destination = dropoffLocation  else { return }
        
        pickupMapBoxMarker.coordinate = source
        mapBoxView.addAnnotation(driverMapBoxMarker)
        dropOffMapBoxMarker.coordinate = destination
        mapBoxView.addAnnotation(userMapBoxMarker)
        
    }
    
    func updateDriverCustomerMapboxMarker( driverLocation : CLLocationCoordinate2D? , customerLocation  : CLLocationCoordinate2D? , trackString : String?, stops: [Stops]?) {
        
        
        guard let source = driverLocation , let destination = customerLocation  else { return }
        guard let currOrder = self.currentOrder else {return}
        driverMapBoxMarker.coordinate = source
        mapBoxView.addAnnotation(driverMapBoxMarker)
        userMapBoxMarker.coordinate = destination
        mapBoxView.addAnnotation(userMapBoxMarker)
        
        stops?.forEachEnumerated {[weak self] (index, stop) in
            let markerStoppage = GMSMarker()
            print(/stop.latitude)
            print(/stop.longitude)
            markerStoppage.position = CLLocationCoordinate2D(latitude: /stop.latitude, longitude:  /stop.longitude)
            markerStoppage.icon = #imageLiteral(resourceName: "ic_location_pin")
            markerStoppage.map = self?.mapView
            self?.addIconViewToMarker(marker: markerStoppage, text: "Stop \(index + 1)")
            markerStoppage.map = self?.mapView
        }
        if let degrees = self.driverBearing  {
            guard let currOrder = self.currentOrder else {return}
            let id = /currOrder.serviceId
            let image = UIImage().getDriverImage(type: id)
            self.driverMapBoxImageMarker.image =  image.rotate(radians: Float(degrees))
        }
        
        
    }
    
    func setDriverIcon(brancdId:Int? = nil){
        let id = brancdId ?? serviceRequest.selectedBrand?.categoryBrandId
        driverMarker.icon = UIImage().getDriverImage(type: /id)
        //        if let urlString = serviceRequest.selectedBrand?.map_icon,let url = URL(string: APIBasePath.imageURL + urlString){
        //            UIImageView().sd_setImage(with: url) { (image,_,_,_) in
        //                self.driverMarker.icon = image?.imageWithImage(scaledToSize: .init(width: 44, height: 44))
        //            }
        //        }else{
        //            driverMarker.icon = UIImage().getDriverImage(type: /order?.serviceId)
        //        }
        
        
    }
    
    
    
    func updateDriverCustomerMarker( driverLocation : CLLocationCoordinate2D? , customerLocation  : CLLocationCoordinate2D? , trackString : String?, stops: [Stops]?) {
        
        self.cleanPath()
        
        ez.runThisInMainThread { [weak self] in
            
            guard let source = driverLocation , let destination = customerLocation  else { return }
            guard let currOrder = self?.currentOrder else {return}
            
            self?.viewDriverAccepted.lblTimeEstimation.text = self?.durationLeft
            (ez.topMostVC as? OngoingRideDetailsViewController)?.lblTimeEstimation.text = self?.durationLeft // to update data
            
            if /currOrder.serviceId > 3 {
                self?.userMarker.icon = nil // Ankush currOrder.orderStatus == .Ongoing ? #imageLiteral(resourceName: "DropMarker") : #imageLiteral(resourceName: "PickUpMarker")
            }else{
                self?.userMarker.icon = nil // Ankush #imageLiteral(resourceName: "DropMarker")
            }
            
            if self?.mapType == .google{
                self?.mapView.clear()
                
                
            } else{
                self?.clearAnnotationWithPath()
            }
            self?.cleanPath()
            self?.userMarker.isFlat = false
            self?.userMarker.map = self?.mapView
            self?.userMarker.position = CLLocationCoordinate2D(latitude: destination.latitude, longitude: destination.longitude)
            
            let id = /currOrder.serviceId
            self?.driverMarker.icon = UIImage().getDriverImage(type: id)
            self?.driverMarker.isFlat = false
            self?.driverMarker.map = self?.mapView
            
            /* let imageView = UIImageView(image: R.image.dropMarker())
             imageView.kf.setImage(with: URL(string: /self?.currentOrder?.driverAssigned?.icon_image_url), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
             self?.driverMarker.iconView = imageView */
            
            self?.driverMarker.isFlat = false
            self?.driverMarker.map = self?.mapView
            
            
            stops?.forEachEnumerated {[weak self] (index, stop) in
                let markerStoppage = GMSMarker()
                print(/stop.latitude)
                print(/stop.longitude)
                markerStoppage.position = CLLocationCoordinate2D(latitude: /stop.latitude, longitude:  /stop.longitude)
                markerStoppage.icon = #imageLiteral(resourceName: "ic_location_pin")
                markerStoppage.map = self?.mapView
                self?.addIconViewToMarker(marker: markerStoppage, text: "Stop \(index + 1)")
                markerStoppage.map = self?.mapView
            }
            
            
            CATransaction.begin()
            CATransaction.setAnimationDuration(2.5)
            
            self?.driverMarker.position = CLLocationCoordinate2D(latitude: source.latitude, longitude: source.longitude)
            self?.driverMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            if let degrees = self?.driverBearing  {
                self?.driverMarker.rotation = /CLLocationDegrees(degrees)
            }
            CATransaction.commit()
            guard let lineTrack = trackString  else { return }
            self?.showPath(polyStr: lineTrack)
        }
    }
}

//MARK:- Custom Delegates
//MARK:-

extension HomeVC : BookRequestDelegate {
    func reloadDrivers(_ serviceSelected: Service) {
        //getLocalDriverForParticularService(service: serviceSelected)
    }
    
    func reloadDrivers1(_ serviceSelected: Service) {
        
        //Davender Verma
       // getLocalDriverForParticularService1(service: serviceSelected)
       
        getLocalDriverForParticularService1(service: serviceSelected)
    }
    
    func didSelectContactForGift() {
        self.view.endEditing(true)
        let contactPickerScene = EPContactsPicker(delegate: self, multiSelection:false, subtitleCellType: SubtitleCellValue.phoneNumber)
        let navigationController = UINavigationController(rootViewController: contactPickerScene)
        navigationController.modalPresentationStyle = .overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func didShowCheckList(order: OrderCab?) {
        guard let fullOrderDetails = R.storyboard.bookService.fullOrderDetails() else { return }
        fullOrderDetails.orderCurrent = order
        self.presentVC(fullOrderDetails)
    }
    
    
    
    func didBuyEToken(brandId :Int, brandProductId:Int) {
        
        guard let etoken = R.storyboard.drinkingWater.tokenListingVC() else{return}
        
        print(brandId)
        
        CouponSelectedLocation.categoryId = brandId
        CouponSelectedLocation.categoryBrandId = brandProductId
        CouponSelectedLocation.selectedAddress = /serviceRequest.locationName
        CouponSelectedLocation.Brands = serviceRequest.serviceSelected?.brands
        CouponSelectedLocation.latitude = /serviceRequest.latitude
        CouponSelectedLocation.longitude = /serviceRequest.longitude
        
        self.pushVC(etoken)
        
    }
    
    func didPaymentModeChanged(paymentMode: PaymentType, selectedCard: CardCab?) {
        
        serviceRequest.paymentMode = paymentMode
        serviceRequest.selectedCard = selectedCard
        
        viewOrderPricing.updatePaymentMode(service: serviceRequest)
    }
  
    
    func didGetRequestDetails(request: ServiceRequest) {
        
    }
    
    
    
    
    
    func didGetRequestDetails(product_detail: String) {
        
        self.serviceRequest.product_detail = product_detail
        // serviceRequest.isBookingFromPackage = isFromPackage
        
        //        if APIBasePath.appScheme == .shipusnow && (request.categoryType == .export || request.categoryType == .moving || request.categoryType == .lagos){
        //            bookService()
        //        }else{
    
        if  serviceRequest.requestType == .Future {
            screenType = ScreenType(mapMode: .OrderPricingMode , entryType: .Forward)
        } else{
            
            let mapmode : MapMode  = serviceRequest.requestType == .Present  ? .OrderPricingMode : .ScheduleMode
            screenType = ScreenType(mapMode: mapmode , entryType: .Forward)
        }
        btnLeftNavigationBar.isHidden = true
        buttonNewSideMenu.isHidden = true
        viewFreightOrder.minimizeFreightOrderView()
        // }
    }
    
    func didGetRequestDetailWithScheduling(request: ServiceRequest) {
        self.serviceRequest = request
        guard let vc = R.storyboard.orderDetails.enterOrderDetailsViewController() else{return}
        vc.serviceReq = serviceRequest
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        presentVC(vc, true)
        
    }
    
    func didRequestTimeout() {
        orderEventFire()
    }
    
    func didSelectServiceType(_ serviceSelected: Service) {
        
        // no need to select service
        //3April
        
        if serviceSelected.serviceName == "Cab Booking"{
            btnRoadPickup.isHidden = false
        }else{
            btnRoadPickup.isHidden = true
        }
        serviceRequest.serviceSelected = serviceSelected
        currentService = /serviceSelected.serviceCategoryId
        
        getLocalDriverForParticularService(service: serviceSelected)
        locationEdit = currentService > 3 ? .PickUp : .DropOff
        
        // call terminology API
        getTerminologyData(serviceId: /serviceSelected.serviceCategoryId)
        
        if serviceSelected.serviceCategoryId == 12 {
            bottomViewBookforFriend.showBookForFriendView(superView: view, requestPara: serviceRequest)
        }
        
        let categoryID = serviceRequest.serviceSelected?.serviceCategoryId
        //    let is_manual_assignment = serviceRequest.serviceSelected?.is_manual_assignment ?? "false"
        //        if APIBasePath.appScheme == .shipusnow{
        //            switch categoryID{
        //            case 13:
        //                serviceRequest.categoryType = .export
        //            case 14:
        //                serviceRequest.categoryType = .lagos
        //            case 17:
        //                serviceRequest.categoryType = .moving
        //            default:
        //                serviceRequest.categoryType = .none
        //            }
        //        }
        //serviceRequest.is_manual_assignment = false
        
        getNearByDrivers()
    }
    
    func didSelectBrandProduct(_ brand: Brand, _ product: ProductCab) {
        //        if APIBasePath.appScheme == .asap{
        //            setDriverIcon()
        //        }
        serviceRequest.selectedBrand = brand
        serviceRequest.selectedProduct = product
    }
    
    func didSelectQuantity(count: Int) {
        
        serviceRequest.quantity = count
        
    }
    
    
    func didSelectNext(type: ActionType) {
        
        if type == .SelectLocation {
            
            for i in 0 ..< /arrTitle?.count {
                let dict = arrTitle?[i] as? AppForum
                
                if dict?.key_name?.lowercased() == "drop_location" {
                    txtDropOffLocation.placeholder = dict?.terminology
                }
                
            }
            
            guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{return}
            
            self.locationEdit =    serviceID > 3  ?  .PickUp : .DropOff
            screenType = ScreenType(mapMode: .SelectingLocationMode , entryType: .Forward)
            changeLocationType(move: .Forward)
            
        }else if type == .NextGasType {
            
            screenType = ScreenType(mapMode: .OrderPricingMode , entryType: .Forward)
        }else if type == .SubmitOrder {
            viewOrderPricing.request = serviceRequest
            if viewOrderPricing.request.isPool ?? false{
                viewOrderPricing?.calculatePriceForPoolBooking()
            }else{
                viewOrderPricing?.calculatePriceForNormalBooking()
            }
            bookService()
            
            //            guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{return}
            
            //            if serviceID == 1 || serviceID == 4 || serviceID == 3 || serviceID == 2 || serviceID == 7 || serviceID == 10 {
            //                bookService()
            //            } else {
            //                showWorkingProgressVC()
            //            }
            
        }else if type == .CancelOrderOnSearch {
            
            cancelRequest(strReason: reasonString, orderID: /currentOrder?.orderId)
            
        } else if  type == .CancelHalfwayStop {
            cancelHalfWayStop()
        } else if  type == .CancelVehiclebreakdown {
            cancelVehicleBreakdown()
        } else if type == .Payment {
            let paymentGateway = PaymentGateway(rawValue:(/UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id))
            if paymentGateway == .paymaya {
                getPaymayaUrl()
            } else{
                //let notification = NotificationCenter.default
                NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.etokenNotification(notifcation:)), name: Notification.Name(rawValue: LocalNotifications.eTokenSelected.rawValue), object: nil)
                
                guard let PaymentVCCab = R.storyboard.bookService.paymentVCCab() else{return}
                PaymentVCCab.categoryId = self.serviceRequest.serviceSelected?.serviceCategoryId
                PaymentVCCab.paymentType = serviceRequest.paymentMode
                PaymentVCCab.delegate = self
                
                pushVC(PaymentVCCab)
            }
            
            
        }else if type == .DoneInvoice {
            showDriverRatingView()
            
        }else if type == .CancelTrackingOrder {
            showCancellationFormVC()
        } else if type == .BackFromFreightBrand {
            //back pressed on freight Brand View
            screenType = ScreenType(mapMode: .SelectingLocationMode, entryType: .Backward)
            
        } else if type == .BackFromFreightProduct {
            //back pressed on freight product View
            screenType = ScreenType(mapMode: .SelectingFreightBrand, entryType: .Backward)
        } else if type == .SelectingFreightBrand {
            //            if APIBasePath.appScheme == .shipusnow && (serviceRequest.categoryType == .export || serviceRequest.categoryType == .lagos || serviceRequest.categoryType == .moving){
            //                serviceRequest.selectedBrand = serviceRequest.serviceSelected?.brands?.first
            //                serviceRequest.selectedProduct = homeAPI?.brands?.first?.products?.first
            //                screenType = ScreenType(mapMode: .OrderDetail, entryType: .Forward)
            //            }else{
            screenType = ScreenType(mapMode: .SelectingFreightBrand, entryType: .Forward)
            //  }
        }
    }
    func didSelectGiftNext(type: ActionType, serviceSelected: ServiceRequest) {
        
        // if serviceSelected.serviceName == "Send A Gift" {
        bottomViewBookforFriend.showBookForFriendView(superView: view, requestPara: serviceSelected)
        // }
        
    }
    func didRatingSubmit(ratingValue: Int, comment: String) {
        submitRating(rateValue: ratingValue, comment: comment)
    }
    
    func didSkipRating() {
        submitRating(rateValue: 0, comment: "")
        viewDriverRating.minimizeDriverRatingView()
        viewNewInvoice.minimizeNewInvoiceView()
        
        if self.mapType == .google{
            mapView.clear()
        } else{
            self.clearAnnotationWithPath()
        }
        cleanPath()
        screenType = ScreenType(mapMode: .NormalMode , entryType: .Backward)
        
        // To move to Home in case of Package
        if /isFromPackage {
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func didSelectSchedulingDate(date: Date, minDate:Date) {
        showSchedular(date: date, minDate: minDate)
    }
    
    func didSelectedFreightBrand(brand : Brand,isPromo:Bool,coupon:Coupon?)  {
        
        serviceRequest.isPromo = isPromo
        serviceRequest.coupon = coupon
        serviceRequest.selectedBrand = brand
        
        let product = brand.products?.first
        
        // if brand.categoryId == 7 ||  brand.categoryId == 4 || brand.categoryId == 10 || brand.categoryId == 13, let product = brand.products?.first { // to show first object selected
        
        serviceRequest.selectedProduct = product
        serviceRequest.selectedBrand = brand // Ankush
        serviceRequest.requestType = .Present
        serviceRequest.orderDateTime = Date()
        
        
        
        
        //            let template = AppTemplate(rawValue: /UDSingleton.shared.appTerminology?.key_value?.template?.toInt())
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
        switch template {
        
        case .Mover?:
            serviceRequest.requestType = .Present
            serviceRequest.selectedProduct = homeAPI?.brands?.first?.products?.first
            serviceRequest.selectedBrand = brand
            
            if serviceRequest.serviceSelected?.booking_flow == "1" {
                screenType = ScreenType(mapMode: .OrderDetail , entryType: .Forward)
            } else{
                screenType = ScreenType(mapMode: .OrderPricingMode , entryType: .Forward)
            }
            
        default:
            screenType = ScreenType(mapMode: .SelectingFreightProduct , entryType: .Forward)
            
        }
        
        
        
    }
    
    func didSelectedFreightProduct(product : ProductCab, isSchedule: Bool) {
        
        serviceRequest.selectedProduct = product
        
        if isSchedule {
            serviceRequest.requestType = .Future
            screenType = ScreenType(mapMode: .ScheduleMode , entryType: .Forward)
        } else {
            serviceRequest.requestType = .Present
            if serviceRequest.serviceSelected?.booking_flow == "1" {
                guard let vc = R.storyboard.orderDetails.enterOrderDetailsViewController() else{return}
                vc.serviceReq = serviceRequest
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .coverVertical
                vc.delegate = self
                presentVC(vc, true)
            } else {
                screenType = ScreenType(mapMode: .OrderPricingMode, entryType: .Forward)
            }
            
            
        }
    }
    
    func didClickConfirmBooking(request: ServiceRequest) {
        debugPrint("Need to handle")
        
        serviceRequest = request
        
        /* serviceRequest.finalPrice = finalPrice
         serviceRequest.distance_price_fixed = String(/selectedPackageProduct?.distance_price_fixed)
         serviceRequest.price_per_min = String(/selectedPackageProduct?.price_per_min)
         serviceRequest.time_fixed_price = String(/selectedPackageProduct?.time_fixed_price)
         serviceRequest.price_per_km = String(/selectedPackageProduct?.price_per_km) */
        if /UDSingleton.shared.appSettings?.appSettings?.is_childrenTravelling == "true" {
            let alert = UIAlertController(title: "Children Travelling \n Select your desired option", message: "Are male children traveling?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                self.serviceRequest.is_children = 0
                if self.serviceRequest.booking_type == "RoadPickup" {
                    self.didSelectNext(type: .SubmitOrder)
                } else {
                    if APIBasePath.isPikkup {
                        self.didSelectNext(type: .SubmitOrder)
                    }else {
                        self.showConfirmPickUpController()
                    }
                    
                }
            }))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.serviceRequest.is_children = 1
                let alertChild = UIAlertController(title: "Children Travelling", message: "Male children must be under 12 years old", preferredStyle: .alert)
                alertChild.addAction(UIAlertAction(title: "ok", style: .default, handler: { action in
                    
                    if self.serviceRequest.booking_type == "RoadPickup" {
                        self.didSelectNext(type: .SubmitOrder)
                    } else {
                        if APIBasePath.isPikkup {
                            self.didSelectNext(type: .SubmitOrder)
                        }else {
                            self.showConfirmPickUpController()
                        }
                    }
                }))
                
                self.present(alertChild, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
            
        } else{
            if serviceRequest.booking_type == "RoadPickup" {
                didSelectNext(type: .SubmitOrder)
            } else {
                if APIBasePath.isPikkup {
                    didSelectNext(type: .SubmitOrder)
                }else {
                    self.showConfirmPickUpController()
                }
            }
            
        }
        
        // Ankush screenType = ScreenType(mapMode: .ConfirmPickup , entryType: .Forward) // For confirm pickup
    }
    
    func didClickInvoiceInfoButton() {
        showInvoiceView()
    }
    
    func didClickContinueToBookforFriend(request: ServiceRequest) {
        
        bottomViewBookforFriend.minimizeBookForFriendView()
        
        isSwitchRiderViewOpened = false
        ViewForfriend.isHidden = true
        
        ViewEnterPickUpDropOff.isHidden = false
        imageViewHeaderProfilePic.isHidden = true
        
       // viewForSavedAddress.isHidden = false
        
        buttonHeaderBookingFor.setTitle("Home.For_Friend".localizedString, for: .normal)
        
        self.serviceRequest = request
        
        if request.isGifted == 1 {
            viewSelectService.minimizeSelectServiceView()
            didSelectNext(type: .SelectingFreightBrand)
        }
    }
    
    
    
    func displayScannedService(object: RoadPickupModal?){
        print(serviceRequest.category_brand_id)
        
        guard let filteredBrand = homeAPI?.brands?.filter({$0.categoryBrandId == serviceRequest.category_brand_id}).first else {return}
        let scannedProduct = filteredBrand.products?.first
        
        serviceRequest.selectedProduct = scannedProduct
        
        // Open enter location popup
        self.locationEdit =    /object?.categoryId > 3  ?  .PickUp : .DropOff
        screenType = ScreenType(mapMode: .SelectingLocationMode , entryType: .Forward)
        changeLocationType(move: .Forward)
    }
    
    
    func didScanRoadPickUPQRCode(object: RoadPickupModal?) {
        
        debugPrint(object as Any)
        
        serviceRequest.booking_type = "RoadPickup"
        serviceRequest.category_id = object?.categoryId
        serviceRequest.category_brand_id = object?.categoryBrandId
        serviceRequest.requestType = .Present
        serviceRequest.orderDateTime = Date()
        serviceRequest.driver_id = object?.userId
        if  self.viewSelectService.serviceLocal.count > 0{
            
            if let index = self.viewSelectService.getServiceIndex(id: /object?.categoryId){
                let serviceSelected = self.viewSelectService.serviceLocal[index].objService
                serviceRequest.serviceSelected = serviceSelected
                currentService = /serviceSelected.serviceCategoryId
               
                getLocalDriverForParticularService(service: serviceSelected)
                locationEdit = currentService > 3 ? .PickUp : .DropOff
            }else{
                
                
                return
            }
        }
        
        
        
        
        if let filteredBrand = homeAPI?.brands?.filter({$0.categoryBrandId == serviceRequest.category_brand_id}).first{
            
            let scannedProduct = filteredBrand.products?.first
            
            serviceRequest.selectedProduct = scannedProduct
            
            // Open enter location popup
            self.locationEdit =    /object?.categoryId > 3  ?  .PickUp : .DropOff
            screenType = ScreenType(mapMode: .SelectingLocationMode , entryType: .Forward)
            changeLocationType(move: .Forward)
            
        } else {
            
            
           getLocalDriverForParticularService(service:  serviceRequest.serviceSelected!,isScanCode: true,object: object)
        }
        
        
    }
    
    func optionApplyCouponCodeClicked(object: Coupon?) {
        viewOrderPricing.setupDataAfterPromoApplied(object: object)
    }
    
    func didPayOutstanding(request: ServiceRequest) {
        debugPrint("Make another request using outstanding price")
        
        self.serviceRequest = request
        bookService()
    }
    
    
    func didChooseAddressFromRecent(place: AddressCab?, isPickup: Bool?,isFromStop:Bool?,stopIndex:Int) {
        
        if isFromStop == false
        {
            if /isPickup {
                
                txtPickUpLocation.text = place?.address
                serviceRequest.locationNameDest = place?.address
                serviceRequest.latitudeDest =  /place?.addressLatitude
                serviceRequest.longitudeDest =  /place?.addressLongitude
                
            } else {
                
                txtDropOffLocation.text = place?.address
                serviceRequest.locationNickName = place?.addressName
                serviceRequest.locationName = place?.address
                serviceRequest.latitude =  /place?.addressLatitude
                serviceRequest.longitude =  /place?.addressLongitude
            }
            
            callLocationSelected()
        }
        else
        {
            let count = self.serviceRequest.stops.count
            guard count > 0 else {
                return
            }
            let modal = Stops(latitude:  /place?.addressLatitude, longitude: /place?.addressLongitude, priority: 0, address:place?.address)
            self.serviceRequest.stops[count-1] = modal
            self.stopsTableView.reloadData()
            
            buttonAddStop.isHidden = false
            
            let numberOfStops =  /Int(/UDSingleton.shared.appSettings?.appSettings?.max_number_of_stops)
            buttonAddStop.isHidden = serviceRequest.stops.count >= numberOfStops
            
            
        }
        
        
    }
    
    func didAddLocationFromSavedAddress(buttonTag: Int, isFromSearch: Bool?,isFromStop:Bool) {
        
        let strybord = UIStoryboard(name: "MainCab", bundle: nil)
        let bookingVC = strybord.instantiateViewController(withIdentifier: "SavedAddressVC") as? SavedAddressVC
        bookingVC?.comingFrom = "HomeVC"
        bookingVC?.buttonTag = buttonTag
        bookingVC?.isFromSearch = isFromSearch
        bookingVC?.delegate = self
        bookingVC?.isFromStop = isFromStop
        self.navigationController?.pushViewController(bookingVC!, animated: false)
    }
    
    func messageData(buttonTag: Int, isFromSearch: Bool?, isFromStop: Bool?, address: String, lat: Double, long: Double,nic_name:String) {
       
            
          if isFromStop ?? false{
                    let modal = Stops(latitude:lat, longitude: long, priority: 3, address: address)
                    self.serviceRequest.stops[buttonTag] = modal
                    self.stopsTableView.reloadData()
            
                    let numberOfStops =  /Int(/UDSingleton.shared.appSettings?.appSettings?.max_number_of_stops)
                    self.buttonAddStop.isHidden = serviceRequest.stops.count >= numberOfStops
            
            
                    //self.buttonAddStop.isHidden = false
            }
            else{
            /*  viewLocationTableContainer.alpha = 1 // Ankush
             btnCurrentLocation.isHidden = true
             imgViewPickingLocation.isHidden = true
             
             viewStoppageDesc.isHidden = !(buttonTag == 2 || buttonTag == 3) */
            
            // pickup- 1, Stop1- 2, Stop2- 3, Stop3- 101, drop- 4, 5- add Home, 6- Add Work, 7- Edit Home, 8- Edit Work
            
            switch buttonTag {
            
           
            
            case 1:
                
                self.txtPickUpLocation.text = address
                self.serviceRequest.locationNameDest = address
                self.serviceRequest.latitudeDest =  lat
                self.serviceRequest.longitudeDest =  long
              
                self.callLocationSelected()
                
         
                
            case 4:
                
                self.txtDropOffLocation.text = address
                self.serviceRequest.locationNickName = nic_name
                self.serviceRequest.locationName = address
                self.serviceRequest.latitude =  lat
                self.serviceRequest.longitude =  long
                
             
                self.callLocationSelected()
                
              
                
            case 5:
                
                if homeAPI?.addresses?.home?.address == nil || /homeAPI?.addresses?.home?.address?.isEmpty {
                    self.apiAddAddress(address: address, name: nic_name, latitude: lat, longitude: long, category: "HOME")
                } else {
                    viewChooseAddress.showView(address: homeAPI?.addresses?.home,count:self.serviceRequest.stops.count)
                    
                }
                
            case 6:
                
                if homeAPI?.addresses?.work?.address == nil || /homeAPI?.addresses?.work?.address?.isEmpty {
                    debugPrint("Work")
                    self.apiAddAddress(address: address, name: nic_name, latitude: lat, longitude: long, category: "WORK")
                } else {
                    viewChooseAddress.showView(address: homeAPI?.addresses?.work,count:self.serviceRequest.stops.count)
                }
                
            case 7:
                debugPrint("Home")
                self.apiEditAddress(address: address, name: nic_name, latitude: lat, longitude: long, category: "HOME")
            case 8:
                
                debugPrint("WORK")
                self.apiEditAddress(address: address, name: nic_name, latitude: lat, longitude: long, category: "WORK")
                
            default:
                break
            }
            }
      
    }
    
    
    func didAddLocationFrom(buttonTag: Int, isFromSearch: Bool?,isFromStop:Bool) {
        
        if /isFromSearch {
            
            if isFromStop{
                GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
                    
                    let modal = Stops(latitude: /place?.coordinate.latitude, longitude: /place?.coordinate.longitude, priority: 3, address: place?.formattedAddress)
                    self?.serviceRequest.stops[buttonTag] = modal
                    self?.stopsTableView.reloadData()
                    
                    let numberOfStops =  /Int(/UDSingleton.shared.appSettings?.appSettings?.max_number_of_stops)
                    self?.buttonAddStop.isHidden = (self?.serviceRequest.stops.count ?? 0) >= numberOfStops
                    
                   // self?.buttonAddStop.isHidden = false
                }
            }
            else{
            /*  viewLocationTableContainer.alpha = 1 // Ankush
             btnCurrentLocation.isHidden = true
             imgViewPickingLocation.isHidden = true
             
             viewStoppageDesc.isHidden = !(buttonTag == 2 || buttonTag == 3) */
            
            // pickup- 1, Stop1- 2, Stop2- 3, Stop3- 101, drop- 4, 5- add Home, 6- Add Work, 7- Edit Home, 8- Edit Work
            
            switch buttonTag {
            case 1:
                
                GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
                    
                    self?.txtPickUpLocation.text = place?.formattedAddress
                    self?.serviceRequest.locationNameDest = place?.formattedAddress
                    self?.serviceRequest.latitudeDest =  /place?.coordinate.latitude
                    self?.serviceRequest.longitudeDest =  /place?.coordinate.longitude
                    
                    //  self?.setCameraGoogleMap(latitude: /place?.coordinate.latitude, longitude: /place?.coordinate.longitude)
                    
                    self?.callLocationSelected()
                }
                
//            case 2:
//
//                GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
//
//
//                    if let object = self?.serviceRequest.stops.filter({$0.priority == 1}).first {
//                        self?.serviceRequest.stops.remove(object: object)
//                    }
//
//                    self?.txtStop1.text = place?.formattedAddress
//
//                    let modal = Stops(latitude: /place?.coordinate.latitude, longitude: /place?.coordinate.longitude, priority: 1, address: place?.formattedAddress)
//                    self?.serviceRequest.stops.append(modal)
//
//
//                    debugPrint(self?.serviceRequest.stops as Any)
//                }
//
//            case 3:
//
//                GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
//
//                    if let object = self?.serviceRequest.stops.filter({$0.priority == 2}).first {
//                        self?.serviceRequest.stops.remove(object: object)
//                    }
//
//                    self?.txtStop2.text = place?.formattedAddress
//                    let modal = Stops(latitude: /place?.coordinate.latitude, longitude: /place?.coordinate.longitude, priority: 2, address: place?.formattedAddress)
//                    self?.serviceRequest.stops.append(modal)
//
//                    debugPrint(self?.serviceRequest.stops as Any)
//                }
//            case 101:
//
//                GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
//
//                    if let object = self?.serviceRequest.stops.filter({$0.priority == 3}).first {
//                        self?.serviceRequest.stops.remove(object: object)
//                    }
//
//                    self?.txtStop3.text = place?.formattedAddress
//                    let modal = Stops(latitude: /place?.coordinate.latitude, longitude: /place?.coordinate.longitude, priority: 3, address: place?.formattedAddress)
//                    self?.serviceRequest.stops.append(modal)
//
//                    debugPrint(self?.serviceRequest.stops as Any)
//                }
                
            case 4:
                
                GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
                    
                    self?.txtDropOffLocation.text = place?.formattedAddress
                    self?.serviceRequest.locationNickName = place?.name
                    self?.serviceRequest.locationName = place?.formattedAddress
                    self?.serviceRequest.latitude =  /place?.coordinate.latitude
                    self?.serviceRequest.longitude =  /place?.coordinate.longitude
                    
                    //  self?.setCameraGoogleMap(latitude: /place?.coordinate.latitude, longitude: /place?.coordinate.longitude)
                    
                    self?.callLocationSelected()
                }
                
            case 5:
                
                if homeAPI?.addresses?.home?.address == nil || /homeAPI?.addresses?.home?.address?.isEmpty {
                    
                    GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
                        
                        debugPrint("Home")
                        self?.apiAddAddress(address: place?.formattedAddress, name: place?.name, latitude: place?.coordinate.latitude, longitude: place?.coordinate.longitude, category: "HOME")
                    }
                    
                } else {
                    
                    viewChooseAddress.showView(address: homeAPI?.addresses?.home,count:self.serviceRequest.stops.count)
                    
                }
                
            case 6:
                
                if homeAPI?.addresses?.work?.address == nil || /homeAPI?.addresses?.work?.address?.isEmpty {
                    
                    GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
                        
                        debugPrint("Work")
                        self?.apiAddAddress(address: place?.formattedAddress, name: place?.name, latitude: place?.coordinate.latitude, longitude: place?.coordinate.longitude, category: "WORK")
                    }
                } else {
                    viewChooseAddress.showView(address: homeAPI?.addresses?.work,count:self.serviceRequest.stops.count)
                }
                
            case 7:
                
                GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
                    
                    debugPrint("Home")
                    self?.apiEditAddress(address: place?.formattedAddress, name: place?.name, latitude: place?.coordinate.latitude, longitude: place?.coordinate.longitude, category: "HOME")
                }
                
            case 8:
                
                GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
                    
                    debugPrint("WORK")
                    self?.apiEditAddress(address: place?.formattedAddress, name: place?.name, latitude: place?.coordinate.latitude, longitude: place?.coordinate.longitude, category: "WORK")
                }
                
            default:
                break
            }
            }
        } else {
            
            /* viewLocationTableContainer.alpha = 0 // Ankush
             btnCurrentLocation.isHidden = false
             imgViewPickingLocation.isHidden = false
             viewStoppageDesc.isHidden = true
             */
            
            // pickup- 1, Stop1- 2, Stop2- 3, drop- 4, 5- add Home, 6- Add Work, 7- Edit Home, 8- Edit Work
            
            showAddLocationFromMapController(buttonTag: buttonTag,isFromStop: isFromStop)
            
            /*  switch buttonTag {
             
             case 1:
             locationAddEdit = .PickUp
             
             case 2:
             locationAddEdit = .Stop1
             
             case 3:
             locationAddEdit = .Stop2
             
             case 4:
             locationAddEdit = .DropOff
             
             case 5, 7:
             locationAddEdit = .Home
             
             case 6,8:
             locationAddEdit = .Work
             
             default:
             break
             } */
            
            
            // viewTitlePackageBooking.isHidden = true
            // self?.txtDropOffLocation.becomeFirstResponder()
            
            // self?.segmentedMapType.isHidden = true
            // self?.constraintNavigationBasicBar.constant = BookingPopUpFrames.navigationBarHeight + BookingPopUpFrames.navigationTopPadding
            
            // btnOnlyBack.alpha = 0
            //  mapView.isUserInteractionEnabled = true
            
            /*  guard let selectedService  = self?.serviceRequest.serviceSelected else{return}
             
             if  /selectedService.serviceCategoryId > 3 {
             self?.locationType = .PickUpDropOff
             self?.imgViewPickingLocation.image = #imageLiteral(resourceName: "PickUpMarker")
             
             // Ankush self?.constraintYAddLocationView.constant = -(BookingPopUpFrames.navigationBarHeight + CGFloat(115) )
             
             // 96 - Pick up and dropoff View size
             let locationViewHeight = (!(/self?.viewStop1.isHidden) && !(/self?.viewStop2.isHidden)) ? (96 + 96) : (!(/self?.viewStop1.isHidden) || !(/self?.viewStop2.isHidden)) ? (96 + 48) : 96
             self?.constraintYAddLocationView.constant = -(BookingPopUpFrames.navigationBarHeight + CGFloat(locationViewHeight ))
             self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingXPickUpLocation
             self?.viewStoppageDesc.isHidden = ((/self?.viewStop1.isHidden) && (/self?.viewStop2.isHidden))
             } else{
             
             self?.constraintYAddLocationView.constant = -(BookingPopUpFrames.navigationBarHeight + CGFloat(55)) // 55 - Height for navigation View (Book a taxi)
             self?.locationType = .OnlyDropOff
             self?.imgViewPickingLocation.image = #imageLiteral(resourceName: "DropMarker")
             self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingXDropOffLocation
             } */
            
        }
    }
    
    
    func didSelectAddTip(order : OrderCab) {
        
        guard let vc = R.storyboard.bookService.tipScreen() else{return}
        vc.amountAdded = { amount in
            
            guard amount > 0 else { return }
            
            var order = self.viewInvoice.orderDone
            order?.payment!.tipCharge = String(describing: amount)
            
            self.viewInvoice.orderDone = order
            
        }
        
        vc.orderId = order.orderId
        vc.modalPresentationStyle = .overCurrentContext
        self.presentVC(vc)
    }
    
    
    func didBackFromSlot(type: MoveType) {
        screenType = ScreenType(mapMode: .SelectingFreightProduct, entryType: .Backward)
    }
    func didSelectScription() {
        guard let vc = R.storyboard.sideMenu.cardListViewController() else {return}
        vc.delegate = self
        vc.isFromSideMenu = true
        self.pushVC(vc)
    }
    
}

//MARK:- TextField Delegates
//MARK:-

extension HomeVC :UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtDropOffLocation {
            
            locationEdit = .DropOff
            
            if let latitude =   serviceRequest.latitude , let longitude = serviceRequest.longitude  , let previousSelected = serviceRequest.locationName {
                
                txtDropOffLocation.text = previousSelected
                // textField.text = previousSelected
                self.setCameraGoogleMap(latitude: latitude, longitude: longitude)
            }
            
        } else if textField == txtPickUpLocation {
            
            locationEdit = .PickUp
            if let latitude =   serviceRequest.latitudeDest , let longitude = serviceRequest.longitudeDest  , let previousSelected = serviceRequest.locationNameDest {
                
                txtPickUpLocation.text = previousSelected
                // textField.text = previousSelected
                self.setCameraGoogleMap(latitude: latitude, longitude: longitude)
                
            }
        }
    }
}

extension HomeVC{
    
    
    func getAllzones(){
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let getZone = BookServiceEndPoint.getAllZones
        getZone.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) { (response) in
            
            switch response {
            case .success(let data):
                
                if let zoneArray = data as? [ZoneModel]{
                    
                    self.zoneArray = zoneArray
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
            }
        }
        
    }
}

//MARK:- Confirm

extension HomeVC: ConfirmPickUpViewControllerDelegate {
    
    func confirmedPickupandSubmitedOrder(request: ServiceRequest?) {
        
        serviceRequest = request ?? ServiceRequest()
        
        // to clear path
        self.cleanPath()
        polyPath = GMSMutablePath()
        polyline?.map = nil
        
        self.i = 0
        self.animationPath = GMSMutablePath()
        self.animationPolyline.map = nil
        
        dropPickUpAndDropOffPins(pickLat: /serviceRequest.latitudeDest, pickLong: /serviceRequest.longitudeDest, dropLat: /serviceRequest.latitude, dropLong: /serviceRequest.longitude, isConfirmedPickUp: true)
        //        callLocationSelected()
        
        didSelectNext(type: .SubmitOrder)
    }
}

//MARK:- AddLocationFromMapViewControllerDelegate
extension HomeVC : AddLocationFromMapViewControllerDelegate {
    func getDataFromAnotherVC(address: String?, name: String?, latitude: Double?, longitude: Double?, getCountry:String,getLocality:String,getSublocality:String) {
        
    }
    
    func getDataFromAnotherVC1(address: String?, name: String?, latitude: Double?, longitude: Double?, getCountry: String, getLocality: String, getSublocality: String,strComingFrom:String?) {
    }
    
    func locationSelectedFromMap(address: String?, name: String?, latitude: Double?, longitude: Double?, locationType: Int,isFromStop:Bool) {
        
        // pickup- 1, Stop1- 2, Stop2- 3, drop- 4, 5- add Home, 6- Add Work, 7- Edit Home, 8- Edit Work
       
        
        if isFromStop{
            
            
            let modal = Stops(latitude: /latitude, longitude: /longitude, priority: 2, address: /address)
            serviceRequest.stops[locationType] = modal
            stopsTableView.reloadData()
            
            
            let numberOfStops =  /Int(/UDSingleton.shared.appSettings?.appSettings?.max_number_of_stops)
            self.buttonAddStop.isHidden = serviceRequest.stops.count >= numberOfStops
            
           // buttonAddStop.isHidden = false
            
            
        }else{
        switch locationType {
        
        case 1:
            txtPickUpLocation.text = /address
           
            serviceRequest.locationNameDest = /address
            serviceRequest.latitudeDest =  /latitude
            serviceRequest.longitudeDest =  /longitude
            
        // callLocationSelected()
        
//        case 2:
//
//
//
//            if let object = serviceRequest.stops.filter({$0.priority == 1}).first {
//                serviceRequest.stops.remove(object: object)
//            }
//
//            txtStop1.text = /address
//
//            let modal = Stops(latitude: /latitude, longitude: /longitude, priority: 1, address: /address)
//            serviceRequest.stops.append(modal)
//
//
//            debugPrint(serviceRequest.stops as Any)
//
//        case 3:
//
//
//            if let object = serviceRequest.stops.filter({$0.priority == 2}).first {
//                serviceRequest.stops.remove(object: object)
//            }
//
//            txtStop2.text = /address
//            let modal = Stops(latitude: /latitude, longitude: /longitude, priority: 2, address: /address)
//            serviceRequest.stops.append(modal)
//
//            debugPrint(serviceRequest.stops as Any)
//
//
        case 4:
            
            txtDropOffLocation.text = /address
            serviceRequest.locationNickName = /name
            serviceRequest.locationName = address
            serviceRequest.latitude =  /latitude
            serviceRequest.longitude =  /longitude
            
            callLocationSelected()
            
        case 5:
            apiAddAddress(address: address, name: name,  latitude: latitude, longitude: longitude, category: "HOME")
            
        case 6:
            apiAddAddress(address: address, name: name, latitude: latitude, longitude: longitude, category: "WORK")
            
        case 7:
            apiEditAddress(address: address, name: name, latitude: latitude, longitude: longitude, category: "HOME")
            
        case 8:
            apiEditAddress(address: address, name: name, latitude: latitude, longitude: longitude, category: "WORK")
            
        default:
            break
            
            
        }
        }
    }
}


//MARK:- RazorPay
extension HomeVC {
    
    internal func showPaymentForm(order: OrderCab?) {
        
        let options: [String:Any] = [
            "amount": /order?.finalCharge, //This is in currency subunits. 100 = 100 paise= INR 1.
            "currency": /UDSingleton.shared.appSettings?.appSettings?.currency,//We support more that 92 international currencies.
            "description": "Your Ride Payment charges",
            "image": "https://url-to-image.png",
            "name": "Royo",
            "prefill": [
                "contact": /UDSingleton.shared.userData?.userDetails?.user?.phoneNumber,
                "email": /UDSingleton.shared.userData?.userDetails?.user?.email
            ],
            "theme": [
                "color": /UDSingleton.shared.appSettings?.appSettings?.app_color_code
            ]
        ]
        
        ez.runThisInMainThread {
            self.razorpay?.open(options)
        }
        
    }
}

extension HomeVC: RazorpayPaymentCompletionProtocol {
    
    func onPaymentError(_ code: Int32, description str: String) {
        let alertController = UIAlertController(title: "FAILURE", message: str, preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(cancelAction)
        //  self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        let alertController = UIAlertController(title: "SUCCESS", message: "Payment Id \(payment_id)", preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(cancelAction)
        // self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        razorPaymentAPI(order_id: String(/currentOrder?.orderId), payment_id: payment_id)
    }
    
    
    func razorPaymentAPI(order_id: String, payment_id: String) {
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let walletBalance = BookServiceEndPoint.razorPayReturnUrl(order_id: order_id, payment_id: payment_id)
        walletBalance.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) { (response) in
            
            switch response {
            case .success(let data):
                break
                
            case .failure(let strError):
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
            }
        }
    }
}


extension HomeVC{
    
    //MARK: - Paypal Payment
    func payViaPayPal() {
        
        BrainTreeManagerCab.sharedInstance.payViaPayPal( success: { [unowned self] (nonce) in
            debugPrint(nonce)
            print(nonce.description)
            //   self.returnPaymentDetail?(nonce,"Paypal","")
            
            self.makePaypalCheckout(nonce: nonce)
        },cancelled: {
            self.showNewInvoiceView()
        }) { [weak self] (error) in
            print(error?.localizedDescription)
            // SKToast.makeToast(error?.localizedDescription)
            // self?.handleError(error)
            
            self?.showNewInvoiceView()
        }
    }
    
    
    func makePaypalCheckout(nonce:String){
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let brainTreeToken = BookServiceEndPoint.braintreeCheckout(amount: /currentOrder?.payment?.finalCharge, nonce: nonce, orderId: /currentOrder?.orderId)
        
        
        brainTreeToken.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) { (response) in
            
            switch response {
            
            case .success(let data):
                
                self.showNewInvoiceView()
                
                
                
                
            case .failure(let strError):
                self.showNewInvoiceView()
                
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
            }
        }
    }
    
    
    
    func showPayTab(){
        let amount = /Float(/currentOrder?.payment?.finalCharge)
        let bundle = Bundle(url: Bundle.main.url(forResource: "Resources", withExtension: "bundle")!)
        
        let cust_email = UDSingleton.shared.userData?.userDetails?.user?.email ?? "customer@yopmail.com"
        let cust_phone = (UDSingleton.shared.userData?.userDetails?.user?.countryCode ?? "") + "\(UDSingleton.shared.userData?.userDetails?.user?.phoneNumber ?? 12345678)"
        
        let email = UDSingleton.shared.appSettings?.appSettings?.merchantEmail ?? ""
        let secret = UDSingleton.shared.appSettings?.appSettings?.merchantSecretKey ?? ""
        let color = UIColor.init(hexString: UDSingleton.shared.appSettings?.appSettings?.secondary_colour ?? "")
        
        var initialSetupViewController: PTFWInitialSetupViewController!
        initialSetupViewController = PTFWInitialSetupViewController.init(
            bundle: bundle,
            andWithViewFrame: self.view.frame,
            andWithAmount: amount,
            andWithCustomerTitle: "Pikkup",
            andWithCurrencyCode: "AED",
            andWithTaxAmount: 0.0,
            andWithSDKLanguage: "en",
            andWithShippingAddress: "Manama",
            andWithShippingCity: "Manama",
            andWithShippingCountry: "USA",
            andWithShippingState: "Manama",
            andWithShippingZIPCode: "123456",
            andWithBillingAddress: "Manama",
            andWithBillingCity: "Manama",
            andWithBillingCountry: "USA",
            andWithBillingState: "Manama",
            andWithBillingZIPCode: "12345",
            andWithOrderID: "\(currentOrder?.orderId ?? 12345)" ,
            andWithPhoneNumber: cust_phone,
            andWithCustomerEmail: cust_email,
            andIsTokenization:true,
            andIsPreAuth: false,
            andWithMerchantEmail: email,
            andWithMerchantSecretKey: secret,
            andWithAssigneeCode: "SDK",
            andWithThemeColor:color,
            andIsThemeColorLight: false)
        
        
        initialSetupViewController.didReceiveBackButtonCallback = {
            self.showNewInvoiceView()
        }
        
        initialSetupViewController.didStartPreparePaymentPage = {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        initialSetupViewController.didFinishPreparePaymentPage = {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        
        initialSetupViewController.didReceiveFinishTransactionCallback = { [weak self](responseCode, result, transactionID, tokenizedCustomerEmail, tokenizedCustomerPassword, token, transactionState) in
            print("Response Code: \(responseCode)")
            print("Response Result: \(result)")
            
            // In Case you are using tokenization
            print("Tokenization Cutomer Email: \(tokenizedCustomerEmail)");
            print("Tokenization Customer Password: \(tokenizedCustomerPassword)");
            print("TOkenization Token: \(token)");
            if responseCode == 100 {
                self?.paymentWithPayTab(payment_id: String(transactionID))
            }
            
        }
        
        //        self.view.addSubview(initialSetupViewController.view)
        //        self.addChild(initialSetupViewController)
        //        initialSetupViewController.didMove(toParent: self)
        
        
    }
    
    
    func paymentWithPayTab(payment_id:String?){
        guard let order_id = currentOrder?.orderId, let payment_id = payment_id else{
            return
        }
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let walletBalance = BookServiceEndPoint.payTabReturnUrl(order_id: "\(order_id)", payment_id: payment_id)
        walletBalance.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) { (response) in
            
            switch response {
            case .success(let data):
                self.showNewInvoiceView()
            case .failure(let strError):
                Alerts.shared.show(alert: "Pikkup", message: /strError , type: .error )
                
            }
        }
    }
    
}
//MARK: - PayMaya
extension HomeVC {
    
    func getPaymayaUrl(){
        let amount = /Float(/currentOrder?.payment?.finalCharge)
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let objectRequest = BookServiceEndPoint.getPaymayaUrl(amount: "\(/amount)", currency: /UDSingleton.shared.appSettings?.appSettings?.currency , successUrl: "https://billing.royoapps.com/payment-success", failureUrl: "https://billing.royoapps.com/payment-error")
        
        objectRequest.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey])  { [weak self] (response) in
            switch response {
            case .success(let object):
                guard let self = self else { return }
                APIManagerCab.shared.hideLoader()
                
            // guard let data = object as? PaymayaModal else {return}
            
            //                if let paymentUrl = data.redirectUrl, !paymentUrl.isEmpty {
            //
            //                    let vc = PaymentWebviewVC.getVC(.payment)
            //                    vc.urlStr = paymentUrl
            //                    vc.gatewayUniqueId = /self.selectedPaymentMethod?.gatewayUniqueId
            //
            //                    vc.paymentSucccessBlock = { ref_id in
            //
            //                        vc.popVC()
            //                        self.selectedPaymentMethod?.token = ref_id
            //
            //
            //                        self.addMoneyApi()
            //                    }
            //                    self.pushVC(vc)
            // }
            default:
                APIManagerCab.shared.hideLoader()
                break
                
            }
        }
        
    }
}
//MARK::- PICKER
extension HomeVC : EPPickerDelegate {
    
    //MARK: EPContactsPicker
    func epContactPicker(_: EPContactsPicker, didContactFetchFailed error : NSError){
        debugPrint("Failed with error \(error.description)")
    }
    
    func epContactPicker(_: EPContactsPicker, didSelectContact contact : EPContact) {
        
        //if contact.phoneNumbers.count > 1 {
        
        var array = [[String:Any]]()
        
        let codes = contact.phoneNumbers.first?.phoneNumber.getISOAndCountryCode()
        let iso = (/codes?.0).isEmpty ?  DefaultCountry.ISO.rawValue : codes?.0
        var countryCode =  ((/codes?.1).isEmpty ?  DefaultCountry.countryCode.rawValue : /codes?.1)
        
        
        
        let pureNumber = contact.phoneNumbers.first?.phoneNumber.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        let numberWithoutContryCode = pureNumber?.components(separatedBy: /countryCode).last
        
        //let modal = ContactNumberModal(contactNumber: numberWithoutContryCode, name: contact.displayName(), ISO: iso, countryCode: countryCode)
        
        // let dict = ["phone_number":numberWithoutContryCode,"phone_code":countryCode]
        
        // array.append(dict)
        // print("array: \(array)")
        
        let name = "\(contact.firstName) \(contact.lastName)"
        bottomViewBookforFriend.textfieldPhoneNumber.text = numberWithoutContryCode
        bottomViewBookforFriend.textfieldFullName.text = name
        // }
        
    }
    
    func epContactPicker(_: EPContactsPicker, didCancel error : NSError){
        debugPrint("User canceled the selection");
    }
    
    
}


extension HomeVC: AVAudioRecorderDelegate{
    func startRecording(orderID:Int) {
        vwRecording.isHidden = true
        let audioFilename = getDocumentsDirectory().appendingPathComponent("\(Date().timeIntervalSince1970)_recording.m4a")
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder?.delegate = self
            audioRecorder?.record()
            
            
        } catch {
            finishRecording(success: false, orderID: orderID)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func finishRecording(success: Bool, orderID:Int) {
        
        if audioRecorder != nil {
            audioRecorder?.stop()
            
            vwRecording.isHidden = true
            //uploadMedia(orderID: orderID)
            
        }
    }
    
    func finishRecording1(success: Bool) {
        if audioRecorder != nil {
            audioRecorder?.stop()
            //audioRecorder = nil
            vwRecording.isHidden = true
        }
        
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        //if !flag {
        finishRecording1(success: false)
        //}
    }
    
    
    
}


extension HomeVC:ProductDetailDelegate{
    func addAddressesWithDetails(items:[AddressItem]) {
        serviceRequest.addressItems = items
        guard let vc = R.storyboard.orderDetails.showOrderDetailsViewController() else{return}
        vc.delegates = self
        vc.serviceRequest = serviceRequest
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .coverVertical
        presentVC(vc, true)
    }

    func editAddressWithDetails(request:ServiceRequest?) {

    }

    func back() {

        screenType = ScreenType(mapMode: .SelectingFreightProduct, entryType: .Backward)
    }


}


extension GMSMutablePath

{
    func appendPath(path:GMSPath?)
    {
        if let path = path
            {
            for i in 0..<path.count()
            {
                self.add(path.coordinate(at: i))
            }
        }
    }
}
