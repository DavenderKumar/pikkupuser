//
//  FareBreakdownViewController.swift
//  Trava
//
//  Created by Apple on 15/11/19.
//  Copyright © 2019 CodeBrewLabs. All rights reserved.
//

import UIKit

class FareBreakdownViewController: UIViewController {

    //MARK:- Outlet
    
    @IBOutlet weak var lblTitle1: UILabel!
    
    @IBOutlet weak var lblTitle2: UILabel!
    
    @IBOutlet weak var labelValueBaseFare: UILabel!
    @IBOutlet weak var labelValueMinimumFare: UILabel!
    @IBOutlet weak var labelValuePerMinute: UILabel!
    @IBOutlet weak var labelValuePerKm: UILabel!
    
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var vwStatusBar: UIView!
    
    // MARK:- Properties
    var product: ProductCab?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
    }
}

// MARK: - Actions
extension FareBreakdownViewController {
    
    func initialSetup() {
        vwHeader.setViewBackgroundColorHeader()
        vwStatusBar.setViewBackgroundColorHeader()
        
        lblTitle2.text =  product?.description ?? "Cancellation charges and waiting charges may apply. Please refer to our Terms & Conditions."
        
        labelValueBaseFare.text = (/UDSingleton.shared.appSettings?.appSettings?.currency) + " " + String(/product?.alphaPrice).getThreeDecimalFloat()
        labelValueMinimumFare.text = (/UDSingleton.shared.appSettings?.appSettings?.currency) + " " + String(/product?.actualPrice).getThreeDecimalFloat()
        labelValuePerMinute.text = (/UDSingleton.shared.appSettings?.appSettings?.currency) + " " + String(/product?.price_per_hr).getThreeDecimalFloat()
        labelValuePerKm.text = (/UDSingleton.shared.appSettings?.appSettings?.currency) + " " + String(/product?.pricePerDistance).getThreeDecimalFloat()
    }
    
}

// MARK: - Actions
extension FareBreakdownViewController {
    
    @IBAction func actionButtonCancel(_ sender: Any) {
        
        dismissVC(completion: nil)
    }
    
}
