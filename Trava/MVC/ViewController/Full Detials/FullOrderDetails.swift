//
//  FullOrderDetails.swift
//  RoyoRide
//
//  Created by Rohit Prajapati on 01/06/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//


import UIKit
import SDWebImage

class FullOrderDetails: UIViewController {
   
    //MARK:- OUTLETS
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var checkListTable: UITableView!
    @IBOutlet weak var lblPayment: UILabel!
    @IBOutlet weak var lblPaymentType: UILabel!
    @IBOutlet weak var lblMaterialType: UILabel!
    @IBOutlet weak var lblMaterialQuantity: UILabel!
    @IBOutlet weak var lblAdditionalInfo: UILabel!
    @IBOutlet weak var lblPersonToDeliver: UILabel!
    @IBOutlet weak var lblPersonDeliverPhoneNumber: UILabel!
    @IBOutlet weak var lvlInvoiceNumber: UILabel!
    @IBOutlet weak var lblDeliveryPerson: UILabel!
    @IBOutlet weak var orderStack: UIStackView!
    @IBOutlet weak var checkListStack: UIStackView!
    @IBOutlet weak var btnDone2: UIButton!
    @IBOutlet weak var viewTollParkingContainer: UIView!
    @IBOutlet weak var tblViewTollParking: UITableView!
    @IBOutlet weak var bookingDetailStackView: UIStackView!
    @IBOutlet weak var cvPhotos: UICollectionView!

    
    //MARK: PROPERTIES
    var orderCurrent : OrderCab?
    var orderCheckList = [CheckLists]()
    var checkListFinalArray = [CheckListModel]()
    var tollParkingImages = [[String]]()
    
    //MARK:- Properties
    var collectionViewDataSource : CollectionViewDataSourceCab?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      

        lvlInvoiceNumber.isHidden = true
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(updatedCheckList), name: NSNotification.Name(rawValue: LocalNotifications.SerCheckList.rawValue), object: nil)
        
        setUPUI()
    }
    
    func deleteCheckListItem(checkList: CheckListModel?, indexValue: Int) {
        
        let checkListID = [/checkList?.check_list_id].toJson()
        
        
        let req = BookServiceEndPoint.removeCheckListItem(checkList: checkListID)
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        req.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) {(response) in
            switch response {
            case .success(_):
                self.checkListFinalArray.remove(at: indexValue)
                self.checkListTable.reloadData()
                
                 
                if self.checkListFinalArray.count == 0 {
                    self.lblTotal.text = "Total Amount \(0) \(/UDSingleton.shared.appSettings?.appSettings?.currency)"
                }
                
                 var total = 0
                if var userData = UDSingleton.shared.userData?.order?.first?.check_lists {
                    for (index,element) in userData.enumerated() {
                        if element.check_list_id == /checkList?.check_list_id {
                            userData.remove(at: index)
                            UDSingleton.shared.userData?.order?.first?.check_lists = userData
                        } else {
                            total = total + (Int(/(element.after_item_price)) ?? 0) + /(element.tax)
                            self.lblTotal.text = "Total Amount \(total) \(/UDSingleton.shared.appSettings?.appSettings?.currency)"
                        }
                    }
                }
                 
                
                break
            
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }

        
    }
    
    @objc func updatedCheckList(notification: NSNotification) {
        
        checkListFinalArray.removeAll()
        var total = 0
        let data = notification.userInfo as? [String: Any]
        let order = data?["order"] as? [String: Any]
        let checkList = order?["check_lists"] as? [[String: Any]]
        
       if let checkListArray = checkList {
           for element in checkListArray {
               let checklist = CheckListModel(afteritemprice: element["after_item_price"] as? String,
                                              beforeitemprice: element["before_item_price"] as? String,
                                              checklistid: element["check_list_id"] as? Int,
                                              createdat: element["created_at"] as? String,
                                              itemname: element["item_name"] as? String,
                                              orderid: element["order_id"] as? String,
                                              updatedat:  element["updated_at"] as? String,
                                              userdetailid: element["user_detail_id"] as? String,
                                              tax: element["tax"] as? Int,
                                              price: element["price"] as? String)
            
                    total = total + (Int(/(element["after_item_price"] as? String)) ?? 0) + Int(/(element["tax"] as? Int))
                   
               
                    checkListFinalArray.append(checklist)
                }
        
                checkListTable.reloadData()
            }
        
         checkListTable.reloadData()
        
         lblTotal.text = "Total Amount \(/UDSingleton.shared.appSettings?.appSettings?.currency).\(total)"
        
         Alerts.shared.show(alert: "Checklist", message: "Tax Price is updated by driver." , type: .error)
        
         alertConfimation(totalPrice: total)
        }
    
    func alertConfimation(totalPrice: Int) {
        alertBox(message: "Total price has been updated.", title: "Price including Tax \(/UDSingleton.shared.appSettings?.appSettings?.currency). \(totalPrice)") {
            //Pay.....
        }
    }
    
    func setupTollParkingData(){

        tollParkingImages.removeAll()
        var tollImagesArray = [String]()
        var parkingImagesArray = [String]()
        if let tollImages = orderCurrent?.payment?.toll_images{
            tollImagesArray = tollImages
        }
        if let parkingImages = orderCurrent?.payment?.parking_images{
            parkingImagesArray = parkingImages
        }
        tollParkingImages.append(tollImagesArray)
        tollParkingImages.append(parkingImagesArray)
        tblViewTollParking.delegate = self
        tblViewTollParking.dataSource = self
        tblViewTollParking.reloadData()
    }

    
    
        func setUPUI() {
            
            
            let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
                
                switch template {
                    
            
                    
                case .Corsa:
                    
                    bookingDetailStackView.isHidden = true
                    tblViewTollParking.isHidden = false
                    getLocalDriverForParticularService()
                    
                    
                    return
                    
                default:
                    bookingDetailStackView.isHidden = false
                    tblViewTollParking.isHidden = true
                    
                    
                }
            
        
            var total = 0
            orderStack.isHidden = false
                       checkListStack.isHidden = true
            
            for element in  orderCheckList  {
                 let checklist = CheckListModel(afteritemprice: element.after_item_price,
                                                beforeitemprice: element.before_item_price,
                                                checklistid: element.check_list_id,
                                                createdat: element.created_at,
                                                itemname: element.item_name,
                                                orderid: element.order_id,
                                                updatedat:  element.updated_at,
                                                userdetailid: element.user_detail_id,
                                                tax: element.tax,
                                                price: element.price)
                
                total = total + (Int(/element.after_item_price) ?? 0)
                total = total + Int(/element.tax)
                 
                checkListFinalArray.append(checklist)
             }
            
            
            if checkListFinalArray.count == 0 {
                segmentController.isHidden = true
            } else {
                segmentController.isHidden = false
            }
            
            checkListTable.reloadData()
            
            configureCollectionView()
                        
            lblTotal.text = "Total Amount \(/UDSingleton.shared.appSettings?.appSettings?.currency) \(total)"
            
            //----------------
            lblPayment.text = /UDSingleton.shared.appSettings?.appSettings?.currency + " " + /orderCurrent?.payment?.finalCharge?.getTwoDecimalFloat() 
            lblPaymentType.text = /orderCurrent?.payment?.paymentType
            lblMaterialType.text = /orderCurrent?.material_details
            lblMaterialQuantity.text = /orderCurrent?.payment?.productWeight
            lblAdditionalInfo.text = orderCurrent?.details
            lblPersonToDeliver.text = /orderCurrent?.pickup_person_name
            lblPersonDeliverPhoneNumber.text = /orderCurrent?.pickup_person_phone
            btnDone2.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
            
            
        
    }
    
    
    func getLocalDriverForParticularService() {
        
   
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let homeAPI =  BookServiceEndPoint.homeApi(categoryID: orderCurrent?.serviceId)
        homeAPI.request(isLoaderNeeded: false, header: ["language_id" : LanguageFile.shared.getLanguage(), "access_token" :  token]) {
            [weak self] (response) in
            
            switch response {
                
            case .success(let data):
                
                guard let model = data as? HomeCab else { return }
                
                if let currentOrder = model.orders.first(where: {$0.orderId == self?.orderCurrent?.orderId}){
                    
                    self?.orderCurrent = currentOrder
                }
                
                self?.setupTollParkingData()
                
             
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
     
    
    @IBAction func segmentOrderValue(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            orderStack.isHidden = false
            checkListStack.isHidden = true
            
            break
        case 1:
            orderStack.isHidden = true
            checkListStack.isHidden = false
            
            break
        default:
            print("default")
        }
    }
    
    @IBAction func btnDeleteItems(_ sender: UIButton) {
        
        if UIApplication.topViewController()!.isKind(of: UIAlertController.self) {
            
        } else {
            alertBoxOption(message: "Are you sure you want to remove this item form your checklist.", title: "Remove Item", leftAction: "Cancel", rightAction: "Pay", ok: {
                    self.deleteCheckListItem(checkList: self.checkListFinalArray[sender.tag], indexValue: sender.tag)
                              
                }) {
                  print("Cencel")
             }
        }
       
    }
    
    func convertIntoJSONString(arrayObject: [Any]) -> String? {

        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
            if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                return jsonString as String
            }
            
        } catch let error as NSError {
            print("Array convertIntoJSON - \(error.description)")
        }
        return nil
    }
    
    
    @IBAction func btnDone(_ sender: Any) {
        
        var arrayElement = [[String: String]]()
        arrayElement.removeAll()
        for (index,element) in checkListFinalArray.enumerated() {
            var tempObject = [String: String]()
            tempObject["after_item_price"] = element.after_item_price
            tempObject["before_item_price"] = element.before_item_price
            tempObject["check_list_id"] = String(/element.check_list_id)
            tempObject["created_at"] = element.created_at
            tempObject["item_name"] = element.item_name
            tempObject["order_id"] = element.order_id
            tempObject["updated_at"] = element.updated_at
            tempObject["tax"] = String(/element.tax)
            tempObject["price"] = element.price
            tempObject["user_detail_id"] = element.user_detail_id
            
            UDSingleton.shared.userData?.order?.first?.check_lists?[index].after_item_price = element.after_item_price
            arrayElement.append(tempObject)
            
        }
        
        
        let checkListArrayJson = arrayElement.toJson()
        
        let req = BookServiceEndPoint.editCheckList(checkList: checkListArrayJson)
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        req.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) {(response) in
            switch response {
            case .success(_):
                 Alerts.shared.show(alert: "AppName".localizedString, message: "Checklist Updated" , type: .success )
                
            
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FullOrderDetails: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
           
           if tableView == tblViewTollParking{
               
               return tollParkingImages.count
           }
            return 1
           
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblViewTollParking{
            
            return tollParkingImages[section].count
        }
        return checkListFinalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblViewTollParking{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TollParkingTblCell", for: indexPath) as! TollParkingTblCell
            let imagePath = APIBasePath.imageBasePathWithoutVersion + tollParkingImages[indexPath.section][indexPath.row]
            
            if let imageUrl = URL(string:imagePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!){
                cell.imgView.kf.setImage(with: imageUrl)
                
            }
            
            return cell
            
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckListOrderCell", for: indexPath) as! CheckListOrderCell
        cell.lblItemName.text = checkListFinalArray[indexPath.row].item_name
        cell.lblPriceSymbol.text = /UDSingleton.shared.appSettings?.appSettings?.currency
        cell.txfItemPrice.text = "\(/checkListFinalArray[indexPath.row].after_item_price)"
        cell.lblTaxPrice.text = "*Tax price \(/checkListFinalArray[indexPath.row].tax) \(/UDSingleton.shared.appSettings?.appSettings?.currency)"
        cell.txfItemPrice.delegate = self
        cell.btnDelete.tag = indexPath.row
        return cell
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
             if tableView == tblViewTollParking{
             
                return 150
            }
        return UITableView.automaticDimension
        }
        
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            
             if tableView == tblViewTollParking{
                
                return 50.0
            }
            return 0.0
            
        }
        
        
        func tableView(_ tableView: UITableView, viewForHeaderInSectionCab section: Int) -> UIView? {
            
            if tableView == tblViewTollParking{
                
                
                let headerView = UIView(frame:CGRect(x: 0, y: 0, width: self.tblViewTollParking.frame.width, height: 50))
                headerView.backgroundColor = UIColor.white
                let lblType = UILabel(frame: CGRect(x: 15, y: 0, width: 150, height: 50))
                
                
              
                
                let lblPrice = UILabel(frame: CGRect(x: (tableView.frame.width-150), y: 0, width:150, height: 50))
                
                if section == 0{
                    
                    lblType.text = "Toll Charges".localizedString
                    lblPrice.text = "\(/orderCurrent?.payment?.toll_charges) \(/UDSingleton.shared.appSettings?.appSettings?.currency)"
                    
                } else{
                    
                    lblType.text = "Parking Charges".localizedString
                    lblPrice.text = "\(/orderCurrent?.payment?.parking_charges) \(/UDSingleton.shared.appSettings?.appSettings?.currency)"
                }
                lblType.textAlignment = .left
                lblPrice.textAlignment = .right
                lblPrice.textColor = UIColor.black
                lblType.textColor = UIColor.black
             
                headerView.addSubview(lblType)
                headerView.addSubview(lblPrice)
                
                return headerView
            }
            return nil
        }
    
}

extension FullOrderDetails : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        let textFieldIndex = textField.tag
        
        if newString == "" {
            checkListFinalArray[textFieldIndex].after_item_price = "0"
        } else {
            checkListFinalArray[textFieldIndex].after_item_price = newString
        }
        
        
        var total = 0
        for checkListItem in checkListFinalArray {
            total = total + (Int(/checkListItem.after_item_price) ?? 0) + Int(/checkListItem.tax)
        }
                   
        lblTotal.text = "Total Amount \(/UDSingleton.shared.appSettings?.appSettings?.currency) \(total)"
        //checkListTable.reloadData()

        return true
    }
}


extension FullOrderDetails {
    
    private  func configureCollectionView() {
        
        let configureCellBlock : ListCellConfigureBlockCab = {(cell, item, indexPath) in
            if let cell = cell as? OrderImageCellCab , let model = item as? imagesArray {
                
                guard let url = URL(string: /model.image_url) else { return }
                
                cell.imgViewOrder.sd_setImage(with: url, completed: nil)
            }
        }
        
        let didSelectBlock : DidSelectedRowCab = { [weak self] (indexPath, cell, item) in
             
        }
                
        collectionViewDataSource =  CollectionViewDataSourceCab(items: orderCurrent?.order_images_url , collectionView: cvPhotos, cellIdentifier: "photoCV", cellHeight: 120, cellWidth: 120 , configureCellBlock: configureCellBlock )
        collectionViewDataSource?.aRowSelectedListener = didSelectBlock
        cvPhotos.delegate = collectionViewDataSource
        cvPhotos.dataSource = collectionViewDataSource
        cvPhotos.reloadData()
        
    }

}


class OrderImageCellCab:UICollectionViewCell{
    @IBOutlet weak var imgViewOrder: UIImageView!
    
}





