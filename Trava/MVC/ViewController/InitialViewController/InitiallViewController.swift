//
//  InitiallViewController.swift
//  RoyoRide
//
//  Created by Ankush on 08/05/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GoogleSignIn
import AVFoundation
import AVKit

class InitiallViewController: UIViewController {
    //MARK::- IBOutlets
    @IBOutlet weak var imgVwSmall: UIImageView!
    @IBOutlet weak var imgVwFullView: UIImageView!
    @IBOutlet weak var vwVideo: UIView!
    
    @IBOutlet weak var btnSkip: UIButton!
    //MARK::- Variables
    var player = AVPlayer()
   
    
    //MARK::- ViewController lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        getAppSettings()
        
        //guard let vc = R.storyboard.mainCab.landingAndPhoneInputVC() else{ return }
        //self.pushVC(vc)
    }
    

    func playVideo() {
        
        let urlstring = /UDSingleton.shared.appSettings?.appSettings?.play_video_after_splash_images
        let urlString1 = urlstring.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

        guard let url = URL(string: /urlString1) else { return  }
        
         player = AVPlayer(url: url)
      
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        vwVideo.layer.addSublayer(playerLayer)
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        player.play()
    }

    @objc func playerDidFinishPlaying(){
   
        self.player.pause()
        moveToNextView()
 
   
    }
    
    //MARKK:- IBAction
    @IBAction func actionSkipBtn(_ sender: Any) {
        moveToNextView()
    }
}

//MARK:- API

extension InitiallViewController {
    
    func getAppSettings() {
        self.setSplash()
        let getSettings = LoginEndpoint.appSetting
        getSettings.request( isLoaderNeeded: false, header: ["language_id" : LanguageFile.shared.getLanguage()]) {[weak self] (response) in
            switch response {
                
            case .success(let data):
                guard let model = data as? AppSettingModel, let _ = model.appSettings else { return }
                
                UDSingleton.shared.appSettings = model
                
                if /UDSingleton.shared.appSettings?.appSettings?.is_play_video_after_splash == "true" && UDSingleton.shared.userData == nil{
                    self?.imgVwSmall.isHidden = true
                    self?.imgVwFullView.isHidden = true
                    self?.vwVideo.isHidden = false
                    self?.btnSkip.isHidden = false
                    self?.playVideo()
                } else{
                    self?.imgVwSmall.isHidden = false
                    self?.imgVwFullView.isHidden = true
//                    self?.vwVideo.isHidden = true
//                    self?.btnSkip.isHidden = true
                    self?.setSplash()
                    if APIBasePath.isPikkup {
                        let seconds = 5.0
                        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                            self?.moveToNextView()
                        }
                    }else{
                        self?.moveToNextView()
                    }
                    
                }
                debugPrint(data as Any)
                
                
                
                
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
                //Toast.show(text: strError, type: .error)
            }
        }
    }
    
    
    func setSplash(){
        vwVideo.isHidden = true
        imgVwSmall.isHidden = true
        imgVwFullView.isHidden = true
        switch APIBasePath.appScheme{
        case .shipusnow:
            imgVwSmall.isHidden = false
            imgVwSmall.image = UIImage(named: "shipusnow_splash")
        default:
            imgVwFullView.isHidden = false
            
        imgVwFullView.loadGif(name: "pik")
           // imgVwFullView.loadGif(name: "pik")
            //imgVwFullView.image = UIImage(named: "ic_Launch")
        }
    }
    
    
    func moveToNextView() {
        if /UDSingleton.shared.appSettings?.appSettings?.ios_google_api == "" {
            print("Google Place API")
            GMSServices.provideAPIKey( /UDSingleton.shared.appSettings?.appSettings?.google_place_api)
            GMSPlacesClient.provideAPIKey(/UDSingleton.shared.appSettings?.appSettings?.google_place_api)
        } else {
            GMSServices.provideAPIKey( /UDSingleton.shared.appSettings?.appSettings?.ios_google_api)
            GMSPlacesClient.provideAPIKey(/UDSingleton.shared.appSettings?.appSettings?.ios_google_api)
        }
        
        let userInfo = UDSingleton.shared
        
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
        switch template {
        case .Moby?:
            
            if (userInfo.userData != nil) {
                 ez.runThisInMainThread {
                    AppDelegate.shared().setHomeAsRootVC()
                 }
              }else  {
               
                ez.runThisInMainThread {
                    guard let vc = R.storyboard.newTemplateLoginSignUp.ntLoginSignupTypeViewController() else {return}
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
            
        case .DeliverSome:
            
            if (userInfo.userData != nil) {
                ez.runThisInMainThread {
                   AppDelegate.shared().setHomeAsRootVC()
                }
            }else  {
                
                ez.runThisInMainThread {
                   guard let loginTemplate1 = R.storyboard.template1_Design.landingAndPhoneInputVCTemplate1() else { return }
                    self.navigationController?.pushViewController(loginTemplate1, animated: false)
               }
            }
        
            
        default:
            if (userInfo.userData != nil) {
                 ez.runThisInMainThread {
                    if APIBasePath.isMedc2u {
                        let storyboard = UIStoryboard(name: "MainCab", bundle: nil)
                        let secondViewController = storyboard.instantiateViewController(withIdentifier: "BoydInitiallViewController") as! BoydInitiallViewController
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                    }else{
                        AppDelegate.shared().setHomeAsRootVC()
                    }
                 }
              }else  {
               
                if UDSingleton.shared.hasSocialLogin{
                    ez.runThisInMainThread {
                        guard let vc = R.storyboard.newTemplateLoginSignUp.ntLoginSignupTypeViewController() else {return}
                        vc.template = template ?? .Default
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }else{
                    ez.runThisInMainThread {
                        guard let vc = R.storyboard.mainCab.landingAndPhoneInputVC() else {return}
                        self.navigationController?.setViewControllers([vc], animated: true)
                    }
                }
            }
        }
    }
}


