//
//  TipScreen.swift
//  Trava
//
//  Created by CHANCHAL WARDE on 10/04/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit

class TipScreen: UIViewController {
    
    
    //MARK:- IBOutlet
    @IBOutlet var tfAmount: UITextField!
    
    
    //MARK:- Variable
    @IBOutlet weak var btnSubmit: UIButton!
    var orderId : Int?
    var amountAdded : (_ amount : Double) -> Void = { _ in }
   // private let tips = ["5","10","15"]
    private let tips = ["0.500","0.750","1.000"]
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSubmit.layer.cornerRadius = 6
        btnSubmit.clipsToBounds = true
        btnSubmit.setViewBackgroundColorSecondary()
        
    }
    @IBAction func tipAction(_ sender: UIButton) {
        
        Utility.shared.showDropDown(anchorView: sender, dataSource:  tips, width: sender.frame.width, handler: { [weak self] (index, strValu) in
            self?.tfAmount.text = strValu
            
        })
        
    }
    
    @IBAction func btnSubmit(_ sender: UIButton) {
        guard let amount = tfAmount.text?.toDouble() else { showError();  return }
        
        apiToTip(amount : amount)
    }
}

//MARK:- Textfield Delegate

extension TipScreen : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func showError() {
        Alerts.shared.show(alert: "", message: "Please enter amount", type: .info)
    }
    
    
    @IBAction func dismiss(_ sender  : UIButton) {
        self.dismissVC(completion: nil)
    }
    
    func apiToTip(amount : Double?) {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        BookServiceEndPoint.addTip(tip: amount, orderId: orderId, gateway_unique_id: /UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id).request(header:  ["language_id" : LanguageFile.shared.getLanguage(), "access_token" :  token], completion: {
            [weak self] (response) in
            
            switch response {
                
            case .success(let data):
                self?.amountAdded(amount ?? 0)
                self?.dismissVC(completion: nil)
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        })
    }
    
}

