//
//  BoydInitiallViewController.swift
//  RoyoRide
//
//  Created by Pushpinder Kaur on 21/11/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit

class BoydInitiallViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    //MAEK::- Button Action
    
    @IBAction func actionBtnBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func actionButtonRequestAMassage(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.setHomeAsRootVC()
        
    }
    
    @IBAction func actionButtonRequestMediclServ(_ sender: Any) {
        
        Alerts.shared.showOnTop(alert: "Reqest Medical Services",  message: "Coming soon..." , type: .success)
    }
    
    @IBAction func actionButtonRequestCovidTest(_ sender: Any) {
        Alerts.shared.showOnTop(alert: "Request COVID19 Test",  message: "Coming soon..." , type: .success)
    }
    
    @IBAction func actionButtonRequestCovidVaccine(_ sender: Any) {
        Alerts.shared.showOnTop(alert: "Request COVID19 Vaccine",  message: "Coming soon..." , type: .success)
    }
    
   
}
