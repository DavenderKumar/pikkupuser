//
//  playVideoViewController.swift
//  RoyoRide
//
//  Created by admin on 21/12/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//



import UIKit
import GoogleMaps
import GooglePlaces
import GoogleSignIn
import AVFoundation
import AVKit

class playVideoViewController: UIViewController {
    var playerLayer = AVPlayerLayer()
    
    //@IBOutlet weak var vwvideo: UIView!
    
   // @IBOutlet weak var imgfull: UIImageView!
    @IBOutlet weak var skipbtn: UIButton!
    
    var player = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //skipbtn.backgroundColor = UIColor.black
        //skipbtn.setTitleColor(UIColor.black, for: .normal)
        playVideo()

        // Do any additional setup after loading the view.
    }
    
    
    func playVideo() {
        
        guard let urlVideoStr =  UDSingleton.shared.appSettings?.appSettings?.play_video_url else {
            print("Video Url not found")
            return
        }
        
        var newUrl = ""
        if  urlVideoStr == "" || urlVideoStr == "0"{
            newUrl = "https://cdn-royorides.imgix.net/images/admin79dec7f95aa0f48c06c00MQDvPikkup_TutorialEDITED.mp4"
        }else{
            newUrl = urlVideoStr
        }
        
        
        let videoURL = URL(string:newUrl)
        let player = AVPlayer(url: videoURL!)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = .resizeAspect
       // playerLayer.backgroundColor = UIColor.white.cgColor
        self.view.layer.addSublayer(playerLayer)
        player.play()
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        playerLayer.frame = CGRect(x: 0,y: 0,width: size.width, height: size.height)
    }
    
    
    
     @objc func playerDidFinishPlaying(){
      
           self.player.pause()
           moveToNextView()
    
      
       }
    
    @IBAction func actionbackbtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func skipbtn(_ sender: Any) {
        moveToNextView()
    }
    
    

}

extension playVideoViewController {
    
   
    

    
    
    func moveToNextView() {
        if /UDSingleton.shared.appSettings?.appSettings?.ios_google_api == "" {
            print("Google Place API")
            GMSServices.provideAPIKey( /UDSingleton.shared.appSettings?.appSettings?.google_place_api)
            GMSPlacesClient.provideAPIKey(/UDSingleton.shared.appSettings?.appSettings?.google_place_api)
        } else {
            GMSServices.provideAPIKey( /UDSingleton.shared.appSettings?.appSettings?.ios_google_api)
            GMSPlacesClient.provideAPIKey(/UDSingleton.shared.appSettings?.appSettings?.ios_google_api)
        }
        
        let userInfo = UDSingleton.shared
        
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
        switch template {
        case .Moby?:
            
            if (userInfo.userData != nil) {
                 ez.runThisInMainThread {
                    AppDelegate.shared().setHomeAsRootVC()
                 }
              }else  {
               
                ez.runThisInMainThread {
                    guard let vc = R.storyboard.newTemplateLoginSignUp.ntLoginSignupTypeViewController() else {return}
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
            
        case .DeliverSome:
            
            if (userInfo.userData != nil) {
                ez.runThisInMainThread {
                   AppDelegate.shared().setHomeAsRootVC()
                }
            }else  {
                
                ez.runThisInMainThread {
                   guard let loginTemplate1 = R.storyboard.template1_Design.landingAndPhoneInputVCTemplate1() else { return }
                    self.navigationController?.pushViewController(loginTemplate1, animated: false)
               }
            }
        
            
        default:
            if (userInfo.userData != nil) {
                 ez.runThisInMainThread {
                    if APIBasePath.isMedc2u {
                        let storyboard = UIStoryboard(name: "MainCab", bundle: nil)
                        let secondViewController = storyboard.instantiateViewController(withIdentifier: "BoydInitiallViewController") as! BoydInitiallViewController
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                    }else{
                        AppDelegate.shared().setHomeAsRootVC()
                    }
                 }
              }else  {
               
                if UDSingleton.shared.hasSocialLogin{
                    ez.runThisInMainThread {
                        guard let vc = R.storyboard.newTemplateLoginSignUp.ntLoginSignupTypeViewController() else {return}
                        vc.template = template ?? .Default
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }else{
                    ez.runThisInMainThread {
                        guard let vc = R.storyboard.mainCab.landingAndPhoneInputVC() else {return}
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }
            }
        }
    }
}
