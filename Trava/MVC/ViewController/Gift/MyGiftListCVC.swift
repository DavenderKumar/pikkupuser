//
//  MyGiftListCVC.swift
//  RoyoRide
//
//  Created by Pushpinder Kaur on 13/11/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit

class MyGiftListCVC: UICollectionViewCell {
    
    @IBOutlet weak var bgVw: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    override func awakeFromNib() {
        
        bgVw.setViewBorderColorSecondary()
        lblStatus.setTextColorSecondary()
    }

    func assignData(model : OrderCab, type:String) {
    
    guard let orderId = model.orderToken else {return}
  
        if type == "Received" {
            lblTitle.text = "You have received a gift request from \(/model.customer?.name) on \(/model.customer?.phone_number) "
        } else{
            lblTitle.text = "You have successfully sent a gift request to \(/model.friend_name) on \(/model.friend_phone_number) "
        }
    
        
      
       
        
    if model.friend_order_status == "0" {
        
        lblStatus.text = "Pending"
        lblStatus.setTextColorTheme()
        if type == "Received" {
        btnReject.isHidden = false
        btnAccept.isHidden = false
        }
    } else if  model.friend_order_status == "2"  {
        
        lblStatus.text = "Rejected"
        lblStatus.textColor = R.color.appRed()
        if type == "Received" {
        btnReject.isHidden = true
        btnAccept.isHidden = true
        }
        
    }else if model.friend_order_status == "1" {
        
        lblStatus.text = "Accepted"
        lblStatus.setTextColorTheme()
        if type == "Received" {
        btnReject.isHidden = true
        btnAccept.isHidden = true
        }
    }
    
        
       
        
    guard let lati = model.dropOffLatitude else{return}
    guard let long = model.dropOffLongitude else{return}
    
    if let orderDate = model.orderLocalDate{
        lblDate.text = orderDate.getBookingDateStr()
    }
    
    guard let payment = model.payment else{return}
    
    
    updateSemantic()
    
  let strURL = Utility.shared.setStaticPolyLineOnMap(pickUpLat: /model.pickUpLatitude, pickUpLng: /model.pickUpLongitude, dropLat: /model.dropOffLatitude, dropLng: /model.dropOffLongitude,exactPath:/model.exactPath)
    

   // let nsString  = NSString.init(string: strURL).addingPercentEscapes(using: String.Encoding.utf8.rawValue)!

    
    if let url = NSURL(string: strURL) {
      //  imgViewMapTrack.sd_setImage(with:url as URL, placeholderImage: nil, options: .refreshCached, progress: nil, completed: nil)
    }
}

private  func updateSemantic() {
    if  LanguageFile.shared.isLanguageRightSemantic() {
        lblStatus.textAlignment = .left
        lblTitle.textAlignment = .left
        lblDate.textAlignment  = .right
       
    }
 }
}
