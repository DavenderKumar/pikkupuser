//
//  MyGiftListVCViewController.swift
//  RoyoRide
//
//  Created by Pushpinder Kaur on 13/11/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit
enum GiftListType : String {
    
    case Sent = "Send"
    case Received = "Received"
    
    
}
class MyGiftListVCViewController: BaseVCCab {
    @IBOutlet var stackView: UIStackView!
    @IBOutlet weak var viewTabsBack: UIView!
    @IBOutlet weak var vwSent: UIView!
    @IBOutlet weak var vwReceived: UIView!
    @IBOutlet weak var collectionViewReceived: UICollectionView!
    @IBOutlet weak var collectionViewSent: UICollectionView!
    @IBOutlet var viewScroll: UIScrollView!
    @IBOutlet var constraintCentreMovingLine: NSLayoutConstraint!
    @IBOutlet var viewMovingLine: UIView!
    @IBOutlet weak var btnSent: UIButton!
    
    @IBOutlet weak var btnReceived: UIButton!
    @IBOutlet var btnBackBase: UIButton!
    
    var collectionViewPastDataSource : CollectionViewDataSourceCab?
    lazy  var arrSentGift : [OrderCab] = [OrderCab]()
    
    var collectionViewUpComingDataSource : CollectionViewDataSourceCab?
    lazy  var arrReceivedGift : [OrderCab] = [OrderCab]()
    
    var listType : GiftListType = .Sent
    var isRightToLeft = false
    var pagingSent : Int = 0
    var pagingReceived : Int = 0
    var isAllItemSentFetched : Bool = false
    var isAllItemReceivedFetched : Bool = false
   
    
    
    private lazy var refreshControlSent: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshList(refresh:)), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.black
        return refreshControl
    }()
    
    private lazy var refreshControlReceived: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshList(refresh:)), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.black
        return refreshControl
    }()
    
    
    lazy var emptyStateViewSent: EmptyStateView = {
          let eView: EmptyStateView = .fromNib()
          return eView
    }()
    
    lazy var emptyStateViewReceived: EmptyStateView = {
          let eView: EmptyStateView = .fromNib()
          return eView
    }()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUI()
        swapStackViews()
        configurePastCollectionView()
        configureUpcomingCollectionView()
        initialPastRefreshProgrammatically()
        getRecievedGiftList()
        viewMovingLine.setViewBackgroundColorSecondary()
        // Do any additional setup after loading the view.
    }
    
    public func initialPastRefreshProgrammatically() {
        refreshControlSent.beginRefreshing()
        let offsetPoint = CGPoint.init(x: 0, y: -refreshControlSent.frame.size.height)
        collectionViewSent.setContentOffset(offsetPoint, animated: true)
        getSendGiftList()
    }
    //MARK:- Functions
    func swapStackViews() {
        if isRightToLeft == true {
            if let myView = stackView.subviews.first {
                stackView.removeArrangedSubview(myView)
                stackView.setNeedsLayout()
                stackView.layoutIfNeeded()
                stackView.insertArrangedSubview(myView, at: 1)
                stackView.setNeedsLayout()
            }
        }
    }
    
    
    public func showVCPlaceholder(type: NoDataTitle, scrollView: UIScrollView?, emptyView: EmptyStateView) {
        guard let scrollableView = scrollView else {
            return
        }
        emptyView.frame = scrollableView.bounds
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            scrollableView.addSubview(emptyView)
            emptyView.setData(type: type)
        }
    }
    
    
    func setUpUI() {
        
        viewTabsBack.setViewBackgroundColorHeader()
       viewScroll.delegate = self
        var imgBack = R.image.ic_back_arrow_black()
        if  let languageCode = UserDefaultsManager.languageId{
            guard let intVal = Int(languageCode) else {return}
            switch intVal{
            case 3, 5:
                imgBack = #imageLiteral(resourceName: "Back_New")
            default :
                imgBack = R.image.ic_back_arrow_black()
            }
        }
        //btnBackBase.setImage(imgBack?.setLocalizedImage(), for: .normal)
       // configureRefreshControl()
         lblTitle?.text = "My Bookings"
        let template = AppTemplate(rawValue: (UDSingleton.shared.appSettings?.appSettings?.app_template?.toInt() ?? 0) )
        switch template {
        case .Moby?:
            //btnBackBase.setImage(R.image.back(), for: .normal)
            btnBackBase.setButtonWithTintColorHeaderText()
            
        case .DeliverSome?:
            //btnBackBase.setImage(R.image.back(), for: .normal)
            btnBackBase.setButtonWithTintColorHeaderText()
            
        case .GoMove?:
            lblTitle?.text = "My Bookings"
            btnBackBase.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
      
            
        default:
          
            btnBackBase.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           //  btnCalendar.isHidden = false
            break
        }
        
        viewTabsBack.setViewBackgroundColorHeader()
        
        btnSent.isSelected = true
        btnSent.setTitleColor(UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.Btn_Text_Colour ?? DefaultColor.color.rawValue), for: .selected)
        btnSent.setTitleColor(.white, for: .normal)
        
        btnReceived.setTitleColor(UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.Btn_Text_Colour  ?? DefaultColor.color.rawValue), for: .selected)
        btnReceived.setTitleColor(.white, for: .normal)
        
        
         switch template {
             case .Corsa:
            
            
                btnSent.setTitleColor(.black, for: .selected)
                btnSent.setTitleColor(.white, for: .normal)
                   
                btnReceived.setTitleColor(.black, for: .selected)
                btnReceived.setTitleColor(.white, for: .normal)
            
            default:break
            
        }
        
        
    }
    func animateSwipeControl(type : GiftListType) {
        UIView.animate(withDuration: 0.4, animations: { [weak self] in
            if  self?.isRightToLeft == true {
                self?.constraintCentreMovingLine.constant = self?.listType == .Sent ? 0 : -(UIScreen.main.bounds.width/2)
            }else{
                self?.constraintCentreMovingLine.constant = self?.listType == .Sent ? 0 : UIScreen.main.bounds.width/2
            }
            self?.toggleBtnStates()
            self?.view.layoutIfNeeded()
        }) { (success) in
            
        }
    }
    
    func toggleBtnStates() {
        
        btnSent.isSelected = listType == .Sent ? true : false
        btnReceived.isSelected = listType == .Received ? true : false
        btnSent.titleLabel?.font = listType == .Sent ? UIFont.systemFont(ofSize: 14, weight: .bold) : UIFont.systemFont(ofSize: 14, weight: .medium)
        btnReceived.titleLabel?.font = listType == .Received ? UIFont.systemFont(ofSize: 14, weight: .bold) : UIFont.systemFont(ofSize: 14, weight: .medium)
    }
    
    @IBAction func actionBackPressed(_ sender: Any) {
        popVC()
    }
    
    @IBAction func actionBtnPastPressed(_ sender: Any) {
        viewScroll.scrollRectToVisible(CGRect(x: 0 , y: 0, width: ez.screenWidth, height: viewScroll.bounds.height), animated: true)
        listType = .Sent
        animateSwipeControl(type: listType)
    }
    
    @IBAction func actionBtnUpcomingPressed(_ sender: Any) {
        
        viewScroll.scrollRectToVisible(CGRect(x: ez.screenWidth , y: 0, width: ez.screenWidth, height: viewScroll.bounds.height), animated: true)
//         listType = .Upcoming
//        animateSwipeControl(type: listType)
    }
    
    func showBookingDetails(order : OrderCab , type : GiftListType) {
        guard let detailsVC = R.storyboard.sideMenu.bookingDetailVC() else{return}
          detailsVC.order = order
          //detailsVC.type = type
         // detailsVC.delegateCancellation = self
        self.pushVC(detailsVC)
       // self.navigationController?.pushVC(detailsVC)
      //   self.navigationController?.pushViewController(detailsVC, animated: true)
    }
    
    func showEtokenBookingDetails(order : OrderCab , type : GiftListType) {
        guard let detailsVC = R.storyboard.drinkingWater.drinkingWaterETokenDeliver() else {return}
        detailsVC.order = order
        //detailsVC.type = type
        self.pushVC(detailsVC)
        //self.navigationController?.pushViewController(detailsVC, animated: true)
    }
    func configurePastCollectionView() {
      
      let configureCellBlock : ListCellConfigureBlockCab = {(cell, item, indexPath) in
          if let cell = cell as? MyGiftListCVC , let model = item as? OrderCab {
              cell.assignData(model: model, type:"Sent")
            //cell.bgVw.setViewBorderColorSecondary()
          }
      }
      
      let willDisplayCell : WillDisplay = { [weak self]  (indexPath) in
          if indexPath.row + 1 == self?.arrSentGift.count && !(/self?.isAllItemSentFetched)  {
              self?.pagingSent =   (/self?.pagingSent + 1) * 10
              self?.getSendGiftList()
          }
      }

      let didSelectBlock : DidSelectedRowCab = { [weak self] (indexPath, cell, item) in
          if let _ = cell as? MyGiftListCVC , let item = item as? OrderCab {
            if item.friend_order_status != "0" {
                self?.showBookingDetails(order: item, type: .Sent)
            }
        
          }
      }
     
       let height = 143
      
        collectionViewPastDataSource =  CollectionViewDataSourceCab(items:  arrSentGift, collectionView: collectionViewSent, cellIdentifier: "MyGiftListCVC", cellHeight: CGFloat(height), cellWidth: ez.screenWidth , configureCellBlock: configureCellBlock )
      
      collectionViewPastDataSource?.willDisplay = willDisplayCell
    collectionViewPastDataSource?.aRowSelectedListener = didSelectBlock
      
        collectionViewSent.delegate = collectionViewPastDataSource
        collectionViewSent.dataSource = collectionViewPastDataSource
        collectionViewSent.reloadData()
   }
    
    func configureUpcomingCollectionView() {
        
        let configureCellBlock : ListCellConfigureBlockCab = {(cell, item, indexPath) in
            if let cell = cell as? MyGiftListCVC , let model = item as? OrderCab {
                cell.assignData(model: model, type:"Received")
                //cell.bgVw.setViewBorderColorSecondary()
                cell.btnReject.tag = indexPath.item
                cell.btnAccept.tag = indexPath.item
                cell.btnAccept.addTarget(self, action: #selector(self.handleAcceptButton(_:)), for: .touchUpInside)
                cell.btnReject.addTarget(self, action: #selector(self.handleRejectButton(_:)), for: .touchUpInside)
            }
        }
        
        let didSelectBlock : DidSelectedRowCab = { [weak self] (indexPath, cell, item) in
            if let _ = cell as? MyGiftListCVC , let item = item as? OrderCab {
                if item.friend_order_status != "0" {
                    item.organisationCouponUserId == 0 ?  self?.showBookingDetails(order: item, type: .Received) : self?.showEtokenBookingDetails(order: item, type: .Received)
                }
                
            }
        }
        let willDisplayCell : WillDisplay = { [weak self]  (indexPath) in

            if indexPath.row + 1 == self?.arrReceivedGift.count && !(/self?.isAllItemReceivedFetched)  {
                self?.pagingReceived =   /self?.pagingReceived + 1
                self?.getRecievedGiftList()
            }
        }

        let height = 143
        
        collectionViewUpComingDataSource =  CollectionViewDataSourceCab(items:  arrReceivedGift, collectionView: collectionViewReceived, cellIdentifier: "MyGiftListCVC", cellHeight: CGFloat(height), cellWidth: ez.screenWidth , configureCellBlock: configureCellBlock )
      
        collectionViewUpComingDataSource?.aRowSelectedListener = didSelectBlock
        collectionViewUpComingDataSource?.willDisplay = willDisplayCell

        collectionViewReceived.delegate = collectionViewUpComingDataSource
        collectionViewReceived.dataSource = collectionViewUpComingDataSource
        
        collectionViewReceived.reloadData()
    }
  
    @objc func handleAcceptButton(_ sender: UIButton) {
        print("accept")
        
            acceptRejectGiftRequest(orderId:/arrReceivedGift[sender.tag].orderId , action:1 )
        }
    @objc func handleRejectButton(_ sender: UIButton) {
        acceptRejectGiftRequest(orderId:/arrReceivedGift[sender.tag].orderId , action:2 )
        print("reject")
        }
    func getRecievedGiftList() {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let bookingList = BookServiceEndPoint.receivedGiftRequest(skip: pagingReceived, take: 10)
        bookingList.request(isLoaderNeeded: false, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) { [weak self] (response) in
            switch response {
                
            case .success(let data):
                
                if let arrBookings = data as? [OrderCab] {
                  
                    if self?.pagingReceived == 0  {
                        self?.refreshControlReceived.endRefreshing()

                        self?.isAllItemReceivedFetched = false
                        self?.arrReceivedGift.removeAll()
                    }
                    
                    if arrBookings.count == 0 || arrBookings.count < 10 {
                         self?.isAllItemReceivedFetched = true
//                        if  self?.pagingUpcoming != 0 {
//                            self?.pagingUpcoming =  /self?.pagingUpcoming - 1
//                        }
                    }
                    
                    self?.arrReceivedGift.append(contentsOf: arrBookings)
                    self?.collectionViewUpComingDataSource?.items = self?.arrReceivedGift
                    self?.collectionViewReceived.reloadData()
                    
                    /self?.arrReceivedGift.count == 0 ? self?.showVCPlaceholder(type: .NO_RECEIVED_GIFT, scrollView: self?.collectionViewReceived, emptyView: (self?.emptyStateViewReceived)!) : (self?.emptyStateViewReceived.removeFromSuperview())
                    
                }
            case .failure(let strError):
                
                self?.refreshControlReceived.endRefreshing()
                self?.isAllItemReceivedFetched = false
               if  self?.pagingReceived != 0 {
                 self?.pagingReceived =  /self?.pagingReceived - 1
               }
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    
    @objc func refreshList(refresh : UIRefreshControl) {

        refresh.beginRefreshing()
        if refresh == refreshControlSent {
            isAllItemSentFetched = false
            pagingSent = 0
            getSendGiftList()
            emptyStateViewSent.removeFromSuperview()
        }else{
            isAllItemReceivedFetched = false
            pagingReceived = 0
            getRecievedGiftList()
            emptyStateViewReceived.removeFromSuperview()
        }
    }
    
    func getSendGiftList() {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let bookingList = BookServiceEndPoint.sentGiftList(skip: pagingSent, take: 10)
        
        bookingList.request(isLoaderNeeded: false, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) { [weak self] (response) in
            switch response {
            case .success(let data):
                if let arrBookings = data as? [OrderCab] {
                    
                    if self?.pagingSent == 0  {
                        self?.refreshControlSent.endRefreshing()
                         self?.isAllItemSentFetched = false
                        self?.arrSentGift.removeAll()
                    }
                    
                    if arrBookings.count == 0 || arrBookings.count < 10 {
                        self?.isAllItemSentFetched = true
                    }
                    
                    self?.arrSentGift.append(contentsOf: arrBookings)
                    
                    ez.runThisInMainThread {
                        self?.collectionViewPastDataSource?.items = self?.arrSentGift
                        self?.collectionViewSent.reloadData()
                        /self?.arrSentGift.count == 0 ? self?.showVCPlaceholder(type: .NO_SENT_GIFT, scrollView: self?.collectionViewSent, emptyView: (self?.emptyStateViewSent)!) : (self?.emptyStateViewSent.removeFromSuperview())
                    }
                 
                }
            case .failure(let strError):
                
                self?.isAllItemSentFetched = false
                self?.refreshControlSent.endRefreshing()

                if  self?.pagingSent != 0 {
                    self?.pagingSent =  /self?.pagingSent - 1
                }
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )

            }
        }
    }
    
    
    func acceptRejectGiftRequest(orderId:Int, action:Int) {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let bookingList = BookServiceEndPoint.giftRequestRequest(order_id: orderId, action: action)
        
        bookingList.request(isLoaderNeeded: false, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) { [weak self] (response) in
            switch response {
            case .success(let data):
                self?.isAllItemReceivedFetched = false
                self?.pagingReceived = 0
                self?.getRecievedGiftList()
                self?.emptyStateViewReceived.removeFromSuperview()
            case .failure(let strError):
                
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )

            }
        }
    }
}
//MARK:- Scroll View Delegates

extension  MyGiftListVCViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        listType = pageNumber == 0 ? .Sent : .Received
        animateSwipeControl(type: listType)
    }
}
