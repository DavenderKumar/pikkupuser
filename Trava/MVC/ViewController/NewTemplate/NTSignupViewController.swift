//
//  NTSignupViewController.swift
//  RoyoRide
//
//  Created by Ankush on 15/05/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit

class NTSignupViewController: UIViewController {
    
    @IBOutlet var viewPhone: UIView!
    @IBOutlet var imgViewCountryCode: UIImageView!
    @IBOutlet weak var stackViewUserEmail: UIStackView!
    @IBOutlet weak var stackViewPassword: UIStackView!
    @IBOutlet weak var stackViewConfirmPassword: UIStackView!
    
    @IBOutlet weak var stackViewContainer: UIStackView!
    
    @IBOutlet var txtFieldMobileNo: UITextField!
    @IBOutlet weak var textFieldUsernameEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldConfirmPassword: UITextField!
    
    @IBOutlet weak var segmentPhoneEmail: UISegmentedControl!
    
    @IBOutlet weak var lblFlagImage: UILabel!
    @IBOutlet weak var buttonNext: UIButton!
    
    @IBOutlet var lblCountryCode: UILabel!
    @IBOutlet var lblISOCode: UILabel!
    
    @IBOutlet weak var btnTermsAccept: UIButton!
   
    var toggle = false
    var iso: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        textFieldPassword.text = "123456"
        textFieldConfirmPassword.text = "123456"
    }
}

extension NTSignupViewController {
    
    func initialSetup() {
        
        segmentPhoneEmail.selectedSegmentIndex = 0
        
        setupUI()
        setupData()
    }
    
    func setupUI() {
        
        stackViewUserEmail.isHidden = segmentPhoneEmail.selectedSegmentIndex == 0
        stackViewPassword.isHidden = segmentPhoneEmail.selectedSegmentIndex == 0
        stackViewConfirmPassword.isHidden = segmentPhoneEmail.selectedSegmentIndex == 0
        
        segmentPhoneEmail.setTextColorBtnText()
        
        buttonNext.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        buttonNext.setButtonWithTintColorBtnText()
        
        txtFieldMobileNo.delegate = self
        btnTermsAccept.setButtonWithTintColorSecondary()
    }
    
    
    func setupData() {
        iso =  UDSingleton.shared.appSettings?.appSettings?.iso_code ?? DefaultCountry.ISO.rawValue
        
        lblCountryCode.text =  UDSingleton.shared.appSettings?.appSettings?.default_country_code ?? DefaultCountry.countryCode.rawValue
        lblISOCode.text =  UDSingleton.shared.appSettings?.appSettings?.iso_code ?? DefaultCountry.ISO.rawValue
        if iso?.count == 2 {
            
            imgViewCountryCode.image = UIImage(named: "\(/iso?.lowercased()).png")
            
            if let path = Bundle.main.path(forResource: "CallingCodes", ofType: "plist"),
                 let arr:[NSDictionary] = NSArray(contentsOfFile: path) as? [NSDictionary]{

                 let dict:[[String:AnyObject]] = arr.filter{($0["code"] as! String) == /iso} as! [[String : AnyObject]]

                 if let val = dict.first {
                     let data = val as NSDictionary
                     let dialCode = data.value(forKey: "dial_code") as! String
                     lblCountryCode.text = dialCode
                 }
             }
            
            lblFlagImage.text = countryFlag(countryCode: /iso?.lowercased())
            
            
        } else {
            guard let isoAlpha2 = CountryUtility.getISOAlpha2(isoAlpha3: /iso) else { return }
                   imgViewCountryCode.image = UIImage(named: "\(isoAlpha2.lowercased()).png")
                   if let path = Bundle.main.path(forResource: "CallingCodes", ofType: "plist"),
                       let arr:[NSDictionary] = NSArray(contentsOfFile: path) as? [NSDictionary]{

                       let dict:[[String:AnyObject]] = arr.filter{($0["code"] as! String) == /isoAlpha2} as! [[String : AnyObject]]

                       if let val = dict.first {
                           let data = val as NSDictionary
                           let dialCode = data.value(forKey: "dial_code") as! String
                           lblCountryCode.text = dialCode
                       }
                   }
            
            lblFlagImage.text = countryFlag(countryCode: isoAlpha2)
        }
       
        imgViewCountryCode.isHidden = false
        lblFlagImage.isHidden  = true
    }
}
func countryFlag(countryCode: String) -> String {
  return String(String.UnicodeScalarView(
     countryCode.unicodeScalars.compactMap(
       { UnicodeScalar(127397 + $0.value) })))
}
//MARK:- Button Selector
extension NTSignupViewController {
    
    @IBAction func segmentClicked(_ sender: UISegmentedControl) {
        
        setupUI()
        
      /*  switch sender.selectedSegmentIndex {
            
        case 0:
            viewEmail.removeFromSuperview()
            viewPhone.isHidden = false
            constraintHeightContainerView.constant = 53.0
            
        case 1:
            viewPhone.isHidden = true
            constraintHeightContainerView.constant = 248.0
            view.layoutSubviews()
            
            viewEmail.frame = viewContainer.bounds
            viewContainer.addSubview(viewEmail)
            
            
        default:
            break
        } */
        
        
    }
    
    @IBAction func btnAcceptTerns(_ sender: UIButton) {
        toggle = !toggle
        
        if toggle {
            btnTermsAccept.setImage(#imageLiteral(resourceName: "ic_checkbox_active"), for: .normal)
        } else {
            btnTermsAccept.setImage(#imageLiteral(resourceName: "ic_checkbox_inactive"), for: .normal)
        }
    }
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        
        // 0- Phone, 1- Email
        // 1- Back, 2- Terms, 3- Privacy, 4- Next, 5- Country code
        
        switch sender.tag {
        case 1:
            popVC()
            
        case 2:
            debugPrint("Terms")
            
        case 3:
            debugPrint("Policy")
            
        case 4:
            
            let code = /lblCountryCode.text
            let number = /txtFieldMobileNo.text?.trimmed()
            
            switch segmentPhoneEmail.selectedSegmentIndex {
            case 0:
                
                if Validations.sharedInstance.validatePhoneNumber(phone: number) {
                    if toggle {
                        self.sendOtp(code: code, number: number, signup_as: .PhoneNo)
                    } else {
                         Alerts.shared.show(alert: "AppName".localizedString, message: "Please accept terms and conditions" , type: .error )
                    }
                    
                    
                }
                
            case 1:
                if Validations.sharedInstance.validateSignupUsernameAndPassword(usernameOrEmail: /textFieldUsernameEmail.text?.trimmed(), password: /textFieldPassword.text?.trimmed(), confirmPassword: /textFieldConfirmPassword.text?.trimmed(), phone: /txtFieldMobileNo.text?.trimmed() ) {
                    if toggle {
                   
                        sendOtp(code: code, number: number, signup_as: .Email, email: /textFieldUsernameEmail.text?.trimmed(), password: /textFieldPassword.text?.trimmed())
                    } else {
                         Alerts.shared.show(alert: "AppName".localizedString, message: "Please accept terms and conditions" , type: .error )
                    }
                    debugPrint("Success")
                    
                    
                }

            default:
                break
            }
            
        case 5:
            
            guard let countryPicker = R.storyboard.mainCab.countryCodeSearchViewController() else{return}
            countryPicker.delegate = self
            self.presentVC(countryPicker)
            
        
        default:
            break
        }
    }
    
}

//MARK: - Country Picker Delegates

extension NTSignupViewController: CountryCodeSearchDelegate {
    
    func didTap(onCode detail: [AnyHashable : Any]!) {
        imgViewCountryCode.image = UIImage(named:/(detail["code"] as? String)?.lowercased())
        lblCountryCode.text = /(detail["dial_code"] as? String)
        lblISOCode.text = /(detail["code"] as? String)
        
        imgViewCountryCode.isHidden = false
               lblFlagImage.isHidden  = true
        
        iso = /(detail["code"] as? String)
    }
    
    func didSuccessOnOtpVerification() {
        
    }
    
    
}

//MARK:- API

extension NTSignupViewController {
    
    func sendOtp(code:String, number:String, signup_as: LoginSignupType?, email: String? = nil, password: String? = nil) {
        
        guard let accountType = signup_as else {return}
        
        let sendOTP = LoginEndpoint.sendOtp(countryCode: code, phoneNum: number, iso: lblISOCode.text, social_key: nil, signup_as: accountType.rawValue, email: email, password: password)
        sendOTP.request( header: ["language_id" : LanguageFile.shared.getLanguage()]) {[weak self] (response) in
            switch response {
                
            case .success(let data):
                
                guard let model = data as? SendOtp else { return }
                model.countryCode = code
                model.mobileNumber = number
                model.iso = self?.lblISOCode.text
                
                guard let vc = R.storyboard.newTemplateLoginSignUp.ntVerificationCodeViewController() else{return}
                vc.sendOTP = model
                vc.signup_as = signup_as
                self?.pushVC(vc)
                
                break
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
                //Toast.show(text: strError, type: .error)
            }
        }
    }
}

//MARK:- UItextfield delegates

extension NTSignupViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        if  string == numberFiltered {
            if text == "" && string == "0" {
                return false
            }
            let newLength = text.length + string.length - range.length
            return newLength <= 15
        } else {
            return false
        }
    }
}
