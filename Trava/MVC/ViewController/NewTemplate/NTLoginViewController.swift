//
//  NTLoginViewController.swift
//  RoyoRide
//
//  Created by Ankush on 15/05/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit

class NTLoginViewController: BaseVCCab {
    
    //MARK:- Enum
    enum ScreenType: String {
        case phone
        case email
        case emailInstitution
    }
    
    //MARK:- Outlet
    @IBOutlet var imgViewCountryCode: UIImageView!
    @IBOutlet weak var lblFlagImage: UILabel!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonLoginWithEmailPhone: UIButton!
    
    @IBOutlet weak var constraintHeightContainerView: NSLayoutConstraint!
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet var viewPhone: UIView!
    @IBOutlet var viewEmail: UIView!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet var lblCountryCode: UILabel!
    @IBOutlet var lblISOCode: UILabel!
    
    @IBOutlet var txtFieldMobileNo: UITextField!
    @IBOutlet weak var textFieldUsernameEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    @IBOutlet weak var stackLoginWith: UIStackView!
    
    @IBOutlet weak var stckVwRembrme: UIStackView!
    
    
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var dropdownImageView: UIImageView!
    @IBOutlet weak var languageView: UIView!
    
    //MARK:- Property
    var screenType: ScreenType = .phone
    var iso: String?
    
  
    var languageNameArray = [String]()
    var languageArray = UDSingleton.shared.appSettings?.languages
    //MARK:- View LIfe Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        
        languageNameArray.removeAll()
        for language in languageArray ?? [] {
            languageNameArray.append(language.language_name ?? "")
        }
    }
}


//MARK:- Function
extension NTLoginViewController {
    
    func initialSetup() {
        let isLanguageEnabled = UDSingleton.shared.appSettings?.appSettings?.is_language_selection_on_splash == "true"
        
        languageView.isHidden = !isLanguageEnabled
        
        setupUI()
        setupData()
    }
    
    func setupUI() {
        
        switch screenType {
        case .email, .emailInstitution:
            viewPhone.isHidden = true
            constraintHeightContainerView.constant = 153.0
            view.layoutSubviews()
            
            viewEmail.frame = viewContainer.bounds
            viewContainer.addSubview(viewEmail)
            
            
        case .phone:
            viewEmail.removeFromSuperview()
            viewPhone.isHidden = false
            constraintHeightContainerView.constant = 53.0
            
            txtFieldMobileNo.delegate = self
        }
        
         
        
       
        let title = (screenType == .phone ? " " + "email".localizedString : "phone".localizedString)
        buttonLoginWithEmailPhone.setTitle(title, for: .normal)
        
        labelTitle.text = "loginTo".localizedString + " " + "AppName".localizedString + " " + (screenType == .emailInstitution ? ( "institution".localizedString) : "")

        stackLoginWith.isHidden = screenType == .emailInstitution
        stackLoginWith.isHidden = self.title == "cooprate login" ? true: false
        stckVwRembrme.isHidden = self.title == "cooprate login" ? true: false
        buttonNext.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        buttonNext.setButtonWithTintColorBtnText()
        
        languageView.setViewBorderColorSecondary()
        languageLabel.setTextColorSecondary()
        dropdownImageView.setImageTintColorSecondary()
        
        setUpPreviousValues()
 
    }
    
    
    func setUpPreviousValues() {
    
        
        if  let languageCode = UserDefaultsManager.languageCode{
            //guard let intVal = Int(languageCode) else {return}
            //btnSelectedLanguage.setTitle(languageArry[intVal - 1], for: .normal)
            switch languageCode{
            case "en":
                languageLabel.text = languageArry[0]
            case "ur":
                languageLabel.text = languageArry[1]
                
            case "zh" :
                languageLabel.text = languageArry[3]
            case  "ar":
                languageLabel.text = languageArry[2]
            case "es":
                 languageLabel.text = languageArry[4]
            case "fr":
                languageLabel.text = languageArry[5]
            default :
                languageLabel.text = languageArry[0]
            }
        }
    }
    func setupData() {
        
        switch screenType {
        case .phone:
            
            lblCountryCode.text =  UDSingleton.shared.appSettings?.appSettings?.default_country_code ?? DefaultCountry.countryCode.rawValue
            lblISOCode.text =  UDSingleton.shared.appSettings?.appSettings?.iso_code ?? DefaultCountry.ISO.rawValue
            iso =  UDSingleton.shared.appSettings?.appSettings?.iso_code ?? DefaultCountry.ISO.rawValue
            if iso?.count == 2 {
                
                imgViewCountryCode.image = UIImage(named: "\(/iso?.lowercased()).png")
                
                if let path = Bundle.main.path(forResource: "CallingCodes", ofType: "plist"),
                     let arr:[NSDictionary] = NSArray(contentsOfFile: path) as? [NSDictionary]{

                     let dict:[[String:AnyObject]] = arr.filter{($0["code"] as! String) == /iso} as! [[String : AnyObject]]

                     if let val = dict.first {
                         let data = val as NSDictionary
                         let dialCode = data.value(forKey: "dial_code") as! String
                         lblCountryCode.text = dialCode
                     }
                 }
                
                lblFlagImage.text = countryFlag(countryCode: /iso?.lowercased())
                
                
            } else {
                guard let isoAlpha2 = CountryUtility.getISOAlpha2(isoAlpha3: /iso) else { return }
                       imgViewCountryCode.image = UIImage(named: "\(isoAlpha2.lowercased()).png")
                       if let path = Bundle.main.path(forResource: "CallingCodes", ofType: "plist"),
                           let arr:[NSDictionary] = NSArray(contentsOfFile: path) as? [NSDictionary]{

                           let dict:[[String:AnyObject]] = arr.filter{($0["code"] as! String) == /isoAlpha2} as! [[String : AnyObject]]

                           if let val = dict.first {
                               let data = val as NSDictionary
                               let dialCode = data.value(forKey: "dial_code") as! String
                               lblCountryCode.text = dialCode
                           }
                       }
                
                lblFlagImage.text = countryFlag(countryCode: isoAlpha2)
            }
            
           
            
            
            imgViewCountryCode.isHidden = false
            lblFlagImage.isHidden  = true
        default:
            
            break
        }
        
        
        
    }
    @IBAction func btnLanguageAction(_ sender: UIButton) {
        if  let languageCode = UserDefaultsManager.languageCode{
           // guard let intVal = Int(languageCode) else {return}
            
            switch languageCode{
            
            case "ar","ur":
                showDropDown(view: sender)
                
            default :
                showDropDown(view: sender)
            }
        }
    }
    func showDropDown(view : UIButton) {
            
        Utility.shared.showDropDown(anchorView: view, dataSource: languageNameArray , width: view.frame.width, handler: { (index, strValu) in
                LanguageFile.shared.setLanguage(languageID: /self.languageArray?[index].language_id,languageCode: /self.languageArray?[index].language_code?.lowercased())
                
                self.updatedAppData()
                
            })
            
        }
    
    
    
    func updatedAppData() {
        AppDelegate.shared().setInitialAsRootVC()
    }
}


//MARK:- Button Selectors
extension NTLoginViewController {
    
    // 1- Back, LoginWith -2, Next- 3, Country code - 4, 5- RememberMe, 6- Forgot password
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            popVC()
            
        case 2:
            screenType = screenType == .phone ? .email : .phone
            setupUI()
            
        case 3:
            debugPrint("Next")
            
            switch screenType {
            case .phone:
                
                let code = /lblCountryCode.text
                let number = /txtFieldMobileNo.text?.trimmed()
                
                if Validations.sharedInstance.validatePhoneNumber(phone: number) {
                    
                    self.sendOtp(code: code, number: number, login_as: .PhoneNo)
                    
                }
                
            case .email:
                if Validations.sharedInstance.validateLoginUsernameAndPassword(usernameOrEmail: /textFieldUsernameEmail.text?.trimmed(), password: /textFieldPassword.text?.trimmed()) {
                
                    debugPrint("Success")
                    
                    emailLogin(login_as: .Email)
                }
                
            case .emailInstitution:
                if Validations.sharedInstance.validateLoginUsernameAndPassword(usernameOrEmail: /textFieldUsernameEmail.text?.trimmed(), password: /textFieldPassword.text?.trimmed()) {
                
                    debugPrint("Success")
                    emailLogin(login_as: .PrivateCooperative)
                }
            }
            
        case 4:
            
            guard let countryPicker = R.storyboard.mainCab.countryCodeSearchViewController() else{return}
            countryPicker.delegate = self
            self.presentVC(countryPicker)
            
            
        case 5:
            debugPrint("Remember me")
            sender.isSelected = !sender.isSelected
            
        case 6:
            debugPrint("Forgot password")
            
            
        default:
            break
        }
    }
}


//MARK: - Country Picker Delegates

extension NTLoginViewController: CountryCodeSearchDelegate {
    
    func didTap(onCode detail: [AnyHashable : Any]!) {
        imgViewCountryCode.image = UIImage(named:/(detail["code"] as? String)?.lowercased())
        lblCountryCode.text = /(detail["dial_code"] as? String)
        lblISOCode.text = /(detail["code"] as? String)
        
        imgViewCountryCode.isHidden = false
               lblFlagImage.isHidden  = true
        
        iso = /(detail["code"] as? String)
    }
    
    func didSuccessOnOtpVerification() {
        
    }
    
    
}

//MARK:- API

extension NTLoginViewController {
    
    func sendOtp(code:String, number:String, login_as: LoginSignupType?) {
        
        guard let accountType = login_as else {return}
        
        let sendOTP = LoginEndpoint.sendOtp(countryCode: code, phoneNum: number, iso: lblISOCode.text, social_key: nil, signup_as: accountType.rawValue)
        sendOTP.request( header: ["language_id" : LanguageFile.shared.getLanguage()]) {[weak self] (response) in
            switch response {
                
            case .success(let data):
                
                guard let model = data as? SendOtp else { return }
                model.countryCode = code
                model.mobileNumber = number
                model.iso = self?.lblISOCode.text
                
                guard let vc = R.storyboard.newTemplateLoginSignUp.ntVerificationCodeViewController() else{return}
                vc.sendOTP = model
                vc.signup_as = login_as
                self?.pushVC(vc)
                
                break
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
                //Toast.show(text: strError, type: .error)
            }
        }
    }
    
    func emailLogin(login_as: LoginSignupType?) {
        
        guard let accountType = login_as else {return}
        
        let emailLogin = LoginEndpoint.emailLogin(login_as: accountType.rawValue, email: textFieldUsernameEmail.text, password: textFieldPassword.text)
        
        emailLogin.request( header: ["language_id" : LanguageFile.shared.getLanguage()]) { (response) in
            switch response {
                
            case .success(let data):
                
               guard let model = data as? LoginDetail else { return }
               
               UDSingleton.shared.userData = model
               let appDelegate = UIApplication.shared.delegate as? AppDelegate
               appDelegate?.setHomeAsRootVC()
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
                //Toast.show(text: strError, type: .error)
            }
        }
    }
    
}

//MARK:- UItextfield delegates

extension NTLoginViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        if  string == numberFiltered {
            if text == "" && string == "0" {
                return false
            }
            let newLength = text.length + string.length - range.length
            return newLength <= 15
        } else {
            return false
        }
    }
}

