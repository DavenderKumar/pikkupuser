//
//  fullOrderDetailPikkup.swift
//  RoyoRide
//
//  Created by admin on 16/02/21.
//  Copyright © 2021 CodeBrewLabs. All rights reserved.
//

import UIKit

class fullOrderDetailPikkup: UIViewController {
    var order : OrderCab?
   
    
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        
        
    }
    
    @IBAction func actionBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
    

    
}

extension fullOrderDetailPikkup:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order?.payment?.product_detail?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "fullOrderDetailTableview", for: indexPath) as! fullOrderDetailTableview
        
        cell.lblProductName.text = order?.payment?.product_detail?[indexPath.row].product_name
        cell.productAddress.text =  order?.payment?.product_detail?[indexPath.row].address
        cell.titleLocation.text =  order?.payment?.product_detail?[indexPath.row].addressType
        cell.btnOnImage.addTarget(self, action: #selector(goToLargeImg(_:)), for: .touchUpInside)
        cell.btnOnImage.tag = indexPath.row
        
        if order?.payment?.product_detail?[indexPath.row].addressType == "Pickup details" {
            cell.stackReceivername.isHidden = true
            cell.stackReceiverNumber.isHidden = true
            
        }else{
            cell.stackReceivername.isHidden = false
            cell.stackReceiverNumber.isHidden = false
        }
        
        if order?.payment?.product_detail?[indexPath.row].product_weight == ""{
            cell.stackProductWeight.isHidden = true
        }
        else{
            cell.lblProductWeight.text = order?.payment?.product_detail?[indexPath.row].product_weight
            cell.stackProductWeight.isHidden = false
            
        }
        if order?.payment?.product_detail?[indexPath.row].receiver_name == ""{
            cell.stackReceivername.isHidden = true
        }else{
            cell.lblReceiverName.text = order?.payment?.product_detail?[indexPath.row].receiver_name
            cell.stackReceivername.isHidden = false
            
        }
        if order?.payment?.product_detail?[indexPath.row].receiver_number == ""{
            cell.stackReceiverNumber.isHidden = true
        }else{
            cell.lblReceiverPhone.text = order?.payment?.product_detail?[indexPath.row].receiver_number
            cell.stackReceiverNumber.isHidden = false
            
        }
        
        if order?.payment?.product_detail?[indexPath.row].store_name == ""{
            cell.storeNameLabel.superview?.isHidden = true
        }else{
            cell.storeNameLabel.text = order?.payment?.product_detail?[indexPath.row].store_name
            cell.storeNameLabel.superview?.isHidden = false
            
        }
        if order?.payment?.product_detail?[indexPath.row].store_number == ""{
            cell.storeNumberLabel.superview?.isHidden = true
        }else{
            cell.storeNumberLabel.text = order?.payment?.product_detail?[indexPath.row].store_number
            cell.storeNumberLabel.superview?.isHidden = false
            
        }
        
        if order?.payment?.product_detail?[indexPath.row].additional_info == ""{
            cell.stackAdditionalInfo.isHidden = true
        }else{
            cell.lblAdditionalInfo.text = order?.payment?.product_detail?[indexPath.row].additional_info
            cell.stackAdditionalInfo.isHidden = false
            
        }
        
     
        if order?.payment?.product_detail?[indexPath.row].url_image == ""{
            cell.stackProductImg.isHidden = true
        }else{
            cell.productimg.sd_setImage(with: URL(string: APIBasePath.imageBasePathWithoutVersion + (order?.payment?.product_detail?[indexPath.row].url_image)!), placeholderImage: UIImage(),options: .refreshCached ,progress: nil, completed: nil )

            
            
            cell.stackProductImg.isHidden = false
            
        }
        
      
        
        
        
        
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    @objc func goToLargeImg(_ sender:UIButton){
        guard  let vc = R.storyboard.orderDetails.largeImagePikkupViewController() else { return }
        let position = sender.tag
        vc.ImagePath = "\(APIBasePath.imageBasePathWithoutVersion + (order?.payment?.product_detail?[position].url_image)!)"
       // guard let topVC = topViewController() else { return }
        
        
        ez.topMostVC?.presentVC(vc)
        
    
    }
    
   
    
    
    
}

class fullOrderDetailTableview :UITableViewCell{
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductWeight: UILabel!
    @IBOutlet weak var lblReceiverName: UILabel!
    @IBOutlet weak var lblReceiverPhone: UILabel!
    @IBOutlet weak var lblAdditionalInfo: UILabel!
    @IBOutlet weak var productimg: UIImageView!
    @IBOutlet weak var productAddress: UILabel!
    @IBOutlet weak var stackReceivername: UIStackView!
    @IBOutlet weak var stackReceiverNumber: UIStackView!
    @IBOutlet weak var titleLocation: UILabel!
    @IBOutlet weak var stackProductWeight: UIStackView!
    @IBOutlet weak var stackAdditionalInfo: UIStackView!
    @IBOutlet weak var stackProductImg: UIStackView!
    @IBOutlet weak var btnOnImage: UIButton!
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var storeNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
