//
//  EnterOrderDetailsViewController.swift
//  RoyoRide
//
//  Created by admin on 24/12/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit
var count = 2



var arrFilterStops = NSMutableArray()
var arrpi = Int()

class AddressItem{

    enum ItemType:String{
        case pick = "Pickup details"
        case drop = "Final Dropoff details"
    }

    var type:ItemType = .pick
    var productName:String?
    var name:String?
    var phone:String?
    var description:String?
    var landmark:String?
    var imageUrl:String?
    var address:String?
    var latitude:Double?
    var longitude:Double?
    var image:UIImage?
    var stopNumber:Int?
    var storeName:String?
    var storeNumber:String?


    init(type:ItemType,address:String?,latitude:Double?,longitude:Double?,stopNumber:Int? = nil){
        self.type = type
        self.address = address
        self.latitude = latitude
        self.longitude = longitude
        self.stopNumber = stopNumber
    }

}




class EnterOrderDetailsViewController: UIViewController, UITextViewDelegate {

    //MARK::- IBOutlets
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var btnsubmit: UIButton!
    @IBOutlet weak var btnAdd: UIButton!




    //MARK::- Variable
    var delegate: ProductDetailDelegate?

    var index:Int?
    var serviceReq: ServiceRequest?
    var addressesItems = [AddressItem]()

    //MARK::- ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()


        tableview.delegate = self
        tableview.dataSource = self
        btnAdd.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        btnAdd.tintColor = .white
        btnsubmit.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        btnAdd.setTitleColor(.white, for: .normal)
        //EDIT
        if let index = index,let addresses = serviceReq?.addressItems{
            addressesItems = [addresses[index]]
            tableview.reloadData()
            //ADD
        }else{

            if serId == 15{
                btnAdd.isHidden = false
                let pickUpAddress = AddressItem(type: .drop, address: serviceReq?.locationNameDest, latitude: serviceReq?.latitudeDest, longitude: serviceReq?.longitudeDest,stopNumber: 1)
                addressesItems.append(pickUpAddress)
            }else{
                btnAdd.isHidden = true
                let pickUpAddress = AddressItem(type: .pick, address: serviceReq?.locationNameDest, latitude: serviceReq?.latitudeDest, longitude: serviceReq?.longitudeDest)
                addressesItems.append(pickUpAddress)
                for (index,stop) in (serviceReq?.stops ?? []).enumerated(){
                    let stopAddress = AddressItem(type: .drop, address: stop.address, latitude: stop.latitude, longitude: stop.longitude,stopNumber:index+1)
                    addressesItems.append(stopAddress)
                }

                let dropOffAddress = AddressItem(type: .drop, address: serviceReq?.locationName, latitude: serviceReq?.latitude, longitude: serviceReq?.longitude)
                addressesItems.append(dropOffAddress)
            }
        }


    }

    @objc func openPhotoOptionsActionSheet(button: UIButton) {
        self.view.endEditing(true)

        let row = button.tag

        CameraImage.shared.captureImage(from: self, At: self , mediaType: nil, captureOptions: [.camera, .photoLibrary], allowEditting: true) { [unowned self] (image) in
            guard let img = image else { return }
            let index = IndexPath(row: row, section: 0)
            let cell = self.tableview.cellForRow(at: index) as? TVEnterOrderDetails
            cell?.userImage?.image = img
            cell?.btnRemoveImage?.isHidden = true
            self.upload(image:img,for:row)
        }
    }


    //MARK::- IBAction
    @IBAction func backbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.back()
    }

    @IBAction func ActionAdd(_ sender: Any) {

        tableview.reloadData()

        if addressesItems.count < 5{
            let pickUpAddress = AddressItem(type: .drop, address: serviceReq?.locationNameDest, latitude: serviceReq?.latitudeDest,longitude: serviceReq?.longitudeDest, stopNumber : addressesItems.count + 1)
            addressesItems.append(pickUpAddress)
            tableview.reloadData()
        }else{
            Alerts.shared.show(alert: "AppName".localizedString, message: "Only five items can be added" , type: .error )
        }
    }


    @IBAction func actionbtnsubmit(_ sender: UIButton) {
        var isValid = true
        
        if self.serviceReq?.serviceSelected?.serviceName == "Purchase" || self.serviceReq?.serviceSelected?.serviceCategoryId == 15
        {
            for (index,item) in addressesItems.enumerated(){
                
               
                guard let productName = item.productName, !productName.isEmpty else {
                    Alerts.shared.show(alert: "AppName".localizedString, message: "Enter product name for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
                    isValid = false
                    return
                }
                
//                guard let storeName = item.storeName, !storeName.isEmpty else {
//                    Alerts.shared.show(alert: "AppName".localizedString, message: "Enter store name for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
//                    isValid = false
//                    return
//                }
//                
//                guard let storeNumber = item.storeNumber, !storeNumber.isEmpty else {
//                    Alerts.shared.show(alert: "AppName".localizedString, message: "Enter store phone number for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
//                    isValid = false
//                    return
//                }
                
                guard let name = item.name, !name.isEmpty else {
                    Alerts.shared.show(alert: "AppName".localizedString, message: "Enter receiver name for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
                    isValid = false
                    return
                }
                
//                guard let phone = item.phone, !phone.isEmpty else {
//                    Alerts.shared.show(alert: "AppName".localizedString, message: "Enter receiver number for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
//                    isValid = false
//                    return
//                }
                
                guard let landmark = item.landmark, !landmark.isEmpty else {
                    Alerts.shared.show(alert: "AppName".localizedString, message: "Enter landmark or company name for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
                    isValid = false
                    return
                }
                
//                guard let description = item.description, !description.isEmpty else {
//                    Alerts.shared.show(alert: "AppName".localizedString, message: "Enter description or additional information for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
//                    isValid = false
//                    return
//                }
//
//                guard item.image != nil else {
//                    Alerts.shared.show(alert: "AppName".localizedString, message: "Please upload Image for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
//                    isValid = false
//                    return
//                }
                
              
            }
        }else
        {
            for (index,item) in addressesItems.enumerated(){
                
                
                guard let address = item.address, !address.isEmpty else{
                    Alerts.shared.show(alert: "AppName".localizedString, message: "Please select a \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
                    isValid = false
                    return
                }
                guard let productName = item.productName, !productName.isEmpty else {
                    Alerts.shared.show(alert: "AppName".localizedString, message: "Enter product name for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
                    isValid = false
                    return
                }
                
                if index != 0 {
                    
                    guard let name = item.name, !name.isEmpty else {
                        Alerts.shared.show(alert: "AppName".localizedString, message: "Enter receiver name for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
                        isValid = false
                        return
                    }
                    
//                    guard let phone = item.phone, !phone.isEmpty else {
//                        Alerts.shared.show(alert: "AppName".localizedString, message: "Enter receiver number for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
//                        isValid = false
//                        return
//                    }
                }
                
                guard let landmark = item.landmark, !landmark.isEmpty else {
                    Alerts.shared.show(alert: "AppName".localizedString, message: "Enter landmark or company name for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
                    isValid = false
                    return
                }
                
//                guard let description = item.description, !description.isEmpty else {
//                    Alerts.shared.show(alert: "AppName".localizedString, message: "Enter description or additional information for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
//                    isValid = false
//                    return
//                }
//
//                guard item.image != nil else {
//                    Alerts.shared.show(alert: "AppName".localizedString, message: "Please upload Image for \(item.type == .pick ? "pickup" : "dropoff") location at index \(index + 1)" , type: .error )
//                    isValid = false
//                    return
//                }
                
              
            }
        }
        
        if isValid{
            dismiss(animated: true){
                if let index = self.index,let address = self.addressesItems.first{
                    self.serviceReq?.addressItems?[index] = address
                    if serId == 15 && index == 0{
                        self.serviceReq?.addressItems?.forEach({ item in
                            item.name = address.name
                            item.phone = address.phone
                        })
                    }
                    self.delegate?.editAddressWithDetails(request:self.serviceReq)
                }else{
                    self.delegate?.addAddressesWithDetails(items: self.addressesItems)
                }
            }
        }
    }





    func upload(image:UIImage?,for row:Int) {
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let file_upload = BookServiceEndPoint.fileUpload
        file_upload.request(image: image, filename: "", header:  ["language_id" : LanguageFile.shared.getLanguage() , "access_token" : token]) { [weak self] (response) in
            switch response {
            case .success(let data):
                let dict = data as? NSDictionary
                let url = dict?["result"] as? String
                self?.addressesItems[row].image = image
                self?.addressesItems[row].imageUrl = url
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message:/strError , type: .error )
            }
        }
    }


    func showDropDown(view : UIView, datasource: [String]) {

        Utility.shared.showDropDown(anchorView: view, dataSource: datasource , width: view.frame.size.width, handler: {[weak self] (index, strValu) in


            self?.tableview.reloadData()
        })
    }


    @objc func dropAction(sender:UIButton){
        sender.isSelected = true
        addressesItems[sender.tag].type = sender.isSelected ? .drop : .pick
        tableview.reloadData()
    }

    @objc func pickAction(sender:UIButton){
        sender.isSelected = true
        addressesItems[sender.tag].type = sender.isSelected ? .pick : .drop
        tableview.reloadData()
    }

}


extension EnterOrderDetailsViewController:UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressesItems.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }



    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "TVEnterOrderDetails", for: indexPath) as! TVEnterOrderDetails
        if serId == 15{
            addressesItems[indexPath.row].name = addressesItems.first?.name
            addressesItems[indexPath.row].phone = addressesItems.first?.phone
        }
        let item = addressesItems[indexPath.row]
        [cell.txtfeildProductName,cell.txtfeildProductWeight,cell.txtfeildReceiverName,cell.lblLandMark,cell.txtfeildReceiverNumber,cell.storeNameField,cell.storeNumberField].forEach { field in
            field?.delegate = self
            field?.tag = indexPath.row
            field?.setAlignment()
            field?.setBorderColorSecondary()
            field?.addShadowToTextFieldColorSecondary()
        }
        cell.txtVwAdditionalinformation.delegate = self
        cell.txtVwAdditionalinformation.tag = indexPath.row
        cell.btnAddimage.tag = indexPath.row
        cell.btnPick.tag = indexPath.row
        cell.btnDrop.tag = indexPath.row
        cell.addressLabel.text = item.address

        cell.btnPick.addTarget(self, action: #selector(pickAction(sender:)), for: .touchUpInside)
        cell.btnDrop.addTarget(self, action: #selector(dropAction(sender:)), for: .touchUpInside)
        cell.btnPick.isSelected = item.type == .pick
        cell.btnDrop.isSelected = item.type == .drop
        cell.btnAddimage.addTarget(self, action: #selector(openPhotoOptionsActionSheet), for: .touchUpInside)

        cell.txtfeildProductName.text = item.productName
        cell.userImage.image = item.image
        cell.storeNameField.text = item.storeName
        cell.storeNumberField.text = item.storeNumber
        cell.txtfeildReceiverName.text = item.name
        cell.txtfeildReceiverNumber.text = item.phone
        cell.txtVwAdditionalinformation.text = item.description
        cell.lblLandMark.text = item.landmark
        cell.btnRemove.setViewBorderColorSecondary()
        cell.btnRemove.setTitle("Remove", for: .normal)
        cell.btnRemove.titleLabel?.font = .systemFont(ofSize: 14)

        cell.txtVwAdditionalinformation.setAlignment()
        cell.txtVwAdditionalinformation.setBorderColorSecondary()
        cell.txtVwAdditionalinformation.addShadowToTextViewColorSecondary()



        cell.addressStack.isHidden = false
        cell.lblLocationName.isHidden = false
        if let stopNumber = item.stopNumber{
            cell.lblLocationName.text = "DROP OFF DETAILS \(stopNumber)"
        }else{
            cell.lblLocationName.text = item.type.rawValue
        }
        if serId == 15{
            cell.btnRemove.isHidden = false
            cell.lblLocationName.isHidden = true
            cell.addressStack.isHidden = true
            cell.lblProductRecivername.text = "RECEIVER NAME"
            cell.lblProductReciverNumber.text = "RECEIVER NUMBER"
            cell.storeNameField.superview?.isHidden = false
            cell.storeNumberField.superview?.isHidden = false
            if index != nil && index == 0{
                cell.txtfeildReceiverName.isUserInteractionEnabled = true
                cell.txtfeildReceiverNumber.isUserInteractionEnabled = true
            }else if index == nil && indexPath.row == 0 {
                cell.txtfeildReceiverName.isUserInteractionEnabled = true
                cell.txtfeildReceiverNumber.isUserInteractionEnabled = true
            }else{
                cell.txtfeildReceiverName.isUserInteractionEnabled = false
                cell.txtfeildReceiverNumber.isUserInteractionEnabled = false
            }
        }else{
            cell.btnRemove.isHidden = true
            cell.lblProductNAme.text = "ITEM NAME"
            cell.txtfeildProductName.placeholder = "Item Name"
            cell.lblProductRecivername.text = "RECEIVER NAME"
            cell.lblProductReciverNumber.text = "RECEIVER NUMBER"
            cell.storeNameField.superview?.isHidden = true
            cell.storeNumberField.superview?.isHidden = true
            cell.txtfeildReceiverName.isUserInteractionEnabled = true
            cell.txtfeildReceiverNumber.isUserInteractionEnabled = true
        }
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(remove(sender:)), for: .touchUpInside)
        cell.stackviewReceiverName.isHidden = false
        cell.stackviewReceiverNumber.isHidden = false
        if item.type == .pick {
            cell.stackviewReceiverName.isHidden = true
            cell.stackviewReceiverNumber.isHidden = true
            cell.lblLandMark.superview?.isHidden = false
        }else{
            cell.stackviewReceiverName.isHidden = false
            cell.stackviewReceiverNumber.isHidden = false
           // cell.lblLandMark.superview?.isHidden = true
            cell.lblLandMark.superview?.isHidden = false
        }

        return cell


    }


    @objc func remove(sender:UIButton){
        let alert = UIAlertController(title: "Alert", message: "Are you sure to delete the item", preferredStyle: .alert)
        alert.addAction(.init(title: "No", style: .cancel, handler: nil))
        alert.addAction(.init(title: "Delete", style: .destructive, handler: { _ in
            self.addressesItems.remove(at: sender.tag)
            self.tableview.reloadData()
        }))
        self.present(alert, animated: true)
    }





    func textFieldDidEndEditing(_ textField: UITextField) {

        if addressesItems.count != 0 {
            let row = textField.tag

            if let cell: TVEnterOrderDetails = self.tableview.cellForRow(at: IndexPath(row: row, section: 0)) as? TVEnterOrderDetails{

            switch textField {
            case cell.txtfeildProductName:
                addressesItems[row].productName = textField.text
            case cell.txtfeildReceiverName:
                addressesItems[row].name = textField.text
                if serId == 15 && row == 0{
                    tableview.reloadData()
                }
            case cell.txtfeildReceiverNumber:
                addressesItems[row].phone = textField.text
                if serId == 15 && row == 0{
                    tableview.reloadData()
                }
            case cell.lblLandMark:
                addressesItems[row].landmark = textField.text
            case cell.storeNameField:
                addressesItems[row].storeName = textField.text
            case cell.storeNumberField:
                addressesItems[row].storeNumber = textField.text
            default:
                break
            }

        }

        }

    }


    func textViewDidEndEditing(_ textView: UITextView) {
        if addressesItems.count != 0 {
        let row = textView.tag
        addressesItems[row].description = textView.text
        }
    }

}



class TVEnterOrderDetails: UITableViewCell {

    @IBOutlet weak var addressStack: UIStackView!
    @IBOutlet weak var txtfeildProductName: UITextField!
    @IBOutlet weak var txtfeildProductWeight: UITextField!
    @IBOutlet weak var txtfeildReceiverName: UITextField!
    @IBOutlet weak var txtfeildReceiverNumber: UITextField!
    @IBOutlet weak var txtVwAdditionalinformation: UITextView!
    @IBOutlet weak var stackviewProductName: UIStackView!
    @IBOutlet weak var stackviewProductWeight: UIStackView!
    @IBOutlet weak var stackviewReceiverName: UIStackView!
    @IBOutlet weak var stackviewReceiverNumber: UIStackView!
    @IBOutlet weak var stackviewAdditionalinfo: UIStackView!
    @IBOutlet weak var vwInfo: UIView!
    @IBOutlet weak var lblLocationName: UILabel!
    @IBOutlet weak var btnRemoveImage: UIButton!
    @IBOutlet weak var lblProductNAme: UILabel!

    @IBOutlet weak var lblProductWeight: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var btnAddimage: UIButton!
    @IBOutlet weak var lblProductReciverNumber: UILabel!
    @IBOutlet weak var lblAdditionalInfo: UILabel!
    @IBOutlet weak var lblLandMark: UITextField!

    @IBOutlet weak var lblProductRecivername: UILabel!
    @IBOutlet weak var btnPick: UIButton!
    @IBOutlet weak var btnDrop: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    var img:UIImage?
    @IBOutlet weak var storeNameField: UITextField!
    @IBOutlet weak var storeNumberField: UITextField!
    @IBOutlet weak var btnRemove: UIButton!




}






////
////  EnterOrderDetailsViewController.swift
////  RoyoRide
////
////  Created by admin on 24/12/20.
////  Copyright © 2020 CodeBrewLabs. All rights reserved.
////
//
//import UIKit
//var count = 1
//
//var serviceReqGlobal: ServiceRequest?
//let arrStopsGlobal = NSMutableArray()
//let arrPickupGlobal = NSMutableArray()
//let arrDropoffGlobal = NSMutableArray()
//
//
//
//var arrFilterStops = NSMutableArray()
//var arrpi = Int()
//
//
//
//
//protocol PikkupViewDelegate {
//    func omcoPickUP()
//    func omcoReceive()
//}
//
//
//class EnterOrderDetailsViewController: UIViewController, UITextViewDelegate {
//    static let shared = EnterOrderDetailsViewController()
//    var serviceRequest : ServiceRequest?
//
//    //MARK::- IBOutlets
//    @IBOutlet weak var buttonadd: UIButton!
//    @IBOutlet weak var tableview: UITableView!
//    @IBOutlet weak var btnsubmit: UIButton!
//
//
//
//
//
//    //MARK::- Variable
//    var delegates: ProductDetail?
//    var delegate:PikkupViewDelegate?
//    var productName:String = ""
//    var recieverName:String = ""
//    var productWeight:String = ""
//    var phoneNumber:String = ""
//    var lat:String = ""
//    var long:String = ""
//    var address: String = ""
//    var image:String = ""
//    var additionalInfo: String = ""
//    var dictProduct = NSDictionary()
//    var index:Int?
//    var serviceReq: ServiceRequest?
//    let arrStops = NSMutableArray()
//
//    let arrDropoff = NSMutableArray()
//    var arrPickup = NSMutableArray()
//    var selectedAddressType: String = ""
//    var isFirst: Bool = true
//    var uploadImg : UIImage?
//    var toggle = false
//    var touch = false
//    var urlImg: String = ""
//
//    var arrayIndex:Int?
//    var arr = [String]()
//    var isSeleced:Bool?
//    //var userImg:UIImage?
//
//    //MARK::- ViewController life cycle
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//
//        if self.title == "editProduct" {
//            showData()
//        }
//
//
//
//        tableview.delegate = self
//        tableview.dataSource = self
//
//        buttonadd.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
//        btnsubmit.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
//
//
//        print("Peoduct Arr \(arrFinalProduct)")
//        print("addressArr \(arrFilterStops)")
//
//
//
//
//
//
//
//        for i in 0 ..< /arrFinalProduct.count {
//            let dict = arrFinalProduct[i] as? NSDictionary
//            let name = dict?["address"] as? String ?? ""
//
//            let searchPredicate = NSPredicate(format: "address CONTAINS[C] %@", /name)
//            let arrFilter = (arrFilterStops).filtered(using: searchPredicate)
//            let arrPiclupLoc = (arrPickup).filtered(using: searchPredicate)
//            let arrDropOffLoc = (arrDropoff).filtered(using: searchPredicate)
//            if arrFilter.count > 0 {
//                arrFilterStops.remove(arrFilter[0])
//
//            }
//            if arrPiclupLoc.count > 0 {
//                arrPickup.remove(arrPiclupLoc[0])
//
//            }
//            if arrDropOffLoc.count > 0 {
//                arrDropoff.remove(arrDropOffLoc[0])
//
//            }
//
//
//        }
//
//        print("afterFilterArr \(arrFilterStops)")
//
//       // arrPickup = NSMutableArray()
//        let pickupDict = ["address":/serviceReq?.locationNameDest,
//                          "lat":/serviceReq?.latitudeDest,
//                          "long":/serviceReq?.longitudeDest,
//                          "seletedType":"Pickup details",
//                          "type": "0"] as [String : Any]
//
//        arrPickup.add(pickupDict)
//        arrStops.add(pickupDict)
//
//
//
//        for i in 0 ..< /serviceReq?.stops.count {
//            let dict = ["address":/serviceReq?.locationNameDest,
//                        "lat":/serviceReq?.latitudeDest, //serviceReq?.stops[i].latitude
//                        "long":/serviceReq?.longitudeDest,
//                        "seletedType":"Dropoff location-\(i+1) details",
//                        "type": "1"] as [String : Any]
//
//            arrDropoff.add(dict)
//            arrStops.add(dict)
//            //  arrFilterStops.add(dict)
//            //arrStopsGlobal.add(dict)
//            //print(arrStopsGlobal)
//        }
//        let dict = ["address":/serviceReq?.locationName,
//                    "lat":/serviceReq?.latitude,
//                    "long":/serviceReq?.longitude,
//                    "seletedType": /serviceReq?.stops.count > 0 ? "Dropoff location \(/serviceReq?.stops.count+1) details":"Dropoff location",
//                    "type": "1"] as [String : Any]
//        arrDropoff.add(dict)
//        arrStops.add(dict)
//        //arrFilterStops.add(dict)
//        //arrDropoffGlobal.add(dict)
//        //print(arrDropoffGlobal)
//
//
//        print(arrStops)
//    }
//
//    @objc func openPhotoOptionsActionSheet(buttonType: UIButton) {
//        self.view.endEditing(true)
//
//
//        CameraImage.shared.captureImage(from: self, At: self , mediaType: nil, captureOptions: [.camera, .photoLibrary], allowEditting: true) { [unowned self] (image) in
//            guard let img = image else { return }
//            let index = IndexPath(row: buttonType.tag, section: 0)
//            let cell = self.tableview.cellForRow(at: index) as? TVEnterOrderDetails
//
//            self.uploadImg = img
//            cell?.userImage?.image = img
//            isImageUpload = true
//            // cell?.btnAddimage?.setImage(img, for: .normal)
//            cell?.btnRemoveImage?.isHidden = true
//
//
//
//        }
//    }
//
//
//    func showData()  {
//        productName = dictProduct["product_name"] as? String ?? ""
//        productWeight = dictProduct["product_weight"] as? String ?? ""
//        recieverName = dictProduct["receiver_name"] as? String ?? ""
//        phoneNumber = dictProduct["receiver_number"] as? String ?? ""
//        additionalInfo = dictProduct["additional_info"] as? String ?? ""
//        lat = dictProduct["latitude"] as? String ?? ""
//        long = dictProduct["longitude"] as? String ?? ""
//        address = dictProduct["address"] as? String ?? ""
//        selectedAddressType = dictProduct["addressType"] as? String ?? ""
//        btnsubmit.setTitle("Update", for: .normal)
//        uploadImg = dictProduct["user_Image"] as? UIImage
//       // lblStopName.text = address
//        isFirst = false
//        print(selectedAddressType)
//
////        if serId == 15{
////            lat = "\(/serviceReq?.latitudeDest)"
////            long =  "\(/serviceReq?.longitudeDest)"
////            address = "\(/serviceReq?.locationNameDest)"
////
////        }
//
//
////        if selectedAddressType == "Pickup details"{
////
////            btnDropoffCheckBox.isSelected = false
////            btnPickUpCheckBox.isSelected = true
////            //btnDropoffCheckBox.isSelected = true
////        }
////        else{
////            btnPickUpCheckBox.isSelected = false
////            btnDropoffCheckBox.isSelected = true
////            //btnPickUpCheckBox.isSelected = false
////        }
//
//    }
//
//
//    //MARK::- IBAction
//    @IBAction func backbtn(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
//    }
//
//    @IBAction func ActionAdd(_ sender: Any) {
//
//    }
//    @IBAction func actionSelectStops(_ sender: UIButton) {
//
//
//        if self.title != "editProduct" {
//
//            arr = [String]()
//
//            for i in 0..<arrFilterStops.count {
//                let dict = arrFilterStops[i] as? NSDictionary ?? NSDictionary()
//                arr.append(dict["address"] as? String ?? "")
//            }
//
//            showDropDown(view: sender, datasource: arr)
//        }
//    }
//
//    @IBAction func actionbtnsubmit(_ sender: UIButton) {
//
//            imageNotUpload()
//
//
//    }
//
//    @objc func buttonPressed(_ sender: AnyObject) {
//        count = count + 1
//
//    }
//
//    func imageNotUpload(){
//        if isImageUpload{
//
//            getFileUpload()
//
//        }else{
//            isImageUpload = false
//            if self.productName == "" {
//                Alerts.shared.show(alert: "AppName".localizedString, message: "Enter product name" , type: .error )
//                return
//            }
//
//            if self.selectedAddressType == "Pickup details" {
//                self.recieverName = ""
//                self.phoneNumber = ""
//
//            }
//
//            if self.title == "editProduct" {
//
//
//                self.delegates?.editProductDetails(self.productName, self.productWeight, self.recieverName, self.phoneNumber, self.additionalInfo, self.index, self.lat, self.long, self.address,  self.selectedAddressType, self.uploadImg, "")
//            }else{
//                self.delegates?.addProductDetails(self.productName, self.productWeight, self.recieverName, self.phoneNumber, self.additionalInfo, self.lat, self.long, self.address, self.selectedAddressType, self.uploadImg, "")
//            }
//
//            self.dismiss(animated: true, completion: nil)
//
//        }
//
//    }
//
//
//    func getFileUpload() {
//
//
//
//
//        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
//
//
//        let file_upload = BookServiceEndPoint.fileUpload
//
//
//        file_upload.request(image: uploadImg, filename: "", header:  ["language_id" : LanguageFile.shared.getLanguage() , "access_token" : token]) { [weak self] (response) in
//
//
//
//            switch response {
//            case .success(let data):
//                isImageUpload = true
//                let dict = data as? NSDictionary ?? NSDictionary()
//
//                let url = dict["result"] as? String ?? ""
//
//                self?.view.endEditing(true)
//                if self?.productName == "" {
//                    Alerts.shared.show(alert: "AppName".localizedString, message: "Enter product name" , type: .error )
//                    return
//                }
//
//                if self?.selectedAddressType == "Pickup details" {
//                    self?.recieverName = ""
//                    self?.phoneNumber = ""
//
//                }
//
//
//                if self?.title == "editProduct" {
//
//                    self?.delegates?.editProductDetails(self?.productName, self?.productWeight, self?.recieverName, self?.phoneNumber, self?.additionalInfo, self?.index, self?.lat, self?.long, self?.address,  self?.selectedAddressType, self?.uploadImg,url)
//                }else{
//                    self?.delegates?.addProductDetails(self?.productName, self?.productWeight, self?.recieverName, self?.phoneNumber, self?.additionalInfo, self?.lat, self?.long, self?.address, self?.selectedAddressType, self?.uploadImg, url)
//                }
//
//                self?.dismiss(animated: true, completion: nil)
//
//            case .failure(let strError):
//                Alerts.shared.show(alert: "AppName".localizedString, message:/strError , type: .error )
//            }
//        }
//    }
//
//
////    @objc @IBAction func ActionBtnPickup(_ sender: UIButton) {
////
////        if sender.isSelected{
////            sender.isSelected = false
////            btnDropoffCheckBox.isSelected = false
////
////
////
////
////        }
////        else{
////            sender.isSelected = true
////            btnDropoffCheckBox.isSelected = false
////            lblStopName.text = ""
////            tableview.reloadData()
////        }
////
////
////
////
////
////        for i in 0 ..< /arrFinalProduct.count {
////            let dict = arrFinalProduct[i] as? NSDictionary
////            let name = dict?["address"] as? String ?? ""
////
////            let searchPredicate = NSPredicate(format: "address CONTAINS[C] %@", /name)
////
////            let arrPiclupLoc = (arrPickup).filtered(using: searchPredicate)
////
////
////
////            if arrPiclupLoc.count > 0 {
////                arrPickup.remove(arrPiclupLoc[0])
////
////            }
////
////
////
////
////        }
////
////
////        print(arrPickup.count)
////
////        arrFilterStops = NSMutableArray()
////
////        for i in 0..<arrPickup.count {
////            let dict = arrPickup[i] as? NSDictionary ?? NSDictionary()
////
////
////            arrFilterStops.add(dict)
////        }
////
////
////    }
//
////
////    @objc @IBAction func ActionBtnDropoff(_ sender: UIButton) {
////
////        if sender.isSelected {
////            sender.isSelected = false
////            btnPickUpCheckBox.isSelected = false
////
////        }
////        else{
////            sender.isSelected = true
////            btnPickUpCheckBox.isSelected = false
////            lblStopName.text = ""
////
////            tableview.reloadData()
////        }
////
////
////        for i in 0 ..< /arrFinalProduct.count {
////            let dict = arrFinalProduct[i] as? NSDictionary
////            let name = dict?["address"] as? String ?? ""
////
////            let searchPredicate = NSPredicate(format: "address CONTAINS[C] %@", /name)
////
////            let arrDropOffLoc = (arrDropoff).filtered(using: searchPredicate)
////
////            if arrDropOffLoc.count > 0 {
////                arrDropoff.remove(arrDropOffLoc[0])
////
////            }
////
////
////        }
////
////        arrFilterStops = NSMutableArray()
////        for i in 0..<arrDropoff.count {
////            let dict = arrDropoff[i] as? NSDictionary ?? NSDictionary()
////
////
////            arrFilterStops.add(dict)
////        }
////
////    }
//
//    func showDropDown(view : UIView, datasource: [String]) {
//
//        Utility.shared.showDropDown(anchorView: view, dataSource: datasource , width: view.frame.size.width, handler: {[weak self] (index, strValu) in
//
//            let dict = arrFilterStops[index] as? NSDictionary
//
//           // self?.lblStopName.text = dict?["address"] as? String ?? ""
//            print(arrFilterStops.count)
//
//            self?.lat = "\(dict?["lat"] as? Double ?? 0.00)"
//            self?.long = "\(dict?["long"] as?  Double ?? 0.00)"
//            self?.address = dict?["address"] as? String ?? ""
//            self?.selectedAddressType = dict?["seletedType"] as? String ?? ""
//            self?.isFirst = false
//            self?.tableview.reloadData()
//        })
//    }
//}
//
////
//extension EnterOrderDetailsViewController:UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return count
//    }
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 650
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "TVEnterOrderDetails", for: indexPath) as! TVEnterOrderDetails
//        cell.txtfeildProductName.delegate = self
//        cell.txtfeildProductWeight.delegate = self
//       // cell.txtfeildReceiverName.delegate = self
//       // cell.txtfeildReceiverNumber.delegate = self
//        cell.txtVwAdditionalinformation.delegate = self
//        //  cell.userImage.delegate = self
//
//
//
//
//
//
//
//
//
//
//
//
//        cell.btnAddimage.tag = indexPath.row
//        cell.btnAddimage.addTarget(self, action: #selector(openPhotoOptionsActionSheet), for: .touchUpInside)
//
//        cell.txtfeildProductName.text = productName
//        cell.txtfeildProductWeight.text = productWeight
////        cell.txtfeildReceiverName.text = recieverName
////        cell.txtfeildReceiverNumber.text = phoneNumber
//        cell.txtVwAdditionalinformation.text = additionalInfo
//        cell.userImage.image = uploadImg
//
//
//        cell.txtVwAdditionalinformation.setAlignment()
//        cell.txtVwAdditionalinformation.setBorderColorSecondary()
//        cell.txtVwAdditionalinformation.addShadowToTextViewColorSecondary()
//
//
//        cell.txtfeildProductName.setAlignment()
//        cell.txtfeildProductName.setBorderColorSecondary()
//        cell.txtfeildProductName.addShadowToTextFieldColorSecondary()
//
//
//
//        cell.txtfeildProductWeight.setAlignment()
//        cell.txtfeildProductWeight.setBorderColorSecondary()
//        cell.txtfeildProductWeight.addShadowToTextFieldColorSecondary()
//
//
////        cell.txtfeildReceiverName.setAlignment()
////        cell.txtfeildReceiverName.setBorderColorSecondary()
////        cell.txtfeildReceiverName.addShadowToTextFieldColorSecondary()
////
////
////        cell.txtfeildReceiverNumber.setAlignment()
////        cell.txtfeildReceiverNumber.setBorderColorSecondary()
////        cell.txtfeildReceiverNumber.addShadowToTextFieldColorSecondary()
//
//
//
////        if btnPickUpCheckBox.isSelected{
////            cell.stackviewReceiverName.isHidden = true
////
////                        cell.txtfeildProductName.text = ""
////                        cell.txtfeildProductWeight.text = ""
////                        cell.txtVwAdditionalinformation.text = ""
////
////        }else{
////                        cell.txtfeildProductName.text = ""
////                        cell.txtfeildProductWeight.text = ""
////                        cell.txtVwAdditionalinformation.text = ""
////
////        }
////        if selectedAddressType == "Pickup details" {
////            cell.stackviewReceiverName.isHidden = true
////            cell.stackviewReceiverNumber.isHidden = true
//////            cell.txtfeildProductName.text = ""
//////            cell.txtfeildProductWeight.text = ""
//////            cell.txtVwAdditionalinformation.text = ""
////
////        }else{
////            cell.stackviewReceiverName.isHidden = false
////            cell.stackviewReceiverNumber.isHidden = false
//////            cell.txtfeildProductName.text = ""
//////            cell.txtfeildProductWeight.text = ""
//////            cell.txtVwAdditionalinformation.text = ""
//////            cell.txtfeildReceiverName.text = ""
//////            cell.txtfeildReceiverNumber.text = ""
////        }
//
//     //   cell.stckVwDestinationName.isHidden = isFirst
//       //
//     //   cell.lblLocationName.text = selectedAddressType
//        return cell
//    }
//
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        let index = IndexPath(row: 0, section: 0)
//        let cell: TVEnterOrderDetails = self.tableview.cellForRow(at: index) as! TVEnterOrderDetails
//
//        productName = /cell.txtfeildProductName.text
//        productWeight = /cell.txtfeildProductWeight.text
//    //    recieverName = /cell.txtfeildReceiverName.text
//      //  phoneNumber = /cell.txtfeildReceiverNumber.text
//        uploadImg = /cell.userImage.image
//
//
//
//        //tableview.reloadData()
//
//    }
//
//
//    func textViewDidEndEditing(_ textView: UITextView) {
//        let index = IndexPath(row: 0, section: 0)
//        let cell: TVEnterOrderDetails = self.tableview.cellForRow(at: index) as! TVEnterOrderDetails
//        additionalInfo = /cell.txtVwAdditionalinformation.text
//    }
//
//}
//
//
//
//
////
//
//
//class TVEnterOrderDetails: UITableViewCell {
//
//    @IBOutlet weak var txtfeildProductName: UITextField!
//    @IBOutlet weak var txtfeildProductWeight: UITextField!
//    //@IBOutlet weak var txtfeildReceiverName: UITextField!
//    //@IBOutlet weak var txtfeildReceiverNumber: UITextField!
//    @IBOutlet weak var txtVwAdditionalinformation: UITextView!
//    @IBOutlet weak var stackviewProductName: UIStackView!
//    @IBOutlet weak var stackviewProductWeight: UIStackView!
//  //  @IBOutlet weak var stackviewReceiverName: UIStackView!
//   // @IBOutlet weak var stackviewReceiverNumber: UIStackView!
//    @IBOutlet weak var stackviewAdditionalinfo: UIStackView!
//    @IBOutlet weak var vwInfo: UIView!
//   // @IBOutlet weak var stckVwDestinationName: UIStackView!
//   // @IBOutlet weak var lblLocationName: UILabel!
//    @IBOutlet weak var btnRemoveImage: UIButton!
//
//    @IBOutlet weak var userImage: UIImageView!
//    @IBOutlet weak var btnAddimage: UIButton!
//    var img:UIImage?
//
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
//
//
//
//    @IBAction func btnremove(_ sender: Any) {
//
//
//
//    }
//
//}
//
//
