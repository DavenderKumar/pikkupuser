//
//  ShowOrderDetailsViewController.swift
//  RoyoRide
//
//  Created by admin on 24/12/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit

//protocol ProductDetail {
//    func addProductDetails(_ productName : String?, _ productWeight : String?, _ receiverName: String?, _ receiverNumber: String?, _ additionalInfo: String?, _ lat:String?, _ long:String?, _ address: String?, _ addressType: String?, _ userImage:UIImage?, _ imgUrl: String?)
//
//
//
//    func editProductDetails(_ productName : String?, _ productWeight : String?, _ receiverName: String?, _ receiverNumber: String?, _ additionalInfo: String?, _ index: Int?, _ lat:String?, _ long:String?, _ address: String?, _ addressType: String?, _ userImage:UIImage?, _ imgUrl:String?)
//}


protocol ProductDetailDelegate {

   func addAddressesWithDetails(items:[AddressItem])

    func editAddressWithDetails(request:ServiceRequest?)
    func back()

}

var arrFinalProduct = NSMutableArray()
var arrProduct = NSMutableArray()


var isImageUpload:Bool = false

class ShowOrderDetailsViewController: UIViewController{
    //MARK::- IBOutlet
    @IBOutlet weak var btnadd: UIButton!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblStopName: UILabel!
    @IBOutlet weak var btnSelectStops: UIButton!
    @IBOutlet weak var vwStop: UIView!
    @IBOutlet weak var stckVwDestination: UIStackView!
    @IBOutlet weak var lblDestinationName: UILabel!
    @IBOutlet weak var constHeightDestination: NSLayoutConstraint!
    @IBOutlet weak var stackViewSelectLocation: UIStackView!
    @IBOutlet weak var selectLocationheight: NSLayoutConstraint!
    //MARK::- Veriables
    
    
    var delegates: BookRequestDelegate?
    var serviceRequest : ServiceRequest?
    var arrStops = NSMutableArray()
    
    var originalCount = 0
    
    
    
    
    //MARK::- Viewcontroller life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
  
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let arr = serviceRequest?.addressItems ?? []
         originalCount = arr.count
         
         
 //
 //        tableview.delegate = self
 //        tableview.dataSource = self
 //        //btnadd.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
 //        btnContinue.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
 //        //vwStop.setViewBorderColorSecondary()
 //        checkArrCount()
 //
 //        vwStop.setViewBorderColorSecondary()
 //        vwStop.addShadowToViewColorSecondary()
 //        btnadd.setButtonWithTitleAndBorderColorSecondary()
 //        stckVwDestination.isHidden = true
 //        constHeightDestination.constant = 0
 //
 //
 //
 //
 //        stackViewSelectLocation.isHidden = false
 //
 ////        if serId == 15{
 ////            stackViewSelectLocation.isHidden = true
 ////            selectLocationheight.constant = 0
 ////
 ////        }else{
 ////            stackViewSelectLocation.isHidden = false
 ////            RemoveAddBtn()
 ////        }
         
         
         
         tableview.delegate = self

                tableview.dataSource = self

                btnadd.setButtonWithBackgroundColorThemeAndTitleColorBtnText()

                btnContinue.setButtonWithBackgroundColorThemeAndTitleColorBtnText()

                vwStop.setViewBorderColorSecondary()

                checkArrCount()

                

              //  vwStop.setViewBorderColorSecondary()

            //    vwStop.addShadowToViewColorSecondary()

                btnadd.setButtonWithTitleAndBorderColorSecondary()
         
         

                stckVwDestination.isHidden = true

                constHeightDestination.constant = 0

                let pickupDict = ["address":/serviceRequest?.locationNameDest,

                            "lat":/serviceRequest?.latitudeDest,

                            "long":/serviceRequest?.longitudeDest,

                            "seletedType":"Pickup details"] as [String : Any]

                

                arrStops.add(pickupDict)
         

                

                for i in 0 ..< /serviceRequest?.stops.count {

                    let dict = ["address":/serviceRequest?.stops[i].address,

                                "lat":/serviceRequest?.stops[i].latitude,

                                "long":/serviceRequest?.stops[i].longitude,

                                "seletedType":"Dropoff location-\(i+1) details"] as [String : Any]

                    arrStops.add(dict)
                

                }

                let dict = ["address":/serviceRequest?.locationName,

                            "lat":/serviceRequest?.latitude,

                            "long":/serviceRequest?.longitude,

                            "seletedType": /serviceRequest?.stops.count > 0 ? "Dropoff location-\(/serviceRequest?.stops.count+1) details":"Dropoff location"] as [String : Any]

                arrStops.add(dict)
         

                

                let dict1 = ["address":"All",

                            "lat":"",

                            "long":"",

                            "seletedType":""] as [String : Any]

                arrStops.add(dict1)
        

        //        if serId == 15{

        //            stackViewSelectLocation.isHidden = true

        //

        //        }else{

        //            stackViewSelectLocation.isHidden = false

        //            RemoveAddBtn()

        //        }

         
         arrFinalProduct.removeAllObjects()
         arrProduct.removeAllObjects()
         
         for item in serviceRequest?.addressItems ?? []{
             var addressType:String
             if let stopNumber = item.stopNumber{
                 addressType = serId == 15 ? "ITEM \(stopNumber)" : "DROP OFF DETAILS \(stopNumber)"
             }else{
                 addressType = item.type.rawValue
             }

             let dictionary = [
                 "product_name": item.productName ?? "",
                 "product_weight":"",
                 "store_name":item.storeName ?? "",
                 "store_phone":item.storeNumber ?? "",
                 "receiver_name":item.name ?? "",
                 "receiver_number":item.phone ?? "",
                 "additional_info":item.description ?? "",
                 "latitude":item.latitude ?? 0,
                 "longitude":item.longitude ?? 0,
                 "address":item.address ?? "",
                 "addressType":addressType,
                 "image":item.image,
                 "url_image" : item.imageUrl ?? "",
                 "landmark_info" : item.landmark ?? ""] as [String : Any]
             arrProduct.add(dictionary)
             arrFinalProduct.add(dictionary)
         }
         
             
        
         
         print(arrProduct)
         print(arrFinalProduct)

         print(arrStops)
        
    }
    
    
    func addProductDetails(_ productName: String?, _ productWeight: String?, _ receiverName: String?, _ receiverNumber: String?, _ additionalInfo: String?, _ lat:String?, _ long:String?, _ address:String?, _ addressType: String?, _ userImage:UIImage?, _ url: String?) {

          let dict = ["product_name": /productName,

                      "product_weight":/productWeight,

                      "receiver_name":/receiverName,

                      "receiver_number":/receiverNumber,

                      "additional_info":/additionalInfo,

                      "latitude":/lat,

                      "longitude":/long,

                      "address":/address,

                      "addressType":/addressType,

                      "user_Image":/userImage,

                      "url_image": /url

              ] as [String : Any]

          arrProduct.add(dict)

          arrFinalProduct.add(dict)

          

  //        if  serId == 15{

  //            stackViewSelectLocation.isHidden = true

  //

  //        }else{

  //            stackViewSelectLocation.isHidden = false

  //            RemoveAddBtn()

  //        }

          

          tableview.reloadData()

      }

      

      func editProductDetails(_ productName: String?, _ productWeight: String?, _ receiverName: String?, _ receiverNumber: String?, _ additionalInfo: String?, _ index: Int?, _ lat:String?, _ long:String?, _ address:String?, _ addressType: String?, _ userImage:UIImage?,_ url: String?) {

          let dict = ["product_name": /productName,

                      "product_weight":/productWeight,

                      "receiver_name":/receiverName,

                      "receiver_number":/receiverNumber,

                      "additional_info":/additionalInfo,

                      "latitude":/lat,

                      "longitude":/long,

                      "address":/address,

                      "addressType":/addressType,

                      "user_Image" : /userImage,

              "url_image": /url] as [String : Any]

          

         // arrProduct.remove(/index)

          let dictOld = arrFinalProduct[/index]

          arrProduct.remove(dictOld)

          arrFinalProduct.remove(dictOld)

          arrProduct.add(dict)

          arrFinalProduct.add(dict)

          

  //        if  serId == 15{

  //            stackViewSelectLocation.isHidden = true

  //

  //        }else{

  //            stackViewSelectLocation.isHidden = false

  //            RemoveAddBtn()

  //        }

          

          tableview.reloadData()

      }
    
    func RemoveAddBtn()  {
        
    }
    
    
    
    
    //MARK::- IBAction
    @IBAction func actionbtnadd(_ sender: Any) {
        
        stckVwDestination.isHidden = true
        constHeightDestination.constant = 0
        lblStopName.text = "Select Locations"
        guard let vc = R.storyboard.orderDetails.enterOrderDetailsViewController() else{return}
        
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .coverVertical
        vc.serviceReq = serviceRequest
        presentVC(vc, false)
    }
    
    @IBAction func actionBtnContinue(_ sender: Any) {
       // moveToNextPopUp()
        
      
        if arrFinalProduct.count != originalCount
            {
            Alerts.shared.show(alert: R.string.localizable.appName(), message: "Please add all Pickup/DropOff location", type: .info)
        }
        else
        {
            var items = [[String:Any]]()
            for item in serviceRequest?.addressItems ?? []{
                var addressType:String
                if let stopNumber = item.stopNumber{
                    addressType = serId == 15 ? "ITEM \(stopNumber)" : "DROP OFF DETAILS \(stopNumber)"
                }else{
                    addressType = item.type.rawValue
                }

                let dictionary = [
                    "product_name": item.productName ?? "",
                    "product_weight":"",
                    "store_name":item.storeName ?? "",
                    "store_phone":item.storeNumber ?? "",
                    "receiver_name":item.name ?? "",
                    "receiver_number":item.phone ?? "",
                    "additional_info":item.description ?? "",
                    "latitude":item.latitude ?? 0,
                    "longitude":item.longitude ?? 0,
                    "address":item.address ?? "",
                    "addressType":addressType,
                    "url_image" : item.imageUrl ?? "",
                    "landmark_info" : item.landmark ?? ""] as [String : Any]
                items.append(dictionary)
            }
            if let jsonString = convertIntoJSONString(arrayObject: items){
                moveToNextPopUp(product_detail:jsonString)
            }
        }
        
        
        
    }
    

    
    @IBAction func actionBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        delegates?.didBackFromSlot(type: .Backward)
    }
    
    func checkArrCount() {

            btnContinue.isHidden = arrProduct.count > 0 ? false:true

            lblNote.isHidden = arrProduct.count > 0 ? true:false

            tableview.isHidden = arrProduct.count > 0 ? false:true

        }
    
//    func checkArrCount() {
//        btnContinue.isHidden = serviceRequest?.addressItems?.count ?? 0 > 0 ? false:true
//        lblNote.isHidden = serviceRequest?.addressItems?.count ?? 0 > 0 ? true:false
//        tableview.isHidden = serviceRequest?.addressItems?.count ?? 0 > 0 ? false:true
//    }
    @IBAction func actionSelectStops(_ sender: UIButton) {
        
        var arr = [String]()
        for i in 0..<arrStops.count {
            let dict = arrStops[i] as? NSDictionary ?? NSDictionary()
            arr.append(dict["address"] as? String ?? "")
        }
        //let array = arrStops.map({/($0 as! String).address})
        showDropDown(view: sender, datasource: arr)
    }
  
    
    func showDropDown(view : UIView, datasource: [String]) {
        
        Utility.shared.showDropDown(anchorView: view, dataSource: datasource , width: view.frame.size.width, handler: {[weak self] (index, strValu) in

            arrFinalProduct.removeAllObjects()

            let dict = self?.arrStops[index] as? NSDictionary

            self?.lblDestinationName.text = dict?["seletedType"] as? String ?? ""
            self?.lblStopName.text = dict?["address"] as? String ?? ""


            if arrProduct.count > 0  {
                if dict?["address"] as? String ?? "" == "All" {
                   // arrFinalProduct = NSMutableArray()
                    arrFinalProduct = NSMutableArray.init(array: arrProduct)
                    self?.stckVwDestination.isHidden = true
                    self?.constHeightDestination.constant = 0
                }else{
                    self?.stckVwDestination.isHidden = false
                    self?.constHeightDestination.constant = 0
                    let searchPredicate = (NSPredicate(format: "address CONTAINS[C] %@", /self?.lblStopName.text))
                   
                    
                     let arr = (arrProduct).filtered(using: searchPredicate)
                
                        arrFinalProduct = NSMutableArray.init(array: arr )
                        if arr.count == 0 {
                            self?.lblNote.text = "No data found"
                            self?.lblNote.isHidden = false
                            self?.tableview.isHidden = true
                        }else
                        {
                            self?.lblNote.isHidden = true
                            self?.tableview.isHidden = false
                        }
                    
                  
                }
            }


            print(arrFinalProduct)

            self?.tableview.reloadData()

        })
    }
}



extension ShowOrderDetailsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFinalProduct.count
        //return serviceRequest?.addressItems?.count ?? 0
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "ListOrderDetails", for: indexPath) as! ListOrderDetails

        let item = arrFinalProduct[indexPath.row] as? NSDictionary

        cell.productname.text = item?["product_name"] as? String
        
        cell.stackviewproductweight.isHidden = true
        
        if let info = item?["additional_info"] as? String,!info.isEmpty{
            cell.stackviewadditionalinfo.isHidden = false
            cell.additionalinformation.text = info
        }else{
            cell.stackviewadditionalinfo.isHidden = true
        }
//
//
//        if serId == 15{
//            cell.phoneHintLabel.text = "Store Phone Number:"
//            cell.nameHintLabel.text = "Store Name:"
//        }else{
        cell.phoneHintLabel.text = "Receiver Phone Number:"
        cell.nameHintLabel.text = "Receiver Name:"
//        }
//
        cell.btnOnImage.tag = indexPath.row
        cell.buttonedit.tag = indexPath.row
        cell.buttondelete.tag = indexPath.row

        if let stopNumber = item?["stopNumber"] as? String{
            cell.titleLocation.text = serId == 15 ? "ITEM \(stopNumber)" : "DROP OFF DETAILS \(stopNumber)"
        }else{
            cell.titleLocation.text = item?["addressType"] as? String
        }
        
        
        
        cell.buttonedit.addTarget(self, action: #selector(actionEditProduct), for: .touchUpInside)
        cell.buttondelete.addTarget(self, action: #selector(actionDeleteProduct), for: .touchUpInside)
        cell.vwBg.backgroundColor = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.secondary_colour ?? DefaultColor.color.rawValue, alpha: 0.5)
        cell.vwBg.setViewBorderColorSecondary()
        
        if item?["addressType"] as? String == "Pickup details"//.pick
            {
            cell.stackviewreceivername.isHidden = true
            cell.stackviewreceivernumber.isHidden = true
        }else{
            cell.stackviewreceivername.isHidden = false
            cell.stackviewreceivernumber.isHidden = false
        }
 
        
            if let phone = item?["receiver_number"] as? String,!phone.isEmpty{
                cell.stackviewreceivernumber.isHidden = false
                cell.receivernumber.text = phone
            }else{
                cell.stackviewreceivernumber.isHidden = true
    
            }
    
            if let phone = item?["store_phone"] as? String,!phone.isEmpty{
                cell.storeNumberLabel.superview?.isHidden = false
                cell.storeNumberLabel.text = phone
            }else{
                cell.storeNumberLabel.superview?.isHidden = true
            }
    
            if let name = item?["store_name"] as? String,!name.isEmpty{
                cell.storeNameLabel.superview?.isHidden = false
                cell.storeNameLabel.text = name
            }else{
                cell.storeNameLabel.superview?.isHidden = true
            }
    
            if let name = item?["receiver_name"] as? String,!name.isEmpty{
                cell.stackviewreceivername.isHidden = false
                cell.receivername.text = name
            }else{
                cell.stackviewreceivername.isHidden = true
    
            }
    
            if let landmark = item?["landmark_info"] as? String,!landmark.isEmpty{
                cell.stackViewLAndMark.isHidden = false
                cell.lblLandMark.text = landmark
            }else{
                cell.stackViewLAndMark.isHidden = true
    
            }
        
        let imageValue = item?["image"] as? UIImage
                if  imageValue?.size.width == 0 {
                    cell.stackviewImg.isHidden = true
                }else{
                    cell.imageUser.image = imageValue
                    cell.stackviewImg.isHidden = false
        
                }
               

        
        
                checkArrCount()
                return cell

        

    }

//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "ListOrderDetails", for: indexPath) as! ListOrderDetails
//
//
//
//       let item = serviceRequest?.addressItems?[indexPath.row]
//
//        cell.btnOnImage.addTarget(self, action: #selector(goToLargeImage(_:)), for: .touchUpInside)
//
//        cell.productname.text = item?.productName
//
//        cell.stackviewproductweight.isHidden = true
//
//
//        if let info = item?.description,!info.isEmpty{
//            cell.stackviewadditionalinfo.isHidden = false
//            cell.additionalinformation.text = info
//        }else{
//            cell.stackviewadditionalinfo.isHidden = true
//        }
//
//
////        if serId == 15{
////            cell.phoneHintLabel.text = "Store Phone Number:"
////            cell.nameHintLabel.text = "Store Name:"
////        }else{
//            cell.phoneHintLabel.text = "Receiver Phone Number:"
//            cell.nameHintLabel.text = "Receiver Name:"
////        }
//
//
//
//
//        cell.btnOnImage.tag = indexPath.row
//        cell.buttonedit.tag = indexPath.row
//        cell.buttondelete.tag = indexPath.row
////        if let stopNumber = item?.stopNumber{
////            cell.titleLocation.text = serId == 15 ? "ITEM \(stopNumber)" : "DROP OFF DETAILS \(stopNumber)"
////        }else{
////            cell.titleLocation.text = item?.type.rawValue
////        }
//
//
//
//        cell.buttonedit.addTarget(self, action: #selector(actionEditProduct), for: .touchUpInside)
//        cell.buttondelete.addTarget(self, action: #selector(actionDeleteProduct), for: .touchUpInside)
//        cell.vwBg.backgroundColor = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.secondary_colour ?? DefaultColor.color.rawValue, alpha: 0.5)
//        cell.vwBg.setViewBorderColorSecondary()
//
//        if item?.type == .pick {
//            cell.stackviewreceivername.isHidden = true
//            cell.stackviewreceivernumber.isHidden = true
//        }else{
//            cell.stackviewreceivername.isHidden = false
//            cell.stackviewreceivernumber.isHidden = false
//        }
//
//        if let phone = item?.phone,!phone.isEmpty{
//            cell.stackviewreceivernumber.isHidden = false
//            cell.receivernumber.text = phone
//        }else{
//            cell.stackviewreceivernumber.isHidden = true
//
//        }
//
//        if let phone = item?.storeNumber,!phone.isEmpty{
//            cell.storeNumberLabel.superview?.isHidden = false
//            cell.storeNumberLabel.text = phone
//        }else{
//            cell.storeNumberLabel.superview?.isHidden = true
//        }
//
//        if let name = item?.storeName,!name.isEmpty{
//            cell.storeNameLabel.superview?.isHidden = false
//            cell.storeNameLabel.text = name
//        }else{
//            cell.storeNameLabel.superview?.isHidden = true
//        }
//
//        if let name = item?.name,!name.isEmpty{
//            cell.stackviewreceivername.isHidden = false
//            cell.receivername.text = name
//        }else{
//            cell.stackviewreceivername.isHidden = true
//
//        }
//
//        if let landmark = item?.landmark,!landmark.isEmpty{
//            cell.stackViewLAndMark.isHidden = false
//            cell.lblLandMark.text = landmark
//        }else{
//            cell.stackViewLAndMark.isHidden = true
//
//        }
//        let imageValue = item?.image
//        if  imageValue?.size.width == 0 {
//            cell.stackviewImg.isHidden = true
//        }else{
//            cell.imageUser.image = imageValue
//            cell.stackviewImg.isHidden = false
//
//        }
//
//
//        checkArrCount()
//        return cell
//
//    }

    @objc func goToLargeImage(_ sender:UIButton){

        guard  let vc = R.storyboard.orderDetails.largeImagePikkupViewController() else { return }
        vc.Image = serviceRequest?.addressItems?[sender.tag].image
        ez.topMostVC?.presentVC(vc)

    }



    @objc func actionEditProduct(sender:UIButton) {
        
//        guard let vc = R.storyboard.orderDetails.enterOrderDetailsViewController() else{return}
//        vc.modalPresentationStyle = .overFullScreen
//        vc.modalTransitionStyle = .coverVertical
//        vc.title = "editProduct"
//
//        vc.index = sender.tag
//        vc.dictProduct = arrFinalProduct[sender.tag] as? NSDictionary ?? NSDictionary()
//        vc.serviceReq = serviceRequest
//        vc.delegates = self
//        presentVC(vc, false)
       

        guard let vc = R.storyboard.orderDetails.enterOrderDetailsViewController() else{return}
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .coverVertical
        vc.index = sender.tag
        vc.serviceReq = serviceRequest
        vc.delegate = self
        presentVC(vc, false)

    }


    @objc func actionDeleteProduct(sender:UIButton) {
        
        
                    alertBoxOption(message: "Are you sure you want to delete selected data?", title: "", leftAction: "No", rightAction: "Yes", ok: { [weak self] in
        
                        let dict = arrFinalProduct[sender.tag]
        
                        arrProduct.remove(dict)
        
                        arrFinalProduct.remove(dict)
        
                        
                        self?.serviceRequest?.addressItems?.remove(at: sender.tag)
            //            if  serId == 15{
        
            //                self?.stackViewSelectLocation.isHidden = true
        
            //
        
            //            }else{
        
            //                self?.stackViewSelectLocation.isHidden = false
        
            //                self?.RemoveAddBtn()
        
            //            }
        
        
        
                        self?.tableview.reloadData()
        
                    },cancel: {})


//        alertBoxOption(message: "Are you sure you want to delete selected data?", title: "", leftAction: "No", rightAction: "Yes", ok: { [weak self] in
//            self?.serviceRequest?.addressItems?.remove(at: sender.tag)
//            self?.tableview.reloadData()
//        },cancel: {})
    }
    private func moveToNextPopUp(product_detail:String) {
        if let request = serviceRequest{
            delegates?.didGetRequestDetails(product_detail: product_detail)
            dismiss(animated: true, completion: nil)
        }

    }



}


//extension ShowOrderDetailsViewController:UITableViewDelegate,UITableViewDataSource{
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        return arrFinalProduct.count
//
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//
//        return 1
//
//    }
//
//
//
//    
//
    
//    
//    
//    @objc func actionEditProduct(sender:UIButton) {
//
//            guard let vc = R.storyboard.orderDetails.enterOrderDetailsViewController() else{return}
//
//            
//
//            
//
//            vc.modalPresentationStyle = .overFullScreen
//
//            vc.modalTransitionStyle = .coverVertical
//
//            vc.title = "editProduct"
//
//            vc.index = sender.tag
//
//            vc.dictProduct = arrFinalProduct[sender.tag] as? NSDictionary ?? NSDictionary()
//
//            
//
//            vc.serviceReq = serviceRequest
//
//            vc.delegates = self
//
//            presentVC(vc, false)
//
//           
//
//        }
//
//      
//
//        
//
//        @objc func actionDeleteProduct(sender:UIButton) {
//
//            
//
//           
//
//            alertBoxOption(message: "Are you sure you want to delete selected data?", title: "", leftAction: "No", rightAction: "Yes", ok: { [weak self] in
//
//                let dict = arrFinalProduct[sender.tag]
//
//                arrProduct.remove(dict)
//
//                arrFinalProduct.remove(dict)
//
//    //            if  serId == 15{
//
//    //                self?.stackViewSelectLocation.isHidden = true
//
//    //
//
//    //            }else{
//
//    //                self?.stackViewSelectLocation.isHidden = false
//
//    //                self?.RemoveAddBtn()
//
//    //            }
//
//                
//
//                self?.tableview.reloadData()
//
//            },cancel: {})
//
//        }
////
////    private func moveToNextPopUp() {
////
////
////
////           guard var request = serviceRequest else{return}
////
////           let dict = arrProduct[0] as? NSDictionary
////
////
////
////           request.materialType = dict?["product_name"] as? String
////
////           request.additionalInfo = dict?["additional_info"] as? String
////
////
////
////               request.pickupPersonName = dict?["receiver_name"] as? String
////
////               request.pickupPersonPhone = dict?["receiver_number"] as? String
////
////
////
////
////
////           if let weight = dict?["product_weight"] as? String? {
////
////               request.weight = Double(weight ?? "0.0")
////
////           }
////
////
////
////
////
////           let arr = NSMutableArray()
////
////           for i in 0 ..< /arrProduct.count {
////
////               let dict = arrProduct[i] as? NSDictionary
////
////
////
////               let dict1 = ["product_name": dict?["product_name"]  as? String ,
////
////                                  "product_weight":dict?["product_weight"] as? String,
////
////                                  "receiver_name":dict?["receiver_name"] as? String,
////
////                                  "receiver_number":dict?["receiver_number"] as? String,
////
////                                  "additional_info":dict?["additional_info"] as? String,
////
////                                  "latitude":dict?["latitude"] as? String,
////
////                                  "longitude":dict?["longitude"] as? String,
////
////                                  "address":dict?["address"] as? String,
////
////                                  "addressType":dict?["addressType"] as? String,
////
////                                  "url_image" : dict?["url_image"] as? String] as [String : Any]
////
////
////
////               arr.add(dict1)
////
////           }
////
////
////
////
////
////           request.product_detail = convertIntoJSONString(arrayObject: arr as! [Any])
////
////
////
////
////
////                delegates?.didGetRequestDetails(request: request)
////
////
////
////           dismiss(animated: true, completion: nil)
////
////
////
////
////
////       }
//
//       
//
//       
//
//       func convertIntoJSONString(arrayObject: [Any]) -> String? {
//
//
//
//           do {
//
//               let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
//
//               if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
//
//                   return jsonString as String
//
//               }
//
//               
//
//           } catch let error as NSError {
//
//               print("Array convertIntoJSON - \(error.description)")
//
//           }
//
//           return nil
//
//       }
//    
//}
extension ShowOrderDetailsViewController:ProductDetailDelegate{
    func addAddressesWithDetails(items: [AddressItem]) {

    }

    func editAddressWithDetails(request: ServiceRequest?) {
        serviceRequest = request
       
        arrProduct.removeAllObjects()
        arrFinalProduct.removeAllObjects()
        
        let arr = serviceRequest?.addressItems ?? []
        originalCount = arr.count
        
        for item in serviceRequest?.addressItems ?? []{
            var addressType:String
            if let stopNumber = item.stopNumber{
                addressType = serId == 15 ? "ITEM \(stopNumber)" : "DROP OFF DETAILS \(stopNumber)"
            }else{
                addressType = item.type.rawValue
            }

            let dictionary = [
                "product_name": item.productName ?? "",
                "product_weight":"",
                "store_name":item.storeName ?? "",
                "store_phone":item.storeNumber ?? "",
                "receiver_name":item.name ?? "",
                "receiver_number":item.phone ?? "",
                "additional_info":item.description ?? "",
                "latitude":item.latitude ?? 0,
                "longitude":item.longitude ?? 0,
                "address":item.address ?? "",
                "addressType":addressType,
                "url_image" : item.imageUrl ?? "",
                "image":item.image,
                "landmark_info" : item.landmark ?? ""] as [String : Any]
            arrProduct.add(dictionary)
            arrFinalProduct.add(dictionary)
        }
        
            
       
        
        print(arrProduct)
        print(arrFinalProduct)

        
        tableview.reloadData()
    }

    func back() {

    }


}



func convertIntoJSONString(arrayObject: [Any]) -> String? {
    
    do {
        let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
        if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
            return jsonString as String
        }
        
    } catch let error as NSError {
        print("Array convertIntoJSON - \(error.description)")
    }
    return nil
}


class ListOrderDetails: UITableViewCell {
    
    @IBOutlet weak var productname: UILabel!
    @IBOutlet weak var productWeight: UILabel!
    @IBOutlet weak var receivername: UILabel!
    @IBOutlet weak var receivernumber: UILabel!
    @IBOutlet weak var additionalinformation: UILabel!
    @IBOutlet weak var buttonedit: UIButton!
    @IBOutlet weak var buttondelete: UIButton!
    @IBOutlet weak var stackviewproductname: UIStackView!
    @IBOutlet weak var stackviewproductweight: UIStackView!
    @IBOutlet weak var stackviewreceivername: UIStackView!
    @IBOutlet weak var stackviewreceivernumber: UIStackView!
    @IBOutlet weak var stackvieweditdelete: UIStackView!
    @IBOutlet weak var stackviewadditionalinfo: UIStackView!
    @IBOutlet weak var vwBg: UIView!
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var titleLocation: UILabel!
    @IBOutlet weak var stackviewImg: UIStackView!
    @IBOutlet weak var lblLandMark: UILabel!
    @IBOutlet weak var stackViewLAndMark: UIStackView!
    @IBOutlet weak var btnOnImage: UIButton!
    @IBOutlet weak var phoneHintLabel: UILabel!
    @IBOutlet weak var nameHintLabel: UILabel!
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var storeNumberLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func actiondeletebutton(_ sender: Any) {
        
        
    }
    @IBAction func actioneditbutton(_ sender: Any) {
        
    }
}


extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
