//
//  AddCardViewControllerCab.swift
//  Trava
//
//  Created by Apple on 15/01/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit
import WebKit
import Stripe

typealias CardAdded = (_ card:CreditCard)->()

class AddCardViewControllerCab: UIViewController {

    //MARK:-Outlet
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var viewCardContainer: UIView!
    @IBOutlet weak var txtFldCardHolderName: UITextField!
    @IBOutlet weak var viewCard: STPPaymentCardTextField!
    @IBOutlet weak var btnAddCard: UIButton!
    
    @IBOutlet weak var btnback: UIButton!
    //MARK:-Properties
  
    typealias WebPayment = ()->()
    
    var cardAdded:CardAdded?
    var webPayment:WebPayment?
    var backCompeletion:(()->())?
    var url: String?
    let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
    let paymentGateway = PaymentGateway(rawValue:(/UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id))
    
  
    @IBOutlet weak var contactUs: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      

        webView.navigationDelegate = self
        webView.uiDelegate = self
        initialSetup()
        setupUI()
//        viewCard.postalCodeEntryEnabled = false
        
        if paymentGateway == PaymentGateway.credimax {
            btnback.isHidden = true
        }
        
    }
    @IBAction func contactUsAction(_ sender: Any) {
        guard let contactusVC = R.storyboard.sideMenu.contactUsVC() else{return}
        navigationController?.pushViewController(contactusVC, animated: true)
    }
    
    @IBAction func btnAddCardAction(_ sender: UIButton) {
        
        if !txtFldCardHolderName.isHidden && txtFldCardHolderName.text!.isEmpty {

            Alerts.shared.show(alert:  "AppName".localizedString, message: "Please enter CardHolder Name.", type: .error)
           
            return
        }else if !self.viewCard.isValid{
            
             Alerts.shared.show(alert:  "AppName".localizedString, message: "Please enter valid card details.", type: .error)
        }
        else{
        
        
        switch paymentGateway {
        case .stripe,.credimax:
            addStripCard(sender: sender)
            break
            
        case .epayco:
            addEpaycoCard(sender: sender)
            break
            
        case .peach:
            break
            
        default:
            break
        }
        }
        
    }
    
  
    
    
    func addEpaycoCard(sender:UIButton){
        
        if let cardAdded = self.cardAdded{
            let creditCard = CreditCard()
            creditCard.name = self.txtFldCardHolderName.text!
            creditCard.cvv = self.viewCard?.cvc
            creditCard.expMonth = UInt(/self.viewCard?.expirationMonth)
            creditCard.expYear = UInt(/self.viewCard?.expirationYear)
            creditCard.cardNumber = self.viewCard?.cardNumber
         
            
            cardAdded(creditCard)
        }
        self.popVC()
        
    }
    
    func addStripCard(sender:UIButton){
        
        sender.isUserInteractionEnabled = false
        let cardParams = STPCardParams()
        cardParams.number = viewCard?.cardNumber
        cardParams.expMonth = UInt(/viewCard?.expirationMonth)
        cardParams.expYear = UInt(/viewCard?.expirationYear)
        cardParams.cvc = viewCard?.cvc
        
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        STPAPIClient.shared.createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            guard let token = token, error == nil else {
                sender.isUserInteractionEnabled = true
                Alerts.shared.show(alert: "AppName".localizedString, message: /error?.localizedDescription , type: .error )
                return
            }
            
            if let cardAdded = self.cardAdded{
                let creditCard = CreditCard()
                creditCard.name = self.txtFldCardHolderName.text!
                creditCard.cardNumber = self.viewCard?.cardNumber
                creditCard.expMonth = UInt(/self.viewCard?.expirationMonth)
                creditCard.expYear = UInt(/self.viewCard?.expirationYear)
                creditCard.cardNumber = self.viewCard?.cardNumber
                creditCard.token = token.tokenId
                
                cardAdded(creditCard)
            }
            self.popVC()
            //            APIManager.shared.request(with: UserEndPoint.addCard(tokenId: token.tokenId, cardHolderName: /self.tfName.text, expMonth: "\(self.vwCardDetails!.expirationMonth)", expYear: "\(self.vwCardDetails!.expirationYear)"), completion: { (response) in
            //                sender.isUserInteractionEnabled = true
            //
            //                switch response {
            //
            //                case .success(_ ):
            //
            //                    self.popVC()
            //                    self.makeToast(text: ConstantString.cardAddedSuccessfully.localized, type: .success)
            //
            //                case.failure(let str):
            //
            //                    self.makeToast(text: str, type: .error)
            //                }
            //
            //            }, loader: true)
        }
    }
    
    
    func setupUI(){
        
        
        btnAddCard.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        txtFldCardHolderName.setBorderColorSecondary()
        txtFldCardHolderName.addShadowToTextFieldColorSecondary()
        txtFldCardHolderName.setLeftPaddingPoints(10)
        
        
    }
    
    
    func setupStrip(){
        
        Stripe.setDefaultPublishableKey(/UDSingleton.shared.appSettings?.appSettings?.stripe_public_key)
    }
    
    
    func setupPaymentGateway(){
        
        switch paymentGateway {
        case .stripe:
            txtFldCardHolderName.isHidden = true
            setupStrip()
            
        default:
            txtFldCardHolderName.isHidden = true
            break
        }
    }
    
}

extension AddCardViewControllerCab {
    
    func initialSetup() {
        
        switch paymentGateway {
        case .stripe,.credimax:
            if url == nil || url == ""{
                webView.isHidden = true
                viewCardContainer.isHidden = false
                setupStrip()
            }else{
                viewCardContainer.isHidden = true
                webView.isHidden = false
                guard let encodedURL  = url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                    let URL = URL(string: encodedURL) else {return}
                webView.load(URLRequest(url: URL))
            }
            
        case .epayco:
            webView.isHidden = true
            viewCardContainer.isHidden = false
            break
            
        case .paystack,.payku:
            
            viewCardContainer.isHidden = true
            webView.isHidden = false
            
            guard let encodedURL  = url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                let URL = URL(string: encodedURL) else {return}
            
            webView.load(URLRequest(url: URL))
            break
            
        case .peach:
            webView.isHidden = true
            viewCardContainer.isHidden = false
            break
        case .wipay:
            viewCardContainer.isHidden = true
            webView.isHidden = false
            guard let html  = url else {return}
            webView.loadHTMLString(html, baseURL: nil)
        default:
            viewCardContainer.isHidden = true
            webView.isHidden = false
            guard let encodedURL  = url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                let URL = URL(string: encodedURL) else {return}
            webView.load(URLRequest(url: URL))
            
        }
    }
    
    
    func webPaymentCompleted() {
        
        if let  webPayment =  webPayment{
            
            webPayment()
        }
    }
    
}

//MARK:- Button Selectors
extension AddCardViewControllerCab {
    
    @IBAction func buttonBackClicked( _ sender: Any) {
        
        if paymentGateway == PaymentGateway.paystack || paymentGateway == PaymentGateway.payku || paymentGateway == .benefit{
           webPaymentCompleted()
        }
        
        backCompeletion?()
        popVC()
    }
    
    
}

extension AddCardViewControllerCab:WKNavigationDelegate,WKUIDelegate{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print(#function)
        let urlString  = webView.url?.absoluteString ?? ""
        if (paymentGateway == .credimax || paymentGateway == .benefit) && (urlString.contains(APIBasePath.benefitSuccessUrl) || urlString.contains("https://pikkupapp.com/benefit_payment/sample/approved.php")){
            self.popVC()
            webPaymentCompleted()
        }
        else if urlString == "\(APIBasePath.basePath)/" {
            self.popVC()
            webPaymentCompleted()
        }
        else if (paymentGateway == .credimax || paymentGateway == .benefit) && urlString.contains("https://www.benefit-gateway.bh/payment/paymentcancel")
        {
            Alerts.shared.show(alert: "AppName".localizedString, message:"Payment Cancelled." , type: .error )
            backCompeletion?()
            self.popVC()
        }
        else if (paymentGateway == .credimax || paymentGateway == .benefit) &&  urlString.contains("https://pikkupapp.com/benefit_payment_munira/sample/err-response.php") || urlString.contains("https://pikkupapp.com/admin/benefit_payment_save/")
            {
            Alerts.shared.show(alert: "AppName".localizedString, message:"Payment unsuccessful. Please Try again!" , type: .error )
            backCompeletion?()
            self.popVC()
        }
        else if (paymentGateway == .credimax || paymentGateway == .benefit) &&  urlString.contains("https://pikkupapp.com/benefit_payment_munira/sample/declined.php"){
            //|| urlString.contains("https://www.benefit-gateway.bh/payment/paymentrouter.htm")

            Alerts.shared.show(alert: "AppName".localizedString, message:"Payment declined." , type: .error )
            backCompeletion?()
            self.popVC()
            
        }
        
       
        stopAnimating()
    }
    
        
        

    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
          print(#function)
         print(webView.url)
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
          print(#function)
         print(webView.url)
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
          print(#function)
        print(webView.url)
    }
    
 
}
