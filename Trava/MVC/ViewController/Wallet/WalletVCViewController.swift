//
//  WalletVCViewController.swift
//  RoyoRide
//
//  Created by Rohit Prajapati on 08/06/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit

class WalletVCViewController: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var walletTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnSendMoney: UIButton!
    @IBOutlet weak var btnAddMoney: UIButton!
    @IBOutlet weak var lblMinBalBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var lblRecentRecharge: UILabel!
    @IBOutlet weak var lblWalletAmountTitle: UILabel!
    
    //MARK:- Properties
    var tableDataSource : TableViewDataSourceCab?
    var walletArray = [WalletModel]()
    var amoutAdd = Int()
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.black
        return refreshControl
    }()
    
    lazy var emptyStateView: EmptyStateView = {
        let eView: EmptyStateView = .fromNib()
        return eView
    }()
    
    let paymentGateway = PaymentGateway(rawValue:(/UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if paymentGateway == .stripe{
            
            btnAddMoney.isHidden = false
            
        } else{
            
            btnAddMoney.isHidden = true
        }
        
        
        
        // lblMinBalBottomConstraints.constant = 0.0
        
        headerView.setViewBackgroundColorSecondary()
        // topView.setViewBackgroundColorSecondary()
        btnSendMoney.setButtonWithBackgroundColorSecondaryAndTitleColorBtnTextSecondary(alpha: 0.3)
        btnAddMoney.setButtonWithBackgroundColorSecondaryAndTitleColorBtnTextSecondary(alpha: 0.3)
        btnAddMoney.setButtonWithTintColorSecondary()
        btnSendMoney.setButtonWithTintColorSecondary()
        
        configureTableView()
        
        
        walletTableView.addSubview(refreshControl)
        
        refreshProgrammatically()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updatedWallet), name: NSNotification.Name(rawValue: LocalNotifications.updateWallet.rawValue), object: nil)
        setupUI()
    }
    
    
    func setupUI(){
        
        btnSendMoney.setTitle("Send Money".localizedString, for: .normal)
        lblRecentRecharge.text = "Recent Recharge".localizedString
        lblNavTitle.text = "Wallet".localizedString
        //lblWalletAmountTitle.text = "Your wallet amount".localizedString
        
    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
        emptyStateView.removeFromSuperview()
        // Fetch Weather Data
        getWalletData(skip: "0", limit: "50")
        getWalletBalance()
    }
    
    
    @objc func updatedWallet(notification: NSNotification) {
        getWalletData(skip: "0", limit: "50")
        getWalletBalance()
    }
    
    public func refreshProgrammatically() {
        
        refreshControl.beginRefreshing()
        let offsetPoint = CGPoint.init(x: 0, y: -refreshControl.frame.size.height)
        walletTableView.setContentOffset(offsetPoint, animated: true)
        getWalletData(skip: "0", limit: "50")
        getWalletBalance()
        
    }
    
    public func showVCPlaceholder(type: NoDataTitle, scrollView: UIScrollView?) {
        guard let scrollableView = scrollView else {
            return
        }
        emptyStateView.frame = scrollableView.bounds
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            scrollableView.addSubview(self.emptyStateView)
            self.emptyStateView.setData(type: type)
        }
    }
    
    @IBAction func btnSendMoneyAction(_ sender: Any) {
        guard let sendMoneyVC = R.storyboard.sideMenu.sendMoneyVCViewController() else { return }
        sendMoneyVC.modalPresentationStyle = .overCurrentContext
        self.presentVC(sendMoneyVC, false)
        
    }
    
    
    @IBAction func btnAddMoneyAction(_ sender: Any) {
        
        guard let vc = R.storyboard.sideMenu.addMoneyVC() else { return }
        vc.modalPresentationStyle = .overCurrentContext
        vc.amountEntered = { [weak self](amount) in
            
            self?.amoutAdd = amount
            self?.goToCardViewController()
        }
        self.presentVC(vc, false)
        
    }
    
    
    func goToCardViewController(){
        
        guard let vc = R.storyboard.sideMenu.cardListViewController() else { return }
        vc.cardListDelegate = self
        vc.isWallet = true
        self.pushVC(vc)
        
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        self.popVC()
    }
    
    
    func configureTableView() {
        let  configureCellBlock : ListCellConfigureBlockCab = { ( cell , item , indexpath) in
            if let cell = cell as? WalletTableCell , let model = item as? WalletModel{
                cell.lblName.text = String((model.transaction_by ?? "").prefix(2)).uppercased()
                cell.lblDate.text = model.created_at?.toLocalDateStr(format: "yyyy-MM-dd HH:mm:ss")
                
                
                if (model.transaction_type ?? "") == "recharged"{
                    cell.lblStatus.text = "Added to wallet:"
                    //(model.transaction_type ?? "").capitalizedFirst()
                    cell.lblAmount.text = "+\(/UDSingleton.shared.appSettings?.appSettings?.currency)\(/model.amount)"
                    cell.lblAmount.setTextColorSecondary()
                }
                else if (model.transaction_type ?? "") == "credited"{
                    cell.lblStatus.text = "Money received from:\n\((model.transaction_by ?? "").capitalizedFirst())"
                    //(model.transaction_type ?? "").capitalizedFirst()
                    cell.lblAmount.text = "+\(/UDSingleton.shared.appSettings?.appSettings?.currency)\(/model.amount)"
                    
                }
                else if (model.transaction_type ?? "") == "debited"{
                    cell.lblStatus.text = "Money transferred to:\n\((model.transaction_by ?? "").capitalizedFirst())"
                    //(model.transaction_type ?? "").capitalizedFirst()
                    cell.lblAmount.text = "-\(/UDSingleton.shared.appSettings?.appSettings?.currency)\(/model.amount)"
                    
                }
                
                cell.viewNameCOntainer.backgroundColor = UIColor.random
                
                // if model.transaction_type == ""
                
                
            }
        }
        
        tableDataSource = TableViewDataSourceCab(items: walletArray, tableView: walletTableView, cellIdentifier: R.reuseIdentifier.walletTableCell.identifier, cellHeight: 80.0)
        tableDataSource?.configureCellBlock = configureCellBlock
        
        walletTableView.delegate = tableDataSource
        walletTableView.dataSource = tableDataSource
        walletTableView.reloadData()
    }
    
    
    func getWalletData(skip: String?, limit: String?) {
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let walletObjectService = BookServiceEndPoint.walletLogs(skip: skip, limit: limit)
        walletObjectService.request(isLoaderNeeded: false, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) { (response) in
            self.refreshControl.endRefreshing()
            switch response {
                
            case .success(let data):
                
                let walletDataArray = data as? [WalletModel]
                self.walletArray = walletDataArray ?? []
                self.tableDataSource?.items = self.walletArray
                self.walletTableView.reloadData()
                /self.walletArray.count == 0 ? self.showVCPlaceholder(type: .NO_TRANSACTIONS, scrollView: self.walletTableView) : (self.emptyStateView.removeFromSuperview())
                
            case .failure(let strError):
                
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    
    func getWalletBalance() {
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let walletBalance = BookServiceEndPoint.getWalletBalance
        walletBalance.request(isLoaderNeeded: false, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) { (response) in
            
            switch response {
                
            case .success(let data):
                let walletData = data as? WalletDetails
                self.lblTotalAmount.text = "\(/UDSingleton.shared.appSettings?.appSettings?.currency)\(/walletData?.amount)"
                
                let amount = Double(/walletData?.amount)
                
                self.btnSendMoney.isUserInteractionEnabled = amount > 0
                self.btnSendMoney.isHidden = amount <= 0
                if (Double(/walletData?.details?.minBalance) ?? 0.0) > 0.0{
                    
                    // self.lblAmountDes.text = "Please maintain a minimum balance of".localizedString + " " +   "\(/walletData?.details?.minBalance) \(/UDSingleton.shared.appSettings?.appSettings?.currency)" + " " + "in the wallet in order to receive the booking request.".localizedString
                    self.lblMinBalBottomConstraints.constant = 25
                    
                }
                else{
                    //   self.lblAmountDes.text = ""
                    //  self.lblMinBalBottomConstraints.constant = 0
                    
                }
                
                //   self.lblAmountDes.text = "Please maintain a minimum balance of  \(/walletData?.details?.minBalance) \(/UDSingleton.shared.appSettings?.appSettings?.currency) in the wallet in order to receive the booking request."
                //self.refreshControl.endRefreshing()
                
            case .failure(let strError):
                
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    func addMoney(cardId:Int){
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let walletBalance = BookServiceEndPoint.addWalletMoney(amount: amoutAdd, cardId: cardId, gatewayId: /UDSingleton.shared.appSettings?.appSettings?.gateway_unique_id)
        walletBalance.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) { (response) in
            
            switch response {
                
            case .success(let data):
                
                self.getWalletData(skip: "0", limit: "50")
                self.getWalletBalance()
                
                
            case .failure(let strError):
                
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    
}


extension WalletVCViewController:CardListViewControllerDelegate{
    
    func cardSelected(card: CardCab?) {
        
        print(/card?.customerToken)
        
        self.addMoney(cardId: /card?.userCardId)
        
        
    }
    
    
    
}
