//
//  PaymentWebviewVC.swift
//  Sneni
//
//  Created by Ankit Chhabra on 25/05/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit
import WebKit

class PaymentWebviewVC: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    //MARK: Properties

    var urlStr = String()
    var finalUrl = String()
    var gatewayUniqueId = String()
    
    var paymentSucccessBlock:((_ token:String ) -> ())?

    //MARK: Outlets
    @IBOutlet weak var webKit: WKWebView!
    
    @IBOutlet weak var btnBack: ThemeButtonCab!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        HTTPCookieStorage.shared.cookieAcceptPolicy = .always
        guard let url = URL(string: urlStr) else {return}
        var urlRequest = URLRequest(url: url)
        urlRequest.httpShouldHandleCookies = true
        
        
        webKit.navigationDelegate = self
        
        APIManagerCab.shared.showLoader()
        webKit.load(urlRequest)
        
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
         print("Navigation")
               print(/webKit.url?.absoluteString)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Navigation Start")
        print(/webKit.url?.absoluteString)
        
        if let text = webKit.url?.absoluteString, text.range(of: "cancel") != nil, self.gatewayUniqueId == "mpaisa" {
            //SKToast.makeToast("Payment Failure")
            self.popVC()
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        APIManagerCab.shared.hideLoader()
        finalUrl = /webKit.url?.absoluteString
        print(finalUrl)
        if let text = webKit.url?.absoluteString {
            if text.range(of: "success") != nil {
                btnBack.isUserInteractionEnabled = false
                ez.runThisAfterDelay(seconds: 1.0) {
                        //SKToast.makeToast("Payment Successful")
                        let ref_id = self.webKit.url?.getParam(queryParam: "ref_id")
                        self.paymentSucccessBlock?(/ref_id)
                }
            } else if text.range(of: "failure") != nil || text.range(of: "error") != nil {
                //SKToast.makeToast("Payment Failure")
                Alerts.shared.show(alert: "AppName".localizedString, message: "Payment Failure" , type: .error )
            }
        }
    }
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        guard let response = navigationResponse.response as? HTTPURLResponse,
            let url = navigationResponse.response.url else {
                decisionHandler(.cancel)
                return
        }
        
        if let headerFields = response.allHeaderFields as? [String: String] {
            let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: url)
            cookies.forEach { cookie in
                webView.configuration.websiteDataStore.httpCookieStore.setCookie(cookie)
            }
        }
        
        decisionHandler(.allow)
    }
    
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        APIManagerCab.shared.hideLoader()
        self.popVC()
        self.dismiss(animated: false, completion: nil)
    }
    
    private func webView(webView: WKWebView, didFailNavigation navigation: WKNavigation!, withError error: NSError) {
        APIManagerCab.shared.hideLoader()
       // SKToast.makeToast(error.description)
        Alerts.shared.show(alert: "AppName".localizedString, message: error.description , type: .error )
        //handleError(error: error)
    }
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        APIManagerCab.shared.hideLoader()
        //SKToast.makeToast(error.description)
        Alerts.shared.show(alert: "AppName".localizedString, message: error.description , type: .error )
        //handleError(error: error)
    }
    

    func handleError(error: NSError) {
        if let failingUrl = error.userInfo["NSErrorFailingURLStringKey"] as? String {
            if let url = NSURL(string: failingUrl) {
                //let didOpen = UIApplication.sharedApplication().openURL(url)
                //                if didOpen {
                //                    print("openURL succeeded")
                //                    return
                //                }
            }
        }
    }
    
}



extension WKWebView {
    class func clean() {
        guard #available(iOS 9.0, *) else {return}
        
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                #if DEBUG
                print("WKWebsiteDataStore record deleted:", record)
                #endif
            }
        }
    }
}



extension URL {
    func getParam(queryParam:String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParam })?.value
    }
}
