//
//  SavedAddressCell.swift
//  RoyoRide
//
//  Created by admin on 22/11/21.
//  Copyright © 2021 CodeBrewLabs. All rights reserved.
//

import UIKit

class SavedAddressCell: UITableViewCell {
    
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var lblAddress2: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var widthConstForEditBtn: NSLayoutConstraint!
    @IBOutlet weak var widthConstForDeleteBtn: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
