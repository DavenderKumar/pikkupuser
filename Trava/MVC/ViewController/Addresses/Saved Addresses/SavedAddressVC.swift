//
//  SavedAddressVC.swift
//  RoyoRide
//
//  Created by admin on 22/11/21.
//  Copyright © 2021 CodeBrewLabs. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation


protocol SavedAddressVCDelegate_Delegate {
    func messageData(buttonTag:Int,isFromSearch:Bool?,isFromStop: Bool?,address:String,lat:Double,long:Double,nic_name:String)
}


class SavedAddressVC : UIViewController,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate, AddLocationFromMapViewControllerDelegate{
  
    func locationSelectedFromMap(address: String?, name: String?, latitude: Double?, longitude: Double?, locationType: Int, isFromStop: Bool) {
        
    }
    
    func getDataFromAnotherVC(address: String?, name: String?, latitude: Double?, longitude: Double?, getCountry: String, getLocality: String, getSublocality: String) {
        
        //        self.tfArea.text = address
        //       // self.tfAddressNickname.text = name
        //        self.latValue = latitude ?? 0.0
        //        self.longvalue = longitude ?? 0.0
        //        self.tfBlock.text = getLocality
        //        self.tfStreet.text = getSublocality
        //        self.tfCountry.text = getCountry
        //

    }
    
    
    func getDataFromAnotherVC1(address: String?, name: String?, latitude: Double?, longitude: Double?, getCountry: String, getLocality: String, getSublocality: String,strComingFrom:String?) {
//
//        self.strComingFrom = "SavedAddress"
//
//        let strybord = UIStoryboard(name: "MainCab", bundle: nil)
//        let bookingVC = strybord.instantiateViewController(withIdentifier: "AddNewAddressVC") as! AddNewAddressVC
//
//
//        if self.presentedViewController==nil{
//            self.present(bookingVC, animated: false)
//        }else{
//            self.presentedViewController!.present(bookingVC, animated: false)
//        }
        
       
//        bookingVC.latValue = latitude ?? 0.0
//        bookingVC.longvalue = longitude ?? 0.0
//        bookingVC.tfBlock.text = getLocality
//        bookingVC.tfArea.text = address
//        bookingVC.tfStreet.text = getSublocality
//        bookingVC.tfCountry.text = getCountry
       // presentVC(bookingVC, false)
//
//        guard let vc = R.storyboard.bookService.addLocationFromMapViewController() else{return}
//        vc.modalPresentationStyle = .overFullScreen
//        vc.comingFrom = "AddNewAddress"
//        vc.latitude = self.latValue
//        vc.longitude = self.longvalue
//        vc.delegate = self
//        presentVC(vc, false)
//
//        self.tfArea.text = address
//       // self.tfAddressNickname.text = name
//        self.latValue = latitude ?? 0.0
//        self.longvalue = longitude ?? 0.0
//        self.tfBlock.text = getLocality
//        self.tfStreet.text = getSublocality
//        self.tfCountry.text = getCountry
//
    }
    
    
    var strComingFrom = ""
    
    
    //MARk:- Variable
    var comingFrom = ""
    var buttonTag : Int?
    var isFromSearch : Bool?
    var isFromStop : Bool?
    var arrData = NSArray()
    
    
    var latValue = 0.0
    var longvalue = 0.0
    var locationManager = CLLocationManager()
    
    var delegate: SavedAddressVCDelegate_Delegate?
    
    //MARK:- IBOutlets
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var tblAddressList: UITableView!
    
   
   
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitials()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "UpdateSavedAddress"), object: nil)
    }
    
    
    func setInitials()
    {
        btnBack.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        btnBack.tintColor = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.Btn_Text_Colour ?? DefaultColor.color.rawValue)
        btnAddress.tintColor = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.Btn_Text_Colour ?? DefaultColor.color.rawValue)
        btnAddress.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        viewNavBar.backgroundColor  = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.Secondary_Btn_Colour ?? DefaultColor.color.rawValue)
        lblTitle.textColor = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.Btn_Text_Colour ?? DefaultColor.color.rawValue)
        
        
        locationManager.delegate = self
              locationManager.desiredAccuracy = kCLLocationAccuracyBest
              locationManager.requestWhenInUseAuthorization()
              locationManager.startUpdatingLocation()
        
        
        if comingFrom == "HomeVC"
        {
            btnAddress.isHidden = true
        }
        
        
        api_getAddress()
        
       
        
    }
    
    
    //MARK:- Handle Notifications
    @objc func showSpinningWheel(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        api_getAddress()
    }
    
    
    //MARK:- Custom Methods
    
    func showLoader() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return}
        appDelegate.window?.rootViewController?.startAnimating(CGSize(width: 24, height: 24), message: nil, messageFont: nil, type: .ballScale, color: UIColor.white, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: nil, textColor: nil, fadeInAnimation: nil)
    }
       
    func hideLoader(){
         guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return}
           appDelegate.window?.rootViewController?.stopAnimating()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
           let userLocation:CLLocation = locations[0] as CLLocation
           
           manager.stopUpdatingLocation()
           
        _ = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
           self.latValue = userLocation.coordinate.latitude
           self.longvalue = userLocation.coordinate.longitude
           
           
           
       }
    
   
    //MARK:- IBActions
    @IBAction func action_backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func action_addNewAddressBtnTapped(_ sender: UIButton) {
        print("Add New Address")
        
        guard let vc = R.storyboard.bookService.addLocationFromMapViewController() else{return}
        vc.modalPresentationStyle = .overFullScreen
        vc.comingFrom = "SavedAddress"
        vc.latitude = self.latValue
        vc.longitude = self.longvalue
        vc.delegate = self
        presentVC(vc, false)
        
        
//        let strybord = UIStoryboard(name: "MainCab", bundle: nil)
//        let bookingVC = strybord.instantiateViewController(withIdentifier: "AddNewAddressVC") as? AddNewAddressVC
//        self.navigationController?.pushViewController(bookingVC!, animated: true)
    }
    
   
    @IBAction func action_deleteAddress(_ sender: UIButton) {
        
        if let tempDic : NSDictionary = self.arrData[sender.tag] as? NSDictionary {
        
            if let addressId : Int = tempDic["id"] as? Int
            {
                self.api_deleteAddress(getAddressId: addressId)
            }
        }
    }
    
    
    @IBAction func action_editAddress(_ sender: UIButton) {
        
        if let tempDic : NSDictionary = self.arrData[sender.tag] as? NSDictionary {
            
            let strybord = UIStoryboard(name: "MainCab", bundle: nil)
            let bookingVC = strybord.instantiateViewController(withIdentifier: "AddNewAddressVC") as! AddNewAddressVC
            bookingVC.comingFrom = "SavedAddressForEdit1"
            bookingVC.getDictionary = tempDic
            
            presentVC(bookingVC, false)
            //self.navigationController?.pushViewController(bookingVC!, animated: true)
        
        }
    }
    
    
    
    
    //Table View data Source and delegate methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SavedAddressCell = tableView.dequeueReusableCell(withIdentifier: "SavedAddressCell", for: indexPath) as! SavedAddressCell
        
        cell.btnDelete.tag = indexPath.row
        cell.btnEdit.tag = indexPath.row
        
        
        cell.btnDelete.tintColor = UIColor.red
        cell.btnEdit.tintColor = UIColor.black
        
        if comingFrom == "HomeVC"
        {
            cell.btnDelete.isHidden = true
            cell.btnDelete.isHidden = true
            
            cell.widthConstForEditBtn.constant = 0
            cell.widthConstForDeleteBtn.constant = 0
        }
        
            
            if let tempDic : NSDictionary = self.arrData[indexPath.row] as? NSDictionary {
            
                if let address : String = tempDic["location_data"] as? String{
                    
                    do{
                        if let json = address.data(using: String.Encoding.utf8){
                            if let jsonData = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String:AnyObject]{
                               if  let name = jsonData["name"] as? String{cell.lblAddress1.text = name}
                                
                                var strAddress = ""
                                if  let block = jsonData["block"] as? String{if block != ""{strAddress = block}}
                                if  let street = jsonData["street"] as? String{if street != ""{strAddress = strAddress + ", " + street}}
                                if  let building = jsonData["building"] as? String{if building != ""{strAddress = strAddress + ", " + building}}
                                if  let floor = jsonData["floor"] as? String{if floor != ""{strAddress = strAddress + ", " + floor}}
                                if  let apartment_number = jsonData["apartment_number"] as? String{if apartment_number != ""{strAddress = strAddress + ", " + apartment_number}}
                                if  let area = jsonData["area"] as? String{if area != ""{strAddress = strAddress + ", " + area}}
                                if  let country = jsonData["country"] as? String{if country != ""{strAddress = strAddress + ", " + country}}
                                
                                cell.lblAddress2.text = strAddress
                            }
                        }
                    }catch {
                        print(error.localizedDescription)

                    }
                }
            }
        
        cell.selectionStyle = .none
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if comingFrom == "HomeVC"
        {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateSavedAddress1"), object: nil, userInfo: nil)
            
            if let tempDic : NSDictionary = self.arrData[indexPath.row] as? NSDictionary {
            
                if let address : String = tempDic["location_data"] as? String{
                    
                    do{
                        if let json = address.data(using: String.Encoding.utf8){
                            if let jsonData = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String:AnyObject]{
                               
                                var strAddress = ""
                                if  let block = jsonData["block"] as? String{if block != ""{strAddress = block}}
                                if  let street = jsonData["street"] as? String{if street != ""{strAddress = strAddress + ", " + street}}
                                if  let building = jsonData["building"] as? String{if building != ""{strAddress = strAddress + ", " + building}}
                                if  let floor = jsonData["floor"] as? String{if floor != ""{strAddress = strAddress + ", " + floor}}
                                if  let apartment_number = jsonData["apartment_number"] as? String{if apartment_number != ""{strAddress = strAddress + ", " + apartment_number}}
                                if  let area = jsonData["area"] as? String{if area != ""{strAddress = strAddress + ", " + area}}
                                if  let country = jsonData["country"] as? String{if country != ""{strAddress = strAddress + ", " + country}}
                                
                                var address_latitude = 0.0
                                var address_longitude = 0.0
                                
                                if  let latitide = jsonData["latitide"] as? Double{address_latitude = latitide}
                                if  let longitude = jsonData["longitude"] as? Double{address_longitude = longitude}
                                
                                var nameNic = ""
                                if  let name = jsonData["name"] as? String{nameNic = name}
                                
                                delegate?.messageData(buttonTag: self.buttonTag!, isFromSearch: false, isFromStop: self.isFromStop, address: strAddress, lat: address_latitude, long: address_longitude,nic_name:nameNic)
                            }
                        }
                    }catch {
                        print(error.localizedDescription)

                    }
                }
            }
            
            self.navigationController?.popViewController(animated: false)
        }
    }
    
   
    
    //MARK:- Web Services
    func api_getAddress()
    {
        self.startAnimating()
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
                
        let path = APIBasePath.basePath + Routes.commonRoutes + "getLocation"
        
        print(path)
                Alamofire.request(path, method: .get, parameters: nil,encoding: URLEncoding.default, headers:  ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]).responseJSON {
                    response in
                   
                    switch response.result {
                    case .success:
                        
                        self.stopAnimating()
                        if let dictSuccess:NSDictionary =  response.value as? NSDictionary
                        {
                            if let arrTemp : NSArray = dictSuccess["result"] as? NSArray {
                                self.arrData = arrTemp.reversed() as NSArray
                                self.tblAddressList.reloadData()
                                
                                if self.arrData.count == 0
                                    {
                                    
                                    let alert = UIAlertController(title: "Alert", message: "No saved address found. Please save address first.", preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        
                                        
                    
                                    }))
                                    
                                    if self.presentedViewController==nil{
                                        self.present(alert, animated: true, completion: nil)
                                    }else{
                                        self.presentedViewController!.present(alert, animated: true, completion: nil)
                                    }
                                    
                                    
                                }
                            }
                        }
                        break
                    case .failure(let error):
                        
                        self.stopAnimating()
                        Alerts.shared.show(alert: "Error".localizedString, message: error.localizedDescription , type: .error )
                        
                        print(response)
                        print(error)
                    }

                }
    }
    
    
    func api_deleteAddress(getAddressId:Int)
    {
        self.startAnimating()
        
        let parameters : [String:Any] = ["location_id":getAddressId,"is_delete":1]
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
                
        let path = APIBasePath.basePath + Routes.commonRoutes + "updateLocation"
        
        print(path)
                Alamofire.request(path, method: .post, parameters: parameters,encoding: URLEncoding.default, headers:  ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]).responseJSON {
                    response in
                   
                    switch response.result {
                    case .success:
                        
                        self.stopAnimating()
                        self.api_getAddress()
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateSavedAddress1"), object: nil, userInfo: nil)
                        
                        
                       
                        break
                    case .failure(let error):
                        
                        self.stopAnimating()
                        Alerts.shared.show(alert: "Error".localizedString, message: error.localizedDescription , type: .error )
                        
                        print(response)
                        print(error)
                    }

                }
    }
    
   
    
    
}
