//
//  AddNewAddressVC.swift
//  RoyoRide
//
//  Created by admin on 22/11/21.
//  Copyright © 2021 CodeBrewLabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import CoreLocation

class AddNewAddressVC: UIViewController,UITextFieldDelegate, AddLocationFromMapViewControllerDelegate, CLLocationManagerDelegate{
   
    
    var locationManager = CLLocationManager()
    
    var block = ""
    var street = ""
    var country = ""
    var areavalue = ""
    
    
    func locationSelectedFromMap(address: String?, name: String?, latitude: Double?, longitude: Double?, locationType: Int, isFromStop: Bool) {
        
    }
    
    func getDataFromAnotherVC1(address: String?, name: String?, latitude: Double?, longitude: Double?, getCountry: String, getLocality: String, getSublocality: String,strComingFrom:String?) {
        
        areavalue = address ?? ""
        street = getSublocality
        block = getLocality
        country = getCountry
        self.latValue = latitude ?? 0.0
        self.longvalue = longitude ?? 0.0
        
//        self.tfArea.text = address
//       // self.tfAddressNickname.text = name
//        self.latValue = latitude ?? 0.0
//        self.longvalue = longitude ?? 0.0
//        self.tfBlock.text = getLocality
//        self.tfStreet.text = getSublocality
//        self.tfCountry.text = getCountry
    }
    
    func getDataFromAnotherVC(address: String?, name: String?, latitude: Double?, longitude: Double? , getCountry:String,getLocality:String,getSublocality:String) {
//        self.tfArea.text = address
//       // self.tfAddressNickname.text = name
//        self.latValue = latitude ?? 0.0
//        self.longvalue = longitude ?? 0.0
//        self.tfBlock.text = getLocality
//        self.tfStreet.text = getSublocality
//        self.tfCountry.text = getCountry
    }
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewNavBar: UIView!
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var viewForAddAddress: UIView!
   
    @IBOutlet weak var tfAddressNickname: UITextField!
    @IBOutlet weak var tfArea: UITextField!
    @IBOutlet weak var tfBlock: UITextField!
    @IBOutlet weak var tfStreet: UITextField!
    @IBOutlet weak var tfBuilding: UITextField!
    @IBOutlet weak var tfFloor: UITextField!
    @IBOutlet weak var tfApartmentNumber: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    
    
    @IBOutlet weak var btnSave: UIButton!
    
    
    var latValue = 0.0
    var longvalue = 0.0
    
    var comingFrom = ""
    var getDictionary = NSDictionary()
    var getAddressId : Int?
    var sendNicName = ""
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnBack.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        btnBack.tintColor = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.Btn_Text_Colour ?? DefaultColor.color.rawValue)
       
        viewNavBar.backgroundColor  = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.Secondary_Btn_Colour ?? DefaultColor.color.rawValue)
        lblTitle.textColor = UIColor().colorFromHexString(UDSingleton.shared.appSettings?.appSettings?.Btn_Text_Colour ?? DefaultColor.color.rawValue)
        
        btnSave.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        
        
        if comingFrom == "SavedAddressForEdit" || comingFrom == "SavedAddressForEdit1"
            {
           // btnSave.setTitle("Update Address", for: .normal)
            
            if  let getAddressId1 = getDictionary["id"] as? Int{getAddressId = getAddressId1}
            
            if let address : String = getDictionary["location_data"] as? String{
                
                do{
                    if let json = address.data(using: String.Encoding.utf8){
                        if let jsonData = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String:AnyObject]{
                            if  let name = jsonData["name"] as? String{self.tfAddressNickname.text = name}
                            if  let area = jsonData["area"] as? String{tfArea.text = area}
                            
                            sendNicName = self.tfAddressNickname.text ?? ""
                           
                            if  let apartment_number = jsonData["apartment_number"] as? String{tfApartmentNumber.text = apartment_number}
                            if  let block = jsonData["block"] as? String{tfBlock.text = block}
                            if  let building = jsonData["building"] as? String{tfBuilding.text = building}
                            if  let country = jsonData["country"] as? String{tfCountry.text = country}
                            if  let floor = jsonData["floor"] as? String{tfFloor.text = floor}
                            if  let street = jsonData["street"] as? String{tfStreet.text = street}
                            if  let latitide = jsonData["latitide"] as? Double{latValue = latitide}
                            if  let longitude = jsonData["longitude"] as? Double{longvalue = longitude}
                            
                            
                        }}
                }catch {print(error.localizedDescription)}
            }
        }
        else if comingFrom == "SavedAddress1" || comingFrom == "SavedAddressForEdit11"
            {
           
            if comingFrom == "SavedAddressForEdit11"
                {
                comingFrom = "SavedAddressForEdit1"
            }
            
            self.tfArea.text = areavalue
            self.tfBlock.text = block
            self.tfStreet.text = street
            self.tfCountry.text = country
            
            self.tfAddressNickname.text = sendNicName
            
            
        }
        else{
            
            
            locationManager.delegate = self
                  locationManager.desiredAccuracy = kCLLocationAccuracyBest
                  locationManager.requestWhenInUseAuthorization()
                  locationManager.startUpdatingLocation()
            
            btnSave.setTitle("Save Address", for: .normal)
        }

    }
    
    //MARK:- Custom Map functions
    
    func autocompleteClicked() {
        
        
        GooglePlaceDataSource.sharedInstance.showAutocomplete {[weak self] (place) in
            
//            self?.txtDropOffLocation.text = place?.formattedAddress
//            self?.serviceRequest.locationNickName = place?.name
//            self?.serviceRequest.locationName = place?.formattedAddress
//            self?.serviceRequest.latitude =  /place?.coordinate.latitude
//            self?.serviceRequest.longitude =  /place?.coordinate.longitude
            
            //  self?.setCameraGoogleMap(latitude: /place?.coordinate.latitude, longitude: /place?.coordinate.longitude)
            
            if place?.name != nil {
                self?.tfAddressNickname.text = "\(String(describing:place?.name))"
            }
            
            if place?.formattedAddress != nil {
                self?.tfArea.text = "\(String(describing: place?.formattedAddress))"
            }

            
            let lat = place?.coordinate.latitude
            let lon = place?.coordinate.longitude
            print("lat lon",lat,lon)

            
            self?.latValue = lat ?? 0.0
            self?.longvalue = lon ?? 0.0
        }
        
        
        
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//
//        // Specify the place data types to return.
//        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
//          UInt(GMSPlaceField.placeID.rawValue) |
//            UInt(GMSPlaceField.coordinate.rawValue) |
//            GMSPlaceField.addressComponents.rawValue |
//            GMSPlaceField.formattedAddress.rawValue)!
//        autocompleteController.placeFields = fields
//
//
//
//        if let searchBar = (autocompleteController.view.subviews
//           .flatMap { $0.subviews }
//           .flatMap { $0.subviews }
//           .flatMap { $0.subviews }
//           .filter { $0 == $0 as? UISearchBar}).first as? UISearchBar {
//                    searchBar.text = "Search Address"
//                    searchBar.delegate?.searchBar?(searchBar, textDidChange: "Search Address") // to get the autoComplete Response
//
//            }
//
//        // Specify a filter.
//        let filter = GMSAutocompleteFilter()
//        filter.type = .address
//        autocompleteController.autocompleteFilter = filter
//
//        // Display the autocomplete view controller.
//        present(autocompleteController, animated: true, completion: nil)
      }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
           let userLocation:CLLocation = locations[0] as CLLocation
           
           manager.stopUpdatingLocation()
           
        _ = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
           self.latValue = userLocation.coordinate.latitude
           self.longvalue = userLocation.coordinate.longitude
           
           
           
       }
    
    //MARK:- IBActions
    
    @IBAction func action_backBtnTapped(_ sender: UIButton) {
        if comingFrom == "SavedAddress1"
            {
            self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
        }
        else if "SavedAddressForEdit" == comingFrom
            {
            self.dismiss(animated: true, completion: nil)
        }
        else if "SavedAddressForEdit1" == comingFrom
            {
            self.dismiss(animated: true, completion: nil)
        }
        
        else
        {
            self.navigationController?.popViewController(animated: true)
            self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
        }
        
    }
    
    @IBAction func action_selectGooglePlaces(_ sender: UIButton) {
        
        if comingFrom == "SavedAddressForEdit" || comingFrom == "SavedAddressForEdit1"
        {
            guard let vc = R.storyboard.bookService.addLocationFromMapViewController() else{return}
            vc.modalPresentationStyle = .overFullScreen
            vc.comingFrom = "SavedAddressForEdit1"
            vc.getAddressIds = self.getAddressId
            vc.getNickName = self.sendNicName
            vc.latitude = self.latValue
            vc.longitude = self.longvalue
            vc.delegate = self
            presentVC(vc, false)
        }
        else
        {
            guard let vc = R.storyboard.bookService.addLocationFromMapViewController() else{return}
            vc.modalPresentationStyle = .overFullScreen
            vc.comingFrom = "SavedAddress"
            vc.latitude = self.latValue
            vc.longitude = self.longvalue
            vc.delegate = self
            presentVC(vc, false)
        }
        
        
        
//        if comingFrom == "SavedAddressForEdit"
//        {
//
//
//        }else
//        {
//
////            guard let vc = R.storyboard.bookService.addLocationFromMapViewController() else{return}
////            vc.modalPresentationStyle = .overFullScreen
////            vc.comingFrom = "SavedAddress"
////            vc.latitude = self.latValue
////            vc.longitude = self.longvalue
////            vc.delegate = self
////            presentVC(vc, false)
//
//            //autocompleteClicked()
//        }
        
    }
    
    @IBAction func action_mapDoneBtnTapped(_ sender: UIButton) {
        print("Map Done button tapped")
    }
    
    
    

    @IBAction func action_saveBtnTpped(_ sender: UIButton) {
        print("Save button tapped")
        
       // guard tfAddressNickname.text != "" else {
        //    Alerts.shared.show(alert: "AppName".localizedString, message: "Please enter address nickname." , type: .error ); return}
        guard tfArea.text != "" else { Alerts.shared.show(alert: "AppName".localizedString, message: "Please select area" , type: .error ); return}
//        guard tfBlock.text != "" else {  Alerts.shared.show(alert: "AppName".localizedString, message: "Please enter block name" , type: .error ); return}
//        guard tfStreet.text != "" else {
//            Alerts.shared.show(alert: "AppName".localizedString, message: "Please enter street address" , type: .error ); return}
//        guard tfBuilding.text != "" else {
//            Alerts.shared.show(alert: "AppName".localizedString, message: "Please enter building/landmark" , type: .error ); return}
//        guard tfFloor.text != "" else {
//            Alerts.shared.show(alert: "AppName".localizedString, message: "Please enter floor" , type: .error ); return}
//        guard tfApartmentNumber.text != "" else {
//            Alerts.shared.show(alert: "AppName".localizedString, message: "Please enter apartment number" , type: .error ); return}
        guard  tfCountry.text != "" else {
            Alerts.shared.show(alert: "AppName".localizedString, message: "Please enter country" , type: .error ); return}
        
        
        if comingFrom != "SavedAddressForEdit1"
            {
            //call the api here
            api_addAddress(getAddressNicName: tfAddressNickname.text ?? "", getArea: tfArea.text ?? "", getBlock: tfBlock.text ?? "", getStreet: tfStreet.text ?? "", getFloor: tfFloor.text ?? "", getApartment: tfApartmentNumber.text ?? "", getBuilding: tfBuilding.text ?? "", getCountry: tfCountry.text ?? "",getlat:"\(self.latValue)" ,getLong:"\(self.longvalue)")
        }
        else
        {
            if getAddressId != nil {
                api_editAddress(getId: getAddressId!, getAddressNicName: tfAddressNickname.text ?? "", getArea: tfArea.text ?? "", getBlock: tfBlock.text ?? "", getStreet: tfStreet.text ?? "", getFloor: tfFloor.text ?? "", getApartment: tfApartmentNumber.text ?? "", getBuilding: tfBuilding.text ?? "", getCountry: tfCountry.text ?? "",getlat:"\(self.latValue)" ,getLong:"\(self.longvalue)")
            }
            else
            {
                print("Id is missing please Check")
            }
            
        }
        
        
        
      
        // api_addAddress(getAddressNicName: tfAddressNickname.text ?? "", getArea: tfArea.text ?? "", getBlock: tfBlock.text ?? "", getStreet: tfStreet.text ?? "", getFloor: tfFloor.text ?? "", getApartment: tfApartmentNumber.text ?? "")
        
    }
    
    
    //MARK:- TextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfAddressNickname {
            tfArea.becomeFirstResponder()
        }else if textField == tfArea {
            tfBlock.becomeFirstResponder()
        }
        else if textField == tfBlock {
            tfStreet.becomeFirstResponder()
        }
        else if textField == tfStreet {
            tfBuilding.becomeFirstResponder()
        }
        else if textField == tfBuilding {
            tfFloor.becomeFirstResponder()
        }
        else if textField == tfFloor
        {
            tfApartmentNumber.becomeFirstResponder()
        }
        else if textField == tfApartmentNumber
            {
            tfCountry.becomeFirstResponder()
        }
        else
        {
            tfAddressNickname.becomeFirstResponder()
        }
        return true
    }
    
   
    //MARK:- Web Services
    
    func api_editAddress(getId:Int,getAddressNicName:String,getArea:String,getBlock:String,getStreet:String,getFloor:String,getApartment:String,getBuilding:String,getCountry:String,getlat:String,getLong:String)
    {
        self.startAnimating()
        
        var parameters:[String:Any]?
        
        let tempDic : [String : Any] = ["apartment_number":getApartment,"area":getArea,"block":getBlock,"building":getBuilding,"country":getCountry,"floor":getFloor,"name":getAddressNicName,"street":getStreet,"latitide":Double(getlat),"longitude":Double(getLong)]
        
        var newStr = ""
        
        var error : NSError?

        if let jsonData = try? JSONSerialization.data(withJSONObject: tempDic, options: JSONSerialization.WritingOptions.prettyPrinted)
        {
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as? String
            {
                newStr = jsonString
            }

            
        }

        
//        let encoder = JSONEncoder()
//        if let jsonData = try? encoder.encode(tempDic) {
//            if let jsonString = String(data: jsonData, encoding: .utf8) {
//                print(jsonString)
//                newStr = jsonString
//            }
//        }
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
                
        parameters  = ["location_data":newStr,"location_id":getId]
        
        let path = APIBasePath.basePath + Routes.commonRoutes + "updateLocation"
        
        print(path)
        print(parameters)

                Alamofire.request(path, method: .post, parameters: parameters,encoding: URLEncoding.default, headers:  ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]).responseJSON {
                    response in
                   
                    switch response.result {
                    
                    case .success:
                        
                        self.stopAnimating()
                        
                        if let dictSuccess:NSDictionary =  response.value as? NSDictionary
                        {
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateSavedAddress"), object: nil, userInfo: nil)
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateSavedAddress1"), object: nil, userInfo: nil)
                            
                            
                           
                            
                            self.tfAddressNickname.text = ""
                            self.tfArea.text = ""
                            self.tfBlock.text = ""
                            self.tfStreet.text = ""
                            self.tfBuilding.text = ""
                            self.tfFloor.text = ""
                            self.tfApartmentNumber.text = ""
                            self.tfCountry.text = ""
                       
                            if let getMessage : String = dictSuccess["msg"] as? String{
                                Alerts.shared.show(alert: "Success".localizedString, message: "Address updated successfully.".localizedString , type: .error )
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                // your code here
                                
                                self.navigationController?.popViewController(animated: true)
                                self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                                
                            }
                        }
                        break
                    case .failure(let error):
                        
                        self.stopAnimating()
                        
                        Alerts.shared.show(alert: "Error".localizedString, message: error.localizedDescription , type: .error )
                        
                        print(response)
                        print(error)
                    }

                }
    }
    
    func api_addAddress(getAddressNicName:String,getArea:String,getBlock:String,getStreet:String,getFloor:String,getApartment:String,getBuilding:String,getCountry:String,getlat:String,getLong:String)
    {
        self.startAnimating()
        
        var parameters:[String:Any]?
        
        let tempDic : [String : Any] = ["apartment_number":getApartment,"area":getArea,"block":getBlock,"building":getBuilding,"country":getCountry,"floor":getFloor,"name":getAddressNicName,"street":getStreet,"latitide":Double(getlat),"longitude":Double(getLong)]
        
        var newStr = ""
        
        var error : NSError?

        if let jsonData = try? JSONSerialization.data(withJSONObject: tempDic, options: JSONSerialization.WritingOptions.prettyPrinted)
        {
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as? String
            {
                newStr = jsonString
            }

            
        }

        
//        let encoder = JSONEncoder()
//        if let jsonData = try? encoder.encode(tempDic) {
//            if let jsonString = String(data: jsonData, encoding: .utf8) {
//                print(jsonString)
//                newStr = jsonString
//            }
//        }
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
                
        parameters  = ["location_data":newStr]
        
        let path = APIBasePath.basePath + Routes.commonRoutes + "addLocation"
        
        print(path)
        print(parameters)

                Alamofire.request(path, method: .post, parameters: parameters,encoding: URLEncoding.default, headers:  ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]).responseJSON {
                    response in
                   
                    switch response.result {
                    
                    case .success:
                        
                        self.stopAnimating()
                        
                        if let dictSuccess:NSDictionary =  response.value as? NSDictionary
                        {
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateSavedAddress"), object: nil, userInfo: nil)
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateSavedAddress1"), object: nil, userInfo: nil)
                            
                            
                           
                            
                            self.tfAddressNickname.text = ""
                            self.tfArea.text = ""
                            self.tfBlock.text = ""
                            self.tfStreet.text = ""
                            self.tfBuilding.text = ""
                            self.tfFloor.text = ""
                            self.tfApartmentNumber.text = ""
                            self.tfCountry.text = ""
                       
                            if let getMessage : String = dictSuccess["msg"] as? String{
                                Alerts.shared.show(alert: "Success".localizedString, message: "Address added successfully.".localizedString , type: .error )
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                // your code here
                                self.navigationController?.popViewController(animated: true)
                                self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                                
                            }
                        }
                        break
                    case .failure(let error):
                        
                        self.stopAnimating()
                        
                        Alerts.shared.show(alert: "Error".localizedString, message: error.localizedDescription , type: .error )
                        
                        print(response)
                        print(error)
                    }

                }
    }
    
    
    func showLoader() {
      
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return}
        appDelegate.window?.rootViewController?.startAnimating(CGSize(width: 24, height: 24), message: nil, messageFont: nil, type: .ballScale, color: UIColor.white, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: nil, textColor: nil, fadeInAnimation: nil)
       }
       
       func hideLoader(){
         guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return}
           appDelegate.window?.rootViewController?.stopAnimating()
       }
    

    
    func getAdressName(coords: CLLocation) {

        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
                if error != nil {
                    print("Hay un error")
                } else {

                    let place = placemark! as [CLPlacemark]
                    if place.count > 0 {
                        let place = placemark![0]
                        var adressString : String = ""
                        if place.thoroughfare != nil {
                            adressString = adressString + place.thoroughfare! + ", "
                        }
                        if place.subThoroughfare != nil {
                            adressString = adressString + place.subThoroughfare! + "\n"
                        }
                        
                        var str = ""
                        if place.subLocality != nil {
                            adressString = adressString + place.subLocality! + ", "
                            str = place.subLocality!  + ", "
                        }
                        if place.locality != nil {
                            adressString = adressString + place.locality! + " - "
                            
                            str = str + place.locality!
                            self.tfBlock.text = place.locality!
                        }
                        
                        self.tfStreet.text = str
                        
                        if place.postalCode != nil {
                            adressString = adressString + place.postalCode! + "\n"
                        }
                        if place.subAdministrativeArea != nil {
                            adressString = adressString + place.subAdministrativeArea! + " - "
                        }
                        if place.country != nil {
                            adressString = adressString + place.country!
                            self.tfCountry.text = place.country!
                        }
                        
                        
                        
//                        binding.etArea.setText(place.address)
//                        binding.etBlock.setText(addressList[0].locality.toString())
//                        binding.etStreet.setText(addressList[0].subLocality.toString() + " , " + addressList[0].locality.toString())
//                        binding.etCountry.setText(addressList[0].countryName.toString())
                        
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
      }
    
}


extension AddNewAddressVC: GMSAutocompleteViewControllerDelegate {
    
    

  // Handle the user's selection.
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//    print("Place name: \(place.name)")
//    print("Place ID: \(place.placeID)")
//    print("Place attributions: \(place.attributions)")
    
    if place.name != nil {
        self.tfAddressNickname.text = "\(String(describing: place.name!))"
    }
    
    if place.formattedAddress != nil {
        self.tfArea.text = "\(String(describing: place.formattedAddress!))"
    }

    
    let lat = place.coordinate.latitude
    let lon = place.coordinate.longitude
    print("lat lon",lat,lon)

    
    self.latValue = lat
    self.longvalue = lon
    
    //let cityCoords = CLLocation(latitude: place.coordinate.latitude , longitude: place.coordinate.longitude)
    
    //self.dismiss(animated: true, completion: nil)
    
//
//    guard let vc = R.storyboard.bookService.addLocationFromMapViewController() else{return}
//    vc.modalPresentationStyle = .overFullScreen
//    vc.comingFrom = "AddNewAddress"
//    vc.latitude = self.latValue
//    vc.longitude = self.longvalue
//    vc.delegate = self
//    presentVC(vc, false)
    
    //self.getAdressName(coords: cityCoords)
    
  }
    
//    internal func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        let views = viewController.view.subviews
//        let subviewsOfSubview = views.first!.subviews
//        let subOfNavTransitionView = subviewsOfSubview[1].subviews
//        let subOfContentView = subOfNavTransitionView[2].subviews
//        let searchBar = subOfContentView[0] as! UISearchBar
//        searchBar.text = "YOUR_TEXT"
//    }
    

  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
  }

  // User canceled the operation.
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    dismiss(animated: true, completion: nil)
  }

   //Turn the network activity indicator on and off again.
  func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }

  func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }

}
