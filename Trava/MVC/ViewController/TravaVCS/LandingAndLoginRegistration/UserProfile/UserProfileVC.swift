//
//  UserProfileVC.swift
//  Buraq24
//
//  Created by MANINDER on 13/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import IBAnimatable

enum ENUM_GENDER:String{
    
    case male = "Male"
    case female = "Female"
    case other = "Other"
}

extension UserProfileVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        do {
            let regex = try NSRegularExpression(pattern: ".*[^A-Za-z ].*", options: [])
            if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                 return false

            } else {
                return true
            }
        }
        catch {
            return true
        }
    }
}


class UserProfileVC: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet weak var textViewAddress: UITextView!
    @IBOutlet weak var btnOthers: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var textFieldLastName: UITextField!
    @IBOutlet weak var textFieldFirstName: UITextField!
    @IBOutlet var txtFieldFullName: UITextField!
    @IBOutlet var constraintBottomButton: NSLayoutConstraint!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var txfReferral: UITextField!
    @IBOutlet weak var genderStack: UIStackView!
    
    @IBOutlet weak var stackViewGender: UIStackView!
    @IBOutlet weak var stackViewEmail: UIStackView!
    @IBOutlet weak var addressStack: UIStackView!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblOther: UILabel!
    @IBOutlet weak var nationalIDStack: UIStackView!
    @IBOutlet weak var referalStack: UIStackView!
    @IBOutlet weak var txfNationalID: UITextField!
    @IBOutlet weak var btnOfficialIDFront: UIButton!
    @IBOutlet weak var btnOfficailIDBack: UIButton!
    @IBOutlet weak var btnRemoveOfficialIDBack: UIButton!
    @IBOutlet weak var btnRemoveOfficialIDFront: UIButton!
    @IBOutlet weak var btnAddressProofImage: UIButton!
    @IBOutlet weak var addressTableView: UITableView!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var officialImagesStack: UIStackView!
    @IBOutlet weak var addressProofImages: UIStackView!
    @IBOutlet weak var stackOther: UIStackView!
    
    @IBOutlet weak var viewProfileImage: UIView!
    @IBOutlet weak var imgViewProfile: UIImageView!
    
    @IBOutlet weak var btnUploadProfileImage: UIButton!
    @IBOutlet weak var stackFamilyName: UIStackView!
    @IBOutlet weak var txtFldFamilyName: UITextField!
    @IBOutlet weak var stackFamilyNumber: UIStackView!
    @IBOutlet weak var txtFldFamilyNumber: UITextField!
    @IBOutlet weak var stackNeighbourName: UIStackView!
    @IBOutlet weak var txtFldNeighbourName: UITextField!
    @IBOutlet weak var stackNeighbourNumber: UIStackView!
    @IBOutlet weak var txtFldNeighbourNumber: UITextField!
    @IBOutlet weak var btnIdentificationSchoolWork: UIButton!
    @IBOutlet weak var btnAddressproff: UIButton!
    @IBOutlet weak var btnRemoveIdentiSchoolWork: UIButton!
    @IBOutlet weak var btnRemoveAddressProof: UIButton!
    
    @IBOutlet weak var stackViewAddressProfPhoto: UIStackView!
    @IBOutlet weak var stackViewIdentificationSchoolWork: UIStackView!
    @IBOutlet weak var stackViewFirstName: UIStackView!
    
    @IBOutlet weak var stackViewlastName: UIStackView!
    
    @IBOutlet weak var vwBottomNeighbour: UIView!
    @IBOutlet weak var vwBottomFirstname: UIView!
    
    @IBOutlet weak var vwBottomReferal: UIView!
    @IBOutlet weak var vwBottomAddress: UIView!
    @IBOutlet weak var vwNationalId: UIView!
    @IBOutlet weak var vwBottomNeighbourContactNo: UIView!
    @IBOutlet weak var vwBottomPhoneNo: UIView!
    @IBOutlet weak var vwBottomEmergency: UIView!
    @IBOutlet weak var vwBottomEmail: UIView!
    @IBOutlet weak var vwBottomLastname: UIView!
    
    @IBOutlet weak var imgVwEdit: UIImageView!
    @IBOutlet weak var vwAddress: UIView!
    @IBOutlet weak var firstnameLabel: UILabel!
    @IBOutlet weak var lastnameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var familyNameLabel: UILabel!
    @IBOutlet weak var familyPhoneLabel: UILabel!
    @IBOutlet weak var neighbourNameLabel: UILabel!
    @IBOutlet weak var neighbourPhoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    //MARK:- PROPERTIES
    var gender:String = "Male"
    var address:String?
    var firstName:String?
    var lastName:String?
    var email: String?
    var isaddress = true
    var loginDetail : LoginDetail?
    var nationalId : String?
    var isOfficialIDFrontImageAdded: Bool?
    var isOfficialIDBackImageAdded: Bool?
    var isProfileImageAdded:Bool?
    var isIdentitySchoolWork:Bool?
    var isAddressProff:Bool?
    var isFirstName:Bool?
    var isLastName:Bool?
    var isEmail:Bool?
    var isGender:Bool?
    var isRefCode:Bool?
    var isAddress:Bool?
    var isNationalId:Bool?
    var isFamily:Bool?
    var isNeighbour:Bool?
    var addressProofRequire:Bool?
    var profilePicRequire:Bool?
    var identifySchoolWorkRequire:Bool?
    var addressImages = [UIImage]()
    var officialIDRequire = false
    var addressProofImage = false
    
    var frontImage:UIImage?
    var backImage:UIImage?
    var addressImage:UIImage?
    var identityImage:UIImage?
    var profileImage:UIImage?
    var uploadImage:UIImage?
    

    
    //MARK:- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        addKeyBoardObserver()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        removeKeyBoardObserver()
    }
    
    
    
    func setUpUI() {
        
        addressTableView.register(UINib(nibName: "UploadedDocCell", bundle: nil), forCellReuseIdentifier: "UploadedDocCell")
        
        addressTableView.delegate = self
        addressTableView.dataSource = self
        
        btnNext.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        imageView.setViewBackgroundColorTheme()
                
       
        
        vwBottomNeighbour.isHidden = true
        vwBottomFirstname.isHidden = true
        
        vwBottomReferal.isHidden = true
        vwBottomAddress.isHidden = true
        vwNationalId.isHidden = true
        vwBottomNeighbourContactNo.isHidden = true
        vwBottomPhoneNo.isHidden = true
        vwBottomEmergency.isHidden = true
        vwBottomEmail.isHidden = true
        vwBottomLastname.isHidden = true
        
        
        
        
        lblSubtitle.setAlignment()
        
        txfReferral.placeholder = "UserProfileVC.ReferalCode".localizedString
        btnNext.setTitle("UserProfileVC.Submit".localizedString, for: .normal)
        lblSubtitle.text = "UserProfileVC.JustSomeMore".localizedString

        
        
        if officialIDRequire {
            officialImagesStack.isHidden = false
        } else {
            officialImagesStack.isHidden = true
        }

        if addressProofImage {
            addressProofImages.isHidden = false
        } else {
            addressProofImages.isHidden = true
        }
        
        

        let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
        switch template {
        case .DeliverSome:
            addressStack.isHidden = true
            break
            
        case .GoMove:
            genderStack.isHidden = true 
            lblSubtitle.text = "We are almost there!"
            lblOther.text = "Non binary"
            addressStack.isHidden = true
            nationalIDStack.isHidden = true
            isaddress = false
            break
            
        case .Corsa:
            referalStack.isHidden = true
            lblOther.text = "Prefer not to say".localizedString
            stackViewGender.axis = .vertical
            break
            
        case .EagleRide:
            referalStack.isHidden = true
            stackOther.isHidden = true
            break
            
        default:
            nationalIDStack.isHidden = true
            addressStack.isHidden = false
        }
       
        
        
        
        
        
        if template == .DeliverSome  {
            vwBottomNeighbour.isHidden = false
            vwBottomFirstname.isHidden = false
            
            vwBottomReferal.isHidden = false
            vwBottomAddress.isHidden = false
            vwNationalId.isHidden = false
            vwBottomNeighbourContactNo.isHidden = false
            vwBottomPhoneNo.isHidden = false
            vwBottomEmergency.isHidden = false
            vwBottomEmail.isHidden = false
            vwBottomLastname.isHidden = false
            } else {
                textFieldFirstName.setPadding(10)
                textFieldFirstName.setBorderColorSecondary()
                textFieldFirstName.addShadowToTextFieldColorSecondary()
                textFieldFirstName.setAlignment()
            
            [textFieldFirstName,textFieldLastName].forEach { field in
                field?.delegate = self
            }
            
            
                textFieldLastName.setPadding(10)
                textFieldLastName.setBorderColorSecondary()
                textFieldLastName.addShadowToTextFieldColorSecondary()
                textFieldLastName.setAlignment()
                
                txfReferral.setPadding(10)
                txfReferral.setBorderColorSecondary()
                txfReferral.addShadowToTextFieldColorSecondary()
                txfReferral.setAlignment()
                
                textFieldEmail.setPadding(10)
                textFieldEmail.setBorderColorSecondary()
                textFieldEmail.addShadowToTextFieldColorSecondary()
                textFieldEmail.setAlignment()
                
                
                txtFldFamilyName.setPadding(10)
                txtFldFamilyName.setBorderColorSecondary()
                txtFldFamilyName.addShadowToTextFieldColorSecondary()
                txtFldFamilyName.setAlignment()
                
                
                txtFldFamilyNumber.setPadding(10)
                txtFldFamilyNumber.setBorderColorSecondary()
                txtFldFamilyNumber.addShadowToTextFieldColorSecondary()
                txtFldFamilyNumber.setAlignment()
                
                
                txtFldNeighbourName.setPadding(10)
                txtFldNeighbourName.setBorderColorSecondary()
                txtFldNeighbourName.addShadowToTextFieldColorSecondary()
                txtFldNeighbourName.setAlignment()
                
                
                txtFldNeighbourNumber.setPadding(10)
                txtFldNeighbourNumber.setBorderColorSecondary()
                txtFldNeighbourNumber.addShadowToTextFieldColorSecondary()
                txtFldNeighbourNumber.setAlignment()
                       
                
                txfNationalID.setBorderColorSecondary()
                txfNationalID.addShadowToTextFieldColorSecondary()
                txfNationalID.setLeftPaddingPoints(10)
                
                textViewAddress.setAlignment()
                textViewAddress.setBorderColorSecondary()
                textViewAddress.addShadowToTextViewColorSecondary()

        }
        
        
        if let user = UDSingleton.shared.userData?.userDetails?.user,let vender_id = user.vendor_id,vender_id > 0{
            textFieldFirstName.text = user.firstName
            textFieldLastName.text = user.lastName
//            textFieldEmail.text = user.email
            textViewAddress.text = user.address
        }
        
        
        
        btnFemale.setButtonWithTintColorSecondary()
        btnMale.setButtonWithTintColorSecondary()
        btnOthers.setButtonWithTintColorSecondary()
        btnUploadProfileImage.setButtonWithTintColorSecondary()
        
        hideShowFields()
        if loginDetail?.userDetails?.user?.email != "" {
//            textFieldEmail.text = loginDetail?.userDetails?.user?.email
//            textFieldEmail.isUserInteractionEnabled = false
        } else{
//            textFieldEmail.isUserInteractionEnabled = true
        }
        
        
        setSigns()
        
    }
    
    func setSigns(){
        
        [firstnameLabel,lastnameLabel,emailLabel,familyNameLabel,familyPhoneLabel,neighbourNameLabel,neighbourPhoneLabel,addressLabel].forEach { label in
            setManditory(label: label)
        }
    }
    
    func setManditory(label:UILabel?){
        if let text = label?.text {
            let star = UDSingleton.shared.appSettings?.appSettings?.is_showMandatorySign == "true" ? "*" : ""
            label?.text = text + star
        }
        
    }
    
    
    
    func hideShowFields(){
        
        guard let formDetailArray = UDSingleton.shared.appSettings?.user_forum else { return }
        
        
        let profilePic = formDetailArray.filter({/$0.key_name == "profile_pic"}).first
        
        
        if profilePic?.required == "0"{
            
            viewProfileImage.isHidden = true
            
            
        } else{
            
            viewProfileImage.isHidden = false
        }
        
         profilePicRequire = profilePic?.required == "1"
        
        
        let gender = formDetailArray.filter({/$0.key_name == "gender"}).first
        
        if gender?.required == "0"{
            
            genderStack.isHidden = true
            
        } else{
            
            genderStack.isHidden = false
        }
        
        let fname = formDetailArray.filter({/$0.key_name == "firstName"}).first
        
        if fname?.required == "0"{
            
            stackViewFirstName.isHidden = true
            
        } else{
            
            stackViewFirstName.isHidden = false
        }
        
        isFirstName = fname?.required == "1"
        
        let lName = formDetailArray.filter({/$0.key_name == "lastName"}).first
        
        if lName?.required == "0"{
            
            stackViewlastName.isHidden = true
            
        } else{
            
            stackViewlastName.isHidden = false
        }
        
        isLastName = lName?.required == "1"
        
        
        let email = formDetailArray.filter({/$0.key_name == "email"}).first
        
        if email?.required == "0"{
            
            stackViewEmail.isHidden = true
            
        } else{
            
            stackViewEmail.isHidden = false
        }
        
        isEmail = email?.required == "1"
        
        let ref = formDetailArray.filter({/$0.key_name == "referral_code"}).first
        
        if ref?.required == "0"{
            
            referalStack.isHidden = true
            
        } else{
            
            referalStack.isHidden = false
        }
        
        isRefCode = ref?.required == "1"
        
        let address = formDetailArray.filter({/$0.key_name == "address"}).first
        
        if address?.required == "0"{
            
            addressStack.isHidden = true
            
        } else{
            
            addressStack.isHidden = false
        }
        
        isAddress = address?.required == "1"
        
        let addressproof = formDetailArray.filter({/$0.key_name == "photo_proof_address"}).first
        
        if addressproof?.required == "0"{
            
            stackViewAddressProfPhoto.isHidden = true
           
            
        } else{
            
            stackViewAddressProfPhoto.isHidden = false
           
        }
        
        addressProofRequire = addressproof?.required == "1"
        
        let nationalId = formDetailArray.filter({/$0.key_name == "national_id"}).first
        
        if nationalId?.required == "0"{
            
            nationalIDStack.isHidden = true
            
        } else{
            
            nationalIDStack.isHidden = false
        }
        
        isNationalId = nationalId?.required == "1"
        
        let family = formDetailArray.filter({/$0.key_name == "name_phnNo_family_member"}).first
        
        if family?.required == "0"{
            
            stackFamilyName.isHidden = true
            stackFamilyNumber.isHidden = true
            
        } else{
            
            stackFamilyName.isHidden = false
            stackFamilyNumber.isHidden = false
        }
        
        isFamily = family?.required == "1"
        
        
        let neighbour = formDetailArray.filter({/$0.key_name == "name_phnNo_family_member"}).first
        
        if neighbour?.required == "0"{
            
            stackNeighbourName.isHidden = true
            stackNeighbourNumber.isHidden = true
            
        } else{
            
            stackNeighbourName.isHidden = false
            stackNeighbourNumber.isHidden = false
        }
        
        isNeighbour = neighbour?.required == "1"
        
        
        let identificationSchoolWork = formDetailArray.filter({/$0.key_name == "identification_school_or_work"}).first
        
        if identificationSchoolWork?.required == "0"{
            
            stackViewIdentificationSchoolWork.isHidden = true
            
            
        } else{
            
            stackViewIdentificationSchoolWork.isHidden = false
        
        }
        
         identifySchoolWorkRequire = identificationSchoolWork?.required == "1"
        
        let formFront = formDetailArray.filter({/$0.key_name == "official_id_front_photo"}).first
        
        let  official_id_front_photo = /formFront?.required
        
        let form = formDetailArray.filter({/$0.key_name == "official_id_back_photo"}).first
        
        let  official_id_back_photo = /form?.required
        
        if official_id_back_photo == "0" &&  official_id_front_photo == "0"{
            officialImagesStack.isHidden = true
            officialIDRequire = false
        } else {
            officialImagesStack.isHidden = false
            officialIDRequire = true
        }
        
        
        //        let form2 = formDetailArray.filter({/$0.key_name == "photo_proof_address"}).first
        //        let  photo_proof_address = /form2?.required
        //
        //        if photo_proof_address == "0" {
        //            isAddressProff = false
        //        } else {
        //            isAddressProff = true
        //        }
    }
    
    //MARK:- ACTIONS
    
    //back aciton
    @IBAction func actionBtnBackPressed(_ sender: UIButton) {
       // self.popVC(to: LandingAndPhoneInputVC.self)
        self.navigationController?.popViewController(animated: true)
    }
    
    //next button
    @IBAction func actionBtnNextPressed(_ sender: UIButton) {
        
        let strName = /txtFieldFullName?.text?.trimmed()
        let firstName = /textFieldFirstName.text?.trimmed()
        let lastName = /textFieldLastName.text?.trimmed()
        let address = /textViewAddress.text?.trimmed()
        let email = /textFieldEmail.text?.trimmed()
        
        
        if Validations.sharedInstance.validateProfileSetup(firstName: firstName, isFirstName: /isFirstName, lastName: lastName, isLastName: /isLastName, address: address, isAddres: /isAddress, email: email, isEmail: /isEmail, officialIDRequire: officialIDRequire, isOfficialIDFrontImageAdded: /isOfficialIDFrontImageAdded, isOfficialIDBackImageAdded: /isOfficialIDBackImageAdded, isAddressProof: /addressProofRequire, addressProofImage: /isAddressProff, isFamil: /isFamily, familyName: /txtFldFamilyName.text?.trimmed(), familyNumber: /txtFldFamilyNumber.text?.trimmed(), isNeighbour: /isNeighbour, neighbourName: /txtFldNeighbourName.text?.trimmed(), neighbourNumber: /txtFldNeighbourNumber.text?.trimmed(), isProfilePic: /profilePicRequire, isProfilePicAdded: /isProfileImageAdded, isidentitySCoolWork: /identifySchoolWorkRequire, isIdentitySchoolWorkAdded: /isIdentitySchoolWork, isNationalId: /isNationalId, nationalId: /txfNationalID.text?.trimmed(), isref: /isRefCode, refCode: /txfReferral.text?.trimmed()){
        
//        if Validations.sharedInstance.validateProfileSetup(firstName: firstName, lastName: lastName, address: isaddress ? address : "-", email: email, officialIDRequire: officialIDRequire,  isOfficialIDFrontImageAdded: /isOfficialIDFrontImageAdded, isOfficialIDBackImageAdded: /isOfficialIDBackImageAdded, addressProofImage: addressProofImage, addressImages: addressImages ) {
            
            if !stackViewEmail.isHidden {
               if Validations.sharedInstance.validateEmail(email: email) {
                    saveName(strName: strName)
                }
            } else {
                saveName(strName: strName)
            }
        }
    }
    
    //select gender action
    @IBAction func didSwitchGender(_ sender: UIButton) {
        
        btnMale.isSelected = sender == btnMale
        btnFemale.isSelected = sender == btnFemale
        btnOthers.isSelected = sender == btnOthers
        
        gender = btnMale.isSelected ? ENUM_GENDER.male.rawValue : btnFemale.isSelected ? ENUM_GENDER.female.rawValue : ENUM_GENDER.other.rawValue
    }
    
    @IBAction func btnActionAddPhotos(_ sender: UIButton) {
        
        switch sender {
            
        case btnUploadProfileImage:
            openPhotoOptionsActionSheet(buttonType: sender)
           break
            
        case btnOfficialIDFront:
            openPhotoOptionsActionSheet(buttonType: sender)
            break
            
        case btnOfficailIDBack:
            openPhotoOptionsActionSheet(buttonType: sender)
            break
        case btnIdentificationSchoolWork:
               openPhotoOptionsActionSheet(buttonType: sender)
               break
            
        case btnAddressproff:
            openPhotoOptionsActionSheet(buttonType: sender)
                   break
            
        case btnRemoveIdentiSchoolWork:
                   btnIdentificationSchoolWork.setImage(#imageLiteral(resourceName: "ic_add_bg-g"), for: .normal)
                   btnRemoveIdentiSchoolWork.isHidden = true
                   isIdentitySchoolWork = false
                   identityImage = nil
                   break
            
        case btnRemoveAddressProof:
                   btnAddressproff.setImage(#imageLiteral(resourceName: "ic_add_bg-g"), for: .normal)
                   btnRemoveAddressProof.isHidden = true
                   isAddressProff = false
                    addressImage = nil
                   break
            
        case btnRemoveOfficialIDFront:
            btnOfficialIDFront.setImage(#imageLiteral(resourceName: "ic_add_bg-g"), for: .normal)
            btnRemoveOfficialIDFront.isHidden = true
            isOfficialIDFrontImageAdded = false
             frontImage = nil
            break
            
        case btnRemoveOfficialIDBack:
            btnOfficailIDBack.setImage(#imageLiteral(resourceName: "ic_add_bg-g"), for: .normal)
            btnRemoveOfficialIDBack.isHidden = true
            isOfficialIDBackImageAdded = false
             backImage = nil
            break
            
        case btnAddressProofImage:
            openPhotoOptionsActionSheet(buttonType: sender)
            break
            
        default:
            break
        }
        
    }
    
    
    
}

//MARK:- API

extension UserProfileVC {
    static let shared = UserProfileVC()
    
    //api to update profile
    func openPhotoOptionsActionSheet(buttonType: UIButton) {
         self.view.endEditing(true)
        
        
        CameraImage.shared.captureImage(from: self, At: self , mediaType: nil, captureOptions: [.camera, .photoLibrary], allowEditting: true) { [unowned self] (image) in
            guard let img = image else { return }
                   
            switch buttonType {
                
            case self.btnUploadProfileImage:
                self.imgViewProfile.image = img
                self.isProfileImageAdded = true
                self.profileImage = img
                
                case self.btnOfficialIDFront:
                    self.btnOfficialIDFront.setImage(img, for: .normal)
                    self.btnRemoveOfficialIDFront.isHidden = false
                    self.isOfficialIDFrontImageAdded = true
                     self.frontImage = img
                    break
                   
                case self.btnOfficailIDBack:
                    self.btnOfficailIDBack.setImage(img, for: .normal)
                    self.btnRemoveOfficialIDBack.isHidden = false
                    self.isOfficialIDBackImageAdded = true
                     self.backImage = img
                    break
                
                case self.btnIdentificationSchoolWork:
                                 self.btnIdentificationSchoolWork.setImage(img, for: .normal)
                                 self.btnRemoveIdentiSchoolWork.isHidden = false
                                 self.isIdentitySchoolWork = true
                                  self.identityImage = img
                                 break
                
                case self.btnAddressproff:
                                 self.btnAddressproff.setImage(img, for: .normal)
                                 self.btnRemoveAddressProof.isHidden = false
                                 self.isAddressProff = true
                                  self.addressImage = img
                                 break
                
                case self.btnAddressProofImage:
                    self.addressImages.append(img)
                    self.tableHeightConstraint.constant = CGFloat(84 * /self.addressImages.count)
                    self.addressTableView.reloadData()
                    break
                   
               default:
                
                   break
               }
            }
        }
    
    
    func saveName(strName : String) {
        
        self.view.endEditing(true)
        self.address = textViewAddress.text
        self.firstName = textFieldFirstName.text
        self.lastName = textFieldLastName.text
        self.email = textFieldEmail.text
        self.nationalId = txfNationalID.text
        
        
        var imageArray:[[String:UIImage?]]? = [[String:UIImage]]()
        
        if /isProfileImageAdded{
            
            let dict = ["profile_pic" : profileImage]
            
            imageArray?.append(dict)
        }
        if /isIdentitySchoolWork{
            
            let dict = ["identification_school_or_work" : identityImage]
                       
                       imageArray?.append(dict)
        }
        
        if /isOfficialIDFrontImageAdded{
            
            let dict = ["official_id_front_photo" : frontImage]
                       
                       imageArray?.append(dict)
        }
        
        if /isOfficialIDBackImageAdded{
            
            let dict = ["official_id_back_photo" : backImage]
                       
                       imageArray?.append(dict)
        }
        
        if /isAddressProff{
            
            let dict = ["photo_proof_address" : addressImage]
                       
                       imageArray?.append(dict)
        }
        
        
        
        let objR = LoginEndpoint.addName(name: [/self.firstName,/self.lastName].joined(separator: " "), firstName: /self.firstName, lastName: /self.lastName, gender: self.gender, address: self.address, email: self.email, referral_code: txfReferral.text, nationalId: self.nationalId,fName:txtFldFamilyName.text!,fNumber: txtFldFamilyNumber.text!,neighbourName: txtFldNeighbourName.text!,neighbourPhone: txtFldNeighbourNumber.text!)
        
        objR.request(isImage:/imageArray?.count != 0,imageDict: imageArray, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" : /loginDetail?.userDetails?.accessToken, "secretdbkey": APIBasePath.secretDBKey ]) { [weak self] (response) in
            switch response {
                
            case .success(let data):
                
                guard let model = data as? UserDetail else { return }
                self?.loginDetail?.userDetails = model
                self?.saveUserInfo()
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message:/strError , type: .error )
            }
        }
    }
    
    
    func saveUserInfo() {
        
        UDSingleton.shared.userData = self.loginDetail
        
        //        guard let vc = R.storyboard.mainCab.selectMainCategoryViewController() else {return}
        //        pushVC(vc)
        
//        if APIBasePath.isPikkup == true{
//
//
//                    //guard let vc = R.storyboard.mainCab.selectMainCategoryViewController() else {return}
//                    //pushVC(vc)
//
//          guard let vc = R.storyboard.mainCab.playVideoViewController() else {return}
//        pushVC(vc)
//
//        }
//        else{
//            let appDelegate = UIApplication.shared.delegate as? AppDelegate
//                appDelegate?.setHomeAsRootVC()
//
//        }
        
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.setHomeAsRootVC()

        
       
    }
}

//MARK:- Common functions

extension UserProfileVC {
    
    func addKeyBoardObserver() {
        
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow),name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillhide),name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeKeyBoardObserver() {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            animateBottomView(true, height: keyboardHeight)
        }
    }
    
    @objc func keyboardWillhide(_ notification: Notification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            animateBottomView(false, height: keyboardHeight)
        }
    }
    
    
    func animateBottomView(_ isToShown : Bool , height : CGFloat) {
        
        UIView.animate(withDuration: 0.2) { [weak self] in
            if isToShown {
                self?.constraintBottomButton?.constant = (height + CGFloat(20))
            }else {
                self?.constraintBottomButton?.constant =  CGFloat(40)
            }
            self?.view.layoutIfNeeded()
        }
    }
    
}

extension UserProfileVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressImages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UploadedDocCell", for: indexPath) as! UploadedDocTableViewCell
        cell.uploadedImageView.image = addressImages[indexPath.row]
        cell.fileNameLabel.text = "Address Image\(indexPath.row + 1)"
        return cell
    }
    
    
}
