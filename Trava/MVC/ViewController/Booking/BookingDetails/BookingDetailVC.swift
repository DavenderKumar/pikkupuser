//
//  BookingDetailVC.swift
//  Buraq24
//
//  Created by MANINDER on 10/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import HCSStarRatingView

enum TabType : String  {
    case Past = "Past"
    case Upcoming = "Upcoming"
}

class BookingDetailVC: BaseVCCab {
    
    @IBOutlet weak var lblTipAmount: UILabel!
    //MARK:- Outlets
    @IBOutlet weak var previousChargesLabel: UILabel!
    @IBOutlet weak var previousChargesView: UIStackView!
    @IBOutlet var lblDropOffText: UILabel?
    @IBOutlet weak var imgDropOff: UIImageView!
    @IBOutlet weak var lblSurCharge: UILabel!
    @IBOutlet weak var cancellationReasonLabel: UILabel!
    
    @IBOutlet var imgViewMap: UIImageView!
    @IBOutlet var lblOrderToken: UILabel!
    @IBOutlet var lblOrderStatus: UILabel!
    
    @IBOutlet var lblOrderPrice: UILabel!
    @IBOutlet var lblOrderDate: UILabel!
    @IBOutlet var lblDropLocationAddress: UILabel?
    @IBOutlet weak var labelPickupAddress: UILabel!
    
    @IBOutlet var lblComment: UILabel!
    
    @IBOutlet var viewDriver: UIView!
    @IBOutlet var constraintDriverHeight: NSLayoutConstraint?
    
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var imgViewRating: UIImageView?
    @IBOutlet var imgViewDriver: UIImageView!
    @IBOutlet weak var viewStarRating: HCSStarRatingView!
    
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var constraintTopPricingView: NSLayoutConstraint?
    @IBOutlet var constraintDateViewHeight: NSLayoutConstraint?
    
    @IBOutlet var lblStartDate: UILabel?
    @IBOutlet var lblEndDate: UILabel?
    @IBOutlet var viewTime: UIView?
    @IBOutlet weak var btnCall: UIButton?
    
    @IBOutlet weak var constraintHeightViewDriver: NSLayoutConstraint!
    
    @IBOutlet var lblBrandName: UILabel!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblBaseFairValue: UILabel!
    @IBOutlet var labelTime: UILabel!
    @IBOutlet var labelTimeValue: UILabel!
    @IBOutlet var labelDistance: UILabel!
    @IBOutlet var labelDistanceValue: UILabel!
    @IBOutlet var labelNormalFareValue: UILabel!
    @IBOutlet var labelBookingFeeValue: UILabel!
    @IBOutlet var labelServiceChargeValue: UILabel!
    @IBOutlet var labelSubTotalValue: UILabel!
    @IBOutlet var lblFinalAmount: UILabel!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var orderDetailStack: UIStackView!
    @IBOutlet weak var checkLIstStack: UIStackView!
    @IBOutlet weak var checklistTableView: UITableView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblWaitingTimeHeading: UILabel!
    @IBOutlet weak var lblWaitingTimeValue: UILabel!
    
    
    @IBOutlet weak var viewAirport: UIStackView!
    @IBOutlet weak var viewZone: UIStackView!
    @IBOutlet weak var viewTollParking: UIStackView!
    @IBOutlet weak var lblAirport: UILabel!
    @IBOutlet weak var lblAirportValue: UILabel!
    @IBOutlet weak var lblZone: UILabel!
    @IBOutlet weak var lblZoneValue: UILabel!
    @IBOutlet weak var lblTollParking: UILabel!
    @IBOutlet weak var lblTollParkingvalue: UILabel!
    @IBOutlet weak var btnTrack: UIButton!
    @IBOutlet weak var btnPayment: UIButton!
    @IBOutlet weak var btnFullOrderDetails: UIButton!
 
    @IBOutlet weak var imgDeliveryProof1: UIImageView!
    @IBOutlet weak var imgDeliveryProof2: UIImageView!
    @IBOutlet weak var stackviewDeliveryProof: UIStackView!
    
    

    //MARK:- Properties
    
    var order : OrderCab?
    var type : TabType = .Past
    var delegateCancellation: RequestCancelDelegate?
    var checkListArray = [CheckLists]()
    var amount = 0
    let template = AppTemplate(rawValue: Int(/UDSingleton.shared.appSettings?.appSettings?.app_template) ?? 0)
    
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentController.isHidden = true
        orderDetailStack.isHidden = false
        checkLIstStack.isHidden = true
        
        getOrderDetails()
        // Do any additional setup after loading the view.
        checklistTableView.delegate = self
        checklistTableView.dataSource = self
        
        
        if order?.serviceId == 4 {
            lblDropOffText?.text = R.string.localizable.delivery_location()
        }
        
        
        switch template {
        case .Corsa:
            btnBack?.setImage(R.image.ic_back_arrow_black(), for: .normal)
        case .DeliverSome:
            imgDropOff.image = #imageLiteral(resourceName: "ic_nav")
            break
            
        default:
            imgDropOff.image = #imageLiteral(resourceName: "ic_drop_location")
        }
        
        
//        if /UDSingleton.shared.appTerminology?.key_value?.check_list == "1" {
//            segmentController.isHidden = true
//        } else {
//            segmentController.isHidden = false
//        }
        
        if order?.orderStatus.rawValue.lowercased() == "Pending".lowercased(){
            btnTrack.isHidden = true
        }
        
        btnTrack.isHidden = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lblOrderStatus.setTextColorTheme()
    }
    
    /*  override var preferredStatusBarStyle: UIStatusBarStyle {
     return .lightContent
     } */
    
    //MARK:- Action
    
    @IBAction func callDriver(_ sender: Any) {
        
        let val = "\(/self.order?.driverAssigned?.driverCountryCode)\(/self.order?.driverAssigned?.driverPhoneNumber)"
        self.callToNumber(number: val)
    }
    
    @IBAction func actionBtnFullOrderDetails(_ sender: Any) {
        guard let vc = R.storyboard.bookService.fullOrderDetailPikkup() else{return}
        vc.order = order
        ez.topMostVC?.presentVC(vc)
        
        
    }
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            orderDetailStack.isHidden = false
            checkLIstStack.isHidden = true
        case 1:
            orderDetailStack.isHidden = true
            checkLIstStack.isHidden = false
        default:
            break
        }
    }
    @IBAction func trackAction(_ sender: Any) {
        let vcs = navigationController?.viewControllers ?? []
        for vc in vcs {
            if let home = vc as? HomeVC{
                home.trackOrder = order
                home.getUpdatedData()
                navigationController?.popToViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func paymentAction(_ sender: Any) {
        if order?.orderStatus == .CustomerCancel || order?.orderStatus == .ServiceComplete{
            let alert = UIAlertController(title: "PikkUp", message: "Are you sure to book same service.", preferredStyle: .alert)
            alert.addAction(.init(title: "Cancel", style: .cancel, handler: { action in
                
            }))
            alert.addAction(.init(title: "Yes", style: .default, handler: { action in
                for vc in self.navigationController?.viewControllers ?? []{
                    print(String(describing: vc))
                    if let viewController =  vc as? HomeVC{
                        if let order = self.order{
                            self.navigationController?.popToViewController(viewController, animated: true)
                            viewController.bookCancelledService(order:order)
                        }

                    }
                }
            }))
            presentVC(alert, true)
        }else{
            showUrlPaymentView()
        }
    }
    
    @IBAction func actionBtnCancelPressed(_ sender: UIButton) {
        
        
        if order?.orderStatus.rawValue == "Pending" {
            showCancellationFormVC()
        } else {
            guard let acceptedDate = order?.accepted_at?.getLocalDate() else {return}
            
            let secondsDifference = acceptedDate.secondsInBetweenDate(Date())
            debugPrint("Seconds ========= \(secondsDifference)")
            
            if secondsDifference > 18000 { // 5 hours
                
                alertBoxOption(message: "cancel_ride_confirmation".localizedString  , title: "AppName".localizedString , leftAction: "no".localizedString , rightAction: "yes".localizedString , ok: { [weak self] in
                    
                    self?.showCancellationFormVC()
                    }, cancel: {})
                
            } else {
                showCancellationFormVC()
            }
        }
        
        btnTrack.isHidden = true
    }
    
    //MARK:- Functions
    
    func setupUI() {
        
        btnCancel.isHidden = type == .Past
        btnTrack.isHidden = type == .Past
        btnTrack.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        btnPayment.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        btnCancel.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        
        btnTrack.isHidden = true
        
    }
    
    func assignBookingData() {
        
        if  /UDSingleton.shared.appSettings?.appSettings?.currency_decimal_places == "3"{
            guard let orderDetail = order  else {return}
            let isOnline = orderDetail.payment?.paymentType?.lowercased() != "cash"
            let isPaymentInComplete = orderDetail.payment?.productStatus?.lowercased() != "completed"
            if isOnline && !isPaymentInComplete && type == .Past{
                btnPayment.isHidden = false
            }else{
                btnPayment.isHidden = true
            }
           if  order?.delivery_proof1 == "" || order?.delivery_proof2 == ""{
            
            stackviewDeliveryProof.isHidden = true
            
            
                
           }else{
            stackviewDeliveryProof.isHidden = false
            
            let oneUrl = /(APIBasePath.imageBasePathWithoutVersion + /order?.customer_signature).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            let twoUrl = /(APIBasePath.imageBasePathWithoutVersion + /order?.delivery_proof1).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            
            imgDeliveryProof1.sd_setImage(with: URL(string: twoUrl), placeholderImage: UIImage(), options: .refreshCached, progress: nil,completed: nil)
            imgDeliveryProof2.sd_setImage(with: URL(string: oneUrl), placeholderImage: UIImage(), options: .refreshCached, progress: nil,completed: nil)
            
           }
            
           
            
          
        
            
            
            
            
            if let orderToken = orderDetail.orderToken {
                
                lblOrderToken.text = "Id: " + orderToken
                
                guard let lati = orderDetail.dropOffLatitude else{return}
                guard let long = orderDetail.dropOffLongitude else{return}
                
                /* let strURL = Utility.shared.getGoogleMapImageURL(long:  String(long), lati: String(lati), width: Int(imgViewMap.size.width), height: Int(imgViewMap.size.height))
                 
                 if let url = NSURL(string: strURL) {
                 imgViewMap.sd_setImage(with: url as URL , completed: nil)
                 } */
                
                let strURL = Utility.shared.setStaticPolyLineOnMap(pickUpLat: /orderDetail.pickUpLatitude, pickUpLng: /orderDetail.pickUpLongitude, dropLat: /orderDetail.dropOffLatitude, dropLng: /orderDetail.dropOffLongitude,exactPath:/orderDetail.exactPath)
                
                
                //orderDetail.dropOffLatitude
                
                //orderDetail.dropOffLongitude
                
                // let nsString  = NSString.init(string: strURL).addingPercentEscapes(using: String.Encoding.utf8.rawValue)!
                
                
                
                if let url = NSURL(string: strURL) {
                    imgViewMap.sd_setImage(with:url as URL, placeholderImage: nil, options: .refreshCached, progress: nil, completed: nil)
                }
                
                lblDropLocationAddress?.text = /orderDetail.dropOffAddress
                labelPickupAddress.text = /orderDetail.pickUpAddress
                
                if let orderDate = orderDetail.orderLocalDate{
                    lblOrderDate.text = orderDate.getBookingDateStr()
                }
                
                guard let service = UDSingleton.shared.getService(categoryId: orderDetail.serviceId) else {return}
                guard let payment = orderDetail.payment else{return}
                
                if orderDetail.orderStatus == .CustomerCancel || orderDetail.orderStatus == .DriverCancel {
                    lblOrderPrice.text = ""
                  
                }
                else
                {
                    lblOrderPrice.text = "\(/payment.paymentType?.uppercased()) - \((/UDSingleton.shared.appSettings?.appSettings?.currency)) \((/payment.finalCharge).getThreeDecimalFloat())"
                  
                }
              
              
             
                
             
                
                let brand = orderDetail.orderProductDetail
                
                lblBrandName.text = brand?.productBrandName
                lblProductName.text = brand?.productName
                
                let currency = (/UDSingleton.shared.appSettings?.appSettings?.currency) + " "
                
                let baseFareValue = /brand?.productAlphaPrice
                let timeValue = (/Float(/orderDetail.payment?.order_time) * /brand?.price_per_hr)
                let distanceValue = (/Float(/orderDetail.payment?.orderDistance) * /brand?.pricePerDistance)
               
                    lblBaseFairValue.text = currency + "\(baseFareValue)".getThreeDecimalFloat()
               
                
                
                
                let time = "\(/orderDetail.payment?.order_time)".getTwoDecimalFloat()
                labelTime.text = "Time(\(time) min)"
                
                    labelTimeValue.text = currency + "\(timeValue)".getThreeDecimalFloat()
                
                labelTime.isHidden = true
                labelTimeValue.isHidden = true
              
               
                
                let distance = "\(/orderDetail.payment?.orderDistance)".getThreeDecimalFloat()
                labelDistance.text = "Distance(\(distance) km)"
                labelDistanceValue.text = currency + "\(distanceValue)".getThreeDecimalFloat()
                
                let normalValue = baseFareValue + timeValue + distanceValue
                labelNormalFareValue.text = currency + "\(normalValue)".getThreeDecimalFloat()
                
                var bookingCharge: Float
                if normalValue > /brand?.productActualPrice {
                    bookingCharge = (normalValue * (/orderDetail.payment?.buraqPercentage)/100)
                } else {
                    bookingCharge = (/brand?.productActualPrice * (/orderDetail.payment?.buraqPercentage)/100)
                }
                
                labelBookingFeeValue.text = currency + "\(bookingCharge)".getThreeDecimalFloat()
                
                let serviceCharge = (normalValue < /brand?.productActualPrice) ? (/brand?.productActualPrice - normalValue) : 0.00
                labelServiceChargeValue.text = currency + "\(serviceCharge)".getThreeDecimalFloat()
                
                
                let subtotal = normalValue + bookingCharge + serviceCharge
                
                    labelSubTotalValue.text = currency + "\(subtotal)".getThreeDecimalFloat()
                    lblFinalAmount.text =  currency + (/orderDetail.payment?.finalCharge).getThreeDecimalFloat()
               
               
                
                if let rating = orderDetail.rating?.ratingGiven {
                    
                    viewDriver.isHidden = false
                    
                    constraintDriverHeight?.constant = 32
                    guard let driver = orderDetail.driverAssigned else{return}
                    
                    lblComment.text = orderDetail.rating?.comment
                    lblDriverName.text = /driver.driverName
                    guard let driverimage = driver.driverProfilePic else{return}
                    if let url = URL(string: driverimage) {
                        imgViewDriver.sd_setImage(with: url , completed: nil)
                    }
                    // imgViewRating.setRating(rating: rating)
                    viewStarRating.value = CGFloat(rating)
                    
                }else {
                    viewDriver.isHidden = true
                    constraintDriverHeight?.constant = 0
                    // constraintTopPricingView?.constant = -60
                }
                
                if orderDetail.orderStatus == .Scheduled || orderDetail.orderStatus == .DriverApprovalPending || orderDetail.orderStatus == .DriverApproval || orderDetail.orderStatus == .DriverSchCancelled || orderDetail.orderStatus == .DriverSchTimeOut  || orderDetail.orderStatus == .SystyemSchCancelled {
                    
                    lblOrderStatus.text = "Scheduled".localizedString
                    
                    //  constraintTopPricingView?.constant = 60
                    
                    lblDriverName.text = /orderDetail.driverAssigned?.driverName
                    guard let driverimage = orderDetail.driverAssigned?.driverProfilePic else{return}
                    if let url = URL(string: driverimage) {
                        imgViewDriver.sd_setImage(with: url , completed: nil)
                    }
                    // imgViewRating.isHidden = true
                    
                    viewStarRating.isHidden = true
                    viewDriver.isHidden = false
                    constraintDriverHeight?.constant = 32
                    
                } else if  orderDetail.orderStatus == .CustomerCancel || orderDetail.orderStatus == .DriverCancel {
                    lblOrderStatus.text = "cancelled".localizedString
                    cancellationReasonLabel.isHidden = false
                    cancellationReasonLabel.text = "Cancellation Reason: \(/orderDetail.cancelReason)" + "\n\nCancellation Charges: " + (/UDSingleton.shared.appSettings?.appSettings?.currency) + " " + " \(/orderDetail.payment?.cancellation_charges)".getThreeDecimalFloat()
                    if orderDetail.orderStatus == .CustomerCancel{
                        btnPayment.setTitle("Re-Create", for: .normal)
                        btnPayment.isHidden = false
                    }
                } else if orderDetail.orderStatus == .ServiceComplete {
                    if orderDetail.orderStatus == .ServiceComplete{
                        btnPayment.setTitle("Re-Create", for: .normal)
                        btnPayment.isHidden = false
                    }
                }  else if orderDetail.orderStatus == .ServiceBreakdown {
                    
                    lblOrderStatus.text = "breakdown".localizedString
                } else if orderDetail.orderStatus == .etokenCustomerConfirm{
                    lblOrderStatus.text = "OrderStatus.Confirmed".localizedString
                }
                else if orderDetail.orderStatus == .Confirmed {
                    
                    lblOrderStatus.text = "OrderStatus.Confirmed".localizedString
                }
                else if orderDetail.orderStatus == .SerHalfWayStop {
                    
                    lblOrderStatus.text = "HalfWayStop".localizedString
                }
                else if order?.orderStatus == .reached {
                    
                    lblOrderStatus.text = "OrderStatus.Reached".localizedString
                }
                else if orderDetail.orderStatus == .ServiceTimeout || orderDetail.orderStatus == .etokenTimeOut{
                    
                    lblOrderStatus.text = "etoken.Timeout".localizedString
                }
                    
                else if orderDetail.orderStatus == .etokenCustomerPending{
                    
                    lblOrderStatus.text = "etoken.ApprovalPending".localizedString
                }
                    
                else if orderDetail.orderStatus == .etokenSerCustCancel{
                    lblOrderStatus.text = "etoken.Rejected".localizedString
                }
                    
                else if orderDetail.orderStatus == .Ongoing {
                    
                    lblOrderStatus.text  = orderDetail.orderStatus.rawValue
                    
                    constraintTopPricingView?.constant = 60
                    
                    lblDriverName.text = /orderDetail.driverAssigned?.driverName
                    guard let driverimage = orderDetail.driverAssigned?.driverProfilePic else{return}
                    if let url = URL(string: driverimage) {
                        imgViewDriver.sd_setImage(with: url , completed: nil)
                    }
                    // imgViewRating.isHidden = true
                    
                    viewStarRating.isHidden = true
                    viewDriver.isHidden = false
                    constraintDriverHeight?.constant = 32
                }
                else{
                    lblOrderStatus.text  = orderDetail.orderStatus.rawValue
                }
                
            }
            updateSemantic()
            
            if let orderDates = orderDetail.orderDates , let orderStartDate =  orderDates.startedDate  {
                
                viewTime?.isHidden = false
                constraintDateViewHeight?.constant = 60
                
                guard let orderCompleteDate = orderDates.completedDate else {return}
                
                lblStartDate?.text = orderStartDate.getBookingDateStr()
                lblEndDate?.text = orderCompleteDate.getBookingDateStr()
                
                self.lblEndDate?.isHidden =  false
                
                if   orderDetail.orderStatus.rawValue == "Ongoing".localizedString || orderDetail.orderStatus.rawValue == "DPending" || orderDetail.orderStatus.rawValue == "Scheduled".localizedString  {
                    self.lblEndDate?.isHidden = true
                }
                
            }else{
                constraintDateViewHeight?.constant = 0
                viewTime?.isHidden = true
            }
            
            if  orderDetail.orderStatus == .Confirmed || orderDetail.orderStatus == .reached || orderDetail.orderStatus == .CustomerCancel || orderDetail.orderStatus == .DriverCancel {
                viewTime?.isHidden = true
                constraintDateViewHeight?.constant = 0
            }
            
            if orderDetail.orderStatus == .CustomerCancel || orderDetail.orderStatus == .DriverCancel {
                
                orderDetailStack.isHidden = true
                stackviewDeliveryProof.isHidden = true
            }
            else
            {
                orderDetailStack.isHidden = false
                stackviewDeliveryProof.isHidden = false
                self.setPayment()
            }
        }else{
            guard let orderDetail = order  else {return}
            let isOnline = orderDetail.payment?.paymentType?.lowercased() != "cash"
            let isPaymentInComplete = orderDetail.payment?.productStatus?.lowercased() != "completed"
            if isOnline && isPaymentInComplete && type == .Past{
                btnPayment.isHidden = false
            }
           if  order?.delivery_proof1 == "" || order?.delivery_proof2 == ""{
            
                
            }
            
            imgDeliveryProof1.sd_setImage(with: URL(string: APIBasePath.imageBasePathWithoutVersion + (order?.delivery_proof1 ?? "")), placeholderImage: UIImage(), options: .refreshCached, progress: nil,completed: nil)
            imgDeliveryProof2.sd_setImage(with: URL(string: APIBasePath.imageBasePathWithoutVersion + (order?.delivery_proof2 ?? "")), placeholderImage: UIImage(), options: .refreshCached, progress: nil,completed: nil)
            
          
        
            
            
            
            
            if let orderToken = orderDetail.orderToken {
                
                lblOrderToken.text = "Id: " + orderToken
                
                guard let lati = orderDetail.dropOffLatitude else{return}
                guard let long = orderDetail.dropOffLongitude else{return}
                
                /* let strURL = Utility.shared.getGoogleMapImageURL(long:  String(long), lati: String(lati), width: Int(imgViewMap.size.width), height: Int(imgViewMap.size.height))
                 
                 if let url = NSURL(string: strURL) {
                 imgViewMap.sd_setImage(with: url as URL , completed: nil)
                 } */
                
                let strURL = Utility.shared.setStaticPolyLineOnMap(pickUpLat: /orderDetail.pickUpLatitude, pickUpLng: /orderDetail.pickUpLongitude, dropLat: /orderDetail.dropOffLatitude, dropLng: /orderDetail.dropOffLongitude,exactPath:/orderDetail.exactPath)
                
                
                //orderDetail.dropOffLatitude
                
                //orderDetail.dropOffLongitude
                
                // let nsString  = NSString.init(string: strURL).addingPercentEscapes(using: String.Encoding.utf8.rawValue)!
                
                
                
                if let url = NSURL(string: strURL) {
                    imgViewMap.sd_setImage(with:url as URL, placeholderImage: nil, options: .refreshCached, progress: nil, completed: nil)
                }
                
                lblDropLocationAddress?.text = /orderDetail.dropOffAddress
                labelPickupAddress.text = /orderDetail.pickUpAddress
                
                if let orderDate = orderDetail.orderLocalDate{
                    lblOrderDate.text = orderDate.getBookingDateStr()
                }
                
                guard let service = UDSingleton.shared.getService(categoryId: orderDetail.serviceId) else {return}
                guard let payment = orderDetail.payment else{return}
                
               
              
                if orderDetail.orderStatus == .CustomerCancel || orderDetail.orderStatus == .DriverCancel {
                    lblOrderPrice.text = ""
                  
                }
                else
                {
                    lblOrderPrice.text = "\(/payment.paymentType?.uppercased()) - \((/UDSingleton.shared.appSettings?.appSettings?.currency)) \((/payment.finalCharge).getThreeDecimalFloat())"
                  
                }
                
                let brand = orderDetail.orderProductDetail
                
                lblBrandName.text = brand?.productBrandName
                lblProductName.text = brand?.productName
                
                let currency = (/UDSingleton.shared.appSettings?.appSettings?.currency) + " "
                
                let baseFareValue = /brand?.productAlphaPrice
                let timeValue = (/Float(/orderDetail.payment?.order_time) * /brand?.price_per_hr)
                let distanceValue = (/Float(/orderDetail.payment?.orderDistance) * /brand?.pricePerDistance)
              
                
                    lblBaseFairValue.text = currency + "\(baseFareValue)".getTwoDecimalFloat()
             
                
                
                
                let time = "\(/orderDetail.payment?.order_time)".getTwoDecimalFloat()
                labelTime.text = "Time(\(time) min)"
                
            
                    labelTimeValue.text = currency + "\(timeValue)".getTwoDecimalFloat()
                    
                labelTime.isHidden = true
                labelTimeValue.isHidden = true
               
                
                let distance = "\(/orderDetail.payment?.orderDistance)".getTwoDecimalFloat()
                labelDistance.text = "Distance(\(distance) km)"
                labelDistanceValue.text = currency + "\(distanceValue)".getTwoDecimalFloat()
                
                let normalValue = baseFareValue + timeValue + distanceValue
                labelNormalFareValue.text = currency + "\(normalValue)".getTwoDecimalFloat()
                
                var bookingCharge: Float
                if normalValue > /brand?.productActualPrice {
                    bookingCharge = (normalValue * (/orderDetail.payment?.buraqPercentage)/100)
                } else {
                    bookingCharge = (/brand?.productActualPrice * (/orderDetail.payment?.buraqPercentage)/100)
                }
                
                labelBookingFeeValue.text = currency + "\(bookingCharge)".getTwoDecimalFloat()
                
                let serviceCharge = (normalValue < /brand?.productActualPrice) ? (/brand?.productActualPrice - normalValue) : 0.00
                labelServiceChargeValue.text = currency + "\(serviceCharge)".getTwoDecimalFloat()
                
                
                let subtotal = normalValue + bookingCharge + serviceCharge
              
              
                    labelSubTotalValue.text = currency + "\(subtotal)".getTwoDecimalFloat()
                    lblFinalAmount.text =  currency + (/orderDetail.payment?.finalCharge).getTwoDecimalFloat()
                    
             
               
                
                if let rating = orderDetail.rating?.ratingGiven {
                    
                    viewDriver.isHidden = false
                    
                    constraintDriverHeight?.constant = 32
                    guard let driver = orderDetail.driverAssigned else{return}
                    
                    lblComment.text = orderDetail.rating?.comment
                    lblDriverName.text = /driver.driverName
                    guard let driverimage = driver.driverProfilePic else{return}
                    if let url = URL(string: driverimage) {
                        imgViewDriver.sd_setImage(with: url , completed: nil)
                    }
                    // imgViewRating.setRating(rating: rating)
                    viewStarRating.value = CGFloat(rating)
                    
                }else {
                    viewDriver.isHidden = true
                    constraintDriverHeight?.constant = 0
                    // constraintTopPricingView?.constant = -60
                }
                
                if orderDetail.orderStatus == .Scheduled || orderDetail.orderStatus == .DriverApprovalPending || orderDetail.orderStatus == .DriverApproval || orderDetail.orderStatus == .DriverSchCancelled || orderDetail.orderStatus == .DriverSchTimeOut  || orderDetail.orderStatus == .SystyemSchCancelled {
                    
                    lblOrderStatus.text = "Scheduled".localizedString
                    
                    //  constraintTopPricingView?.constant = 60
                    
                    lblDriverName.text = /orderDetail.driverAssigned?.driverName
                    guard let driverimage = orderDetail.driverAssigned?.driverProfilePic else{return}
                    if let url = URL(string: driverimage) {
                        imgViewDriver.sd_setImage(with: url , completed: nil)
                    }
                    // imgViewRating.isHidden = true
                    
                    viewStarRating.isHidden = true
                    viewDriver.isHidden = false
                    constraintDriverHeight?.constant = 32
                    
                } else if  orderDetail.orderStatus == .CustomerCancel || orderDetail.orderStatus == .DriverCancel {
                    lblOrderStatus.text = "cancelled".localizedString
                    lblOrderStatus.textColor = UIColor.red
                    cancellationReasonLabel.isHidden = false
                    cancellationReasonLabel.text = "Cancellation Reason: \(/orderDetail.cancelReason)" + "\n\nCancellation Charges: " + (/UDSingleton.shared.appSettings?.appSettings?.currency) + " " + " \(/orderDetail.payment?.cancellation_charges)".getThreeDecimalFloat()
                    
                } else if orderDetail.orderStatus == .ServiceComplete {
                    
                    lblOrderStatus.text = "completed".localizedString
                }  else if orderDetail.orderStatus == .ServiceBreakdown {
                    
                    lblOrderStatus.text = "breakdown".localizedString
                } else if orderDetail.orderStatus == .etokenCustomerConfirm{
                    lblOrderStatus.text = "OrderStatus.Confirmed".localizedString
                }
                else if orderDetail.orderStatus == .Confirmed {
                    
                    lblOrderStatus.text = "OrderStatus.Confirmed".localizedString
                }
                else if orderDetail.orderStatus == .SerHalfWayStop {
                    
                    lblOrderStatus.text = "HalfWayStop".localizedString
                }
                else if order?.orderStatus == .reached {
                    
                    lblOrderStatus.text = "OrderStatus.Reached".localizedString
                }
                else if orderDetail.orderStatus == .ServiceTimeout || orderDetail.orderStatus == .etokenTimeOut{
                    
                    lblOrderStatus.text = "etoken.Timeout".localizedString
                }
                    
                else if orderDetail.orderStatus == .etokenCustomerPending{
                    
                    lblOrderStatus.text = "etoken.ApprovalPending".localizedString
                }
                    
                else if orderDetail.orderStatus == .etokenSerCustCancel{
                    lblOrderStatus.text = "etoken.Rejected".localizedString
                }
                    
                else if orderDetail.orderStatus == .Ongoing {
                    
                    lblOrderStatus.text  = orderDetail.orderStatus.rawValue
                    
                    constraintTopPricingView?.constant = 60
                    
                    lblDriverName.text = /orderDetail.driverAssigned?.driverName
                    guard let driverimage = orderDetail.driverAssigned?.driverProfilePic else{return}
                    if let url = URL(string: driverimage) {
                        imgViewDriver.sd_setImage(with: url , completed: nil)
                    }
                    // imgViewRating.isHidden = true
                    
                    viewStarRating.isHidden = true
                    viewDriver.isHidden = false
                    constraintDriverHeight?.constant = 32
                }
                else{
                    lblOrderStatus.text  = orderDetail.orderStatus.rawValue
                }
                
            }
            updateSemantic()
            
            if let orderDates = orderDetail.orderDates , let orderStartDate =  orderDates.startedDate  {
                
                viewTime?.isHidden = false
                constraintDateViewHeight?.constant = 60
                
                guard let orderCompleteDate = orderDates.completedDate else {return}
                
                lblStartDate?.text = orderStartDate.getBookingDateStr()
                lblEndDate?.text = orderCompleteDate.getBookingDateStr()
                
                self.lblEndDate?.isHidden =  false
                
                if   orderDetail.orderStatus.rawValue == "Ongoing".localizedString || orderDetail.orderStatus.rawValue == "DPending" || orderDetail.orderStatus.rawValue == "Scheduled".localizedString  {
                    self.lblEndDate?.isHidden = true
                }
                
            }else{
                constraintDateViewHeight?.constant = 0
                viewTime?.isHidden = true
            }
            
            if  orderDetail.orderStatus == .Confirmed || orderDetail.orderStatus == .reached || orderDetail.orderStatus == .CustomerCancel || orderDetail.orderStatus == .DriverCancel {
                viewTime?.isHidden = true
                constraintDateViewHeight?.constant = 0
            }
            
            
            if orderDetail.orderStatus == .CustomerCancel || orderDetail.orderStatus == .DriverCancel {
                
                orderDetailStack.isHidden = true
                stackviewDeliveryProof.isHidden = true
                lblOrderPrice.text = ""
                
            }
            else
            {
                orderDetailStack.isHidden = false
                stackviewDeliveryProof.isHidden = false
                self.setPayment()
            }
            
            
        }
        

    }
    
    
    func setPayment() {
        
        if  /UDSingleton.shared.appSettings?.appSettings?.currency_decimal_places == "3"{
            
            guard let currentOrd = order  else {return}
            let payment = currentOrd.payment
            let previousCharges = Float(payment?.previous_charges ?? "") ?? 0
            let brand = currentOrd.orderProductDetail
            let currency = (/UDSingleton.shared.appSettings?.appSettings?.currency) + " "
            if previousCharges == 0 {
                previousChargesView.isHidden = true
                previousChargesLabel.text = ""
            }else{
                previousChargesLabel.text = currency + "\((/payment?.previous_charges).getThreeDecimalFloat())"
                previousChargesView.isHidden = false
            }
            
            
            
            
            if currentOrd.booking_type == "Package" {
                
                
                lblBrandName.text = brand?.productBrandName
                lblProductName.text = brand?.productName
                
                let baseFareValue = /Float(/currentOrd.payment?.productAplhaCharge)
                let timeValue = (/Float(/currentOrd.payment?.extra_time)) * (/Float(/currentOrd.payment?.price_per_min))
                let distanceValue = (/Float(/currentOrd.payment?.extra_distance)) * (/Float(/currentOrd.payment?.price_per_km))
                
            
                lblBaseFairValue.text = currency + "\(baseFareValue)".getThreeDecimalFloat()
                    
                
                let time = "\(/currentOrd.payment?.extra_distance)".getThreeDecimalFloat()
                labelTime.text = "Extra Time(\(time) min)"
           
                
                let distance = "\(/currentOrd.payment?.extra_distance)".getThreeDecimalFloat()
                labelDistance.text = "Extra Distance(\(distance) km)"
                labelDistanceValue.text = currency + "\(distanceValue)".getThreeDecimalFloat()
                
                let bookingCharge: Float = 0.0
                let serviceCharges: Float = 0.0
                
                let normalValue = baseFareValue + timeValue + distanceValue
                labelNormalFareValue.text = currency + "\(normalValue)".getThreeDecimalFloat()
                
                labelBookingFeeValue.text = currency + "\(bookingCharge)".getThreeDecimalFloat()
                labelServiceChargeValue.text = currency + "\(serviceCharges)".getThreeDecimalFloat()
                
                
             
                    labelTimeValue.text = currency + "\(timeValue)".getThreeDecimalFloat()
                    
                
            }else{
                
                viewAirport.isHidden = true
                viewZone.isHidden = true
                viewTollParking.isHidden = true
                let baseFareValue = /Float(/currentOrd.payment?.productAplhaCharge)
                let timeValue = (/Float(/currentOrd.payment?.order_time)) * (/Float(/currentOrd.payment?.product_per_hr_charge))
                var waitingTimeValue:Float = 0.0
                
                if template == .Corsa{
                    
                    waitingTimeValue = (/Float(/currentOrd.payment?.waiting_charges))
                    
                } else{
                    
                   // waitingTimeValue = (/Float(/currentOrd.payment?.waiting_charges)) * (/Float(/currentOrd.payment?.waiting_time))
                    waitingTimeValue =   (/Float(/currentOrd.payment?.waiting_charges))
                    
                }
                let distanceValue = (/Float(/currentOrd.payment?.orderDistance)) * (/Float(/currentOrd.payment?.product_per_distance_charge))
                
             
                    lblBaseFairValue.text = currency + "\(baseFareValue)".getThreeDecimalFloat()
              
               
                
                let time = "\(/currentOrd.payment?.order_time)".getThreeDecimalFloat()
                labelTime.text = "Time(\(time) min)"
                
               
                    labelTimeValue.text = currency + "\(timeValue)".getThreeDecimalFloat()
              
                labelTime.isHidden = true
                labelTimeValue.isHidden = true
                
                let airportCharge = Float(Double(currentOrd.payment?.airport_charges ?? "") ?? 0.0)
                let zoneCharge = Float(Double(currentOrd.payment?.zone_charges ?? "") ?? 0.0)
                let tollCharge = Float(Double(currentOrd.payment?.toll_charges ?? "") ?? 0.0)
                let parkingCharge = Float(Double(currentOrd.payment?.parking_charges ?? "") ?? 0.0)
                
                
                if airportCharge > 0.0{
                    
                    viewAirport.isHidden = false
                    lblAirportValue.text = currency +  "\(airportCharge)".getThreeDecimalFloat()
                }
                
                
                
//                let tipAmount = Float(Double(currentOrd.payment?.tipCharge ?? "") ?? 0.0)
//                lblTipAmount.text = currency + "\(tipAmount)".getThreeDecimalFloat()
                
                
                var tipAmount = ""
                if /currentOrd.payment?.tip == 0
                    {
                    tipAmount = "0.000"
                }
                else
                {
                    tipAmount = "\(currentOrd.payment?.tip ?? 0)".getThreeDecimalFloat()
                }
                
                lblTipAmount.text = currency + tipAmount
                
                
                if zoneCharge > 0.0{
                    
                    viewZone.isHidden = false
                    lblZoneValue.text =  currency +  "\(zoneCharge)".getThreeDecimalFloat()
                }
                
                if (tollCharge + parkingCharge) > 0.0{
                    
                    viewTollParking.isHidden = false
                    lblTollParkingvalue.text =  currency +  "\(tollCharge + parkingCharge)".getThreeDecimalFloat()
                    
                }
                
                let countRideStops = /currentOrd.ride_stops?.count
               // var waitingTime = 0.000
                
//                if countRideStops != 0 {
//                    for i in 0..<countRideStops
//                    {
//                        let waitTime =  /currentOrd.ride_stops?[i].waiting_time
//
//                        waitingTime = waitingTime + (Double(waitTime) ?? 0.0)
//
//                    }
//                }
//
//                let waitingTime1 = "\(/currentOrd.waiting_time)".getTwoDecimalFloat()
//
//                waitingTime = waitingTime + (Double(waitingTime1) ?? 0.000)
                
             //   let waitingTime = "\(/currentOrd.payment?.waiting_time)".getThreeDecimalFloat()
                
//                var waitingTime = 0.000
//                let waitingTimePayment = "\(/currentOrd.payment?.waiting_time)".getThreeDecimalFloat()
//                let minimum_waiting_time = (/UDSingleton.shared.appSettings?.appSettings?.minimum_waiting_time)
//                let minimum_waiting_time1 = (Double(minimum_waiting_time.getThreeDecimalFloat()) ?? 0.000)
//
//                let waitingTimeOuter = (Double("\(/currentOrd.waiting_time)".getThreeDecimalFloat()) ?? 0.000)
//
//                var balanceWaitingTime = 0.000
//                if waitingTimeOuter > minimum_waiting_time1{
//                    balanceWaitingTime = waitingTimeOuter - minimum_waiting_time1
//                }
//
//                waitingTime = balanceWaitingTime + (Double(waitingTimePayment) ?? 0.0)
                
                
                
                
                let waitingTime = "\(/currentOrd.payment?.waiting_time)".getThreeDecimalFloat()
                
                if template == .Corsa{
                    
                    lblWaitingTimeHeading.text = "Waiting Charges"
                    lblWaitingTimeValue.text = currency + "\(waitingTimeValue)".getThreeDecimalFloat()
                    
                } else{
                    //lblWaitingTimeHeading.text = "Waiting Charges"
                    lblWaitingTimeHeading.text = "Waiting Time(\(waitingTime) min)"
                    lblWaitingTimeValue.text = currency + "\(waitingTimeValue)".getThreeDecimalFloat()
                }
                
                let distance = "\(/currentOrd.payment?.orderDistance)".getThreeDecimalFloat()
                labelDistance.text = "Distance(\(distance) km)"
                labelDistanceValue.text = currency + "\(distanceValue)".getThreeDecimalFloat()
                
                var normalValue = baseFareValue + timeValue + distanceValue
                labelNormalFareValue.text = currency + "\(normalValue)".getThreeDecimalFloat()
                normalValue = normalValue + airportCharge + zoneCharge + tollCharge + parkingCharge + waitingTimeValue
                var bookingCharge: Float
                var serviceCharges: Float
                let actualValue = (/Float(/currentOrd.payment?.productActualValue))
                let totalPercentage = ((/currentOrd.payment?.buraqPercentage)/100)
                let exactValue = normalValue + previousCharges
                if exactValue > actualValue {
                    
                    bookingCharge = normalValue * totalPercentage
                    serviceCharges = 0.0
                }else {
                    
                    bookingCharge = actualValue * totalPercentage
                  //  let exactValue = normalValue + previousCharges
                  
                    if exactValue > actualValue {
                        serviceCharges = exactValue - actualValue
                    }else{
                        serviceCharges = actualValue - exactValue
                    }
                }
                
                labelBookingFeeValue.text = currency + "\(bookingCharge)".getThreeDecimalFloat()
                labelServiceChargeValue.text = currency + "\(serviceCharges)".getThreeDecimalFloat()
                let subtotal = normalValue + bookingCharge + serviceCharges + previousCharges
                
                labelSubTotalValue.text = currency + "\(/currentOrd.payment?.finalCharge)".getThreeDecimalFloat()
                    // labelSubTotalValue.text = currency + "\(subtotal)".getThreeDecimalFloat()
                lblSurCharge.text = currency + "\(/currentOrd.payment?.sur_charge)".getThreeDecimalFloat()
                lblFinalAmount.text =  currency + (/currentOrd.payment?.finalCharge).getThreeDecimalFloat()
            }
            
        }else{
            guard let currentOrd = order  else {return}
            let payment = currentOrd.payment
            let previousCharges = Float(payment?.previous_charges ?? "") ?? 0
            let brand = currentOrd.orderProductDetail
            let currency = (/UDSingleton.shared.appSettings?.appSettings?.currency) + " "
            if previousCharges == 0 {
                previousChargesView.isHidden = true
                previousChargesLabel.text = ""
            }else{
                previousChargesLabel.text = currency + "\((/payment?.previous_charges).getTwoDecimalFloat())"
                previousChargesView.isHidden = false
            }
            
            
            
            
            if currentOrd.booking_type == "Package" {
                
                
                lblBrandName.text = brand?.productBrandName
                lblProductName.text = brand?.productName
                
                let baseFareValue = /Float(/currentOrd.payment?.productAplhaCharge)
                let timeValue = (/Float(/currentOrd.payment?.extra_time)) * (/Float(/currentOrd.payment?.price_per_min))
                let distanceValue = (/Float(/currentOrd.payment?.extra_distance)) * (/Float(/currentOrd.payment?.price_per_km))
                
                if  /UDSingleton.shared.appSettings?.appSettings?.currency_decimal_places == "3"{
                    lblBaseFairValue.text = currency + "\(baseFareValue)".getThreeDecimalFloat()
                    
                }
                else{
                    lblBaseFairValue.text = currency + "\(baseFareValue)".getTwoDecimalFloat()
                }
                
                
                
                let time = "\(/currentOrd.payment?.extra_distance)".getTwoDecimalFloat()
                labelTime.text = "Extra Time(\(time) min)"
           
               
                
                let distance = "\(/currentOrd.payment?.extra_distance)".getTwoDecimalFloat()
                labelDistance.text = "Extra Distance(\(distance) km)"
                labelDistanceValue.text = currency + "\(distanceValue)".getTwoDecimalFloat()
                
                let bookingCharge: Float = 0.0
                let serviceCharges: Float = 0.0
                
                let normalValue = baseFareValue + timeValue + distanceValue
                labelNormalFareValue.text = currency + "\(normalValue)".getTwoDecimalFloat()
                
                labelBookingFeeValue.text = currency + "\(bookingCharge)".getTwoDecimalFloat()
                labelServiceChargeValue.text = currency + "\(serviceCharges)".getTwoDecimalFloat()
                
                
                if  /UDSingleton.shared.appSettings?.appSettings?.currency_decimal_places == "3"{
                    labelTimeValue.text = currency + "\(timeValue)".getThreeDecimalFloat()
                    
                }else{
                    labelTimeValue.text = currency + "\(timeValue)".getTwoDecimalFloat()
                }
                
            }else{
                
                viewAirport.isHidden = true
                viewZone.isHidden = true
                viewTollParking.isHidden = true
                let baseFareValue = /Float(/currentOrd.payment?.productAplhaCharge)
                let timeValue = (/Float(/currentOrd.payment?.order_time)) * (/Float(/currentOrd.payment?.product_per_hr_charge))
                var waitingTimeValue:Float = 0.0
                
                if template == .Corsa{
                    
                    waitingTimeValue = (/Float(/currentOrd.payment?.waiting_charges))
                    
                } else{
                    
                    waitingTimeValue = (/Float(/currentOrd.payment?.waiting_charges))
                   // waitingTimeValue =   (/Float(/currentOrd.payment?.waiting_time)) * (/Float(/currentOrd.payment?.product_per_hr_charge))
                    
                }
                let distanceValue = (/Float(/currentOrd.payment?.orderDistance)) * (/Float(/currentOrd.payment?.product_per_distance_charge))
                
                if  /UDSingleton.shared.appSettings?.appSettings?.currency_decimal_places == "3"{
                    lblBaseFairValue.text = currency + "\(baseFareValue)".getThreeDecimalFloat()
                }else{
                    lblBaseFairValue.text = currency + "\(baseFareValue)".getTwoDecimalFloat()
                    
                }
               
                
                let time = "\(/currentOrd.payment?.order_time)".getTwoDecimalFloat()
                labelTime.text = "Time(\(time) min)"
                
                if  /UDSingleton.shared.appSettings?.appSettings?.currency_decimal_places == "3"{
                    labelTimeValue.text = currency + "\(timeValue)".getThreeDecimalFloat()
                }else{
                    labelTimeValue.text = currency + "\(timeValue)".getTwoDecimalFloat()
                }
               
                
                labelTime.isHidden = true
                labelTimeValue.isHidden = true
                
                let airportCharge = Float(Double(currentOrd.payment?.airport_charges ?? "") ?? 0.0)
                let zoneCharge = Float(Double(currentOrd.payment?.zone_charges ?? "") ?? 0.0)
                let tollCharge = Float(Double(currentOrd.payment?.toll_charges ?? "") ?? 0.0)
                let parkingCharge = Float(Double(currentOrd.payment?.parking_charges ?? "") ?? 0.0)
                
                
                if airportCharge > 0.0{
                    
                    viewAirport.isHidden = false
                    lblAirportValue.text = currency +  "\(airportCharge)".getTwoDecimalFloat()
                }
                
                if zoneCharge > 0.0{
                    
                    viewZone.isHidden = false
                    lblZoneValue.text =  currency +  "\(zoneCharge)".getTwoDecimalFloat()
                }
                
                if (tollCharge + parkingCharge) > 0.0{
                    
                    viewTollParking.isHidden = false
                    lblTollParkingvalue.text =  currency +  "\(tollCharge + parkingCharge)".getTwoDecimalFloat()
                    
                }
                
                
                
                //let waitingTime = "\(/currentOrd.payment?.waiting_time)".getTwoDecimalFloat()
                
                let countRideStops = /currentOrd.ride_stops?.count
                
                
//                var waitingTime = 0.000
//
//                if countRideStops != 0 {
//                    for i in 0..<countRideStops
//                    {
//                        let waitTime =  /currentOrd.ride_stops?[i].waiting_time
//
//                        waitingTime = waitingTime + (Double(waitTime) ?? 0.0)
//
//                    }
//                }
//
//                let waitingTime1 = "\(/currentOrd.waiting_time)".getTwoDecimalFloat()
//
//                waitingTime = waitingTime + (Double(waitingTime1) ?? 0.000)
                
//                var waitingTime = 0.000
//                let waitingTimePayment = "\(/currentOrd.payment?.waiting_time)".getThreeDecimalFloat()
//                let minimum_waiting_time = (/UDSingleton.shared.appSettings?.appSettings?.minimum_waiting_time)
//                let minimum_waiting_time1 = (Double(minimum_waiting_time.getThreeDecimalFloat()) ?? 0.000)
//               
//                let waitingTimeOuter = (Double("\(/currentOrd.waiting_time)".getThreeDecimalFloat()) ?? 0.000)
//                
//                var balanceWaitingTime = 0.000
//                if waitingTimeOuter > minimum_waiting_time1{
//                    balanceWaitingTime = waitingTimeOuter - minimum_waiting_time1
//                }
//                
//                waitingTime = balanceWaitingTime + (Double(waitingTimePayment) ?? 0.0)
                
                let waitingTime = "\(/currentOrd.payment?.waiting_time)".getThreeDecimalFloat()
                
                if template == .Corsa{
                    
                    lblWaitingTimeHeading.text = "Waiting Charges"
                    lblWaitingTimeValue.text = currency + "\(waitingTimeValue)".getTwoDecimalFloat()
                    
                } else{
                    lblWaitingTimeHeading.text = "Waiting Time(\(waitingTime) min)"
                   // lblWaitingTimeHeading.text = "Waiting Charges"
                    lblWaitingTimeValue.text = currency + "\(waitingTimeValue)".getTwoDecimalFloat()
                }
                
                let distance = "\(/currentOrd.payment?.orderDistance)".getTwoDecimalFloat()
                labelDistance.text = "Distance(\(distance) km)"
                labelDistanceValue.text = currency + "\(distanceValue)".getTwoDecimalFloat()
                
                var normalValue = baseFareValue + timeValue + distanceValue
                labelNormalFareValue.text = currency + "\(normalValue)".getTwoDecimalFloat()
                normalValue = normalValue + airportCharge + zoneCharge + tollCharge + parkingCharge + waitingTimeValue
                var bookingCharge: Float
                var serviceCharges: Float
                let actualValue = (/Float(/currentOrd.payment?.productActualValue))
                let totalPercentage = ((/currentOrd.payment?.buraqPercentage)/100)
                let exactValue = normalValue + previousCharges
                if exactValue > actualValue {
                    
                    bookingCharge = normalValue * totalPercentage
                    serviceCharges = 0.0
                }else {
                    
                    bookingCharge = actualValue * totalPercentage
                  //  let exactValue = normalValue + previousCharges
                  
                    if exactValue > actualValue {
                        serviceCharges = exactValue - actualValue
                    }else{
                        serviceCharges = actualValue - exactValue
                    }
                }
                
                labelBookingFeeValue.text = currency + "\(bookingCharge)".getTwoDecimalFloat()
                labelServiceChargeValue.text = currency + "\(serviceCharges)".getTwoDecimalFloat()
                let subtotal = normalValue + bookingCharge + serviceCharges + previousCharges
                labelSubTotalValue.text = currency + "\(subtotal)".getTwoDecimalFloat()
                lblSurCharge.text = currency + "\(/currentOrd.payment?.sur_charge)".getTwoDecimalFloat()
                lblFinalAmount.text =  currency + (/currentOrd.payment?.finalCharge).getTwoDecimalFloat()
            }
            
        }
        
        
    
    }
    private  func updateSemantic() {
        
        if  LanguageFile.shared.isLanguageRightSemantic() {
            lblOrderPrice.textAlignment = .left
            lblOrderStatus.textAlignment = .left
            lblOrderToken.textAlignment  = .right
            lblOrderDate.textAlignment = .right
        }
    }
    
    
    func showUrlPaymentView(){
        
        guard let current = order else{return}
        guard  let vc = R.storyboard.sideMenu.addCardViewControllerCab() else{return}
        vc.url = current.payment?.paymentBody
        vc.webPayment = {
            DispatchQueue.main.async {
                Alerts.shared.show(alert: "AppName".localizedString, message: "Payment done successfully".localizedString , type: .error )
            }
        }
        self.pushVC(vc)
    }
}

extension BookingDetailVC : RequestCancelDelegate {
    
    func showCancellationFormVC() {
        
        guard let orderId = order?.orderId , let formCancelation = R.storyboard.bookService.cancellationVC() else { return }
        // formCancelation.view.backgroundColor = UIColor.colorDarkGrayPopUp
        formCancelation.modalPresentationStyle = .overCurrentContext
        formCancelation.modalTransitionStyle = .crossDissolve
        formCancelation.orderId = orderId
        formCancelation.delegateCancellation = self
        presentVC(formCancelation, true)
        
        
    }
    
    func didSuccessOnCancelRequest() {
        if let orderId = order?.orderId, order?.orderStatus != OrderStatus.Pending{
            let nc = NotificationCenter.default
           // nc.post(name: Notification.Name(rawValue: LocalNotifications.nCancelFromBookinScreen.rawValue), object: "\(orderId)")
            
            nc.post(name: Notification.Name(rawValue: LocalNotifications.nCancelFromBookinScreen.rawValue), object: "\(orderId)", userInfo: ["orderStatus":order?.orderStatus ?? OrderStatus.Searching])
        }
        
        
        self.delegateCancellation?.didSuccessOnCancelRequest()
        self.popVC()
    }
}

//MARK:- API

extension BookingDetailVC {
    
    func getOrderDetails() {
        
        guard let orderID = order?.orderId else {  return }
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let objDetail = BookServiceEndPoint.orderDetails(orderID: orderID)
        
        objDetail.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) { [weak self] (response) in
            switch response {
                
            case .success(let data):
                
                if let order = data as? OrderCab {
                    self?.order = order
                    self?.setupUI()
                    self?.checkListArray = self?.order?.check_lists ?? []
                    self?.checklistTableView.reloadData()
                    self?.assignBookingData()
                    
                    if self?.checkListArray.count == 0{
                        self?.segmentController.isHidden = true
                    } else {
                        self?.segmentController.isHidden = false
                    }
                   
                    for element in  self?.checkListArray ?? [] {
                        self?.amount = /self?.amount + (Int(/element.after_item_price) ?? 0)
                    }
                    
                    
                    self?.lblTotal.text = "\(/self?.amount) \(/UDSingleton.shared.appSettings?.appSettings?.currency)"
                    
                    
                }
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
                
                //Toast.show(text: strError, type: .error)
            }
        }
    }
    
}


extension BookingDetailVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return checkListArray.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "CheckListCell", for: indexPath) as! CheckListCell
           cell.lblItem.text = checkListArray[indexPath.row].item_name
           cell.lblCurrency.text = /UDSingleton.shared.appSettings?.appSettings?.currency
           cell.txfPrice.text = "\(/checkListArray[indexPath.row].after_item_price)"
        cell.txfPrice.isUserInteractionEnabled = false
           cell.lblTaxPrice.text = "*Tax price \(/checkListArray[indexPath.row].tax) \(/UDSingleton.shared.appSettings?.appSettings?.currency)"
           return cell
       }
    
}

