//
//  CancellationVC.swift
//  Buraq24
//
//  Created by MANINDER on 03/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.



import UIKit
import CoreLocation


//MARK:- Enum
enum ReasonType {
    case cancel
    case halfWayStop
    case breakdown
    
    func getText() -> String? {
        
        switch self {
        case .cancel:
            return "cancellation"
            
        case .halfWayStop:
            return "half way stop"
            
        case .breakdown:
            return "breakdown"
        }
    }
}


protocol RequestCancelDelegate {
    func didSuccessOnCancelRequest()
    func didSuccessOnHalfWayStopped()
    func didSuccessOnVehicleBreakdown()
}

extension RequestCancelDelegate {
    func didSuccessOnHalfWayStopped() {}
    func didSuccessOnVehicleBreakdown() {}
}

class CancellationVC: UIViewController {
    
    
    //MARK:- OUTLETS
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var txtViewCancelReson: PlaceholderTextView!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonSubmit: UIButton!
    
    
    @IBOutlet weak var btnDriverCancel: UIButton!
    
    @IBOutlet weak var btnEtaTooLong: UIButton!
    
    @IBOutlet weak var btnDriverCallNotReach: UIButton!
    
    @IBOutlet weak var heightTxtVw: NSLayoutConstraint!
    @IBOutlet weak var btnOther: UIButton!
    //MARK:- PROPERTIES
    
    var orderId : Int?
    var delegateCancellation: RequestCancelDelegate?
    var currentOrder: OrderCab?
    var trackingModal: TrackingModel?
    var isFromOngoingDetail: Bool = false
    var reasonType: ReasonType?
    
    //MARK:- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        heightTxtVw.constant = 0
        setupUI()
        setUpNotification()
        
        guard let type = reasonType else {return}
        if type == .halfWayStop{
            btnDriverCallNotReach.isHidden = true
        }
        
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- ACTIONS
    @IBAction func btnReasionsAction(_ sender: UIButton) {
        btnDriverCancel.isSelected = false
        btnEtaTooLong.isSelected = false
        btnDriverCallNotReach.isSelected = false
        btnOther.isSelected = false
        sender.isSelected = true
        switch sender {
        case btnDriverCancel,btnEtaTooLong,btnDriverCallNotReach:
            txtViewCancelReson.text  = sender.currentTitle
            heightTxtVw.constant = 0
        case btnOther:
            txtViewCancelReson.text = ""
            txtViewCancelReson.becomeFirstResponder()
            heightTxtVw.constant = 75
        default:
            break
        }
    }
    
    @IBAction func actionBtnSubmitPressed(_ sender: Any) {
        
        view.endEditing(true)
        if (Validations.sharedInstance.validateCancellingReason(strReason: txtViewCancelReson.text)) {
            let strTrimmed = txtViewCancelReson.text.trimmed()
            
            if isFromOngoingDetail {
                
                guard let type = reasonType else {return}
                switch type {
               
                case .cancel:
                    debugPrint("cancel")
                    cancelRequest(cancelReson: strTrimmed, orderId: currentOrder?.orderId)
                    
                case .halfWayStop:
                    debugPrint("halfWayStop")
                    halfWayStopRequest(cancelReson: strTrimmed)
                    
                case .breakdown:
                    debugPrint("breakdown")
                    breakdownRequest(cancelReson: strTrimmed)
                    
                }
            } else {
                cancelRequest(cancelReson: strTrimmed, orderId: self.orderId)
            }
        }
    }
    
     //MARK:- FUNCTIONS
    
    func setupUI() {
        
        view.backgroundColor = UIColor.colorDarkGrayPopUp

        txtViewCancelReson.placeholder = "write_your_msg_here".localizedString as NSString
        txtViewCancelReson.setAlignment()
        
        view.layoutIfNeeded()
        
        labelTitle.text =  "Type your reason for " + (/isFromOngoingDetail ? /reasonType?.getText() : /ReasonType.cancel.getText())
        
        buttonSubmit.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        
        txtViewCancelReson.text = ""
        txtViewCancelReson.becomeFirstResponder()
        heightTxtVw.constant = 75
        
        btnDriverCancel.isHidden = true
        btnEtaTooLong.isHidden = true
        btnDriverCallNotReach.isHidden = true
        btnOther.isHidden = true
    }
    
    func setUpNotification() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(CancellationVC.dismissPopUp), name: Notification.Name(rawValue: LocalNotifications.DismissCancelPopUp.rawValue), object: nil)
    }
    
   
    func cancelPopUp() {
        dismissVC(completion: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: LocalNotifications.DismissCancelPopUp.rawValue), object: nil)
    }
    
    
    @objc func dismissPopUp(notifcation : Notification) {
        
        if let objOrder = notifcation.object as? OrderCab {
            if /objOrder.orderId == /orderId {
               // cancelPopUp()
                
                if isFromOngoingDetail {
                    removeChildViewController()
                } else {
                    cancelPopUp()
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let firstTouch = touches.first {
            let hitView = self.view.hitTest(firstTouch.location(in: self.view), with: event)
            
            if hitView === viewOuter {
                
                if isFromOngoingDetail {
                  removeChildViewController()
                } else {
                    cancelPopUp()
                }
            }
        }
    }
    
    
}



extension CancellationVC {
    func cancelRequest(cancelReson : String, orderId: Int?) {
        
        
        let pickUpLocation = CLLocation(latitude: /currentOrder?.pickUpLatitude, longitude: /currentOrder?.pickUpLongitude)
               let DropOffLocation = CLLocation(latitude: /trackingModal?.driverLatitude, longitude: /trackingModal?.driverLongitude)
               
               let distance = GoogleMapsDataSource.getDistance(newPosition: DropOffLocation, previous: pickUpLocation)
         var distanceInKM = 0.0
        
        if let orderS = currentOrder?.orderStatus, orderS == .Ongoing{
             
             distanceInKM = /trackingModal?.orderDistance != 0.0 ? /trackingModal?.orderDistance :  Double(Float(distance)/1000)
         
         }
        
        
         let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let objCancel = BookServiceEndPoint.cancelRequest(orderId: /orderId, cancelReason: txtViewCancelReson.text, orderDistance: distanceInKM.rounded(toPlaces: 2))
        objCancel.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) {  [weak self] (response) in
            switch response {
            case .success(_):
                
                if /self?.isFromOngoingDetail {
                    
                    DispatchQueue.main.async {[weak self] in
                        
                        self?.removeChildViewController()
                        self?.delegateCancellation?.didSuccessOnCancelRequest()

                        (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: nil)
                    }
                    
                } else {
                    
                    DispatchQueue.main.async {[weak self] in
                        self?.delegateCancellation?.didSuccessOnCancelRequest()
                        self?.cancelPopUp()
                    }
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    func halfWayStopRequest(cancelReson : String) {
        
        let pickUpLocation = CLLocation(latitude: /currentOrder?.pickUpLatitude, longitude: /currentOrder?.pickUpLongitude)
        let DropOffLocation = CLLocation(latitude: /trackingModal?.driverLatitude, longitude: /trackingModal?.driverLongitude)
        
        let distance = GoogleMapsDataSource.getDistance(newPosition: DropOffLocation, previous: pickUpLocation)
        let distanceInKM = Double(Float(distance)/1000)
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let obj = BookServiceEndPoint.halfWayStop(orderId: currentOrder?.orderId, latitude: trackingModal?.driverLatitude, longitude: trackingModal?.driverLongitude, order_distance: Double(String(format: "%.2f", distanceInKM)), half_way_stop_reason: cancelReson, payment_type: currentOrder?.paymentType.rawValue)
        
        obj.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) {  [weak self] (response) in
            
            switch response {
            case .success(_):
                debugPrint("Successs")
                
                DispatchQueue.main.async {[weak self] in
                        
                    self?.removeChildViewController()
                    self?.delegateCancellation?.didSuccessOnHalfWayStopped()
                    (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: nil)
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    func breakdownRequest(cancelReson : String) {
        
        let pickUpLocation = CLLocation(latitude: /currentOrder?.pickUpLatitude, longitude: /currentOrder?.pickUpLongitude)
        let DropOffLocation = CLLocation(latitude: /trackingModal?.driverLatitude, longitude: /trackingModal?.driverLongitude)
        
        let distance = GoogleMapsDataSource.getDistance(newPosition: DropOffLocation, previous: pickUpLocation)
        let distanceInKM = Double(Float(distance)/1000)
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let obj = BookServiceEndPoint.breakdownRequest(orderId: currentOrder?.orderId, latitude: trackingModal?.driverLatitude, longitude: trackingModal?.driverLongitude, order_distance: Double(String(format: "%.2f", distanceInKM)), reason: cancelReson)
        
        obj.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) {  [weak self] (response) in
            
            switch response {
            case .success(_):
                debugPrint("Successs")
                
                DispatchQueue.main.async {[weak self] in
                    self?.removeChildViewController()
                    self?.delegateCancellation?.didSuccessOnVehicleBreakdown()
                    (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: nil)
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
}


class CancellationVC1: UIViewController {
    
    
    //MARK:- OUTLETS
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var txtViewCancelReson: PlaceholderTextView!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonSubmit: UIButton!
    
    //MARK:- PROPERTIES
    
    var orderId : Int?
    var delegateCancellation: RequestCancelDelegate?
    var currentOrder: OrderCab?
    var trackingModal: TrackingModel?
    var isFromOngoingDetail: Bool = false
    var reasonType: ReasonType?
    var orderStatus: OrderStatus?
   
    
    //MARK:- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setUpNotification()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- ACTIONS
 
    @IBAction func actionBtnSubmitPressed(_ sender: Any) {
        
        view.endEditing(true)
        if (Validations.sharedInstance.validateCancellingReason(strReason: txtViewCancelReson.text)) {
            let strTrimmed = txtViewCancelReson.text.trimmed()
            
            if isFromOngoingDetail {
                
                guard let type = reasonType else {return}
                switch type {
               
                case .cancel:
                    debugPrint("cancel")
                    cancelRequest(cancelReson: strTrimmed, orderId: currentOrder?.orderId)
                    
                case .halfWayStop:
                    debugPrint("halfWayStop")
                    halfWayStopRequest(cancelReson: strTrimmed)
                    
                case .breakdown:
                    debugPrint("breakdown")
                    breakdownRequest(cancelReson: strTrimmed)
                    
                }
            } else {
                cancelRequest(cancelReson: strTrimmed, orderId: self.orderId)
            }
        }
    }
    
     //MARK:- FUNCTIONS
    
    func setupUI() {
        
        view.backgroundColor = UIColor.colorDarkGrayPopUp

        txtViewCancelReson.placeholder = "write_your_msg_here".localizedString as NSString
        txtViewCancelReson.setAlignment()
        
        view.layoutIfNeeded()
        
        labelTitle.text =  "Type your reason for " + (/isFromOngoingDetail ? /reasonType?.getText() : /ReasonType.cancel.getText())
        
        buttonSubmit.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
    }
    
    func setUpNotification() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(CancellationVC.dismissPopUp), name: Notification.Name(rawValue: LocalNotifications.DismissCancelPopUp.rawValue), object: nil)
    }
    
   
    func cancelPopUp() {
        dismissVC(completion: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: LocalNotifications.DismissCancelPopUp.rawValue), object: nil)
    }
    
    
    @objc func dismissPopUp(notifcation : Notification) {
        
        if let objOrder = notifcation.object as? OrderCab {
            if /objOrder.orderId == /orderId {
               // cancelPopUp()
                
                if isFromOngoingDetail {
                    removeChildViewController()
                } else {
                    cancelPopUp()
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let firstTouch = touches.first {
            let hitView = self.view.hitTest(firstTouch.location(in: self.view), with: event)
            
            if hitView === viewOuter {
                
                if isFromOngoingDetail {
                  removeChildViewController()
                } else {
                    cancelPopUp()
                }
            }
        }
    }
    
    
}



extension CancellationVC1 {
    func cancelRequest(cancelReson : String, orderId: Int?) {
        
        
        let pickUpLocation = CLLocation(latitude: /currentOrder?.pickUpLatitude, longitude: /currentOrder?.pickUpLongitude)
              let DropOffLocation = CLLocation(latitude: /trackingModal?.driverLatitude, longitude: /trackingModal?.driverLongitude)
              
              let distance = GoogleMapsDataSource.getDistance(newPosition: DropOffLocation, previous: pickUpLocation)
        var distanceInKM = 0.0
       
        if let orderS = orderStatus, orderS == .Ongoing{
            
            distanceInKM = /trackingModal?.orderDistance != 0.0 ? /trackingModal?.orderDistance :  Double(Float(distance)/1000)
        
        }
        
         let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let objCancel = BookServiceEndPoint.cancelRequest(orderId: /orderId, cancelReason: txtViewCancelReson.text, orderDistance: distanceInKM.rounded(toPlaces: 2))
        objCancel.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) {  [weak self] (response) in
            switch response {
            case .success(_):
                
                if /self?.isFromOngoingDetail {
                    
                    DispatchQueue.main.async {[weak self] in
                        
                        self?.removeChildViewController()
                        self?.delegateCancellation?.didSuccessOnCancelRequest()

                        (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: nil)
                    }
                    
                } else {
                    
                    DispatchQueue.main.async {[weak self] in
                        self?.delegateCancellation?.didSuccessOnCancelRequest()
                        self?.cancelPopUp()
                    }
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    func halfWayStopRequest(cancelReson : String) {
        
        let pickUpLocation = CLLocation(latitude: /currentOrder?.pickUpLatitude, longitude: /currentOrder?.pickUpLongitude)
        let DropOffLocation = CLLocation(latitude: /trackingModal?.driverLatitude, longitude: /trackingModal?.driverLongitude)
        
        let distance = GoogleMapsDataSource.getDistance(newPosition: DropOffLocation, previous: pickUpLocation)
         var distanceInKM = 0.0
              
               if let orderS = orderStatus, orderS == .Ongoing{
                   
                   distanceInKM = /trackingModal?.orderDistance != 0.0 ? /trackingModal?.orderDistance :  Double(Float(distance)/1000)
               
               }
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let obj = BookServiceEndPoint.halfWayStop(orderId: currentOrder?.orderId, latitude: trackingModal?.driverLatitude, longitude: trackingModal?.driverLongitude, order_distance: distanceInKM.rounded(toPlaces: 2), half_way_stop_reason: cancelReson, payment_type: currentOrder?.paymentType.rawValue)
        
        obj.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) {  [weak self] (response) in
            
            switch response {
            case .success(_):
                debugPrint("Successs")
                
                DispatchQueue.main.async {[weak self] in
                        
                    self?.removeChildViewController()
                    self?.delegateCancellation?.didSuccessOnHalfWayStopped()
                    (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: nil)
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    func breakdownRequest(cancelReson : String) {
        
        let pickUpLocation = CLLocation(latitude: /currentOrder?.pickUpLatitude, longitude: /currentOrder?.pickUpLongitude)
        let DropOffLocation = CLLocation(latitude: /trackingModal?.driverLatitude, longitude: /trackingModal?.driverLongitude)
        
        let distance = GoogleMapsDataSource.getDistance(newPosition: DropOffLocation, previous: pickUpLocation)
        let distanceInKM = Double(Float(distance)/1000).rounded(toPlaces: 2)
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let obj = BookServiceEndPoint.breakdownRequest(orderId: currentOrder?.orderId, latitude: trackingModal?.driverLatitude, longitude: trackingModal?.driverLongitude, order_distance: distanceInKM, reason: cancelReson)
        
        obj.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token, "secretdbkey": APIBasePath.secretDBKey]) {  [weak self] (response) in
            
            switch response {
            case .success(_):
                debugPrint("Successs")
                
                DispatchQueue.main.async {[weak self] in
                    self?.removeChildViewController()
                    self?.delegateCancellation?.didSuccessOnVehicleBreakdown()
                    (ez.topMostVC as? OngoingRideDetailsViewController)?.dismissVC(completion: nil)
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    
    
}

